export class ethAddAuthorDateController {

    $onInit() {
        try {
            this.parentCtrl = this.afterCtrl.parentCtrl;
            this.author = null;
            this.date = null;

            if(this.parentCtrl.item.pnx.addata && this.parentCtrl.item.pnx.addata.au && this.parentCtrl.item.pnx.addata.au.length > 0){
                this.author = this.parentCtrl.item.pnx.addata.au[0];
            }

            if(this.parentCtrl.item.pnx.display && this.parentCtrl.item.pnx.display.creationdate && this.parentCtrl.item.pnx.display.creationdate.length > 0){
                this.date = this.parentCtrl.item.pnx.display.creationdate[0];
            }
            
        } catch (e) {
            console.error("***ETH*** an error occured: ethAddAuthorDateController\n\n");
            console.error(e.message);
        }
    }

}
