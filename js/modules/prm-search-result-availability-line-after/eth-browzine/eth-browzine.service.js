/**
* @ngdoc service
* @name ethBrowzineService
*
* @description
* Service to retrieve journal informations from Browzine:
* - browzine journal link
*
*
* <b>Used by</b><br>
*
* Module {@link ETH.ethBrowzineModule}<br>
*
*
 */

export const ethBrowzineService = ['$http', '$sce', function($http, $sce){

    // request the journal availability endpoint via serverside proxy
    function getJournalViaProxy(issn){
        let url = "https://daas.library.ethz.ch/rib/v3/enrichments/browzine?issns=" + issn;
        $sce.trustAsResourceUrl(url);
        return $http.get(url,{headers:{ "X-From-ExL-API-Gateway": undefined }})
            .then(
                function(response){
                    return response.data.data;
                },
                function(httpError){
                    if (httpError.status === 404)return null;
                    let error = "***ETH*** an error occured: browzineService.getJournalViaProxy error callback: " + httpError.status;
                    if (httpError.data && httpError.data.errorMessage) {
                        error += ' - ' + httpError.data.errorMessage;
                    }
                    console.error(error);
                    return null;
                }
            );
    }

    return {
        getJournal: getJournalViaProxy
    };
}];
