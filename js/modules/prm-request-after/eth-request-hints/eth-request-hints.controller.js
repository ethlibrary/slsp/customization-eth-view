export class ethRequestHintsController {
    constructor($scope, $compile, $element, ethConfigService, ethRequestHintsConfig, ethSessionService, $anchorScroll ) {
        this.$scope = $scope;
        this.$compile = $compile;
        this.$element = $element;
        this.ethConfigService = ethConfigService;
        this.config = ethRequestHintsConfig;
        this.ethSessionService = ethSessionService;
        this.$anchorScroll = $anchorScroll;
    }
    $onInit() {
        try{
            this.parentCtrl = this.afterCtrl.parentCtrl;
            this.done = false;
            this.parentCtrl.oldFormData = {};
            this.isETHMember = false;
            this.isETHStudent = false;
            // inside other members?
            this.isOmForm = false;
            let token = this.ethSessionService.getDecodedToken();
            if(token.userGroup === 'ETH_Member' || token.userGroup === 'ETH_E06_GESS-Member' || token.userGroup === 'ETH_E72_INFK-Member' || token.userGroup === 'ETH_E64_MATH-Member'){
                this.isETHMember = true;
            }
            if(token.userGroup === 'ETH_Student'){
                this.isETHStudent = true;
            }
            let prmRequest = this.$element[0].parentElement.parentElement;
            this.feesContainer = prmRequest;
            // isolate form controlling
            this.feesContIdent = 'feesCont_' + Date.now();
            this.pickupContIdent = 'pickupCont_' + Date.now();
            // set css class for request-after inside "other members"
            let omForms = document.querySelectorAll('prm-alma-other-members prm-request');
            omForms.forEach( f => {
                angular.element(f).addClass('eth-om-request-form');
            });
            this.pickupHtml = `
                <div class="eth-pickup-hint {{$ctrl.pickupContIdent}}">
                    <div class="icnWrp"><div class="iconDiv"><svg xmlns="http://www.w3.org/2000/svg" id="Ebene_2" width="100%" height="100%" viewBox="0 0 185 185"><g id="System_icons"><g><rect width="185" height="185" fill="none" opacity=".2"/><g><line x1="6.16" y1="82.58" x2="29.16" y2="82.62" fill="none" stroke="#000" stroke-linecap="round" stroke-linejoin="round" stroke-width="6"/><line x1="17.66" y1="97.91" x2="29.16" y2="97.95" fill="none" stroke="#000" stroke-linecap="round" stroke-linejoin="round" stroke-width="6"/><line x1="25.33" y1="113.24" x2="29.16" y2="113.28" fill="none" stroke="#000" stroke-linecap="round" stroke-linejoin="round" stroke-width="6"/><g><circle cx="151.84" cy="125.5" r="12" fill="none" stroke="#000" stroke-linecap="round" stroke-linejoin="round" stroke-width="6"/><circle cx="67.84" cy="125.5" r="12" fill="none" stroke="#000" stroke-linecap="round" stroke-linejoin="round" stroke-width="6"/><polyline points="163.84 125.5 178.84 125.5 178.84 98.5 160.47 65.84 124.84 65.84 124.84 47.5 40.84 47.5 40.84 125.5 55.84 125.5" fill="none" stroke="#000" stroke-linecap="round" stroke-linejoin="round" stroke-width="6"/><line x1="139.84" y1="125.5" x2="79.84" y2="125.5" fill="none" stroke="#000" stroke-linecap="round" stroke-linejoin="round" stroke-width="6"/><line x1="124.84" y1="53.5" x2="124.84" y2="125.5" fill="none" stroke="#000" stroke-linecap="round" stroke-linejoin="round" stroke-width="6"/><line x1="178.84" y1="98.5" x2="124.84" y2="98.5" fill="none" stroke="#000" stroke-linecap="round" stroke-linejoin="round" stroke-width="6"/></g></g></g></g></svg></div></div>
                    <div>
                        <p class="eth-pickup-hint__question">{{::$ctrl.ethConfigService.getLabel($ctrl.config, 'pickup1')}}</p>
                        <p>
                            {{::$ctrl.ethConfigService.getLabel($ctrl.config, 'pickup2')}}
                            <md-button class="md-primary eth-pickup-hint__button" ng-click="$ctrl.scrollToRapido()">${this.ethConfigService.getLabel(this.config, 'linktext')}</md-button>
                            {{::$ctrl.ethConfigService.getLabel($ctrl.config, 'pickup3')}}
                        </p>
                </div>`;
        }
        catch(e){
            console.error("***ETH*** ethRequestHintsController.$onInit:");
            console.error(e.message);
        }
    }

    $doCheck() {
        // user can change form
        if(this.parentCtrl.oldFormData != this.parentCtrl.formData){
            this.done = false;
        }
        try{
            if(!this.feesContainer || this.done || !this.parentCtrl.formData || (this.parentCtrl.formData.requestType != 'hold' && this.parentCtrl.formData.partial != 'Yes')){
                return;
            }
            let html = '';
            this.parentCtrl.oldFormData = this.parentCtrl.formData;
            this.done = true;
            // is this form part of otherMembers?
            this.isOmForm = angular.element(this.feesContainer).hasClass('eth-om-request-form');

            // Hold
            if(this.parentCtrl.formData.requestType && this.parentCtrl.formData.requestType === 'hold'){
                if(this.isOmForm){
                    html =
                    `<div class="eth-fees-header {{$ctrl.feesContIdent}}">
                        <div class="eth-fees-header-inner">
                            <h4>{{::$ctrl.ethConfigService.getLabel($ctrl.config, 'feesH4OM')}}</h4>
                            <div>
                                <ul>
                                    <li>{{::$ctrl.ethConfigService.getLabel($ctrl.config, 'feesHoldOM1')}}<strong>{{::$ctrl.ethConfigService.getLabel($ctrl.config, 'feesFreeOfCharge')}}</strong></li>
                                    <li>{{::$ctrl.ethConfigService.getLabel($ctrl.config, 'feesHoldOM2')}}</li>
                                </ul>
                            </div>
                        </div>
                    </div>` + this.pickupHtml;
                }
                else if(this.isETHMember){
                    html =
                    `<div class="eth-fees-header {{$ctrl.feesContIdent}}">
                        <div class="eth-fees-header-inner">
                            <h4>{{::$ctrl.ethConfigService.getLabel($ctrl.config, 'feesH4ETH')}}</h4>
                            <div>
                                <ul>
                                    <li>{{::$ctrl.ethConfigService.getLabel($ctrl.config, 'feesHoldETH1')}}<strong>{{::$ctrl.ethConfigService.getLabel($ctrl.config, 'feesFreeOfCharge')}}</strong></li>
                                    <li>{{::$ctrl.ethConfigService.getLabel($ctrl.config, 'feesHoldETH2')}}<strong>{{::$ctrl.ethConfigService.getLabel($ctrl.config, 'feesFreeOfCharge')}}</strong></li>
                                </ul>
                            </div>
                        </div>
                    </div>` + this.pickupHtml;
                }
                else if(this.isETHStudent){
                    html =
                    `<div class="eth-fees-header {{$ctrl.feesContIdent}}">
                        <div class="eth-fees-header-inner">
                            <h4>{{::$ctrl.ethConfigService.getLabel($ctrl.config, 'feesH4Student')}}</h4>
                            <div>
                                <ul>
                                    <li>{{::$ctrl.ethConfigService.getLabel($ctrl.config, 'feesHoldStudent1')}}<strong>{{::$ctrl.ethConfigService.getLabel($ctrl.config, 'feesFreeOfCharge')}}</strong></li>
                                    <li>{{::$ctrl.ethConfigService.getLabel($ctrl.config, 'feesHoldStudent2')}}</li>
                                </ul>
                            </div>
                        </div>
                    </div>` + this.pickupHtml;
                }
                else{
                    html =
                    `<div class="eth-fees-header {{$ctrl.feesContIdent}}">
                        <div class="eth-fees-header-inner">
                            <h4>{{::$ctrl.ethConfigService.getLabel($ctrl.config, 'feesH4External')}}</h4>
                            <div>
                                <ul>
                                    <li>{{::$ctrl.ethConfigService.getLabel($ctrl.config, 'feesHoldExternal1')}}<strong>{{::$ctrl.ethConfigService.getLabel($ctrl.config, 'feesFreeOfCharge')}}</strong></li>
                                </ul>
                            </div>
                        </div>
                    </div>` + this.pickupHtml;
                }
            }
            // Digitization
            else if(this.parentCtrl.formData.partial && this.parentCtrl.formData.partial === 'Yes'){
                if(this.isOmForm){
                    html =
                    `<div class="eth-fees-header {{$ctrl.feesContIdent}}">
                        <div class="eth-fees-header-inner">
                            <h4>{{::$ctrl.ethConfigService.getLabel($ctrl.config, 'feesH4OM')}}</h4>
                            <div>
                                <ul>
                                    <li>{{::$ctrl.ethConfigService.getLabel($ctrl.config, 'feesDigiOM1')}}</li>
                                    <li>{{::$ctrl.ethConfigService.getLabel($ctrl.config, 'feesDigiOM2')}}</li>
                                    <li>{{::$ctrl.ethConfigService.getLabel($ctrl.config, 'feesDigiOM3')}}</li>
                                </ul>
                            </div>
                        </div>
                    </div>`
                }
                else if(this.isETHMember){
                    html =
                    `<div class="eth-fees-header {{$ctrl.feesContIdent}}">
                        <div class="eth-fees-header-inner">
                            <h4>{{::$ctrl.ethConfigService.getLabel($ctrl.config, 'feesH4ETH')}}</h4>
                            <div>
                                <ul>
                                    <li><strong>{{::$ctrl.ethConfigService.getLabel($ctrl.config, 'feesFreeOfChargeDigi')}}</strong></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    `
                }
                else if(this.isETHStudent){
                    html =
                    `<div class="eth-fees-header {{$ctrl.feesContIdent}}">
                        <div class="eth-fees-header-inner">
                            <h4>{{::$ctrl.ethConfigService.getLabel($ctrl.config, 'feesH4Student')}}</h4>
                            <div>
                                <ul>
                                    <li>{{::$ctrl.ethConfigService.getLabel($ctrl.config, 'feesDigiStudent1')}}</li>
                                </ul>
                            </div>
                        </div>
                    </div>`
                }
                else {
                    html =
                    `<div class="eth-fees-header {{$ctrl.feesContIdent}}">
                        <div class="eth-fees-header-inner">
                            <h4>{{::$ctrl.ethConfigService.getLabel($ctrl.config, 'feesH4External')}}</h4>
                            <div>
                                <ul>
                                    <li>{{::$ctrl.ethConfigService.getLabel($ctrl.config, 'feesDigiExternal1')}}</li>
                                    <li>{{::$ctrl.ethConfigService.getLabel($ctrl.config, 'feesDigiExternal2')}}</li>
                                    <li>{{::$ctrl.ethConfigService.getLabel($ctrl.config, 'feesDigiExternal3')}}</li>
                                </ul>
                            </div>
                        </div>
                    </div>`
                }
            }
            if(html != ''){
                let prevFees = document.querySelectorAll('.' + this.feesContIdent);
                if(prevFees){
                    angular.element(prevFees).remove();
                }
                let prevPickup = document.querySelectorAll('.' + this.pickupContIdent);
                if(prevPickup){
                    angular.element(prevPickup).remove();
                }
                angular.element(this.feesContainer).prepend(this.$compile(html)(this.$scope));
            }
        }
        catch(e){
            console.error("***ETH*** an error occured: ethRequestHintsController.$doCheck\n\n");
            console.error(e.message);
            throw(e)
        }
    }

    scrollToRapido(){
        this.$anchorScroll('rapidoOffer');
    }

}
ethRequestHintsController.$inject = ['$scope', '$compile', '$element', 'ethConfigService', 'ethRequestHintsConfig', 'ethSessionService', '$anchorScroll' ];
