/**
* @ngdoc module
* @name ethPartofHintModule
*
* @description
*
* If there is no item, but a partof link: show the link in a getit hint
*
* <b>AngularJS Dependencies</b><br>
* Service /js/services {@link ETH.ethConfigService}<br>
* Service {@link ETH.ethPartofHintConfig}<br>
*
* <b>CSS/Image Dependencies</b><br>
* eth-partof-hint.css
*
*/
import {ethConfigService} from '../../../services/eth-config.service';
import {ethPartofHintConfig} from './eth-partof-hint.config';
import {ethPartofHintController} from './eth-partof-hint.controller';

export const ethPartofHintModule = angular
    .module('ethPartofHintModule', [])
        .factory('ethConfigService', ethConfigService)
        .factory('ethPartofHintConfig', ethPartofHintConfig)
        .controller('ethPartofHintController', ethPartofHintController)
        .component('ethPartofHintComponent', {
            bindings: {afterCtrl: '<'},
            controller: 'ethPartofHintController'
        })
