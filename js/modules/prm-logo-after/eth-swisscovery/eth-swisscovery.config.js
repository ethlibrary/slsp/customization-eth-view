export const ethSwisscoveryConfig = function(){
    return {
        label: {
            swisscovery: {
                de: 'ETH-Bibliothek @ swisscovery',
                en: 'ETH Library @ swisscovery'
            }
        }
    }
}
