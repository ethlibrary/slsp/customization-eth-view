export class ethPartofHintController {

    constructor( $scope, $compile, $timeout, $state, ethConfigService, ethPartofHintConfig ) {
        this.$scope = $scope;
        this.$compile = $compile;
        this.$timeout = $timeout;
        this.$state = $state;
        this.ethConfigService = ethConfigService;
        this.config = ethPartofHintConfig;
    }

    $onInit() {
        try{
            // 99117714740305503, 99117703889805503, 99117708585705503
            // 99117707155405503 no mms id
            this.parentCtrl = this.afterCtrl.parentCtrl;
            // is there a isPartOf Link?
            if(!this.parentCtrl.item || !this.parentCtrl.item.pnx.display.ispartof || this.parentCtrl.item.pnx.display.ispartof.length == 0){
                return;
            }
            let ispartof = this.parentCtrl.item.pnx.display.ispartof[0];
            this.$timeout(() => {
                // Is there a "no items" alert?
                let noItems = document.querySelector('prm-opac [translate="nui.filter.noitems"]');
                if(!noItems){
                    return
                }
                if(ispartof.indexOf('$$Z') <= 0 || ispartof.indexOf('$$Q') === 0){
                    return;
                }
                let mmsid = ispartof.substring(ispartof.indexOf('$$Z') + 3);
                let linktext = ispartof.substring(0, ispartof.indexOf('$$Q'))
                if(linktext.indexOf(';') > -1){
                    linktext = linktext.substring(0,linktext.indexOf(';'))
                }
                let html = `
                <div class='eth-partof-hint__container'>
                    {{::$ctrl.ethConfigService.getLabel($ctrl.config, \'hint\')}}
                    <md-button class="md-primary eth-partof-hint__button" ng-click="$ctrl.loadFulldisplay('${mmsid}');">
                        ${linktext}
                    </md-button>
                </div>
                `;
                angular.element(noItems).append(this.$compile(html)(this.$scope));
                noItems.parentElement.classList.add('eth-partof-hint__bar');
            }, 3000);
        }
        catch(e){
            console.error("***ETH*** an error occured: ethPartofHintController $onInit\n\n");
            console.error(e.message);
        }
    }

    loadFulldisplay(mmsid) {
        let vid = window.appConfig.vid;
        let tab = window.appConfig['primo-view']['available-tabs'][0];
        let lang = this.ethConfigService.getLanguage();
        let url = `/discovery/fulldisplay?lang=${lang}&tab=${tab}&vid=${vid}&docid=alma${mmsid}`;
        location.href = url;
        //this.$state.go('fulldisplay', {vid: vid, tab: tab, lang: lang, docid: 'alma'+ mmsid});
    }

}

ethPartofHintController.$inject = ['$scope', '$compile', '$timeout', '$state', 'ethConfigService', 'ethPartofHintConfig' ];
