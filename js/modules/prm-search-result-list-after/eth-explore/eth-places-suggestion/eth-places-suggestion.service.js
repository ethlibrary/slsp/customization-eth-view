/**
* @ngdoc service
* @name ethPlacesSuggestionService
*
* @description
*
*
* <b>Used by</b><br>
* Module {@link ETH.ethPlacesSuggestionModule}<br>
*
*
 */
export const ethPlacesSuggestionService = ['$http', '$sce', function($http, $sce){

    function getPois(q){
        let url = 'https://daas.library.ethz.ch/rib/v3/graph/q-pois-short?q=';
        url = url + q;
        $sce.trustAsResourceUrl(url);

        return $http.get(url)
            .then(
                function(response){
                    return response.data;
                },
                function(httpError){
                    if (httpError.status === 404)return null;
                    let error = "***ETH*** an error occured: ethPlacesSuggestionService.getPois(): " + httpError.status;
                    if (httpError.data && httpError.data.errorMessage) {
                        error += ' - ' + httpError.data.errorMessage;
                    }
                    console.error(error);
                    return null;
                }
            )
    }

    function getPlaces(q){
        let url = 'https://daas.library.ethz.ch/rib/v3/graph/locations?q=';
        url = url + q;
        $sce.trustAsResourceUrl(url);

        return $http.get(url)
            .then(
                function(response){
                    return response.data;
                },
                function(httpError){
                    if (httpError.status === 404)return null;
                    let error = "***ETH*** an error occured: ethPlacesSuggestionService.getPlaces(): " + httpError.status;
                    if (httpError.data && httpError.data.errorMessage) {
                        error += ' - ' + httpError.data.errorMessage;
                    }
                    console.error(error);
                    return null;
                }
            )
    }


    return {
        getPois: getPois,
        getPlaces: getPlaces
    }

}]
