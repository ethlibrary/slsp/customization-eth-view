export class ethPageTypeController {
    $onInit() {
        try{
            this.parentCtrl = this.afterCtrl.parentCtrl;
            if (!document.querySelector('html') || !document.querySelector('html').classList.add) {
    			console.error("***ETH*** ethPageTypeController.$onInit: classList.add() not available");
    			return;
    		}
            if (!this.parentCtrl.$stateParams) {
    			console.error("***ETH*** ethPageTypeController.$onInit: $stateParams not available");
    			return;
    		}
            if(!this.parentCtrl.$stateParams.query){
                document.querySelector('html').classList.add('eth-startpage');
            }
            else{
                document.querySelector('html').classList.remove('eth-startpage');
            }
        }
        catch(e){
            console.error("***ETH*** an error occured: ethPageTypeController\n\n");
            console.error(e.message);
        }
    }
}
