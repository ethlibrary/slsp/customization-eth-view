/**
* @ngdoc module
* @name ethIdpWarningModule
*
* @description
* If an user with an eth mail address
* does not belong to an eth member usergroup,
* a message is displayed that the user must link the edu-id account with the ETH account.
* Login for Anna Muster:
* fuehrungen@library.ethz.ch 
* Schulungen123!
*
* <b>CSS/Image Dependencies</b><br>
* CSS eth-idp-warning.css
*
*
*/
import {ethIdpWarningController} from './eth-idp-warning.controller';
import {ethConfigService} from '../../../services/eth-config.service';
import {ethIdpWarningConfig} from './eth-idp-warning.config';

export const ethIdpWarningModule = angular
    .module('ethIdpWarningModule', [])
        .factory('ethConfigService', ethConfigService)
        .factory('ethIdpWarningConfig', ethIdpWarningConfig)
        .controller('ethIdpWarningController', ethIdpWarningController)
        .component('ethIdpWarningComponent', {
            bindings: {afterCtrl: '<'},
            controller: 'ethIdpWarningController'
        })
