# Primo VE Customization ETH

The ETH Library customizes its [Primo VE View](https://eth.swisscovery.ethz.ch/discovery/search?vid=41SLSP_ETH:ETH) in the context of [SLSP](https://slsp.ch/).
See the german documentation: customizing_slsp_eth_view.pdf

Code and documentation are work in progress.
