/**
* @ngdoc module
* @name ethPlacesSuggestionModule
*
* @description
*
* Query expansion via geo graph: suggest places for searchterm
*
* <b>AngularJS Dependencies</b><br>
* Service /js/services {@link ETH.ethSessionService}<br>
* Service /js/services {@link ETH.ethConfigService}<br>
* Service {@link ETH.ethPlacesSuggestionConfig}<br>
* Service {@link ETH.ethPlacesSuggestionService}<br>
*
*
*/
import {ethConfigService} from '../../../../services/eth-config.service';
import {ethPlacesSuggestionConfig} from './eth-places-suggestion.config';
import {ethPlacesSuggestionService} from './eth-places-suggestion.service';
import {ethPlacesSuggestionController} from './eth-places-suggestion.controller';
import {ethPlacesSuggestionHtml} from './eth-places-suggestion.html';

export const ethPlacesSuggestionModule = angular
    .module('ethPlacesSuggestionModule', [])
        .factory('ethConfigService', ethConfigService)
        .factory('ethPlacesSuggestionConfig', ethPlacesSuggestionConfig)
        .factory('ethPlacesSuggestionService', ethPlacesSuggestionService)
        .controller('ethPlacesSuggestionController', ethPlacesSuggestionController)
        .component('ethPlacesSuggestionComponent',{
            bindings: {afterCtrl: '<'},
            controller: 'ethPlacesSuggestionController',
            template: ethPlacesSuggestionHtml
        })
