/**
* @ngdoc module
* @name ethAddAuthorDateModule
*
* @description
*
* gallery items: author and date are added<br>
*
*
* <b>AngularJS Dependencies</b><br>
*
* <b>CSS/Image Dependencies</b><br>
* CSS eth-add-author-date.css
*
*/
import {ethAddAuthorDateController} from './eth-add-author-date.controller';
import {ethAddAuthorDateHtml} from './eth-add-author-date.html';

export const ethAddAuthorDateModule = angular
    .module('ethAddAuthorDateModule', [])
        .controller('ethAddAuthorDateController', ethAddAuthorDateController)
        .component('ethAddAuthorDateComponent',{
            bindings: {afterCtrl: '<'},
            controller: 'ethAddAuthorDateController',
            template: ethAddAuthorDateHtml
        })
