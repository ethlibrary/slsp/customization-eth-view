import {ethTabsScopesModule} from './eth-tabs-scopes/eth-tabs-scopes.module';

export const ethSearchBarAfterModule = angular
    .module('ethSearchBarAfterModule', [])
        .component('prmSearchBarAfter',  {
            bindings: {parentCtrl: '<'},
            template: `<eth-tabs-scopes-component after-ctrl="$ctrl"></eth-tabs-scopes-component>`
        });

ethSearchBarAfterModule.requires.push(ethTabsScopesModule.name);
