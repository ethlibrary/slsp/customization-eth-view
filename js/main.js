import {ethAvailabilityLineAfterModule} from './modules/prm-search-result-availability-line-after/eth-availability-line-after.module';
import {ethSearchResultListAfterModule} from './modules/prm-search-result-list-after/eth-search-result-list-after.module';
import {ethBriefResultAfterModule} from './modules/prm-brief-result-after/eth-brief-result-after.module';
import {ethFavoritesLabelsAfterModule} from './modules/prm-favorites-labels-after/eth-favorites-labels-after.module';
import {prmFavoritesEditLabelsMenuAfterModule} from './modules/prm-favorites-edit-labels-menu-after/prm-favorites-edit-labels-menu-after.module';
import {ethSearchAfterModule} from './modules/prm-search-after/eth-search-after.module';
import {ethSearchBarAfterModule} from './modules/prm-search-bar-after/eth-search-bar-after.module';
import {ethLogoAfterModule} from './modules/prm-logo-after/eth-logo-after.module';
import {prmLoginAfterModule} from './modules/prm-login-after/prm-login-after.module';
import {ethLocationsAfterModule} from './modules/prm-locations-after/eth-locations-after.module';
import {prmLocationHoldingsAfterModule} from './modules/prm-location-holdings-after/prm-location-holdings-after.module';
import {ethAlmaViewitAfterModule} from './modules/prm-alma-delivery-after/eth-alma-viewit-after.module';
import {ethAlmaHtgiSvcAfterModule} from './modules/prm-alma-delivery-after/eth-alma-htgi-svc-after.module';
import {ethOpacAfterModule} from './modules/prm-alma-delivery-after/eth-opac-after.module';
import {ethFullViewAfterModule} from './modules/prm-full-view-after/eth-full-view-after.module';
import {ethServiceLinksAfterModule} from './modules/prm-service-links-after/eth-service-links-after.module';
import {ethLocationItemsAfterModule} from './modules/prm-location-items-after/eth-location-items-after.module';
import {ethExploreMainAfterModule} from './modules/prm-explore-main-after/eth-explore-main-after.module';
import {ethRequestAfterModule} from './modules/prm-request-after/eth-request-after.module';
import {slspRequestsAfterModule} from './modules/prm-requests-after/eth-requests-after.module';
import {ethPersonalInfoAfterModule} from './modules/prm-personal-info-after/eth-personal-info-after.module';
import {prmSendEmailAfterModule} from './modules/prm-send-email-after/prm-send-email-after.module';
import {prmCitationAfterModule} from './modules/prm-citation-after/prm-citation-after.module';
import {ethFacetExactAfterModule} from './modules/prm-facet-exact-after/eth-facet-exact-after.module';
import {ethHttpInterceptRequests} from './modules/eth-http-intercept-requests/eth-http-intercept-requests.module';
import {slspHttpInterceptPickupInformation} from './modules/slsp-http-intercept-pickup-information/slsp-http-intercept-pickup-information.module';
import {ethBrowseSearchAfterModule} from './modules/prm-browse-search-after/eth-browse-search-after.module';
import {ethLoanAfterModule} from './modules/prm-loan-after/eth-loan-after.module';
import {ethTopBarBeforeModule} from './modules/prm-top-bar-before/eth-top-bar-before.module';
import {prmServicePhysicalBestOfferAfterModule} from './modules/prm-service-physical-best-offer-after/eth-service-physical-best-offer-after.module';
import {prmServiceNgrsAfterModule} from './modules/prm-service-ngrs-after/prm-service-ngrs-after.module';
import {prmAlmaOtherMembersAfterModule} from './modules/prm-alma-other-members-after/eth-alma-other-members-after.module';
import {prmAlmaViewitItemsAfterModule} from './modules/prm-alma-viewit-items-after/eth-alma-viewit-items-after.module';
import {ethTabsAndScopesSelectorAfterModule} from './modules/prm-tabs-and-scopes-selector-after/eth-tabs-and-scopes-selector-after.module';
import {prmOfferDetailsTileAfterModule} from './modules/prm-offer-details-tile-after/prm-offer-details-tile-after.module';
import {ethUserAreaExpandableAfterModule} from './modules/prm-user-area-expandable-after/eth-user-area-expandable-after.module';
import {prmFinesAfterModule} from './modules/prm-fines-after/prm-fines-after.module';
import {prmServiceButtonAfterModule} from './modules/prm-service-button-after/prm-service-button-after.module';
import {ethGalleryItemAfterModule} from './modules/prm-gallery-item-after/eth-gallery-item-after.module';
import {prmLocationItemAfterModule} from './modules/prm-location-item-after/prm-location-item-after.module';
import {ethLegantoAfterModule} from './modules/prm-leganto-after/eth-leganto-after.module';
import {ethBibNewsModule} from './modules/home/eth-bib-news/eth-bib-news.module';
import {prmGetItRequestAfterModule} from './modules/prm-get-it-request-after';
import {pickupAnywhereFormAfterModule} from './modules/pickup-anywhere-form-after';


let app = angular.module('viewCustom', ['angularLoad'])

app.requires.push(ethAvailabilityLineAfterModule.name);
app.requires.push(ethSearchResultListAfterModule.name);
app.requires.push(ethBriefResultAfterModule.name);
app.requires.push(ethFavoritesLabelsAfterModule.name);
app.requires.push(prmFavoritesEditLabelsMenuAfterModule.name);
app.requires.push(ethSearchAfterModule.name);
app.requires.push(ethSearchBarAfterModule.name);
app.requires.push(ethLogoAfterModule.name);
app.requires.push(prmLoginAfterModule.name);
app.requires.push(ethLocationsAfterModule.name);
app.requires.push(prmLocationHoldingsAfterModule.name);
app.requires.push(ethAlmaViewitAfterModule.name);
app.requires.push(ethAlmaHtgiSvcAfterModule.name);
app.requires.push(ethFullViewAfterModule.name);
app.requires.push(ethOpacAfterModule.name);
app.requires.push(ethServiceLinksAfterModule.name);
app.requires.push(ethLocationItemsAfterModule.name);
app.requires.push(ethExploreMainAfterModule.name);
app.requires.push(ethRequestAfterModule.name);
app.requires.push(slspRequestsAfterModule.name);
app.requires.push(ethPersonalInfoAfterModule.name);
app.requires.push(prmSendEmailAfterModule.name);
app.requires.push(prmCitationAfterModule.name);
app.requires.push(ethFacetExactAfterModule.name);
app.requires.push(ethHttpInterceptRequests.name);
app.requires.push(slspHttpInterceptPickupInformation.name);
app.requires.push(ethBrowseSearchAfterModule.name);
app.requires.push(ethLoanAfterModule.name);
app.requires.push(ethTopBarBeforeModule.name);
app.requires.push(prmServicePhysicalBestOfferAfterModule.name);
app.requires.push(prmServiceNgrsAfterModule.name);
app.requires.push(prmAlmaOtherMembersAfterModule.name);
app.requires.push(prmAlmaViewitItemsAfterModule.name);
app.requires.push(ethTabsAndScopesSelectorAfterModule.name);
app.requires.push(prmOfferDetailsTileAfterModule.name);
app.requires.push(ethUserAreaExpandableAfterModule.name);
app.requires.push(prmFinesAfterModule.name);
app.requires.push(prmServiceButtonAfterModule.name);
app.requires.push(ethGalleryItemAfterModule.name);
app.requires.push(prmLocationItemAfterModule.name);
app.requires.push(ethLegantoAfterModule.name);
app.requires.push(ethBibNewsModule.name);
app.requires.push(prmGetItRequestAfterModule.name);
app.requires.push(pickupAnywhereFormAfterModule.name);

app.run(['$rootScope', '$location', '$sce', 'angularLoad', '$document', function($rootScope, $location, $sce, angularLoad, $document){
    // Matomo tracking
    angularLoad.loadScript('https://library-ethz.opsone-analytics.ch/matomo.js');
    if(!window._paq){
        window._paq = [];
    }
    window._paq.push(['setSiteId', '2']);
    window._paq.push(['setTrackerUrl', 'https://library-ethz.opsone-analytics.ch/matomo.php']);
    window._paq.push(['enableLinkTracking']);

    $rootScope.$on('$locationChangeSuccess', function(event, newUrl){
        window._paq.push(['setCustomUrl', newUrl]);
        if($document.length > 0 && $document[0].title){
            window._paq.push(['setDocumentTitle', $document[0].title]);
        }
        window._paq.push(['trackPageView']);
    });
    // Matomo tracking end
    // fix google problem
    try{
        let robotsMeta = angular.element(document.getElementsByName('robots'))[0];
        robotsMeta.content = 'all';
    }catch(e){}
}]);
