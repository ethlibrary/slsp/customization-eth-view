import {ethLibkeyModule} from './eth-libkey/eth-libkey.module';
import {ethConnectedPapersModule} from './eth-connectedpapers/eth-connectedpapers.module';
import {ethBrowzineModule} from './eth-browzine/eth-browzine.module';
import {ethAvailabilityHintImprovementModule} from './eth-availability-hint-improvement/eth-availability-hint-improvement.module';

export const ethAvailabilityLineAfterModule = angular
    .module('ethAvailabilityLineAfterModule', [])
        .component('prmSearchResultAvailabilityLineAfter',  {
            bindings: {parentCtrl: '<'},
            template: `<eth-browzine-component after-ctrl="$ctrl"></eth-browzine-component>
            <eth-libkey-component after-ctrl="$ctrl"></eth-libkey-component>
            <eth-connected-papers-component after-ctrl="$ctrl"></eth-connected-papers-component>
            <eth-availability-hint-improvement-component after-ctrl="$ctrl"></eth-availability-hint-improvement-component>
            `
        });

ethAvailabilityLineAfterModule.requires.push(ethLibkeyModule.name);
ethAvailabilityLineAfterModule.requires.push(ethBrowzineModule.name);
ethAvailabilityLineAfterModule.requires.push(ethConnectedPapersModule.name);
ethAvailabilityLineAfterModule.requires.push(ethAvailabilityHintImprovementModule.name);
