export class ethLibraryInfoController {

    constructor( ethConfigService, ethLibraryInfoConfig ) {
        this.ethConfigService = ethConfigService;
        this.config = ethLibraryInfoConfig;
    }

    $onInit() {
        try{
            this.parentCtrl = this.afterCtrl.parentCtrl;
            this.isDone =  false;
            this.libraryCode = '';
            this.subLocationCode = '';
            this.isSingleLocation = false;
        }
        catch(e){
            console.error("***ETH*** an error occured: ethLibraryInfoController.$onInit()\n\n");
            console.error(e.message);
        }
    }

    $doCheck() {
        try{
            if (!this.parentCtrl.loc || !this.parentCtrl.loc.location || !this.parentCtrl.loc.location.libraryCode || !this.parentCtrl.loc.location.librarycodeTranslation || this.parentCtrl.loc.location.subLocationCode === "EL") {
                return;
            }
            if(this.libraryCode !== this.parentCtrl.loc.location.libraryCode || this.subLocationCode !== this.parentCtrl.loc.location.subLocationCode){
                // user chooses another location
                this.isDone = false;
            }
            if(this.isDone){
                return;
            }
            this.isSingleLocation = this.parentCtrl.isSingleLocation;
            this.libraryCode = this.parentCtrl.loc.location.libraryCode;
            this.subLocationCode = this.parentCtrl.loc.location.subLocationCode;
            this.mailLink = '';
            this.mailLinkText = '';
            this.shortHint = '';
            let subject = '';
            // Info Box for special hints
            if(this.subLocationCode === 'E06LI' || this.subLocationCode === 'E06EQ' || this.subLocationCode === 'AETH' || this.subLocationCode === 'E73BI' || this.subLocationCode === 'E73MF' || this.libraryCode === 'E76' || this.libraryCode === 'E79' || this.libraryCode === 'E33'){
                if(this.parentCtrl.item && this.parentCtrl.item.pnx){
                    subject = '?subject=' + this.ethConfigService.getLabel(this.config['common'], 'subject') + ' ' + this.parentCtrl.item.pnx.control.sourcerecordid[0];
                }
                // Professur für Literatur- und Kulturwissenschaft
                if(this.subLocationCode === 'E06LI'){ 
                    this.mailLink = this.ethConfigService.getUrl(this.config[this.subLocationCode], 'mail') + subject;
                    this.mailLinkText = this.ethConfigService.getLabel(this.config['E06LI'], 'maillinktext');
                }
                // ETH Diversity
                else if(this.subLocationCode === 'E06EQ'){
                    this.mailLink = this.ethConfigService.getUrl(this.config[this.subLocationCode], 'mail') + subject;
                    this.mailLinkText = this.ethConfigService.getLabel(this.config['E06EQ'], 'maillinktext');
                }
                // HCI
                else if(this.libraryCode === 'E33'){
                    let subjectE33 = '';
                    if(this.parentCtrl.item && this.parentCtrl.item.pnx){
                        subjectE33 = this.ethConfigService.getLabel(this.config[this.libraryCode], 'subject') + ' "' + this.parentCtrl.item.pnx.display.title[0] + '" - ' + this.parentCtrl.item.pnx.control.sourcerecordid[0];
                    }
let bodyE33= `${this.ethConfigService.getLabel(this.config[this.libraryCode], 'body1')}

${this.ethConfigService.getLabel(this.config[this.libraryCode], 'body2')}
${this.ethConfigService.getLabel(this.config[this.libraryCode], 'body3')}
${this.ethConfigService.getLabel(this.config[this.libraryCode], 'body4')}

${this.ethConfigService.getLabel(this.config[this.libraryCode], 'body5')}

${this.ethConfigService.getLabel(this.config[this.libraryCode], 'body6')}`;
                    this.mailLink = this.ethConfigService.getUrl(this.config[this.libraryCode], 'mail') + '?subject=' + encodeURIComponent(subjectE33) + '&body=' + encodeURIComponent(bodyE33);
                    this.mailLinkText = this.ethConfigService.getLabel(this.config[this.libraryCode], 'maillinktext');
                }
                // Handbibliothek Hochschularchiv der ETH Zürich
                else if(this.subLocationCode === 'AETH'){
                    this.mailLink = this.ethConfigService.getUrl(this.config[this.subLocationCode], 'mail') + subject;
                    this.mailLinkText = this.ethConfigService.getLabel(this.config[this.subLocationCode], 'maillinktext');
                }
                // E73MF:Literaturarchiv MFA Werke der ETHZ
                else if(this.subLocationCode === 'E73MF'){
                    this.shortHint = this.ethConfigService.getLabel(this.config[this.subLocationCode], 'hint');
                    this.mailLinkText = this.ethConfigService.getUrl(this.config[this.subLocationCode], 'mail');
                    this.mailLink = this.mailLinkText + subject;
                }
                // E73BI:Literaturarchiv TMA Werke der ETHZ
                else if(this.subLocationCode === 'E73BI'){
                    this.shortHint = this.ethConfigService.getLabel(this.config[this.subLocationCode], 'hint');
                    this.mailLinkText = this.ethConfigService.getUrl(this.config[this.subLocationCode], 'mail');
                    this.mailLink = this.mailLinkText + subject;
                }
                // ETH-GS Graphische Sammlung
                else if(this.libraryCode === 'E76'){
                    this.shortHint = this.ethConfigService.getLabel(this.config[this.libraryCode], 'hint');
                    this.mailLinkText = this.ethConfigService.getUrl(this.config[this.libraryCode], 'mail');
                    this.mailLink = this.mailLinkText + subject;
                }
                // E79:GTA incl Bibliothek Dieter Kienast
                else if(this.libraryCode === 'E79'){
                    this.mailLink = this.ethConfigService.getUrl(this.config['E79'], 'mail') + subject;
                    this.mailLinkText = this.ethConfigService.getLabel(this.config['E79'], 'maillinktext');
                }
            }

            // Link to CMS Page
            // E79:GTA incl Bibliothek Dieter Kienast
            if(this.libraryCode === 'E79'){
                this.libraryName = this.parentCtrl.loc.location.librarycodeTranslation;
                this.cmsUrl = this.ethConfigService.getUrl(this.config['E79'], 'cms');
            }
            else if(this.subLocationCode === 'E06LI'){
                this.libraryName = this.ethConfigService.getLabel(this.config[this.subLocationCode], 'libraryName');
                this.cmsUrl = this.ethConfigService.getUrl(this.config[this.subLocationCode], 'cms');
            }
            else if(this.subLocationCode === 'E06EQ'){
                this.libraryName = this.parentCtrl.loc.location.subLocation;
                this.cmsUrl = this.ethConfigService.getUrl(this.config[this.subLocationCode], 'cms');
            }
            else if(this.subLocationCode === 'AETH'){
                this.libraryName = this.ethConfigService.getLabel(this.config[this.subLocationCode], 'libraryName');
                this.cmsUrl = this.ethConfigService.getUrl(this.config[this.subLocationCode], 'cms');
            }
            else if(this.subLocationCode === 'E73MF'){
                this.libraryName = this.ethConfigService.getLabel(this.config[this.subLocationCode], 'title');
                this.cmsUrl = this.ethConfigService.getUrl(this.config[this.subLocationCode], 'cms');
            }
            else if(this.subLocationCode === 'E73BI'){
                this.libraryName = this.ethConfigService.getLabel(this.config[this.subLocationCode], 'title');
                this.cmsUrl = this.ethConfigService.getUrl(this.config[this.subLocationCode], 'cms');
            }
            // Archiv für Zeitgeschichte 
            else if(this.subLocationCode === 'E98AZ'){
                this.libraryName = this.ethConfigService.getLabel(this.config[this.subLocationCode], 'libraryName');
                this.cmsUrl = this.ethConfigService.getUrl(this.config[this.subLocationCode], 'cms');
            }
            // ETH library (-> ETH location)
            else if(this.config[this.parentCtrl.loc.location.libraryCode]){
                this.libraryName = this.parentCtrl.loc.location.librarycodeTranslation;
                this.cmsUrl = this.ethConfigService.getUrl(this.config[this.libraryCode], 'cms');
            }
            // other SLSP library (not ETH)
            else{
                this.libraryName = this.parentCtrl.loc.location.librarycodeTranslation;
                this.cmsUrl = this.ethConfigService.getUrl(this.config['slsp'], 'cms') + this.libraryCode;
            }
            this.isDone = true;
        }
        catch(e){
            console.error("***ETH*** ethLibraryInfoController.$doCheck:");
            console.error(e.message);
        }

    }

}

ethLibraryInfoController.$inject = [ 'ethConfigService', 'ethLibraryInfoConfig' ];
