/**
* @ngdoc module
* @name ethAltmetricModule
*
* @description
*
* Eventually render an altmetric badge (by doi or isbn).
*
* <b>AngularJS Dependencies</b><br>
*
*
* <b>CSS/Image Dependencies</b><br>
* CSS eth-altmetric.css
*
*
*/
import {ethAltmetricController} from './eth-altmetric.controller';
import {ethAltmetricHtml} from './eth-altmetric.html';

export const ethAltmetricModule = angular
    .module('ethAltmetricModule', [])
        .controller('ethAltmetricController', ethAltmetricController)
        .component('ethAltmetricComponent',  {
            require: {
                briefResult: '^prmBriefResultContainer'
            },
            bindings: {afterCtrl: '<'},
            controller: 'ethAltmetricController',
            template: ethAltmetricHtml
        })
