export class ethAlternativeSearchController {

    constructor( $location, ethConfigService, ethAlternativeSearchConfig ) {
        this.$location = $location;
        this.ethConfigService = ethConfigService;
        this.config = ethAlternativeSearchConfig;
    }

    $onInit() {
        try{
            this.parentCtrl = this.afterCtrl.parentCtrl;
            this.groupName = this.ethConfigService.getLabel(this.config, 'groupName');

            // is group already added?
            let filteredFacets = this.prmFacet.facets.filter( f =>{
                return(f.name === this.groupName)
            })
            // push new Group to facetGroup array, if not already done
            if(filteredFacets.length === 0){
                this.prmFacet.facets.push({
                    name: this.groupName,
                    displayedType: 'exact',
                    limitCount: 0,
                    facetGroupCollapsed: false,
                    values: []
                })
            }
            // Controller for new alternativeSearch Group
            if(this.parentCtrl.facetGroup.name === this.groupName){
                let q1 = this.$location.search().query;
                let aQ = Array.isArray(q1) ? q1 : [q1];
                let searchValue = aQ.map(e => {
                    return e.split(",")[2] || '';
                }).join(' ');
                let vid = window.appConfig.vid.replace(':','-');
                this.searchAlternatives = [
                    {
                        label: "Open Knowledge Maps (BASE)",
                        url: "https://openknowledgemaps.org/search?service=base&type=get&sorting=most-relevant&min_descsize=300&q=" + encodeURIComponent(searchValue),
                        image: "/discovery/custom/" + vid + "/img/Logo_Open_Knowledge_Maps.jpg"
                    },
                    {
                        label: "Open Knowledge Maps (Pubmed)",
                        url: "https://openknowledgemaps.org/search?service=pubmed&type=get&sorting=most-relevant&min_descsize=300&q=" + encodeURIComponent(searchValue),
                        image: "/discovery/custom/" + vid + "/img/Logo_Open_Knowledge_Maps.jpg"
                    },
                    {
                        label: "Dimensions",
                        url: "https://app.dimensions.ai/discover/publication?search_mode=content&search_type=kws&search_field=text_search&search_text=" + encodeURIComponent(searchValue),
                        image: "/discovery/custom/" + vid + "/img/Logo_Dimensions.jpg"
                    },
                    {
                        label: "Connected Papers",
                        url: "https://www.connectedpapers.com/search?q=" + encodeURIComponent(searchValue),
                        image: "/discovery/custom/" + vid + "/img/Logo_ConnectedPapers.jpg"
                    }
                ]
            }
        }
        catch(e){
            console.error("***ETH*** an error occured: ethAlternativeSearchController.onInit()\n\n");
            console.error(e.message);
        }
    }
}

ethAlternativeSearchController.$inject = ['$location', 'ethConfigService', 'ethAlternativeSearchConfig'];
