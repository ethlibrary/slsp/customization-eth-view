export const tmiComposeNbConfig = function(){
    return {
        label: {
            print:{
                de: 'Print-Exemplar in ETH-Bibliothek @ swisscovery',
                en: 'Print Item in in ETH-Bibliothek @ swisscovery'
            },
            online: {
                de: 'Online-Exemplar in ETH-Bibliothek @ swisscovery',
                en: 'Online Item in ETH-Bibliothek @ swisscovery'
            }
        }
    }
}
