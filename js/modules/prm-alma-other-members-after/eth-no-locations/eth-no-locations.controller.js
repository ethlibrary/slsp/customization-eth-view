export class ethNoLocationsController {
    constructor( $scope, $compile, $timeout, ethConfigService, ethNoLocationsConfig) {
        this.$scope = $scope;
        this.$compile = $compile;
        this.ethConfigService = ethConfigService;
        this.config = ethNoLocationsConfig;
        this.$timeout = $timeout;
    }

    $onInit() {
        try {
            this.parentCtrl = this.afterCtrl.parentCtrl;
            this.isHint = false;
            if (this.parentCtrl.serviceMode === 'howovp') {
                if (this.parentCtrl.availabilityType == 'P') {
                    this.$timeout(() => {
                        let alertBarOTB = document.querySelector(`[translate='brief.results.tabs.Get_it_from_other_locations_no_services']`);
                        if(!alertBarOTB){
                            let otherMembersContainer = document.querySelectorAll(`prm-alma-other-members[ng-if="$ctrl.ifShowOther('P')"]`);
                            let html = `<div class="eth-no-records bar alert-bar layout-align-center-center layout-row"><span class="center">{{::$ctrl.ethConfigService.getLabel($ctrl.config, \'hint\')}}</span></div>`;
                            angular.element(otherMembersContainer).prepend(this.$compile(html)(this.$scope));
                        }
                    }, 1500);
                }
            }
        }
        catch (e) {
            console.error("***eth*** an error occured: ethNoLocationsController\n\n");
            console.error(e.message);
        }
    }
 }
ethNoLocationsController.$inject =  ['$scope', '$compile', '$timeout', 'ethConfigService', 'ethNoLocationsConfig'];
