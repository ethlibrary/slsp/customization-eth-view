/**
* @ngdoc module
* @name ethChangeAdressModule
*
* @description
*
* Customization for the page My Account - Personal Details:<br>
* - A card containing links for changing adress is added.
*
*<b>CSS/Image Dependencies</b><br>
* CSS eth-change-adress.css
*
*/
import {ethChangeAdressController} from './eth-change-adress.controller';
import {ethConfigService} from '../../../services/eth-config.service';
import {ethChangeAdressConfig} from './eth-change-adress.config';
import {ethChangeAdressHtml} from './eth-change-adress.html';

export const ethChangeAdressModule = angular
    .module('ethChangeAdressModule', [])
        .factory('ethConfigService', ethConfigService)
        .factory('ethChangeAdressConfig', ethChangeAdressConfig)
        .controller('ethChangeAdressController', ethChangeAdressController)
        .component('ethChangeAdressComponent',  {
            bindings: {afterCtrl: '<'},
            controller: 'ethChangeAdressController',
            template: ethChangeAdressHtml
        })
