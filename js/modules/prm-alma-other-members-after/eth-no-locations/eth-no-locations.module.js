/**
* @ngdoc module
* @name ethNoLocationsModule
*
* @description
*
* If there is no item in an ETH lib
* and if the OTB Warning bar "no records" do not appear:
* insert a warning bar
*
* <b>AngularJS Dependencies</b><br>
* Service /js/services {@link ETH.ethConfigService}<br>
* Service {@link ETH.ethNoLocationsConfig}<br>
*
*
* <b>CSS/Image Dependencies</b><br>
* eth-no-locations.css
*
*/
import {ethConfigService} from '../../../services/eth-config.service';
import {ethNoLocationsConfig} from './eth-no-locations.config';
import {ethNoLocationsController} from './eth-no-locations.controller';

export const ethNoLocationsModule = angular
    .module('ethNoLocationsModule', [])
        .factory('ethConfigService', ethConfigService)
        .factory('ethNoLocationsConfig', ethNoLocationsConfig)
        .controller('ethNoLocationsController', ethNoLocationsController)
        .component('ethNoLocationsComponent', {
            bindings: {afterCtrl: '<'},
            controller: 'ethNoLocationsController'
        })
