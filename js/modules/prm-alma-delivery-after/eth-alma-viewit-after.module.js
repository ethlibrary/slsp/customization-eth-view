// View Online
import {ethFullviewArchivesGetitModule} from './eth-fullview-archives-getit/eth-fullview-archives-getit.module';
import {ethFullviewOffcampusWarningModule} from './eth-fullview-offcampus-warning/eth-fullview-offcampus-warning.module';
import {ethOnlineFeedbackMailModule} from './eth-online-feedback-mail/eth-online-feedback-mail.module';
import {ethVideoModule} from './eth-video/eth-video.module';
import {ethWaybackModule} from './eth-wayback/eth-wayback.module';
import {ethLibrarystackModule} from './eth-librarystack/eth-librarystack.module';
import {ethElendingHintModule} from './eth-elending-hint/eth-elending-hint.module';
import {ethSetIdModule} from './eth-set-id/eth-set-id.module';

export const ethAlmaViewitAfterModule = angular
    .module('ethAlmaViewitAfterModule', [])
        .component('prmAlmaViewitAfter',  {
            bindings: {parentCtrl: '<'},
            template: `<eth-fullview-archives-getit-component after-ctrl="$ctrl"></eth-fullview-archives-getit-component>
            <eth-video-component after-ctrl="$ctrl"></eth-video-component>
            <eth-wayback-component after-ctrl="$ctrl"></eth-wayback-component>
            <eth-librarystack-component after-ctrl="$ctrl"></eth-librarystack-component>
            <eth-elending-hint-component after-ctrl="$ctrl"></eth-elending-hint-component>
            <eth-online-feedback-mail-component after-ctrl="$ctrl"></eth-online-feedback-mail-component>
            <eth-fullview-offcampus-warning-component after-ctrl="$ctrl"></eth-fullview-offcampus-warning-component>
            <eth-rc-right-component after-ctrl="$ctrl"></eth-rc-right-component>
            <eth-set-id-component after-ctrl="$ctrl"></eth-set-id-component>
            `
        });

ethAlmaViewitAfterModule.requires.push(ethFullviewArchivesGetitModule.name);
ethAlmaViewitAfterModule.requires.push(ethFullviewOffcampusWarningModule.name);
ethAlmaViewitAfterModule.requires.push(ethOnlineFeedbackMailModule.name);
ethAlmaViewitAfterModule.requires.push(ethVideoModule.name);
ethAlmaViewitAfterModule.requires.push(ethWaybackModule.name);
ethAlmaViewitAfterModule.requires.push(ethLibrarystackModule.name);
ethAlmaViewitAfterModule.requires.push(ethElendingHintModule.name);
ethAlmaViewitAfterModule.requires.push(ethSetIdModule.name);
