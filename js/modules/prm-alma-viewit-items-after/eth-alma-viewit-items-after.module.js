import { slspIconLabelViewitItemsModule } from './slsp-icon-label-viewit-items/slsp-icon-label-viewit-items.module';
import { slspTranslatePublicNoteModule } from './slsp-translate-public-note/slsp-translate-public-note.module';

export const prmAlmaViewitItemsAfterModule = angular
    .module('prmAlmaViewitItemsAfterModule', [])
        .component('prmAlmaViewitItemsAfter',  {
            bindings: {parentCtrl: '<'},
            template: `<slsp-icon-label-viewit-items-component after-ctrl="$ctrl"></slsp-icon-label-viewit-items-component>
                        <slsp-translate-public-note-component after-ctrl="$ctrl"></slsp-translate-public-note-component>
                    `
        });

prmAlmaViewitItemsAfterModule.requires.push(slspIconLabelViewitItemsModule.name);
prmAlmaViewitItemsAfterModule.requires.push(slspTranslatePublicNoteModule.name);
