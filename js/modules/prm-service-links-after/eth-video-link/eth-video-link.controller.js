export class ethVideoLinkController {

    constructor( ethConfigService, ethVideoLinkConfig ) {
        this.ethConfigService = ethConfigService;
        this.config = ethVideoLinkConfig;
    }

    $onInit() {
        try{
            this.parentCtrl = this.afterCtrl.parentCtrl;
            this.changeLinktext = false;
            this.source = '';
            if(this.parentCtrl.item && this.parentCtrl.item.pnx.display.source && this.parentCtrl.item.pnx.display.source.length > 0){
                this.source = this.parentCtrl.item.pnx.display.source[0];
            }
            if(this.source !== 'ETH_Videoportal'){
                return;
            }
            if (!this.parentCtrl.item || !this.parentCtrl.item.delivery || !this.parentCtrl.item.delivery.link) {
                console.error("***ETH*** ethVideoLinkController.$onInit: delivery links not available");
                return;
            }
            let videoStartpageLinks = this.parentCtrl.item.delivery.link.filter( l => {
                if(l.linkURL === 'https://www.video.ethz.ch/'){
                    return true;
                }
                return false;
            })
            if(videoStartpageLinks.length > 0){
                this.changeLinktext = true;
            }
        }
        catch(e){
            console.error("***ETH*** an error occured: ethVideoLinkController $onInit\n\n");
            console.error(e.message);
        }
    }

    $doCheck() {
        try{
            if (this.changeLinktext){
                let aLinks = Array.from(document.querySelectorAll('prm-service-links a[ng-href="https://www.video.ethz.ch/"]'));
                if(aLinks.length === 0)return;
                this.changeLinktext = false;
                let linkText = this.ethConfigService.getLabel(this.config, 'linktext');
                angular.element(aLinks[0]).text(linkText);
            }
        }
        catch(e){
            console.error("***ETH*** an error occured: ethVideoLinkController $doCheck\n\n");
            console.error(e.message);
        }
    }

}

ethVideoLinkController.$inject = ['ethConfigService', 'ethVideoLinkConfig' ];
