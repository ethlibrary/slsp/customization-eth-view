/**
* @ngdoc service
* @name ethConnectedPapersService
*
* @description
* Service to get paper informations and link to connected papers
*
* Module {@link ETH.ethConnectedPapersModule}<br>
*
*
 */

export const ethConnectedPapersService = ['$http', '$sce', function($http, $sce){

    function getPaper(doi){
        // https://daas.library.ethz.ch/rib/v3/enrichments/connectedpapers?doi=10.1093/nar/gkl986
        // https://connectedpapers.notion.site/Connected-Papers-APIs-6f67ca11cf834b42bc637f3357c7274d
        // https://rest.connectedpapers.com/id_translator/doi/10.1093/nar/gkl986
        // https://rest.connectedpapers.com/paper/b9cc21d97d7fb24beec903a686b5c90c26d547f6

        let url = "https://daas.library.ethz.ch/rib/v3/enrichments/connectedpapers?doi=" + doi;
        $sce.trustAsResourceUrl(url);
        return $http.get(url,{headers:{ "X-From-ExL-API-Gateway": undefined }})
            .then(
                function(response){
                    return response.data;
                },
                function(httpError){
                    if (httpError.status === 404)return null;
                    let error = "***ETH*** an error occured: ethConnectedPapersService.getPaper error callback: " + httpError.status;
                    if (httpError.data && httpError.data.errorMessage) {
                        error += ' - ' + httpError.data.errorMessage;
                    }
                    console.error(error);
                    return null;
                }
            );
    }

    return {
        getPaper: getPaper
    };
}];
