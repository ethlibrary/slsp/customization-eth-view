export class ethLoansHintController {

    constructor( $scope, $compile, ethConfigService, ethLoansHintConfig ) {
        this.$scope = $scope;
        this.$compile = $compile;
        this.ethConfigService = ethConfigService;
        this.config = ethLoansHintConfig;
    }

    $onInit() {
        try{
            this.parentCtrl = this.afterCtrl.parentCtrl;
            let loanHistory = document.querySelector('md-select-value [translate="nui.loan.history"]');
            //let loanAvtive = document.querySelector('md-select-value [translate="nui.loan.active"]');
            if(loanHistory){
                if(document.querySelector('.eth-loan-active-hint__container')){
                    angular.element(document.querySelector('.eth-loan-active-hint__container')).remove();
                }
                if(!document.querySelector('.eth-loan-history-hint__container')){
                    let loansContainer = document.querySelector('prm-loans');
                    if(loansContainer){
                        let html = `
                        <div class='eth-loan-hint__container eth-loan-history-hint__container'>
                            {{::$ctrl.ethConfigService.getLabel($ctrl.config, 'textLoansHistory1')}}
                        </div>
                        `;
                        angular.element(loansContainer).prepend(this.$compile(html)(this.$scope));
                    }
                }
            }
            else {
                if(document.querySelector('.eth-loan-history-hint__container')){
                    angular.element(document.querySelector('.eth-loan-history-hint__container')).remove();
                }
                if(!document.querySelector('.eth-loan-active-hint__container')){
                    let loansContainer = document.querySelector('prm-loans');
                    if(loansContainer){
                        let html = `
                        <div class='eth-loan-hint__container eth-loan-active-hint__container'>
                            {{::$ctrl.ethConfigService.getLabel($ctrl.config, 'textLoansActive1')}}
                            {{::$ctrl.ethConfigService.getLabel($ctrl.config, 'textLoansActive2')}}
                            <a href="mailto:{{::$ctrl.ethConfigService.getLabel($ctrl.config, 'mail')}}">
                                {{::$ctrl.ethConfigService.getLabel($ctrl.config, 'mail')}}
                            </a>
                            {{::$ctrl.ethConfigService.getLabel($ctrl.config, 'textLoansActive3')}}
                            <a href="tel:{{::$ctrl.ethConfigService.getLabel($ctrl.config, 'telnr')}}">
                                {{::$ctrl.ethConfigService.getLabel($ctrl.config, 'tel')}}
                            </a>
                            {{::$ctrl.ethConfigService.getLabel($ctrl.config, 'textLoansActive4')}}
                        </div>
                        `;
                        angular.element(loansContainer).prepend(this.$compile(html)(this.$scope));
                    }
                }
            }
        }
        catch(e) {
            console.error("***ETH*** an error occured: ethLoansHintController");
            console.error(e.message);
        }
    }
}

ethLoansHintController.$inject = ['$scope', '$compile', 'ethConfigService', 'ethLoansHintConfig' ];
