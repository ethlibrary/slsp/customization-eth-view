export class ethSwisscoveryController {
    constructor( $location, ethConfigService, ethSwisscoveryConfig ) {
        this.$location = $location;
        this.ethConfigService = ethConfigService;
        this.config = ethSwisscoveryConfig;
    }

    $onInit() {
        try{
            this.serverHint = "";
            this.vid = window.appConfig.vid;
            this.lang = this.ethConfigService.getLanguage();
            /*if (this.$location.host().indexOf("eth.swisscovery.slsp.ch")===-1) {
                this.serverHint += this.$location.host();
            }*/
        }
        catch(e){
            console.error("***ETH*** an error occured: ethSwisscoveryController\n\n");
            console.error(e.message);
        }
    }
}
ethSwisscoveryController.$inject = ['$location', 'ethConfigService', 'ethSwisscoveryConfig' ];
