export class ethVideoController {

    constructor( ethConfigService, ethVideoConfig ) {
        this.ethConfigService = ethConfigService;
        this.config = ethVideoConfig;
    }

    $onInit() {
        try{
            this.parentCtrl = this.afterCtrl.parentCtrl;
            this.changeLinktext = false;
            this.source = '';
            if(this.parentCtrl.item && this.parentCtrl.item.pnx.display.source && this.parentCtrl.item.pnx.display.source.length > 0){
                this.source = this.parentCtrl.item.pnx.display.source[0];
            }
            if(this.source !== 'ETH_Videoportal'){
                return;
            }
            if (!this.parentCtrl.item || !this.parentCtrl.item.delivery || !this.parentCtrl.item.delivery.link) {
                console.error("***ETH*** ethVideoController.$onInit: delivery links not available");
                return;
            }
            let videoStartpageLinks = this.parentCtrl.item.delivery.link.filter( l => {
                if(l.linkURL === 'https://www.video.ethz.ch/'){
                    return true;
                }
                return false;
            })
            if(videoStartpageLinks.length > 0){
                this.changeLinktext = true;
            }
        }
        catch(e){
            console.error("***ETH*** an error occured: ethVideoController $onInit\n\n");
            console.error(e.message);
        }
    }

    $doCheck() {
        try{
            if (this.changeLinktext){
                let aLinks = Array.from(document.querySelectorAll('prm-alma-viewit-items a[ng-href="https://www.video.ethz.ch/"]'));
                if(aLinks.length === 0)return;
                this.changeLinktext = false;
                let linkText = this.ethConfigService.getLabel(this.config, 'linktext');
                angular.element(aLinks[0]).text(linkText);
            }
        }
        catch(e){
            console.error("***ETH*** an error occured: ethVideoController $doCheck\n\n");
            console.error(e.message);
        }
    }

}

ethVideoController.$inject = ['ethConfigService', 'ethVideoConfig' ];
