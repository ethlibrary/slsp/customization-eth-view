export class ethAltmetricController {

    constructor( angularLoad ) {
        this.angularLoad = angularLoad;
    }

    $onInit() {
        this.parentCtrl = this.briefResult;
        // email template
        if(this.parentCtrl.displayMode && this.parentCtrl.displayMode==='email')return;
        this.isLastItem = false;
        this.tab = '';
        this.doi = '';
        this.isbn = '';
        if (!this.parentCtrl.item || !this.parentCtrl.item.pnx) {
            console.error("***ETH*** ethAltmetricController.$onInit: item or item.pnx not available");
            return;
        }
        if (this.parentCtrl.item.pnx.addata && this.parentCtrl.item.pnx.addata.doi && this.parentCtrl.item.pnx.addata.doi.length > 0) {
            this.doi = this.parentCtrl.item.pnx.addata.doi[0];
        }
        else if (this.parentCtrl.item.pnx.addata && this.parentCtrl.item.pnx.addata.isbn && this.parentCtrl.item.pnx.addata.isbn.length > 0) {
            this.isbn = this.parentCtrl.item.pnx.addata.isbn[0];
        }
        // detail view
        if (!!document.querySelector('prm-full-view-service-container')) {
            let container = angular.element(document.querySelector('prm-full-view-service-container')).controller('prmFullViewServiceContainer');
            this.isLastItem = (this.parentCtrl.item === container.item);
        }
        // result list
        else if(!!document.querySelector('prm-search-result-list')) {
            let itemList = angular.element(document.querySelector('prm-search-result-list')).controller('prmSearchResultList').itemlist;
            this.isLastItem = (this.parentCtrl.item === itemList[itemList.length-1]);
        }
        else{
            console.error("***ETH*** ethAltmetricController.$onInit: detail view or result list not available");
        }
        if(this.isLastItem){
            let _this = this;
            window.setTimeout(function(){
                if (typeof _altmetric_embed_init !== "undefined") {
                    try{_altmetric_embed_init();}catch(e){}
                }
                else{
                    _this.angularLoad.loadScript('https://d1bxh8uas1mnw7.cloudfront.net/assets/embed.js')
                        .then(function() {
                            try{_altmetric_embed_init();}catch(e){}
                        })
                        .catch(function(e) {
                            console.error("***ETH*** ethAltmetricController angularLoad.loadScript()");
                            console.error(e.message);
                        });
                }
            },0);
        };
    };

}

ethAltmetricController.$inject = ['angularLoad'];
