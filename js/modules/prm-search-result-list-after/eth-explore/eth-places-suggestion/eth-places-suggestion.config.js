export const ethPlacesSuggestionConfig = function(){
    return {
        label: {
            headingPlaces: {
                de: 'Orte in der Schweiz zum Suchbegriff',
                en: 'Places in Switzerland matching search term'
            },
            searchThis1: {
                de: 'Klicken Sie bitte auf den Marker, um "',
                en: 'Please click on the marker to open "'
            },
            searchThis2: {
                de: '" zu öffnen.',
                en: '".'
            },
            searchThisETHorama1: {
                de: 'Klicken Sie bitte auf den Marker, um "',
                en: 'Please click on the marker to open "'
            },
            searchThisETHorama2: {
                de: '" in ETHorama zu öffnen (neues Fenster).',
                en: '" in ETHorama (new window).'
            },
            headingPlaces: {
                de: 'Orte in der Schweiz',
                en: 'Places in Switzerland'
            },
            headingIntro1: {
                de: 'Die angezeigten Orte basieren auf einer',
                en: 'The places shown are based on a'
            },
            headingIntro2: {
                de: 'lexikalischen Suche',
                en: 'lexical search'
            },
            headingIntro3: {
                de: 'in den',
                en: 'in the '
            },
            headingIntro4: {
                de: 'Orten.',
                en: 'Places.'
            }
        },
        url:{
            info: {
                de: 'https://library.ethz.ch',
                en: 'https://library.ethz.ch'
            },
            ethorama: {
                de: 'https://ethorama.library.ethz.ch/de',
                en: 'https://ethorama.library.ethz.ch/en'
            },
            ethoramaDossier: {
                de: 'https://ethorama.library.ethz.ch/de/themen/',
                en: 'https://ethorama.library.ethz.ch/en/topics/'
            },
            ethoramaRoute: {
                de: 'https://ethorama.library.ethz.ch/de/reisen/',
                en: 'https://ethorama.library.ethz.ch/en/routes/'
            }
        }
    }
}
