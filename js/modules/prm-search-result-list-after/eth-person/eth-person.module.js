/**
* @ngdoc module
* @name ethPersonModule
*
* @description
*
* - constitutes person page (based on result list)
* - precision recall navigation
* - checks beacon.findbuch, Metagrid, Entityfacts (DNB), Wikidata for person informations (by GND ID)
*
*
* <b>AngularJS Dependencies</b><br>
* Service /js/services {@link ETH.ethSessionService}<br>
* Service /js/services {@link ETH.ethConfigService}<br>
* Service {@link ETH.ethPersonService}<br>
* Service js/modules/shared/eth-person-card {@link ETH.ethPersonCardConfig}<br>
* Service js/modules/shared/eth-person-card {@link ETH.ethPersonCardService}<br>
*
*
* <b>CSS/Image Dependencies</b><br>
* CSS js/modules/shared/eth-person-card/eth-person-card.css
* CSS js/modules/shared/eth-person-card/splus.css
*
*
*/
import {ethConfigService} from '../../../services/eth-config.service';
import {ethSessionService} from '../../../services/eth-session.service';
import {ethPersonCardConfig} from '../../shared/eth-person-card/eth-person-card.config';
import {ethPersonCardService} from '../../shared/eth-person-card/eth-person-card.service';
import {ethPersonCardHtml} from '../../shared/eth-person-card/eth-person-card.html';
import {ethPersonService} from './eth-person.service';
import {ethPersonController} from './eth-person.controller';

export const ethPersonModule = angular
    .module('ethPersonModule', [])
        .factory('ethConfigService', ethConfigService)
        .factory('ethSessionService', ethSessionService)
        .factory('ethPersonCardService', ethPersonCardService)
        .factory('ethPersonCardConfig', ethPersonCardConfig)
        .factory('ethPersonService', ethPersonService)
        .controller('ethPersonController', ethPersonController)
        .component('ethPersonComponent',{
            bindings: {afterCtrl: '<'},
            controller: 'ethPersonController',
            template: ethPersonCardHtml
        })

