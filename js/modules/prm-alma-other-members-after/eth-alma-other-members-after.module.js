import {slspIconLabelOtherMembersModule} from './slsp-icon-label-other-members/slsp-icon-label-other-members.module';
import {ethCollapseOtherInstModule} from './eth-collapse-other-inst/eth-collapse-other-inst.module';
import {ethNoLocationsModule} from './eth-no-locations/eth-no-locations.module';

export const prmAlmaOtherMembersAfterModule = angular
    .module('prmAlmaOtherMembersAfterModule', [])
        .component('prmAlmaOtherMembersAfter',  {
            bindings: {parentCtrl: '<'},
            template: `
            <slsp-icon-label-other-members-component after-ctrl="$ctrl"></slsp-icon-label-other-members-component>
            <eth-collapse-other-inst-component after-ctrl="$ctrl"></eth-collapse-other-inst-component>
            <eth-no-locations-component after-ctrl="$ctrl"></eth-no-locations-component>
            `
        });
prmAlmaOtherMembersAfterModule.requires.push(slspIconLabelOtherMembersModule.name);
prmAlmaOtherMembersAfterModule.requires.push(ethCollapseOtherInstModule.name);
prmAlmaOtherMembersAfterModule.requires.push(ethNoLocationsModule.name);
