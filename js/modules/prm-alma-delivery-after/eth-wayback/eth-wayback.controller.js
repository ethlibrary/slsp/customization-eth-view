export class ethWaybackController {

    constructor( $scope, $compile, ethConfigService, ethWaybackConfig ) {
        this.$scope = $scope;
        this.$compile = $compile;
        this.ethConfigService = ethConfigService;
        this.config = ethWaybackConfig;
    }

    $onInit() {
        // 99117429500405503
        try{
            this.parentCtrl = this.afterCtrl.parentCtrl;
            this.processDoCheck = false;
            this.source = '';
            if(this.parentCtrl.item && this.parentCtrl.item.pnx.display.source && this.parentCtrl.item.pnx.display.source.length > 0){
                this.source = this.parentCtrl.item.pnx.display.source[0];
            }
            if(this.source !== 'ETH_Hochschularchiv'){
                return;
            }
            if (!this.parentCtrl.item || !this.parentCtrl.item.delivery || !this.parentCtrl.item.delivery.link) {
                console.error("***ETH*** ethWaybackController.$onInit: delivery links not available");
                return;
            }
            let waybackLinks = this.parentCtrl.item.delivery.link.filter( l => {
                if(l.linkURL.indexOf('https://wayback.archive-It.org') > -1){
                    return true;
                }
                return false;
            })
            if(waybackLinks.length > 0){
                this.processDoCheck = true;
            }
        }
        catch(e){
            console.error("***ETH*** an error occured: ethWaybackController $onInit\n\n");
            console.error(e.message);
        }
    }

    $doCheck() {
        try{
            if (!this.processDoCheck){return}
            // change link text
            let aLinks = Array.from(document.querySelectorAll('prm-alma-viewit-items a[ng-href^="https://wayback.archive-It.org"]'));
            if(aLinks.length === 0)return;
            this.processDoCheck = false;
            let linkText = this.ethConfigService.getLabel(this.config, 'linktext');
            angular.element(aLinks[0]).text(linkText);
            // render hint container
            let viewit = document.querySelector('prm-alma-viewit');
            let html = `
            <div class='eth-wayback-hint'>
                ${this.ethConfigService.getLabel(this.config, 'hint')}
            </div>
            `;
            angular.element(viewit).append(this.$compile(html)(this.$scope));
        }
        catch(e){
            console.error("***ETH*** an error occured: ethWaybackController $doCheck\n\n");
            console.error(e.message);
            throw(e);
        }
    }

}
ethWaybackController.$inject = ['$scope', '$compile', 'ethConfigService', 'ethWaybackConfig' ];
