/**
* @ngdoc module
* @name ethPersonsSuggestionModule
*
* @description
*
* - Query expansion via wikidata: suggest persons for searchterm
*
* <b>AngularJS Dependencies</b><br>
* Service /js/services {@link ETH.ethSessionService}<br>
* Service /js/services {@link ETH.ethConfigService}<br>
* Service {@link ETH.ethPersonsSuggestionConfig}<br>
* Service {@link ETH.ethPersonsSuggestionService}<br>
*
*
*/
import {ethConfigService} from '../../../../services/eth-config.service';
import {ethPersonsSuggestionConfig} from './eth-persons-suggestion.config';
import {ethPersonsSuggestionService} from './eth-persons-suggestion.service';
import {ethPersonsSuggestionController} from './eth-persons-suggestion.controller';
import {ethPersonsSuggestionHtml} from './eth-persons-suggestion.html';

export const ethPersonsSuggestionModule = angular
    .module('ethPersonsSuggestionModule', [])
        .factory('ethConfigService', ethConfigService)
        .factory('ethPersonsSuggestionConfig', ethPersonsSuggestionConfig)
        .factory('ethPersonsSuggestionService', ethPersonsSuggestionService)
        .controller('ethPersonsSuggestionController', ethPersonsSuggestionController)
        .component('ethPersonsSuggestionComponent',{
            bindings: {afterCtrl: '<'},
            controller: 'ethPersonsSuggestionController',
            template: ethPersonsSuggestionHtml
        })
