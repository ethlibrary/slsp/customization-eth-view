/**
* @ngdoc module
* @name ethTitleAsTextModule
*
* @description
* The title of a loan should not be a link, because of rapido temporary items.
*
*
*
*/
import {ethTitleAsTextController} from './eth-title-as-text.controller';

export const ethTitleAsTextModule = angular
   .module('ethTitleAsTextModule', [])
       .controller('ethTitleAsTextController', ethTitleAsTextController)
       .component('ethTitleAsTextComponent', {
           bindings: {afterCtrl: '<'},
           controller: 'ethTitleAsTextController'
       })
