export class ethLibrarystackController {

    constructor( $scope, $compile, ethConfigService, ethLibrarystackConfig ) {
        this.$scope = $scope;
        this.$compile = $compile;
        this.ethConfigService = ethConfigService;
        this.config = ethLibrarystackConfig;
    }

    $onInit() {
        // cdi_librarystack_primary_159090
        try{
            this.processDoCheck = false;
            this.parentCtrl = this.afterCtrl.parentCtrl;
            // fulltext_linktorsrc
            if(this.parentCtrl.item && this.parentCtrl.item.delivery && this.parentCtrl.item.delivery.availability && this.parentCtrl.item.delivery.availability=='fulltext_linktorsrc'){
                this.processDoCheck = true;    
            }
        }
        catch(e){
            console.error("***ETH*** an error occured: ethLibrarystackController $onInit\n\n");
            console.error(e.message);
        }
    }

    $doCheck() {
        try{
            if (!this.processDoCheck){return}
            if(!this.parentCtrl.item.delivery.electronicServices){return}

            // serviceUrl starts with 'www.librarystack.org'
            if(this.parentCtrl.item.delivery.electronicServices[0] && this.parentCtrl.item.delivery.electronicServices[0].serviceUrl && this.parentCtrl.item.delivery.electronicServices[0].serviceUrl.indexOf('www.librarystack.org') > -1){
                let aLinks = Array.from(document.querySelectorAll('prm-alma-viewit-items a[ng-href^="https://www.librarystack.org"]'));
                if(aLinks.length === 0){return}
                let html = `
                <div class='eth-librarystack-hint'>
                    <p><em><strong>${this.ethConfigService.getLabel(this.config, 'hint1')}</strong></em></p>
                    <p><em>${this.ethConfigService.getLabel(this.config, 'hint2')}</em></p>
                </div>
                `;
                aLinks.forEach(l => {
                    angular.element(l).append(this.$compile(html)(this.$scope));
                })
            }
            this.processDoCheck = false;
        }
        catch(e){
            console.error("***ETH*** an error occured: ethLibrarystackController $doCheck\n\n");
            console.error(e.message);
            throw(e);
        }
    }

}
ethLibrarystackController.$inject = ['$scope', '$compile', 'ethConfigService', 'ethLibrarystackConfig' ];
