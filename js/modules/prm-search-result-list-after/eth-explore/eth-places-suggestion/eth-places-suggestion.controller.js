export class ethPlacesSuggestionController {

    constructor( $timeout, angularLoad, ethConfigService, ethPlacesSuggestionConfig, ethPlacesSuggestionService ) {
        this.$timeout = $timeout;
        this.angularLoad = angularLoad;
        this.ethConfigService = ethConfigService;
        this.config = ethPlacesSuggestionConfig;
        this.ethPlacesSuggestionService = ethPlacesSuggestionService;
    }


    $onInit() {
        try{
            this.parentCtrl = this.afterCtrl.afterCtrl.parentCtrl;
            this.exploreCtrl = this.afterCtrl;

            this.pois = [];

            this.lang = this.ethConfigService.getLanguage();
            this.vid = window.appConfig.vid;
            this.tab = window.appConfig['primo-view']['available-tabs'][0];
            let query = this.parentCtrl.searchService.searchUtil.$stateParams.query;
            // this.search: input in Primo searchField            
            this.search = query.substring(query.indexOf('contains,') + 9);

            this.getPlaces(this.search);
        }
        catch(e){
            console.error("***ETH*** an error occured: ethPlacesSuggestionController.onInit()\n\n");
            console.error(e.message);
        }
    }

    getPlaces(search){
        this.exploreCtrl.hasPois = 'loading';
        this.ethPlacesSuggestionService.getPois(search)
            .then((data) => {
                if(!data || !data.features || data.features.length === 0){
                    this.exploreCtrl.hasPois = 'no';
                    return;
                }
                this.pois = data.features;
                if(this.pois.length === 0){
                    this.exploreCtrl.hasPois = 'no';
                    return;
                }
                this.exploreCtrl.hasPois = 'yes';
                // map should not be rendered before primo results
                let timeoutValue = 0;
                if(this.parentCtrl.itemlist && this.parentCtrl.itemlist.length === 0){
                    timeoutValue = 1500;
                }
                let _this = this;
                window.setTimeout(function(){
                    if (typeof L !== "undefined" && typeof L.markerClusterGroup !== "undefined" ) {
                        _this.renderLeafletMap(_this.pois);
                    }
                    else{
                        _this.angularLoad.loadCSS('https://unpkg.com/leaflet@1.9.4/dist/leaflet.css',{integrity: 'sha512-xwE/Az9zrjBIphAcBb3F6JVqxf46+CDLwfLMHloNu6KEQCAWi6HcDUbeOfBIptF7tcCzusKFjFw2yuvEpDL9wQ==', crossorigin:''});
                        _this.angularLoad.loadCSS('https://cdnjs.cloudflare.com/ajax/libs/leaflet.markercluster/1.5.0/MarkerCluster.Default.min.css',{crossorigin:''});
                        _this.angularLoad.loadScript('https://unpkg.com/leaflet@1.9.4/dist/leaflet.js',{integrity: 'sha512-GffPMF3RvMeYyc1LWMHtK8EbPv0iNZ8/oTtHPx9/cc2ILxQ+u905qIwdpULaqDkyBKgOaB57QTMg7ztg8Jm2Og==', crossorigin:''})
                            .then(function() {
                                _this.angularLoad.loadScript('https://cdnjs.cloudflare.com/ajax/libs/leaflet.markercluster/1.5.0/leaflet.markercluster.js', {crossorigin:''})
                                    .then(function() {
                                        _this.renderLeafletMap(_this.pois);
                                    })
                            })
                            .catch(function(e) {
                                console.error("***ETH*** ethPlacesSuggestionController: angularLoad.renderLeafletMap()");
                                console.error(e.message);
                            });
                    }
                }, timeoutValue);
                return;
            })
            .catch( (e) => {
                console.error("***ETH*** an error occured: ethPlacesSuggestionController ethPlacesSuggestionService.getPlaces");
                console.error(e.message);
                throw(e)
            })
    }

    renderLeafletMap(features){
        if(!document.getElementById('mapid'))return;
        let _this = this;
        // map
        let map = L.map('mapid');
        L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
            attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/map-feedback/">Mapbox</a>',
            tileSize: 512,
            maxZoom: 18,
            zoomOffset: -1,
            id: 'mapbox/streets-v11',
            accessToken: 'pk.eyJ1IjoiYmVybmR1IiwiYSI6ImNrMjBpMHRidjE1ZGkzaHFnNDdlMXV4eWkifQ.RACc6dWL675djVZaMmHBww'
        }).addTo(map);

        // marker cluster
        let markerClusters = L.markerClusterGroup({
            iconCreateFunction: function(cluster) {
                let childCount = cluster.getChildCount();
                let c = 'marker-cluster-';
                if (childCount < 10) {
                    c += 'small';
                }
                else if (childCount < 100) {
                    c += 'medium';
                }
                else {
                    c += 'large';
                }
                return new L.DivIcon({
                    html: '<div><span>' + childCount + '</span></div>',
                    className: 'marker-cluster ' + c, iconSize: new L.Point(40, 40)
                })
            }
        })

        // add map marker
        features.forEach((feature, i) => {
            /*if(!feature.properties.qid){
                return;
            }*/
            let point = feature.geometry.coordinates;
            let lng = point[0];
            let lat = point[1];
            let markerOptions = {
               clickable: true,
               alt: 'Marker ' + feature.properties.name_de
            }
            let marker = L.marker([lat, lng], markerOptions);
            let content = '<div class="marker-info">';
            if(feature.properties.thumbnail){
                content += '<img src="' + feature.properties.thumbnail + '">';
            }
            content += '<div class="marker-info-text"><p class="marker-info-title">' + feature.properties.name_de;
            content += '</p><p class="marker-info-hint">';
            if(feature.properties.qid){
                content += this.ethConfigService.getLabel(this.config, 'searchThis1') + feature.properties.name_de + this.ethConfigService.getLabel(this.config, 'searchThis2');
            }
            else{
                content += this.ethConfigService.getLabel(this.config, 'searchThisETHorama1') + feature.properties.name_de + this.ethConfigService.getLabel(this.config, 'searchThisETHorama2');
            }
            content += '</p></div></div>';
            marker.bindPopup(content);
            marker.on('mouseover', function (e) {
                this.openPopup();
            });
            // touch: long press (1sec)
            marker.on('contextmenu', function (e) {
                this.openPopup();
            });
            marker.on('mouseout', function (e) {
                this.closePopup();
            });
            marker.on('click', function (e) {
                _this.loadPlacePage(feature.properties);
            });
            markerClusters.addLayer(marker);
        });

        map.addLayer(markerClusters);
        map.setView([46.800663464, 8.2226657], 8);
    }

    loadPlacePage(properties){
        // matomo
        if(!window._paq){
            window._paq = [];
        }
        window._paq.push(['setCustomUrl', 'https://eth.swisscovery.slsp.ch/explore/place']);
        window._paq.push(['trackPageView']);
        if(properties.qid){
            let query = '[wd/place]' + properties.qid;
            let vid = window.appConfig.vid;
            let tab = window.appConfig['primo-view']['available-tabs'][0];
            let url = `/discovery/search?tab=${tab}&query=any,contains,${query}&vid=${vid}&lang=${this.lang}&explore=place`;
            location.href = url;
        }
        else{
            let url = 'https://ethorama.library.ethz.ch/de/orte/' + properties.id;
            if(this.lang === 'en'){
                url = 'https://ethorama.library.ethz.ch/en/locations/' + properties.id;
            }
            window.open(url, '_blank');
            return true;
        } 
    }

}
ethPlacesSuggestionController.$inject = ['$timeout', 'angularLoad', 'ethConfigService', 'ethPlacesSuggestionConfig', 'ethPlacesSuggestionService'];
