/**
* @ngdoc service
* @name ethTocLinkService
*
* @description
*
* getTOC(): checks dnb for a TOC, by identifier (ISBN)
*
*/
export const ethTocLinkService = ['$http', '$sce', function($http, $sce){
    function getTOC(identifier){
        let url = 'https://daas.library.ethz.ch/rib/v3/enrichments/dnb/toc/'  + identifier;
        $sce.trustAsResourceUrl(url);
        return $http.get(url)
            .then(
                function(response){
                    return response.data;
                },
                function(httpError){
                    if (httpError.status === 404 || httpError.status === 500)return null;
                    let error = "***ETH*** an error occured: ethTocLinkService.getTOC(): " + httpError.status;
                    if (httpError.data && httpError.data.errorMessage) {
                        error += ' - ' + httpError.data.errorMessage;
                    }
                    console.error(error);
                    return null;
                }
            );
    }

    return {
        getTOC: getTOC
    };

}]
