
export class ethAddLoginstatusClassController {

    constructor(ethSessionService) {
        this.ethSessionService = ethSessionService;
    }

    $doCheck() {
        try{
            this.parentCtrl = this.afterCtrl.parentCtrl;
            this.isGuest = this.ethSessionService.isGuest();
            if (this.isGuest == true) {
                let myEl = angular.element(document.querySelector('primo-explore'));
                return myEl.addClass('logged-out')
            }
            else {
                let myEl = angular.element(document.querySelector('primo-explore'));
                return myEl.removeClass('logged-out');
            }
        }
        catch(e){
            console.error("***ETH*** an error occured: ethAddLoginstatusClassController\n\n");
            console.error(e.message);
        }
    }
}

ethAddLoginstatusClassController.$inject = ['ethSessionService'];
