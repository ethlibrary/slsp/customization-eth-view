import {ethHideAddModule} from './eth-hide-add/eth-hide-add.module';

export const ethLegantoAfterModule = angular
    .module('ethLegantoAfterModule', [])
        .component('prmLegantoAfter',  {
            bindings: {parentCtrl: '<'},
            template: `
            <eth-hide-add-component after-ctrl="$ctrl"></eth-hide-add-component>
            `
        });
ethLegantoAfterModule.requires.push(ethHideAddModule.name);
