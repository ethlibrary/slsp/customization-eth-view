/**
* @ngdoc module
* @name ethPageTypeController
*
* @description
*
* Customization of the height of the header container:<br>
* - It is checked whether the current page is the home page<br>
* - If yes, the CSS class 'eth-startpage' is written into the html element.
*
*
* <b>AngularJS Dependencies</b><br>
*
*
* <b>CSS/Image Dependencies</b><br>
* CSS /css/header.css
*
*
*/
import {ethPageTypeController} from './eth-page-type.controller';

export const ethPageTypeModule = angular
    .module('ethPageTypeModule', [])
        .controller('ethPageTypeController', ethPageTypeController)
        .component('ethPageTypeComponent', {
            bindings: {afterCtrl: '<'},
            controller: 'ethPageTypeController'
        })
