/**
* @ngdoc module
* @name ethBrowseStartpageModule
*
* @description
*
* Customization for the browse search start page
*
*
* <b>AngularJS Dependencies</b><br>
* Service /js/services {@link ETH.ethConfigService}<br>
* Service {@link ETH.ethBrowseStartpageConfig}<br>
*
*
* <b>CSS/Image Dependencies</b><br>
* CSS eth-browse-startpage.css
*
*/
import {ethConfigService} from '../../../services/eth-config.service';
import {ethBrowseStartpageConfig} from './eth-browse-startpage.config';
import {ethBrowseStartpageController} from './eth-browse-startpage.controller';
import {ethBrowseStartpageHtml} from './eth-browse-startpage.html';

export const ethBrowseStartpageModule = angular
    .module('ethBrowseStartpageModule', [])
        .factory('ethConfigService', ethConfigService)
        .factory('ethBrowseStartpageConfig', ethBrowseStartpageConfig)
        .controller('ethBrowseStartpageController', ethBrowseStartpageController)
        .component('ethBrowseStartpageComponent',{
            bindings: {afterCtrl: '<'},
            controller: 'ethBrowseStartpageController',
            template: ethBrowseStartpageHtml
        })
