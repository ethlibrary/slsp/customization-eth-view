export const ethBrowseStartpageConfig = function(){
    return {
        label: {
            heading1: {
                de: 'Signatursuche',
                en: 'Call number search'
            },
            p1a: {
                de: 'Die Signatursuche findet ausschliesslich Dokumente aus den Beständen der',
                en: 'The call number search only finds documents in the'
            },
            p1b: {
                de: 'Bibliotheken der ETH Zürich',
                en: 'the holdings of the ETH Zurich libraries'
            },
            p1c: {
                de: 'Nicht alle Bibliotheken an der ETH Zürich stellen ihre Bestände nach Themen geordnet auf.',
                en: 'Not all libraries at ETH Zurich arrange their holdings by subject on the shelves.'
            },
            h3: {
                de: 'Tipps für die Suche',
                en: 'Tips for searching'
            },
            option1a: {
                de: 'Verwenden Sie die genaue Schreibweise mit Leerschlägen und Sonderzeichen: z.B. ILA 1.1 | 55.',
                en: 'Use the exact spelling with spaces and special characters (e.g. ILA 1.1 | 55).'
            },
            option1b: {
                de: 'Ein senkrechter Strich kann mit der Tastenkombination Alt Gr 7 (Windows) bzw. Alt 7 (Mac) erzeugt werden.',
                en: 'A vertical line can be created with AltGr+7 (Windows) or Alt+7 (Mac).'
            },
            option2a: {
                de: 'Suchen Sie nach Signatur (grün) und nicht nach "Zusätzliche Signatur".',
                en: 'Search for the call number (green) and not the “additional call number”.'
            },
            option2b: {
                de: 'Suchen Sie so',
                en: 'Search like this'
            },
            option2c: {
                de: 'und nicht so',
                en: 'And not like this'
            },
            option3: {
                de: 'Kürzen Sie die Signatur von rechts her, falls Sie das Gewünschte nicht finden.',
                en: 'Shorten the call number from the right to the left if you do not find what you are looking for or your search fails.'
            },
            heading2: {
                de: 'Bibliotheken mit thematisch aufgestellten Beständen',
                en: 'Libraries with thematically arranged holdings'
            },
            p2: {
                de: 'In folgenden Bibliotheken sind die Bestände nach Themen angeordnet und damit für eine Signatursuche geeignet:',
                en: 'In the following libraries, the holdings are arranged by subject and thus suitable for a call number search:'
            },
            lib1: {
                de: 'Baubibliothek',
                en: 'Architecture and Civil Engineering Library'
            },
            lib2: {
                de: 'Bibliothek Dieter Kienast',
                en: 'Dieter Kienast Library'
            },
            lib3: {
                de: 'Bibliothek Erdwissenschaften',
                en: 'Earth Sciences Library'
            },
            lib4: {
                de: 'GESS Bibliothek',
                en: 'GESS Library'
            },
            lib5: {
                de: 'Grüne Bibliothek',
                en: 'Green Library'
            },
            lib6: {
                de: 'gta Bibliothek',
                en: 'gta Library'
            },
            lib7: {
                de: 'Informatik-Bibliothek:',
                en: 'Computer Science Library:'
            },
            lib7linktext: {
                de: 'Lehrbuchsammlung',
                en: 'Textbook Collection'
            },
            lib8: {
                de: 'Infozentrum Chemie Biologie Pharmazie',
                en: 'Chemistry Biology Pharmacy Info Center'
            },
            lib9: {
                de: 'Mathematik-Bibliothek:',
                en: 'Mathematics Library:'
            },
            lib9linktext: {
                de: 'Detailed subject classification',
                en: 'Detailed Subject Classifications'
            },
            lib10: {
                de: 'Physik-Bibliothek',
                en: 'Physics Library'
            },
            lib11: {
                de: 'Selbstlernzentrum Hönggerberg des Sprachenzentrums',
                en: 'Self-Access Centre Hönggerberg of the Language Center'
            },
            p3a: {
                de: 'Zu beachten:',
                en: 'Note:'
            },
            p3b: {
                de: 'Jedes Buch kann nur einem Thema zugeordnet werden, deshalb ist die Signatursuche kein Ersatz für eine thematische Suche in',
                en: 'Each book can only be assigned to one subject. The call number search is therefore no substitute for a thematic search in'
            },
            openingLinktext: {
                de: 'Standorte und Öffnungszeiten',
                en: 'Locations and opening hours'
            },
            swisscoveryLinktext: {
                de: 'ETH Library @ swisscovery',
                en: 'ETH Library @ swisscovery'
            },
        },
        url:{
            lib7link: {
                de: 'https://www.library.inf.ethz.ch/buecher/lehrbuchregal.html',
                en: 'https://www.library.inf.ethz.ch/en/stock/textbook-collection.html'
            },
            lib9link: {
                de: 'https://ethz.ch/content/dam/ethz/special-interest/math/department/Library/subject_classification.pdf',
                en: 'https://ethz.ch/content/dam/ethz/special-interest/math/department/Library/subject_classification.pdf'
            },
            opening: {
                de: 'https://library.ethz.ch/standorte-und-medien/standorte-und-oeffnungszeiten.html',
                en: 'https://library.ethz.ch/en/locations-and-media/locations-and-opening-hours.html'
            },
            swisscovery: {
                de: 'https://eth.swisscovery.slsp.ch/discovery/search?vid=41SLSP_ETH:ETH',
                en: 'https://eth.swisscovery.slsp.ch/discovery/search?vid=41SLSP_ETH:ETH&lang=en'
            }
        }
    }
}
