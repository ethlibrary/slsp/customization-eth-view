/**
* @ngdoc module
* @name ethElendingHintModule
*
* @description
*
* Customization for the links section:<br>
* - add link to a CMS page about E-Lending
*
* <b>AngularJS Dependencies</b><br>
* Service /js/services {@link ETH.ethConfigService}<br>
* Service {@link ETH.ethElendingHintConfig}<br>
*
*
*/
import {ethConfigService} from '../../../services/eth-config.service';
import {ethElendingHintConfig} from './eth-elending-hint.config';
import {ethElendingHintController} from './eth-elending-hint.controller';
import {ethElendingHintHtml} from './eth-elending-hint.html';

export const ethElendingHintModule = angular
    .module('ethElendingHintModule', [])
        .factory('ethConfigService', ethConfigService)
        .factory('ethElendingHintConfig', ethElendingHintConfig)
        .controller('ethElendingHintController', ethElendingHintController)
        .component('ethElendingHintComponent', {
            bindings: {afterCtrl: '<'},
            controller: 'ethElendingHintController',
            template: ethElendingHintHtml
        })
