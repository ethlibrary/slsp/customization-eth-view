import { ethLoginBoxModule } from './eth-login-box/eth-login-box.module';

export const prmLoginAfterModule = angular
    .module('prmLoginAfterModule', [])
    .component('prmLoginAfter', {
        bindings: { parentCtrl: '<' },
        template: `
            <eth-login-box-component after-ctrl="$ctrl"></eth-login-box-component>
            `
    });


prmLoginAfterModule.requires.push(ethLoginBoxModule.name);