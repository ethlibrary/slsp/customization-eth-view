import {ethSectionOrderingModule} from './eth-section-ordering/eth-section-ordering.module';

export const ethFullViewAfterModule = angular
    .module('ethFullViewAfterModule', [])
        .component('prmFullViewAfter',  {
            bindings: {parentCtrl: '<'},
            template: `<eth-section-ordering-component after-ctrl="$ctrl"></eth-section-ordering-component>`
        });

ethFullViewAfterModule.requires.push(ethSectionOrderingModule.name);
