/**
* @ngdoc module
* @name ethGitHintModule
*
* @description
*
* Customization for the Top Bar:<br>
* - get text for an error message from a git file
* - render the message at the top of the page
*
* file: https://github.com/eth-library/snippets/tree/main/primo
*       Page: https://eth-library.github.io/snippets/primo/banner.json
*
* <b>CSS/Image Dependencies</b><br>
* CSS eth-git-hint.css
*
*/
import {ethGitHintService} from './eth-git-hint.service';
import {ethGitHintController} from './eth-git-hint.controller';

export const ethGitHintModule = angular
    .module('ethGitHintModule', [])
        .factory('ethGitHintService', ethGitHintService)
        .controller('ethGitHintController', ethGitHintController)
        .component('ethGitHintComponent',{
            controller: 'ethGitHintController'
        })
