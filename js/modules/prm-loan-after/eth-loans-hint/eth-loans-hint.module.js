/**
* @ngdoc module
* @name ethLoansHintModule
*
* @description
* Displays different information for active loans and previous loans
* Hide renew button/link by css
*
*
*/
import {ethLoansHintController} from './eth-loans-hint.controller';
import {ethConfigService} from '../../../services/eth-config.service';
import {ethLoansHintConfig} from './eth-loans-hint.config';

export const ethLoansHintModule = angular
   .module('ethLoansHintModule', [])
       .factory('ethConfigService', ethConfigService)
       .factory('ethLoansHintConfig', ethLoansHintConfig)
       .controller('ethLoansHintController', ethLoansHintController)
       .component('ethLoansHintComponent', {
           bindings: {afterCtrl: '<'},
           controller: 'ethLoansHintController'
       })
