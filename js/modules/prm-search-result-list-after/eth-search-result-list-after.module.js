import {ethPersonModule} from './eth-person/eth-person.module';
import {ethPlaceModule} from './eth-place/eth-place.module';
import {ethExploreModule} from './eth-explore/eth-explore.module';

export const ethSearchResultListAfterModule = angular
    .module('ethSearchResultListAfterModule', [])
        .component('prmSearchResultListAfter',  {
            bindings: {parentCtrl: '<'},
            template: `
            <eth-person-component after-ctrl="$ctrl"></eth-person-component>
            <eth-place-component after-ctrl="$ctrl"></eth-place-component>
            <eth-explore-component after-ctrl="$ctrl"></eth-explore-component>
            `
        });
ethSearchResultListAfterModule.requires.push(ethPersonModule.name);
ethSearchResultListAfterModule.requires.push(ethPlaceModule.name);
ethSearchResultListAfterModule.requires.push(ethExploreModule.name);
          