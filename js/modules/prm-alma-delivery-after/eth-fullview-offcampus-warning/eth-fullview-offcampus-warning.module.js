/**
* @ngdoc module
* @name ethFullviewOffcampusWarningModule
*
* @description
*
* - Offcampus warning at the bottom of the viewit section, if the browser works outside of the defined ip range
* - IP-Ranges Configuration: https://knowledge.exlibrisgroup.com/Alma/Product_Documentation/010Alma_Online_Help_(English)/050Administration/050Configuring_General_Alma_Functions/020Managing_Institutions_and_Libraries#Configuring_IP_Ranges_for_a_Library
* - NOT if source is E-Lending, HSA etc; checks for deliveryCategory = 'Alma-E'
*
* <b>AngularJS Dependencies</b><br>
* Service {@link ETH.ethSessionService}<br>
* Service /js/services {@link ETH.ethConfigService}<br>
* Service {@link ETH.ethFullviewOffcampusWarningConfig}<br>
*
*
* <b>CSS/Image Dependencies</b><br>
* CSS eth-fullview-offcampus-warning.css
*
*/
import {ethConfigService} from '../../../services/eth-config.service';
import {ethSessionService} from '../../../services/eth-session.service';
import {ethFullviewOffcampusWarningConfig} from './eth-fullview-offcampus-warning.config';
import {ethFullviewOffcampusWarningController} from './eth-fullview-offcampus-warning.controller';
import {ethFullviewOffcampusWarningHtml} from './eth-fullview-offcampus-warning.html';

export const ethFullviewOffcampusWarningModule = angular
    .module('ethFullviewOffcampusWarningModule', [])
        .factory('ethConfigService', ethConfigService)
        .factory('ethSessionService', ethSessionService)
        .factory('ethFullviewOffcampusWarningConfig', ethFullviewOffcampusWarningConfig)
        .controller('ethFullviewOffcampusWarningController', ethFullviewOffcampusWarningController)
        .component('ethFullviewOffcampusWarningComponent',{
            bindings: {afterCtrl: '<'},
            controller: 'ethFullviewOffcampusWarningController',
            template: ethFullviewOffcampusWarningHtml
        })
