export class ethSketchfabLinkController {

    // 99117366259905503
    constructor(ethConfigService, ethSketchfabLinkConfig) {
        this.ethConfigService = ethConfigService;
        this.config = ethSketchfabLinkConfig;
    }

    $onInit() {
        try{
            this.parentCtrl = this.afterCtrl.parentCtrl;
            // only in fullview
            if(!this.parentCtrl.isFullView){
                return;
            }
            // only images
            if(!this.parentCtrl.item.pnx.display.type || !this.parentCtrl.item.pnx.display.type[0] || this.parentCtrl.item.pnx.display.type[0] != 'image'){
                return;
            }
            // check for link in lds09
            if(!this.parentCtrl.item.pnx.display.lds09 || !this.parentCtrl.item.pnx.display.type[0] || this.parentCtrl.item.pnx.display.lds09.length === 0){
                return;
            }
            // "https://skfb.ly/oDOLY"
            let filteredLds09 = this.parentCtrl.item.pnx.display.lds09.filter(i=>{
                if(i.indexOf('https://skfb.ly/')>-1){
                    return true;
                }
                return false;
            })
            if(filteredLds09.length === 0){
                return false;
            }
            this.sketchfabLink = filteredLds09[0];
        }
        catch(e){
            console.error("***ETH*** an error occured: ethSketchfabLinkController $onInit\n\n");
            console.error(e.message);
            throw(e);
        }
    }
}

ethSketchfabLinkController.$inject = ['ethConfigService', 'ethSketchfabLinkConfig' ];
