export class ethPersonController {

    constructor( $location, $timeout, $scope, $compile, $element, $anchorScroll, ethConfigService, ethPersonCardService, ethPersonCardConfig, ethPersonService) {
        this.$location = $location;
        this.$timeout = $timeout;
        this.$scope = $scope;
        this.$compile = $compile;
        this.$element = $element;
        this.$anchorScroll = $anchorScroll;
        this.ethConfigService = ethConfigService;
        this.config = ethPersonCardConfig;
        this.ethPersonCardService = ethPersonCardService;
        this.ethPersonService = ethPersonService;
    }


    $onInit() {
        try{
            this.parentCtrl = this.afterCtrl.parentCtrl;
            this.processDoCheck = false;
            if(!this.parentCtrl.searchService || !this.parentCtrl.searchService.searchUtil || !this.parentCtrl.searchService.searchUtil.$stateParams || !this.parentCtrl.searchService.searchUtil.$stateParams.query)return;

            this.isFullView = false;
            if(this.parentCtrl.searchService.searchUtil.$stateParams.tab){
                this.tab = this.parentCtrl.searchService.searchUtil.$stateParams.tab;
            }
            else{
                this.tab = window.appConfig['primo-view']['available-tabs'][0];
            }
            this.isCDI = false;
            if(this.tab === '41SLSP_DN_CI'){
                this.isCDI = true;
            }
            if(this.parentCtrl.searchService.searchUtil.$stateParams['search_scope']){
                this.scope = this.parentCtrl.searchService.searchUtil.$stateParams['search_scope'];
            }
            else{
                this.scope = window.appConfig['primo-view']['scopes'][0]['scope-id'];
            }

            this.prNav = true;
            // this.search: input in searchField
            this.search = '';
            this.person = {};
            this.persons = [];
            if(this.parentCtrl.searchService.searchUtil.$stateParams.mode == "advanced")return;
            this.search = this.parentCtrl.searchService.searchUtil.$stateParams.query.replace('any,contains,','');
            let facet = this.parentCtrl.searchService.searchUtil.$stateParams.facet;
            let mfacet = this.parentCtrl.searchService.searchUtil.$stateParams.mfacet;
            if (this.search.indexOf('[wd/person]') === -1 || facet || mfacet) {
                return;
            }
            // matomo
            if(!window._paq){
                window._paq = [];
            }
            window._paq.push(['setCustomUrl', 'https://eth.swisscovery.slsp.ch/explore/page/person']);
            window._paq.push(['trackPageView']);

            this.processDoCheck = true;
            this.lang = this.ethConfigService.getLanguage();
            this.oldTotalResults = 0;
            let id = this.search.substring(this.search.indexOf(']')+1);

            this.$timeout(() => {
                // hide pfilter
                let pfilter = document.querySelector('prm-resource-type-filter-bar');
                angular.element(pfilter).addClass('eth-hide-pfilter');
            }, 1000);

            this.ethPersonService.getPerson(id, this.lang)
                .then((data) => {
                    try{
                        if(!data)return;
                        this.person = this.ethPersonCardService.processPersonsResponse(data.results, this.ethConfigService, this.config, this.lang);
                        this.person.qid = data.qid[0];
                        if(!this.person.qid){
                            this.processDoCheck = false;
                            this.handleNoWikidataData();
                            /*let noResultContainer = document.querySelector('prm-no-search-result');
                            if(noResultContainer){
                                noResultContainer.style.display = "block";
                            }*/
                            return;
                        }
                        if(this.person.entityfacts && this.person.entityfacts.preferredName){
                            this.person.label = this.person.entityfacts.preferredName;
                        }
                        else if(this.person.wiki && this.person.wiki.label){
                            this.person.label = this.person.wiki.label;
                        }
                        if(this.person.gnd !="" && data.gnd && data.gnd.length > 0){
                            if(data.gnd[0] != ""){
                                this.person.gnd = data.gnd[0];
                            }
                            else if(data.gnd.length > 1){
                                this.person.gnd = data.gnd[1];
                            }
                        }
                        if(data.gnd.length > 0 || this.person.label){
                            this.renderTopLinks();
                        }

                        if(this.person.wiki && this.person.wiki.aVariants && this.person.wiki.aVariants.length > 0){
                            this.person.variantQuery = this.buildPrimoVariantQueryValue(this.person);
                        }
                        if(this.person.wiki && this.person.wiki.birth){
                            this.person.yearOfBirth = this.person.wiki.birth.substring(0,this.person.wiki.birth.indexOf('-'))
                        }
                        else if(this.person.entityfacts && this.person.entityfacts.dateOfBirth) {
                            this.person.yearOfBirth = this.person.entityfacts.dateOfBirth.substring(this.person.entityfacts.dateOfBirth.lastIndexOf(' ') + 1)
                        }

                        this.enrichPRNavigation();
                        this.persons = [this.person];
                        if(this.person.gnd){
                            //this.parentCtrl.searchService.searchFieldsService.mainSearch = this.person.gnd;
                            document.getElementById('searchBar').value = this.person.gnd;
                            this.parentCtrl.searchService.searchUtil.$stateParams.query = 'lds03,contains,' + this.person.gnd;
                            this.$timeout(() => {
                                this.parentCtrl.searchService.search({query: 'lds03,contains,' + this.person.gnd, tab:this.tab, scope:this.scope},true);
                                document.getElementsByClassName('eth-person__searchprecision__option-gnd')[0].style.display = 'none';
                                angular.element(document.querySelector('prm-facet')).addClass('eth-facet-personpage');
                            }, 600);
                        }
                        else if(this.person.label && (this.person.yearOfBirth || this.person.gnd)){
                            let query = this.person.label;
                            if(this.person.yearOfBirth &&  this.person.gnd){
                                query = query + ' AND (' + this.person.yearOfBirth + ' OR ' + this.person.gnd + ')';
                            }
                            else if (this.person.yearOfBirth) {
                                query = query + ' AND ' + this.person.yearOfBirth;
                            }
                            document.getElementById('searchBar').value = query;
                            this.parentCtrl.searchService.searchUtil.$stateParams.query = 'any,contains,' + query;
                            this.$timeout(() => {
                                this.parentCtrl.searchService.search({query: 'any,contains,' + query, tab: this.tab, scope:this.scope},true);
                                document.getElementsByClassName('eth-person__searchprecision__option-birthyear')[0].style.display = 'none';
                                angular.element(document.querySelector('prm-facet')).addClass('eth-facet-personpage');
                            }, 600);
                        }
                        else if(this.person.label){
                            document.getElementById('searchBar').value = this.person.label;
                            this.parentCtrl.searchService.searchUtil.$stateParams.query = 'any,contains,' + this.person.label;
                            this.$timeout(() => {
                                this.parentCtrl.searchService.search({query: 'any,contains,' + this.person.label, tab: this.tab, scope:this.scope},true);
                                document.getElementsByClassName('eth-person__searchprecision__option-label')[0].style.display = 'none';
                                angular.element(document.querySelector('prm-facet')).addClass('eth-facet-personpage');                                
                            }, 600);
                        }
                    }
                    catch(e){
                        console.error("***ETH*** an error occured: ethPersonController.getPerson(): \n\n");
                        console.error(e.message);
                    }
                });
        }
        catch(e){
            console.error("***ETH*** an error occured: ethPersonController.onInit()\n\n");
            console.error(e.message);
            throw(e);
        }
    }

    renderTopLinks(){
        let resultList = angular.element(this.$element[0].parentElement.parentElement.parentElement);
        if(!resultList){
            return;
        }
        let html = `
            <a name="wikidata-top-anchor"></a>
            <div class="eth-person__top">
                <h2>${this.person.label}</h2>
                <div>
                    <md-button class="md-primary" ng-click="$ctrl.scrollTo('eth-person__searchprecision-anchor')">${this.ethConfigService.getLabel(this.config, 'precision')}</md-button>
                    <md-button class="md-primary" ng-click="$ctrl.scrollTo('eth-person-anchor')">${this.ethConfigService.getLabel(this.config, 'moreInformations')}</md-button>
                </div>
            </div>
        `;
        resultList.prepend(this.$compile(html)(this.$scope));
        return;
    }

    // enrich precision and recall navigation
    enrichPRNavigation(){
        let resultList = angular.element(this.$element[0].parentElement.parentElement);
        if(!resultList || this.isCDI){
            return;
        }
        // determine number of results of search variants
        // search for label
        this.ethPersonService.searchPrimo('any,contains,' + this.person.label, this.tab, this.scope, this.lang)
            .then((data) => {
                let total = data.info.totalResultsLocal;
                if(this.isCDI){
                    total = data.info.total;
                }
                let resultName = "";
                resultName = this.ethConfigService.getLabel(this.config, 'results');
                if(total == 1){
                    resultName = this.ethConfigService.getLabel(this.config, 'result')
                }
                if(document.getElementsByClassName('eth-person__searchprecision__label1').length > 0){
                    document.getElementsByClassName('eth-person__searchprecision__label1')[0].innerHTML = total + ' ' + resultName;
                }
                if(document.getElementsByClassName('eth-person__searchprecision__label2').length > 0){
                    document.getElementsByClassName('eth-person__searchprecision__label2')[0].innerHTML = total + ' ' + resultName;
                }
            })
        // search for gnd
        if(this.person.gnd){
            this.ethPersonService.searchPrimo('lds03,contains,' + this.person.gnd, this.tab, this.scope, this.lang)
                .then((data) => {
                    let total = data.info.totalResultsLocal;
                    let resultName = "";
                    resultName = this.ethConfigService.getLabel(this.config, 'results');
                    if(total == 1){
                        resultName = this.ethConfigService.getLabel(this.config, 'result')
                    }
                    if(document.getElementsByClassName('eth-person__searchprecision__gnd1').length > 0){
                        document.getElementsByClassName('eth-person__searchprecision__gnd1')[0].innerHTML = total + ' ' + resultName;
                    }
                    if(document.getElementsByClassName('eth-person__searchprecision__gnd2').length > 0){
                        document.getElementsByClassName('eth-person__searchprecision__gnd2')[0].innerHTML = total + ' ' + resultName;
                    }
                })
        }
        // search for name variants / aliases
        this.ethPersonService.searchPrimo('any,contains,' + this.person.variantQuery, this.tab, this.scope, this.lang)
            .then((data) => {
                let total = data.info.totalResultsLocal;
                let resultName = "";
                resultName = this.ethConfigService.getLabel(this.config, 'results');
                if(total == 1){
                    resultName = this.ethConfigService.getLabel(this.config, 'result')
                }
                if(document.getElementsByClassName('eth-person__searchprecision__variants').length > 0){
                    document.getElementsByClassName('eth-person__searchprecision__variants')[0].innerHTML = total + ' ' + resultName;
                }
            })
        // search for name AND (GND OR BIRTHYEAR)
        let primoServiceQuery = this.person.label;
        if(this.person.yearOfBirth &&  this.person.gnd){
            primoServiceQuery = primoServiceQuery + ' AND (' + this.person.yearOfBirth + ' OR ' + this.person.gnd + ')';
        }
        else if (this.person.yearOfBirth) {
            primoServiceQuery = primoServiceQuery + ' ' + this.person.yearOfBirth;
        }
        else if (this.person.gnd) {
            primoServiceQuery = primoServiceQuery + ' ' + this.person.gnd;
        }
        this.ethPersonService.searchPrimo('any,contains,' + primoServiceQuery, this.tab, this.scope, this.lang)
            .then((data) => {
                let total = data.info.totalResultsLocal;
                let resultName = "";
                resultName = this.ethConfigService.getLabel(this.config, 'results');
                if(total == 1){
                    resultName = this.ethConfigService.getLabel(this.config, 'result')
                }
                if(document.getElementsByClassName('eth-person__searchprecision__birth1').length > 0){
                    document.getElementsByClassName('eth-person__searchprecision__birth1')[0].innerHTML = total + ' ' + resultName;
                }
                if(document.getElementsByClassName('eth-person__searchprecision__birth2').length > 0){
                    document.getElementsByClassName('eth-person__searchprecision__birth2')[0].innerHTML = total + ' ' + resultName;
                }
            })
    }

    scrollTo(anchorName){
        //this.$location.hash(anchorName);
        this.$anchorScroll(anchorName);
    }

    handleNoWikidataData(){
        let query = this.search.replace('[wd/person]','');
        this.parentCtrl.searchService.searchFieldsService.mainSearch = query;
        document.getElementById('searchBar').value = query;
        this.parentCtrl.searchService.searchUtil.$stateParams.query = 'any,contains,' + query;
        this.parentCtrl.searchService.cheetah.$state.params.query = 'any,contains,' + query;
        this.parentCtrl.searchService.cheetah.$location.$$search.query = 'any,contains,' + query;
        this.processDoCheck = false;
        this.$timeout(() => {
            this.parentCtrl.searchService.search({query: 'any,contains,' + query, tab:this.tab, scope:this.scope},true);
        }, 300);
    }

    searchPersonByLabel(person){
        let query = person.label;
        //this.parentCtrl.searchService.searchFieldsService.mainSearch = query;
        document.getElementById('searchBar').value = query;
        this.parentCtrl.searchService.searchUtil.$stateParams.query = 'any,contains,' + query;
        this.parentCtrl.searchService.cheetah.$state.params.query = 'any,contains,' + query;
        this.parentCtrl.searchService.cheetah.$location.$$search.query = 'any,contains,' + query;
        this.$timeout(() => {
            this.parentCtrl.searchService.search({query: 'any,contains,' + query, tab:this.tab, scope:this.scope},true);
            angular.element(document.querySelector('prm-facet')).addClass('eth-facet-personpage');   
        }, 300);

        let buttons = document.getElementsByClassName('eth-person__searchprecision-option');
        for(let i = 0; i < buttons.length; i++)
        {
           buttons[i].removeAttribute('active');
        }
        if(document.getElementsByClassName('eth-person__searchprecision__intro').length > 0)document.getElementsByClassName('eth-person__searchprecision__intro')[0].style.display = 'none';
        if(document.getElementsByClassName('eth-person__searchprecision__option-label').length > 0)document.getElementsByClassName('eth-person__searchprecision__option-label')[0].style.display = 'block';
        if(document.getElementsByClassName('eth-person__searchprecision__option-gnd').length > 0)document.getElementsByClassName('eth-person__searchprecision__option-gnd')[0].style.display = 'block';
        if(document.getElementsByClassName('eth-person__searchprecision__option-birthyear').length > 0)document.getElementsByClassName('eth-person__searchprecision__option-birthyear')[0].style.display = 'block';
        document.getElementsByClassName('eth-person__searchprecision-label')[0].setAttribute('active','true');
        this.scrollTo('eth-top-anchor');
    }

    searchPersonByGndId(gndid, qid){
        let query = gndid;
        //this.parentCtrl.searchService.searchFieldsService.mainSearch = query;
        document.getElementById('searchBar').value = query;
        this.parentCtrl.searchService.searchUtil.$stateParams.query = 'lds03,contains,' + query;
        this.parentCtrl.searchService.$stateParams.query = 'lds03,contains,' + query;
        this.parentCtrl.searchService.cheetah.$state.params.query = 'lds03,contains,' + query;
        this.parentCtrl.searchService.cheetah.$location.$$search.query = 'lds03,contains,' + query;
        this.$timeout(() => {
            this.parentCtrl.searchService.search({query: 'lds03,contains,' + query, tab:this.tab, scope:this.scope},true);
            angular.element(document.querySelector('prm-facet')).addClass('eth-facet-personpage');   
        }, 300);
        let buttons = document.getElementsByClassName('eth-person__searchprecision-option');;
        for(let i = 0; i < buttons.length; i++)
        {
           buttons[i].removeAttribute('active');
        }
        if(document.getElementsByClassName('eth-person__searchprecision__intro').length > 0)document.getElementsByClassName('eth-person__searchprecision__intro')[0].style.display = 'none';
        if(document.getElementsByClassName('eth-person__searchprecision__option-label').length > 0)document.getElementsByClassName('eth-person__searchprecision__option-label')[0].style.display = 'block';
        if(document.getElementsByClassName('eth-person__searchprecision__option-gnd').length > 0)document.getElementsByClassName('eth-person__searchprecision__option-gnd')[0].style.display = 'block';
        if(document.getElementsByClassName('eth-person__searchprecision__option-birthyear').length > 0)document.getElementsByClassName('eth-person__searchprecision__option-birthyear')[0].style.display = 'block';
        document.getElementsByClassName('eth-person__searchprecision-gnd')[0].setAttribute('active','true');
        this.scrollTo('eth-top-anchor');
    }

    searchPersonByVariantQuery(person){
        let query = person.variantQuery;
        if(!person.variantQuery){
            query = person.label;
        }
        //this.parentCtrl.searchService.searchFieldsService.mainSearch = query;
        document.getElementById('searchBar').value = query
        this.parentCtrl.searchService.searchUtil.$stateParams.query = 'any,contains,' + query;
        this.parentCtrl.searchService.$stateParams.query = 'any,contains,' + query;
        this.parentCtrl.searchService.cheetah.$state.params.query = 'any,contains,' + query;
        this.parentCtrl.searchService.cheetah.$location.$$search.query = 'any,contains,' + query;
        this.$timeout(() => {
            this.parentCtrl.searchService.search({query: 'any,contains,' + query, tab:this.tab, scope:this.scope},true);
            angular.element(document.querySelector('prm-facet')).addClass('eth-facet-personpage');   
        }, 300);
        let buttons = document.getElementsByClassName('eth-person__searchprecision-option');;
        for(let i = 0; i < buttons.length; i++)
        {
           buttons[i].removeAttribute('active');
        }
        if(document.getElementsByClassName('eth-person__searchprecision__intro').length > 0)document.getElementsByClassName('eth-person__searchprecision__intro')[0].style.display = 'none';
        if(document.getElementsByClassName('eth-person__searchprecision__option-label').length > 0)document.getElementsByClassName('eth-person__searchprecision__option-label')[0].style.display = 'block';
        if(document.getElementsByClassName('eth-person__searchprecision__option-gnd').length > 0)document.getElementsByClassName('eth-person__searchprecision__option-gnd')[0].style.display = 'block';
        if(document.getElementsByClassName('eth-person__searchprecision__option-birthyear').length > 0)document.getElementsByClassName('eth-person__searchprecision__option-birthyear')[0].style.display = 'block';
        document.getElementsByClassName('eth-person__searchprecision-variants')[0].setAttribute('active','true');
        this.scrollTo('eth-top-anchor');
    }

    searchBestPossibleForPerson(person){
        let query = person.label;
        if(person.yearOfBirth &&  person.gnd){
            query = query + ' AND (' + person.yearOfBirth + ' OR ' + person.gnd + ')';
        }
        else if (person.yearOfBirth) {
            query = query + ' AND ' + person.yearOfBirth;
        }
        else if (person.gnd) {
            query = query + ' AND ' + person.gnd;
        }
        //this.parentCtrl.searchService.searchFieldsService.mainSearch = query;
        document.getElementById('searchBar').value = query
        this.parentCtrl.searchService.searchUtil.$stateParams.query = 'any,contains,' + query;
        this.parentCtrl.searchService.$stateParams.query = 'any,contains,' + query;
        this.parentCtrl.searchService.cheetah.$state.params.query = 'any,contains,' + query;
        this.parentCtrl.searchService.cheetah.$location.$$search.query = 'any,contains,' + query;

        this.$timeout(() => {
            this.parentCtrl.searchService.search({query: 'any,contains,' + query, tab: this.tab, scope:this.scope},true);
            angular.element(document.querySelector('prm-facet')).addClass('eth-facet-personpage');   
        }, 300);
        let buttons = document.getElementsByClassName('eth-person__searchprecision-option');;
        for(let i = 0; i < buttons.length; i++)
        {
           buttons[i].removeAttribute('active');
        }
        if(document.getElementsByClassName('eth-person__searchprecision__intro').length > 0)document.getElementsByClassName('eth-person__searchprecision__intro')[0].style.display = 'none';
        if(document.getElementsByClassName('eth-person__searchprecision__option-label').length > 0)document.getElementsByClassName('eth-person__searchprecision__option-label')[0].style.display = 'block';
        if(document.getElementsByClassName('eth-person__searchprecision__option-gnd').length > 0)document.getElementsByClassName('eth-person__searchprecision__option-gnd')[0].style.display = 'block';
        if(document.getElementsByClassName('eth-person__searchprecision__option-birthyear').length > 0)document.getElementsByClassName('eth-person__searchprecision__option-birthyear')[0].style.display = 'block';
        document.getElementsByClassName('eth-person__searchprecision-birthyear')[0].setAttribute('active','true');
        this.scrollTo('eth-top-anchor');
    }

    buildPrimoVariantQueryValue(person){
        // in primo 7 rows and max 30 boolean operators
        /*
        The system will display a message and provide suggestions when the following limits are exceeded:
        The query contains more than 30 boolean operators.
        The query contains more than 8 question marks.
        The query contains more than 8 asterisks and the word length is greater than 2 (such as abb* or ab*c).
        The query contains more than 4 asterisks and the word length is less than 3 (such as ab*).
        The entire query consists of a single letter and an asterisk (such as a*).
        */
        let items = person.wiki.aVariants;
        let query = person.wiki.label + ' OR ';
        let maxItems = 20;
        let maxLength = 1000;
        items.forEach( (e, i) => {
            if (i < maxItems) {
                if((query.length + e.length) < maxLength)
                {
                    query += '"' + e + '"';
                    if(i < items.length-1 && i < (maxItems-1)){
                        query += ' OR ';
                    }
                }
            }
        });
        return query;
    }

    $doCheck() {
        try{
            if (!this.processDoCheck || this.search.indexOf('[wd/person]') === -1) {
                return;
            }
            let noResultContainer = document.querySelector('prm-no-search-result');
            if(noResultContainer){
                noResultContainer.style.display = "none";
            }
            if (!this.parentCtrl.searchInfo || !this.parentCtrl.searchInfo.total){
                return;
            }
            let domResultsCount = document.querySelector("[data-qa='change_result_size_per_page'] md-select-value span:first-child");
            if(domResultsCount){
                this.oldTotalResults = this.parentCtrl.searchInfo.total;
                domResultsCount.textContent = this.parentCtrl.searchInfo.total + ' ' + this.ethConfigService.getLabel(this.config, 'results');
            }
        }
        catch(e){
            console.error("***ETH*** ethPersonController.$doCheck:");
            console.error(e.message);
        }

    }


}

ethPersonController.$inject = ['$location', '$timeout', '$scope', '$compile', '$element', '$anchorScroll', 'ethConfigService', 'ethPersonCardService', 'ethPersonCardConfig', 'ethPersonService' ];

