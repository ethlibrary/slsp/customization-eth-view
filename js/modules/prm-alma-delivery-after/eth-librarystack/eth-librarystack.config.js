export const ethLibrarystackConfig = function(){
    return {
        label: {
            hint1:{
                de: 'Password protected access. Restricted to members of ETH Zurich only.',
                en: 'Password protected access. Restricted to members of ETH Zurich only.'
            },
            hint2:{
                de: 'You need to register with your ETH Zurich e-mail address "Sign In". Access to this source is restricted on the domain ethz.ch because of license restrictions.',
                en: 'You need to register with your ETH Zurich e-mail address "Sign In". Access to this source is restricted on the domain ethz.ch because of license restrictions.'
            }
        }
    }
}

