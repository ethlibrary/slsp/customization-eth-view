export const ethLibkeyConfig = function(){
    return {
        label: {
            pdfText: {
                de: 'Download PDF',
                en: 'Download PDF'
            },
            htmlText: {
                de: 'HTML',
                en: 'HTML'
            }
        }
    }
}
