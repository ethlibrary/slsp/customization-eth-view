export const ethRequestHintsConfig = function(){
    return {
        label: {
            pickup1: {
                de: 'Anderer Abholort gewünscht?',
                en: 'Other pickup location desired?'
            },
            pickup2: {
                de: 'Bestellen Sie unter ',
                en: 'Order under '
            },
            linktext: {
                de: 'ETH-Kurier und weitere Lieferoptionen',
                en: 'ETH courier and further delivery options'
            },
            pickup3: {
                de: ' an Ihren bevorzugten Abholort.',
                en: ' to your preferred pickup location.'
            },
            feesH4OM: {
                de: 'Gebühren:',
                en: 'Fees:'
            },
            feesH4ETH: {
                de: 'Gebühren für ETH-Mitarbeitende:',
                en: 'Fees for ETH employees:'
            },
            feesH4Student: {
                de: 'Gebühren für ETH-Studierende:',
                en: 'Fees for ETH students:'
            },
            feesH4External: {
                de: 'Gebühren für Nicht-ETH-Angehörige:',
                en: 'Fees for patrons without ETH affiliation:'
            },
            feesFreeOfCharge: {
                de: 'kostenlos',
                en: 'free of charge'
            },
            feesFreeOfChargeDigi: {
                de: 'Kostenlos',
                en: 'Free of charge'
            },
            feesHoldOM1: {
                de: 'Abholung vor Ort: ',
                en: 'Pickup on site: '
            },
            feesHoldOM2: {
                de: 'Postversand: CHF 12.00',
                en: 'Postal delivery: CHF 12.00'
            },
            feesHoldETH1: {
                de: 'Abholung vor Ort: ',
                en: 'Pickup on site: '
            },
            feesHoldETH2: {
                de: 'Postversand: ',
                en: 'Postal delivery: '
            },
            feesHoldStudent1: {
                de: 'Abholung vor Ort: ',
                en: 'Pickup on site: '
            },
            feesHoldStudent2: {
                de: 'Postversand: CHF 12.00',
                en: 'Postal delivery: CHF 12.00'
            },
            feesHoldExternal1: {
                de: 'Abholung vor Ort: ',
                en: 'Pickup on site: '
            },
            feesDigiOM1: {
                de: 'Normale Gebühr: CHF 5.00',
                en: 'Normal fee: CHF 5.00'
            },
            feesDigiOM2: {
                de: 'Reduzierte Gebühr: CHF 2.50 (Studierende)',
                en: 'Reduced fee: CHF 2.50 (students)'
            },
            feesDigiOM3: {
                de: 'Erhöhte Gebühr: CHF 25.00 (kommerzielle Kundinnen und Kunden)',
                en: 'Augmented fee: CHF 25.00 (corporate clients)'
            },
            feesDigiStudent1: {
                de: 'CHF 2.50',
                en: 'CHF 2.50'
            },
            feesDigiExternal1: {
                de: 'Studierende: CHF 2.50',
                en: 'Students: CHF 2.50'
            },
            feesDigiExternal2: {
                de: 'Privatpersonen: CHF 5.00',
                en: 'Private individuals: CHF 5.00'
            },
            feesDigiExternal3: {
                de: 'Kommerzielle Kundinnen und Kunden: CHF 25.00',
                en: 'Corporate clients: CHF 25.00'
            },
            feesFooter: {
                de: 'Alle Gebühren im Überblick',
                en: 'Overview of all fees'
            }
        },
        url:{
            feesFooter: {
                de: 'https://documentation.library.ethz.ch/x/DADTAw',
                en: 'https://documentation.library.ethz.ch/x/IADTAw'
            }
        }
    }
}
