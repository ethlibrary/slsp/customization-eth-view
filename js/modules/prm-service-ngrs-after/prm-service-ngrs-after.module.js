import {bcuRapidoNoFurtherOptionsModule} from './bcu-rapido-no-further-options/bcu-rapido-no-further-options.module';

export const prmServiceNgrsAfterModule = angular
    .module('prmServiceNgrsAfterModule', [])
        .component('prmServiceNgrsAfter',  {
            bindings: {parentCtrl: '<'},
            template: `<bcu-rapido-no-further-options after-ctrl="$ctrl"></bcu-rapido-no-further-options><slsp-service-ngrs-after parent-ctrl="$parent.$ctrl"></<slsp-service-ngrs-after>`
        });

prmServiceNgrsAfterModule.requires.push(bcuRapidoNoFurtherOptionsModule.name);
