/**
* @ngdoc module
* @name ethProvenienzModule
*
* @description
*
* Only for (online) e-rara-resources: get possible proveniences from "E-Pics Provenienz alter Drucke" and render a link to e-pics
*
*
* <b>AngularJS Dependencies</b><br>
* Service {@link ETH.ethProvenienzService}<br>
* Service /js/services {@link ETH.ethConfigService}<br>
* Service {@link ETH.ethProvenienzConfig}<br>
*
*
* <b>CSS/Image Dependencies</b><br>
* CSS eth-provenienz.css
*
*/
import {ethConfigService} from '../../../services/eth-config.service';
import {ethProvenienzConfig} from './eth-provenienz.config';
import {ethSessionService} from '../../../services/eth-session.service';
import {ethProvenienzService} from './eth-provenienz.service';
import {ethProvenienzController} from './eth-provenienz.controller';
import {ethProvenienzHtml} from './eth-provenienz.html';


export const ethProvenienzModule = angular
    .module('ethProvenienzModule', [])
        .factory('ethConfigService', ethConfigService)
        .factory('ethProvenienzConfig', ethProvenienzConfig)
        .factory('ethSessionService', ethSessionService)
        .factory('ethProvenienzService', ethProvenienzService)
        .controller('ethProvenienzController', ethProvenienzController)
        .component('ethProvenienzComponent',  {
            bindings: {afterCtrl: '<'},
            controller: 'ethProvenienzController',
            template: ethProvenienzHtml
        })
