/**
* @ngdoc service
* @name ethGitHintService
*
* @description
* Service to get test for an error message from a git file
*
* <b>Used by</b><br>
* Module {@link ETH.ethGitHintModule}<br>
*
*
- */
export const ethGitHintService = ['$http', '$sce', function($http, $sce){

    function getMessage(){
        let url = "https://eth-library.github.io/snippets/primo/banner.json";
        $sce.trustAsResourceUrl(url);

        return $http.get(url,{headers:{ "X-From-ExL-API-Gateway": undefined }})
            .then(
                function(response){
                    return response.data;
                },
                function(httpError){
                    if (httpError.status === 404)return null;
                    let error = "***ETH*** an error occured: ethGitHintService.getData error callback: " + httpError;
                    if (httpError.data && httpError.data.errorMessage) {
                        error += ' - ' + httpError.data.errorMessage;
                    }
                    console.error(error);
                    return null;
                }
            );
    }
    return {
        getMessage: getMessage
    }
}]
