import {ethAltmetricModule} from './eth-altmetric/eth-altmetric.module';
import {ethTocLinkModule} from './eth-toc-link/eth-toc-link.module';
import {tmiComposeNbModule} from './tmi-compose-nb/tmi-compose-nb.module';
import {ethSketchfabLinkModule} from './eth-sketchfab-link/eth-sketchfab-link.module';

export const ethBriefResultAfterModule = angular
    .module('ethBriefResultAfterModule', [])
        .component('prmBriefResultAfter',  {
            bindings: {parentCtrl: '<'},
            template: `<eth-altmetric-component after-ctrl="$ctrl"></eth-altmetric-component>
                        <eth-toc-link-component after-ctrl="$ctrl"></eth-toc-link-component>
                        <eth-sketchfab-link-component after-ctrl="$ctrl"></eth-sketchfab-link-component>
                        <tmi-compose-nb-component after-ctrl="$ctrl"></tmi-compose-nb-component>
                       `
        });

ethBriefResultAfterModule.requires.push(ethAltmetricModule.name);
ethBriefResultAfterModule.requires.push(ethTocLinkModule.name);
ethBriefResultAfterModule.requires.push(ethSketchfabLinkModule.name);
ethBriefResultAfterModule.requires.push(tmiComposeNbModule.name);


