export const ethOnlineFeedbackMailConfig = function(){
    return {
        label: {
            linktext: {
                de: 'Zugriffsproblem melden',
                en: 'Report access problem'
            },
            text: {
                de: 'Bitte beschreiben Sie kurz das Zugriffsproblem:',
                en: 'Please describe the access problem briefly:'
            }
        },
        url:{
            mailto: {
                de: 'almakb@library.ethz.ch',
                en: 'almakb@library.ethz.ch'
            }
        }
    }
}
