/**
* @ngdoc service
* @name ethPlacePageLinkService
*
* @description
*
* getPlaces(): get informations about geo topics(651 place) from geodata graph 
*
*
* <b>Used by</b><br>
* Module {@link ETH.ethPlacePageLinkModule}<br>
*
*
 */
export const ethPlacePageLinkService = ['$http', '$sce', function($http, $sce){

    function getPlaces(aGndId){
        let baseurl = 'https://daas.library.ethz.ch/rib/v3/graph/places-by-gnd-list';
        let url = baseurl + '?gnd=' + aGndId.join(',');
        $sce.trustAsResourceUrl(url);
        return $http.get(url)
            .then(
                function(response){
                    return response.data;
                },
                function(httpError){
                    if (httpError.status === 404 || httpError.status === 500 || httpError.statusText === 'Bad Request')return null;
                    let error = "***ETH*** an error occured: ethPlacePageLinkService.getPlaces():" + httpError.status;
                    if (httpError.data && httpError.data.errorMessage) {
                        error += ' - ' + httpError.data.errorMessage;
                    }
                    console.error(error);
                    return null;
                }
            )
    }

    return {
        getPlaces: getPlaces
    };

}]
