export class ethLibkeyController {

    constructor( ethLibkeyService, ethSessionService, ethConfigService, ethLibkeyConfig, $scope, $compile ) {
        this.ethLibkeyService = ethLibkeyService;
        this.ethSessionService = ethSessionService;
        this.ethConfigService = ethConfigService;
        this.config = ethLibkeyConfig;
        this.$scope = $scope;
        this.$compile = $compile;
    }
 
    $onInit() {
        try{
            this.pdfLink = '';
            this.parentCtrl = this.afterCtrl.parentCtrl;

            if (!this.parentCtrl.result || !this.parentCtrl.result.pnx) {
                console.error("***ETH*** ethLibkeyController.$onInit: result or result.pnx not available");
                return;
            }

            // check type = article
            if(!this.parentCtrl.result.pnx.display.type || !this.parentCtrl.result.pnx.display.type.length > 0 || this.parentCtrl.result.pnx.display.type[0] !== 'article'){
                return;
            }

            // check DOI
            if (!this.parentCtrl.result.pnx.addata || !this.parentCtrl.result.pnx.addata.doi || !this.parentCtrl.result.pnx.addata.doi.length > 0) {
                return;
            }

            let vid = window.appConfig.vid.replace(':','-');
            
            // {"fulltext":"https://libkey.io/libraries/1165/articles/32655083/full-text-file?utm_source=api_65","fromLibkey":true,"fromUnpaywall":false,"type":"PDF"}
            this.ethLibkeyService.getArticle( this.parentCtrl.result.pnx.addata.doi[0], this.ethSessionService.isOnCampus() )
                .then((data) => {
                    try{
                        if(!data){return}
                        if(data.fulltext && data.fulltext !== '' ){
                            this.link = data.fulltext;
                            if(data.type === 'PDF'){
                                this.linkText = this.ethConfigService.getLabel(this.config, 'pdfText');
                                this.isPDF = true;
                            }
                            else{
                                this.linkText = this.ethConfigService.getLabel(this.config, 'htmlText');                                
                                this.isPDF = false;
                            }
                        }
                        else{
                            return;
                        }
                        let html = `
                            <div class="eth-libkey">
                            <a class="neutralized-button arrow-link-button md-button md-primoExplore-theme md-ink-ripple" ng-click="$event.stopPropagation();"
                                ng-href="{{$ctrl.link}}" target="_blank" rel="noopener">
                                <img ng-if="$ctrl.isPDF" alt="" class="eth-libkey__pdf-icon" ng-src="/discovery/custom/${vid}/img/pdf-download-icon.svg">
                                <span class="button-content">
                                    <span class="availability-status fulltext">{{$ctrl.linkText}}</span>
                                    <prm-icon external-link icon-type="svg" svg-icon-set="primo-ui" icon-definition="open-in-new"></prm-icon>
                                </span>
                                <prm-icon link-arrow external-link icon-type="svg" svg-icon-set="primo-ui" icon-definition="chevron-right"></prm-icon>
                            </a>
                            </div>
                        `;
                        let availabilityLine = this.parentCtrl.$element[0].querySelector(".layout-align-start-start");
                        if(availabilityLine) {
                            angular.element(availabilityLine).prepend(this.$compile(html)(this.$scope));
                        }
                    }
                    catch(e){
                        console.error("***ETH*** an error occured: ethLibkeyController getArticle(): \n\n");
                        console.error(e.message);
                    }
                });
        }
        catch(e){
            console.error("***ETH*** an error occured: ethLibkeyController\n\n");
            console.error(e.message);
        }
    }
}

ethLibkeyController.$inject = ['ethLibkeyService', 'ethSessionService', 'ethConfigService', 'ethLibkeyConfig', '$scope', '$compile' ];
