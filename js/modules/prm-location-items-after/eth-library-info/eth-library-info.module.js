/**
* @ngdoc module
* @name ethLibraryInfoModule
*
* @description
* - renders a link to a cms page
* - renders a hint how to get to it 
*
*
* <b>AngularJS Dependencies</b><br>
* Service /js/services {@link ETH.ethConfigService}<br>
* Service {@link ETH.ethLibraryInfoConfig}<br>
*
*
* <b>CSS/Image Dependencies</b><br>
* CSS eth-library-info.css
*
*
*/
import {ethConfigService} from '../../../services/eth-config.service';
import {ethLibraryInfoConfig} from './eth-library-info.config';
import {ethLibraryInfoController} from './eth-library-info.controller';
import {ethLibraryInfoHtml} from './eth-library-info.html';

export const ethLibraryInfoModule = angular
    .module('ethLibraryInfoModule', [])
        .factory('ethConfigService', ethConfigService)
        .factory('ethLibraryInfoConfig', ethLibraryInfoConfig)
        .controller('ethLibraryInfoController', ethLibraryInfoController)
        .component('ethLibraryInfoComponent', {
            bindings: {afterCtrl: '<'},
            controller: 'ethLibraryInfoController',
            template: ethLibraryInfoHtml
        })
