export const ethVideoLinkConfig = function(){
    return {
        label: {
            linktext:{
                de: 'Video im Videoportal suchen',
                en: 'Search video in Videoportal'
            }
        }
    }
}
