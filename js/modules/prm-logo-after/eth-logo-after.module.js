import {ethSwisscoveryModule} from './eth-swisscovery/eth-swisscovery.module';

export const ethLogoAfterModule = angular
    .module('ethLogoAfterModule', [])
        .component('prmLogoAfter',  {
            template: `<eth-swisscovery-component after-ctrl="$ctrl"></eth-swisscovery-component>`
        });

ethLogoAfterModule.requires.push(ethSwisscoveryModule.name);
