export class ethIdpWarningController {

    constructor($scope, $compile, ethConfigService, ethIdpWarningConfig, ethSessionService) {
        this.$scope = $scope;
        this.$compile = $compile;
        this.ethConfigService = ethConfigService;
        this.config = ethIdpWarningConfig;
        this.ethSessionService = ethSessionService;
    }

    $onInit() {
        try{
            this.parentCtrl = this.afterCtrl.parentCtrl;
            this.doCheck = false;
            this.tabsWrapper = document.querySelector("md-tabs-wrapper");
            let token = this.ethSessionService.getDecodedToken();
            // Switch Login
            if(token.authenticationProfile != 'Alma'){
                this.doCheck = true;
                this.isETHMember = false;
                if (token.userGroup === 'ETH_Member' || token.userGroup === 'ETH_E06_GESS-Member' || token.userGroup === 'ETH_E72_INFK-Member' || token.userGroup === 'ETH_E64_MATH-Member' || token.userGroup ===  'eth_students' || token.userGroup ===  'ETH_Student') {
                    this.isETHMember = true;
                }
            }
        }
        catch(e){
            console.error("***ETH*** an error occured: ethIdpWarningController\n\n");
            console.error(e.message);
        }
    }

    $doCheck() {
        try{
            if(!this.doCheck)return;
            if(!this.parentCtrl.personalInfoService.personalInfo || !this.parentCtrl.personalInfoService.personalInfo.email || !this.parentCtrl.$element[0].querySelector("[translate='nui.details.patronstatus.group']"))return;
            this.email = this.parentCtrl.personalInfoService.personalInfo.email;
            if((this.email && this.email.indexOf('ethz.ch') > -1 && this.email.indexOf('retired.ethz.ch') === -1) && !this.isETHMember){
                let newHtml = `
                    <div class="eth-idp-warning">
                        <div>
                            ${this.ethConfigService.getLabel(this.config, 'text1')}
                            <a target="_blank" rel="noopener" href="${this.ethConfigService.getUrl(this.config, 'password')}">
                                ${this.ethConfigService.getLabel(this.config, 'text2')}
                                <prm-icon external-link icon-type="svg" svg-icon-set="primo-ui" icon-definition="open-in-new"></prm-icon>
                            </a>
                            ${this.ethConfigService.getLabel(this.config, 'text3')}
                        </div>
                        <div>
                            <a target="_blank" rel="noopener" href="${this.ethConfigService.getUrl(this.config, 'help')}">${this.ethConfigService.getLabel(this.config, 'text4')}
                                <prm-icon external-link icon-type="svg" svg-icon-set="primo-ui" icon-definition="open-in-new"></prm-icon>
                                <prm-icon link-arrow external-link icon-type="svg" svg-icon-set="primo-ui" icon-definition="chevron-right"></prm-icon>
                            </a>
                        </div>
                    </div>
                `;
                angular.element(this.tabsWrapper).prepend(this.$compile(newHtml)(this.$scope));
            }
            this.doCheck = false;
        }
        catch(e){
            console.error("***ETH*** ethIdpWarningController.$doCheck:");
            console.error(e.message);
        }
    }

}

ethIdpWarningController.$inject = ['$scope', '$compile', 'ethConfigService', 'ethIdpWarningConfig', 'ethSessionService'];
