import {ethAlternativeSearchModule} from './eth-alternative-search/eth-alternative-search.module';

export const ethFacetExactAfterModule = angular
    .module('ethFacetExactAfterModule', [])
        .component('prmFacetExactAfter',  {
            bindings: {parentCtrl: '<'},
            template: `<eth-alternative-search-component after-ctrl="$ctrl"></eth-alternative-search-component>`
        });

ethFacetExactAfterModule.requires.push(ethAlternativeSearchModule.name);
