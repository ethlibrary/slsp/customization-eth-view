export const ethPartofHintConfig = function(){
    return {
        label: {
            hint:{
                de: 'Dieses Dokument ist Teil von ',
                en: 'This document is part of '
            }
        }
    }
}
