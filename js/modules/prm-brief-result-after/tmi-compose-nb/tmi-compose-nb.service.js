/**
* @ngdoc service
* @name tmiComposeNbService
*
* @description
* Services to get mmsid of physical or online record of TMA NB
*
*
* Module {@link ETH.tmiComposeNbModule}<br>
*
 */
export const tmiComposeNbService = ['$http', '$sce', function($http, $sce){
    function getPrintData(nebisId){
        // https://daas.library.ethz.ch/rib/v3/mapping/redirect?id=ebi01_prod004464904&result=map
        let url = "https://daas.library.ethz.ch/rib/v3/mapping/redirect?result=map&id=" + nebisId;
        $sce.trustAsResourceUrl(url);

        return $http.get(url)
            .then(
                function(response){
                    return response.data;
                },
                function(httpError){
                    if (httpError.status === 404)return null;
                    let error = "***TMI*** an error occured: tmiComposeNbService.getPrintData: " + httpError.status;
                    if (httpError.data && httpError.data.errorMessage) {
                        error += ' - ' + httpError.data.errorMessage;
                    }
                    console.error(error);
                    return null;
                }
            )
    }

    // https://daas.library.ethz.ch/rib/v3/search?q=any,contains,oai:agora.ch:004464904
    function getOnlineData(oaiId){
        let url = "https://daas.library.ethz.ch/rib/v3/search?q=any,contains," + oaiId;
        $sce.trustAsResourceUrl(url);

        return $http.get(url)
            .then(
                function(response){
                    return response.data;
                },
                function(httpError){
                    if (httpError.status === 404)return null;
                    let error = "***TMI*** an error occured: tmiComposeNbService.getOnlineData: " + httpError.status;
                    if (httpError.data && httpError.data.errorMessage) {
                        error += ' - ' + httpError.data.errorMessage;
                    }
                    console.error(error);
                    return null;
                }
            );
    }

    return {
        getPrintData: getPrintData,
        getOnlineData: getOnlineData
    };
}];
