export const ethLoansHintConfig = function(){
    return {
        label:{
            textLoansActive1: {
                de: 'Ihre Ausleihen werden bis zu maximal fünf Mal automatisch verlängert, sofern die Dokumente nicht anderweitig verlangt werden. In diesem Fall erhalten Sie einen Rückruf per E-Mail.',
                en: 'Your loans will be automatically renewed up to a maximum of five times unless the documents are requested otherwise. In this case you will receive a recall by email.'
            },
            textLoansActive2: {
                de: 'Bei Fragen zur Ausleihdauer wenden Sie sich bitte an',
                en: 'If you have any questions about the loan period, please contact'
            },
            mail: {
                de: 'info@library.ethz.ch',
                en: 'info@library.ethz.ch'
            },
            textLoansActive3: {
                de: 'oder',
                en: 'or'
            },
            tel: {
                de: '+41 44 632 21 35',
                en: '+41 44 632 21 35'
            },
            telnr: {
                de: '+41446322135',
                en: '+41446322135'
            },
            textLoansActive4: {
                de: '.',
                en: '.'
            },
            textLoansHistory1: {
                de: 'In «frühere Ausleihen» werden Ihre Ausleihen der letzten zwei Jahre angezeigt.',
                en: 'In "Previous loans" your loans from the last two years are displayed.'
            }
        }
    }
}
