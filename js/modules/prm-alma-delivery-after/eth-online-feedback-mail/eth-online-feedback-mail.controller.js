export class ethOnlineFeedbackMailController {

    constructor( $location, ethConfigService, ethSessionService, ethOnlineFeedbackMailConfig ) {
        this.$location = $location;
        this.ethConfigService = ethConfigService;
        this.ethSessionService = ethSessionService;
        this.config = ethOnlineFeedbackMailConfig;
    }

    $onInit() {
        try{
            this.parentCtrl = this.afterCtrl.parentCtrl;
            this.feedbackLink = '';
            // not for HSA
            if(this.parentCtrl.item.pnx.control.originalsourceid && this.parentCtrl.item.pnx.control.originalsourceid[0].indexOf('hochschularchiv-der-eth') > -1){
                return;
            }
            // not for open access
            if(this.parentCtrl.item.pnx.addata.openaccess && this.parentCtrl.item.pnx.addata.openaccess[0] === 'true'){
                return;
            }
            // not for cdi open access
            if(this.parentCtrl.item.pnx.addata.oa && this.parentCtrl.item.pnx.addata.oa[0] === 'free_for_read'){
                return;
            }

            let token = this.ethSessionService.getDecodedToken();
            let ip = token.userIp;
            let url = 'https://eth.swisscovery.slsp.ch/discovery' + this.$location.$$url;
            let mmsId = this.parentCtrl.item.pnx.control.recordid[0];
            let title = '';
            if(this.parentCtrl.item.pnx.display.title && this.parentCtrl.item.pnx.display.title.length > 0){
                title = this.parentCtrl.item.pnx.display.title[0];
            }
            let creationdate = '';
            if(this.parentCtrl.item.pnx.display.creationdate && this.parentCtrl.item.pnx.display.creationdate.length > 0){
                creationdate = this.parentCtrl.item.pnx.display.creationdate[0];
            }
            let creator = '';
            if(this.parentCtrl.item.pnx.display.creator && this.parentCtrl.item.pnx.display.creator.length > 0){
                creator = this.parentCtrl.item.pnx.display.creator.join(', ');
            }
            let type = '';
            if(this.parentCtrl.item.pnx.display.type && this.parentCtrl.item.pnx.display.type.length > 0){
                type = this.parentCtrl.item.pnx.display.type[0];
            }
            let identifier = '';
            if(this.parentCtrl.item.pnx.display.identifier && this.parentCtrl.item.pnx.display.identifier.length > 0){
                let ident = this.parentCtrl.item.pnx.display.identifier[0];
                if(ident.indexOf('<b>ISBN')>-1){
                    identifier = this.parentCtrl.item.pnx.display.identifier.join(', ').replace(/<\/b>/g, '').replace(/<b>/g, '');
                }
                else if(ident.indexOf('<b>ISSN')>-1){
                    identifier = this.parentCtrl.item.pnx.display.identifier.join(', ').replace(/<\/b>/g, '').replace(/<b>/g, '');
                }
                else if(ident.indexOf('ISBN')>-1){
                    identifier = 'ISBN: ' + ident.substring(ident.indexOf('$$V') + 3);
                }
                else if(ident.indexOf('ISSN')>-1){
                    identifier = 'ISSN: ' + ident.substring(ident.indexOf('$$V') + 3);
                }
            }
            let userAgent = navigator.userAgent;

let subject = `${this.ethConfigService.getLabel(this.config, 'linktext')}: ${mmsId} - "${title}"`;

let body= `** Attached Metadata **
Title: ${title}
Author: ${creator}
Year: ${creationdate}
Type: ${type}
DocId: ${mmsId}
Identifier: ${identifier}
URL: ${url}
USER_IP: ${ip}
USER_AGENT: ${userAgent}
****

${this.ethConfigService.getLabel(this.config, 'text')}
`;

            this.feedbackLink = 'mailto:' + this.ethConfigService.getUrl(this.config, 'mailto') + '?subject=' + encodeURIComponent(subject) + '&body=' + encodeURIComponent(body);
        }
        catch(e){
            console.error("***ETH*** an error occured: ethOnlineFeedbackMailController.$onInit\n\n");
            console.error(e.message);
            throw(e)
        }
    }
}

ethOnlineFeedbackMailController.$inject = ['$location', 'ethConfigService', 'ethSessionService', 'ethOnlineFeedbackMailConfig' ];
