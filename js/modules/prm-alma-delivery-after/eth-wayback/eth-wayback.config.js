export const ethWaybackConfig = function(){
    return {
        label: {
            linktext:{
                de: 'Link zum Webarchiv',
                en: 'Link to the web archive'
            },
            hint:{
                de: 'Wählen Sie im Webarchiv ein Jahr und ein blau markiertes Datum, um die archivierte Website aufzurufen.',
                en: 'In the web archive, select a year and a date marked in blue to access the archived website.'
            }
        }
    }
}
