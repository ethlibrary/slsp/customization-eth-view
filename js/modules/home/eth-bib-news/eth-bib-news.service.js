/**
* @ngdoc service
* @name ethBibNewsService
*
* @description
* Service to get ETH Library News (AEM CMS)
*
*
* <b>Used by</b><br>
** Module {@link ETH.ethBibNewsModule}<br>
*
*
 */

export const ethBibNewsService = ['$http', '$sce', function($http, $sce){

    function getNews(lang){
        if(!lang){
            lang = 'de';
        }

        let url = 'https://daas.library.ethz.ch/rib/v3/bib-news?lang=' + lang;
        $sce.trustAsResourceUrl(url);

        return $http.get(url,{headers:{ "X-From-ExL-API-Gateway": undefined }})
            .then(
                function(response){
                    return response.data;
                },
                function(httpError){
                    let error = "***ETH*** an error occured: ethBibNewsService.get(): " + httpError.status;
                    if (httpError.data && httpError.data.errorMessage) {
                        error += ' - ' + httpError.data.errorMessage;
                    }
                    console.error(error);
                    return null;
                }
            );
    }


    return {
        getNews: getNews
    };
}];
