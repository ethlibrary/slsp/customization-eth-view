export class ethLoginBoxController {

    constructor(ethConfigService, ethLoginBoxConfig, $scope, $compile, $timeout) {
        this.ethConfigService = ethConfigService;
        this.config = ethLoginBoxConfig;
        this.$scope = $scope;
        this.$compile = $compile;
        this.$timeout = $timeout;
    }

    $onInit() {
        try {
            this.$timeout(() => {
                this.parentCtrl = this.afterCtrl.parentCtrl;

                let linkRegister = `
                <a ng-href="${this.ethConfigService.getUrl(this.config, 'registrationUrl')}" target="_blank" class="parallelLoginDescription">
                    <span>${this.ethConfigService.getLabel(this.config, 'registration')}</span>
                    <svg width="100%" height="100%" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg" fit="" preserveAspectRatio="xMidYMid meet" focusable="false"><path d="M19 19H5V5h7V3H5c-1.11 0-2 .9-2 2v14c0 1.1.89 2 2 2h14c1.1 0 2-.9 2-2v-7h-2v7zM14 3v2h3.59l-9.83 9.83 1.41 1.41L19 6.41V10h2V3h-7z"></path></svg>
                </a>`;

                let authMeth1 = angular.element(document.querySelectorAll(`prm-login div > md-content > md-list > md-list-item:nth-child(1)`));
                angular.element(authMeth1).append(this.$compile(linkRegister)(this.$scope)).addClass('authMethSaml');
            }, 0);
        } catch (e) {
            console.error("***ETH*** an error occured: ethLoginBoxController\n\n");
            console.error(e.message);
        }
    }
}

ethLoginBoxController.$inject = ['ethConfigService', 'ethLoginBoxConfig', '$scope', '$compile', '$timeout'];