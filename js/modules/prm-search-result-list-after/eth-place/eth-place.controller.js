export class ethPlaceController {

    constructor( $location, $timeout, $scope, $compile, $element, $anchorScroll, angularLoad, ethConfigService, ethSessionService, ethPlaceConfig, ethPlaceService ) {
        this.$location = $location;
        this.$timeout = $timeout;
        this.$scope = $scope;
        this.$compile = $compile;
        this.$element = $element;
        this.$anchorScroll = $anchorScroll;
        this.angularLoad = angularLoad;
        this.ethConfigService = ethConfigService;
        this.ethSessionService = ethSessionService;
        this.config = ethPlaceConfig;
        this.ethPlaceService = ethPlaceService;
    }

    $onInit() {
        try{
            this.parentCtrl = this.afterCtrl.parentCtrl;
            this.processDoCheck = false;
            if(!this.parentCtrl.searchService || !this.parentCtrl.searchService.searchUtil || !this.parentCtrl.searchService.searchUtil.$stateParams || !this.parentCtrl.searchService.searchUtil.$stateParams.query)return;
            if(this.parentCtrl.searchService.searchUtil.$stateParams.mode == "advanced")return;
            this.vid = window.appConfig.vid;
            this.tab = window.appConfig['primo-view']['available-tabs'][0];
            this.myScope = window.appConfig['primo-view']['scopes'][0]['scope-id'];
            this.lang = this.ethConfigService.getLanguage();
            this.search = this.parentCtrl.searchService.searchUtil.$stateParams.query.replace('any,contains,','');
            let facet = this.parentCtrl.searchService.searchUtil.$stateParams.facet;
            let mfacet = this.parentCtrl.searchService.searchUtil.$stateParams.mfacet;
            let pfilter = this.parentCtrl.searchService.searchUtil.$stateParams.pfilter;
            if(this.search.indexOf('[wd/place]') === -1 || facet || mfacet || pfilter) {
                return;
            }
            // matomo
            if(!window._paq){
                window._paq = [];
            }
            window._paq.push(['setCustomUrl', 'https://eth.swisscovery.slsp.ch/explore/page/place']);
            window._paq.push(['trackPageView']);
            this.processDoCheck = true;
            this.poi = null;
            this.hasResults = false;
            let identifier = this.search.substring(this.search.indexOf(']')+1);
            this.oldTotalResults = 0;
            if(identifier.substring(0,1)==='Q'){
                this.getPlace(identifier);
            }
            else{
                this.ethPlaceService.getQidForGND(identifier)
                    .then((qid) => { 
                        if(!qid){
                            this.handleNoWikidataData();
                            return;
                        }
                        this.getPlace(qid);
                    })
                    .catch( e => {
                        console.error("***ETH*** an error occured: ethPlaceController.$onInit()");
                        console.error(e.message);
                        throw(e)
                    })
            }
        }
        catch(e){
            console.error("***ETH*** an error occured: ethPlaceController.onInit()\n\n");
            console.error(e.message);
        }
    }

    getPlace(qid){
        this.$timeout(() => {
            // hide pfilter
            let pfilter = document.querySelector('prm-resource-type-filter-bar');
            angular.element(pfilter).addClass('eth-hide-pfilter');
        }, 1000);

        this.ethPlaceService.getPlaceFromETHorama(qid)
            .then((data) => {
                if(!data || data.length === 0)return qid;
                this.poi = data[0];
                this.poi.qid = qid;
                let contentItems = []
                this.poi.contentItems.forEach(item1 => {
                    let dupl = false;
                    contentItems.forEach(item2 => {
                        if(item2.docId === item1.docId){
                            dupl = true;
                        }
                    })
                    if(!dupl){
                        contentItems.push(item1);
                    }
                });
                this.poi.contentItems = contentItems;
                this.poi.links = [];
                this.poi.links.push({'text':'"' + data[0].name['de'] + '"' + this.ethConfigService.getLabel(this.config, 'linktextEthorama'), 'url': this.ethConfigService.getUrl(this.config, 'ethorama') + data[0].id});
                // more than 1 POI -> add contentItems and links from other pois
                if(data.length > 1){
                    for (var i = 1; i < data.length; i++) {
                        data[i].contentItems.forEach(item1 => {
                            let dupl = false;
                            this.poi.contentItems.forEach(item2 => {
                                if(item2.docId === item1.docId){
                                    dupl = true;
                                }
                            })
                            if(!dupl){
                                this.poi.contentItems.push(item1);
                            }
                        });
                        // 990043586820205503
                        this.poi.links.push({'text':'"' + data[i].name['de'] + '"' + this.ethConfigService.getLabel(this.config, 'linktextEthorama'), 'url': this.ethConfigService.getUrl(this.config, 'ethorama') + data[i].id});
                    }
                }
                return qid;
            })
            .then((qid) => {
                return this.ethPlaceService.getGeoTopicsFromGeoGraph(qid);
            })
            .then((data) => {
                this.geoTopics = [];
                data.features.forEach( f => {
                    let eMaps = [];
                    f.properties.eMaps.forEach(i => {
                        eMaps.push({
                            'mmsid': i.mmsid,
                            'url': '/discovery/fulldisplay?vid=' + this.vid + '&docid=alma' + i.mmsid,
                            'title': i.title
                        });
                    });
                    let eRaraItems = [];
                    f.properties.eRaraItems.forEach(i => {
                        eRaraItems.push({
                            'mmsid': i.mmsid,
                            'url': '/discovery/fulldisplay?vid=' + this.vid + '&docid=alma' + i.mmsid,
                            'title': i.title
                        });
                    });
                    this.geoTopics.push({
                        'name': f.properties.name,
                        'url': '/discovery/search?mode=advanced&vid=' + this.vid + '&query=sub,exact,' + encodeURIComponent(f.properties.name),
                        'gnd': f.properties.gnd,
                        'eRaraItems': eRaraItems,
                        'eMaps': eMaps
                    });
                })
                return qid;
            })
            .then((qid) => {
                if(this.poi){
                    return this.ethPlaceService.getPoiFromGeoGraph(this.poi.id);
                }
                else{
                    return null;
                }

            }) 
            .then((data) => {
                if(data && data.features && data.features.length > 0){
                    this.poi.dossiers = [];
                    this.poi.routes = [];
                    data.features[0].properties.dossiers.forEach( i => {
                        if(this.lang == 'en'){
                            let text = i.title_en;
                            if(!text || text === '')text = i.title_de;
                            this.poi.dossiers.push({
                                'url': this.ethConfigService.getUrl(this.config, 'ethoramaDossier') + i.id,
                                'text': text
                            })
                        }
                        else{
                            this.poi.dossiers.push({
                                'url': this.ethConfigService.getUrl(this.config, 'ethoramaDossier') + i.id,
                                'text': i.title_de
                            })
                        }
                    })
                    data.features[0].properties.routes.forEach( i => {
                        if(this.lang == 'en'){
                            let text = i.title_en;
                            if(!text || text === '')text = i.title_de;
                            this.poi.routes.push({
                                'url': this.ethConfigService.getUrl(this.config, 'ethoramaRoute') + i.id,
                                'text': text
                            })
                        }
                        else{
                            this.poi.routes.push({
                                'url': this.ethConfigService.getUrl(this.config, 'ethoramaRoute') + i.id,
                                'text': i.title_de
                            })
                        }
                    })
                }
                return qid;
            })
            .then((qid) => {
                return this.ethPlaceService.getPlaceFromWikidata(qid, this.lang);
            })
            .then((data) => {
                if(!data)return qid;
                if(data.results.bindings && data.results.bindings.length > 0){
                    this.hasResults = true;
                    this.itemLabel = data.results.bindings[0].itemLabel.value;
                    if(this.itemLabel){
                        this.$timeout(() => {
                            // render header for result list
                            let resultList = angular.element(this.$element[0].parentElement.parentElement.parentElement);
                            if(resultList){
                                let html = `
                                <a name="eth-top-anchor"></a>
                                <div class="eth-place__top">
                                    <h2>${this.itemLabel}</h2>
                                    <div>
                                        <md-button ng-if="($ctrl.geoTopics && $ctrl.geoTopics.length > 0) || $ctrl.poi && $ctrl.poi.links && $ctrl.poi.links.length > 0" class="md-primary" ng-click="$ctrl.scrollTo('eth-place-selected-results-anchor')">${this.ethConfigService.getLabel(this.config, 'selectedResults')}</md-button>
                                        <md-button class="md-primary" ng-click="$ctrl.scrollTo('eth-place-anchor')">${this.ethConfigService.getLabel(this.config, 'moreInformations')}</md-button>
                                    </div>
                                </div>
                                `;
                                resultList.prepend(this.$compile(html)(this.$scope));
                            }
                            this.searchAllPlaceRessources(this.itemLabel);                                
                        }, 1000);
                        this.$timeout(() => {
                            if(data.results.bindings[0].coordinate_location){
                                this.renderLeafletMap(data.results.bindings[0]);
                            }
                        }, 500);

                        let place = data.results.bindings[0];
                        if(place.item && place.item.value){
                            this.wikidataURL = place.item.value;
                        }
                        if(place.geonames){
                            this.geonamesURL = 'https://www.geonames.org/' + place.geonames.value;
                        }
                        if(place.wikipedia){
                            this.wikipediaURL = place.wikipedia.value;
                        }
                        if(place.gnd){
                            this.gnd = place.gnd.value;
                            this.gndURL = 'http://d-nb.info/gnd/' + place.gnd.value;
                        }
                        if(place.hls){
                            this.hlsURL = 'http://www.hls-dhs-dss.ch/textes/d/D' + place.hls.value + '.php';
                        }
                        if(place.archinform){
                            this.archinformURL = 'https://www.archinform.net/ort/' + place.archinform.value +'.htm';
                        }
                        this.aPartOf = [];
                        if(place.partOf && place.partOf.value){
                            this.aPartOf.push(place.partOf.value.substring(place.partOf.value.lastIndexOf('/')+1));
                        }
                        // if there is more then 1 binding - partOf
                        if(data.results.bindings.length > 1){
                            for( let i=1; i<data.results.bindings.length; i++){
                                let binding = data.results.bindings[i];
                                if(binding.partOf && binding.partOf.value){
                                    let qid = binding.partOf.value.substring(binding.partOf.value.lastIndexOf('/')+1);
                                    if(!this.aPartOf.includes(qid)){
                                        this.aPartOf.push(qid);
                                    }
                                }
                            }
                        }
                        if(place.coordinate_location){
                            this.coordinates = place.coordinate_location.value;
                            let lng = this.coordinates.substring(6,this.coordinates.indexOf(' '));
                            let lat = this.coordinates.substring(this.coordinates.indexOf(' ') + 1, this.coordinates.length-1);
                            return this.ethPlaceService.getNeartoPois(lat,lng);
                        }
                        else{
                            return null;        
                        }
                    }
                    return null;
                }
                else{
                    this.handleNoWikidataData();
                    return;
                }
            })
            .then((data) => {
                this.nearto = [];
                if(data){
                    data.features.forEach( f => {
                        if(f.properties.qid && f.properties.qid != qid){
                            this.nearto.push({'name': f.properties.name, 'qid':f.properties.qid });
                        }
                    })
                }
                return this.ethPlaceService.getPlaceFromGeolinker(qid);
            })
            .then((data) => {
                try{
                    this.geolinks = [];
                    let aHLS = [];
                    data.data.resolverneo4j.links.forEach(link => {
                        let key = Object.keys(link)[0];
                        let url = link[key];
                        // check whitelist for geolinker links
                        if(this.config.whitelist.indexOf(key) === -1)return;
                        if(url.indexOf('https://search.ortsnamen.ch/api/')>-1 || url.indexOf('https://api3.geo.admin.ch/rest/services/api')>-1)return;
                        if(key === 'HLS'){
                            key = "Historisches Lexikon der Schweiz";
                            let hlsNo = url.substring(url.lastIndexOf('/D') + 2, url.lastIndexOf('.php')).replace(/0/g,'');
                            if(aHLS.indexOf(hlsNo) > -1)return;
                            aHLS.push(hlsNo);
                        }
                        else if(key === 'SSRQ'){
                            key = "Sammlung Schweizerischer Rechtsquellen";
                        }
                        else if(key === 'Dodis'){
                            key = "DODIS (Diplomatische Dokumente der Schweiz)";
                        }
                        else if(key === 'IFS'){
                            key = "Inventar der Fundmünzen der Schweiz";
                        }
                        else if(key === 'OSM relation'){
                            key = "OpenStreetMap Relation";
                        }
                        else if(key === 'GND'){
                            key = "GND (Gemeinsame Normdatei der Deutschen Nationalbibliothek)";
                        }
                        else if(key === 'VIAF'){
                            key = "VIAF (Virtual International Authority File)";
                        }
                        else if(key === 'Ortsnamen'){
                            key = "Ortsnamenforschung ortsnamen.ch";
                        }
                        else if(key === 'SUDOC'){
                            key = "Identifiants et Référentiels pour l'enseignement supérieur et la recherche";
                        }
                        this.geolinks.push([key, url]);
                    });
                    this.geolinks = this.geolinks.sort();
                    /*
                    if(this.gnd){
                        return this.ethPlaceService.getPersonsForPlaceGND(this.gnd);
                    }
                    else return null;
                    */
                }
                catch(e){
                    this.geolinks = null;
                }
            })
            /*
            .then((data) => {
                if(!data || !data.member)return null;
                if(data.totalItems > 18)return null;
                this.persons = data.member.map(p => {
                    p.qid = null;
                    p.sameAs.forEach(s => {
                        if(s.collection && s.collection.name && s.collection.name === 'Wikidata'){
                            p.qid = s.id.substring(s.id.lastIndexOf('/') + 1);
                        }
                    })
                    return p;
                })
            })
            */
            .catch( (e) => {
                console.error("***ETH*** an error occured: ethPlaceController.getPlace()");
                console.error(e.message);
                throw(e)
            })
    }

    renderLeafletMap(wikiResult){
        let _this = this;
        window.setTimeout(function(){
            if (typeof L !== "undefined") {
                _this._renderLeafletMap(wikiResult);
            }
            else{
                _this.angularLoad.loadCSS('https://unpkg.com/leaflet@1.9.4/dist/leaflet.css',{integrity: 'sha512-xwE/Az9zrjBIphAcBb3F6JVqxf46+CDLwfLMHloNu6KEQCAWi6HcDUbeOfBIptF7tcCzusKFjFw2yuvEpDL9wQ==', crossorigin:''});
                _this.angularLoad.loadScript('https://unpkg.com/leaflet@1.9.4/dist/leaflet.js',{integrity: 'sha512-GffPMF3RvMeYyc1LWMHtK8EbPv0iNZ8/oTtHPx9/cc2ILxQ+u905qIwdpULaqDkyBKgOaB57QTMg7ztg8Jm2Og==', crossorigin:''})
                    .then(function() {
                        _this._renderLeafletMap(wikiResult);
                    })
                    .catch(function(e) {
                        console.error("***ETH*** ethPlaceController: angularLoad.renderLeafletMap()");
                        console.error(e.message);
                    });
            }
        },0);
    }

    _renderLeafletMap(wikiResult){
        let point = wikiResult.coordinate_location.value;
        let lng = point.substring(6,point.indexOf(' '));
        let lat = point.substring(point.indexOf(' ') + 1, point.length-1);
        if(!document.getElementById('mapid'))return;
        let map = L.map('mapid').setView([lat, lng], 10);
        L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
            attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/map-feedback/">Mapbox</a>',
            tileSize: 512,
            maxZoom: 18,
            zoomOffset: -1,
            id: 'mapbox/streets-v11',
            accessToken: 'pk.eyJ1IjoiYmVybmR1IiwiYSI6ImNrMjBpMHRidjE1ZGkzaHFnNDdlMXV4eWkifQ.RACc6dWL675djVZaMmHBww'
        }).addTo(map);
        this.renderGeoGraphMaps(map, lat, lng);
        // marker place of this page
        let marker = L.marker([lat, lng]).addTo(map);
    }

    renderGeoGraphMaps(map, lat, lng){
        let _this = this;
        this.ethPlaceService.getMapsFromGeoGraph(lat, lng)
            .then((data) => {
                if(!data || !data.features || data.features.length === 0){
                    return;
                }
                let filteredFeatures = data.features.filter(f => {
                    if(f.properties.scale){
                        if(parseInt(f.properties.scale) <= 50000){
                            return true;
                        }
                    }
                    return false;
                })
                try{
                    filteredFeatures = filteredFeatures.sort((a, b) => a.properties.title.localeCompare(b.properties.title, "de", { ignorePunctuation: true }));
                }
                catch(e){
                    filteredFeatures = filteredFeatures.sort((a, b) => a.properties.title.localeCompare(b.properties.title));
                }
                let opacity = 0.03;
                let openWeight = 4;
                if(filteredFeatures.length > 10){
                    opacity = 0;
                    openWeight = 6;
                }
                function onEachFeature(feature, layer){
                    // array for html template: links to maps
                    if(feature.properties.url){
                        let url = feature.properties.url;
                        if(url.indexOf('10.3931/e-rara') > -1 && url.indexOf('dx.doi.org') === -1){
                            url = 'https://dx.doi.org/' + url;
                        }
                        let hint = null;
                        if (feature.properties.source && feature.properties.source === 'e-maps'){
                            hint = _this.ethConfigService.getLabel(_this.config, 'emapsMap');
                        }
                        else if(feature.properties.source && feature.properties.source === 'e-rara'){
                            hint = _this.ethConfigService.getLabel(_this.config, 'eraraMap');
                        }
                        _this.maps.push({
                            'url': url,
                            'title': feature.properties.title,
                            'hint': hint,
                            'attribution':feature.properties.attribution,
                            'description':feature.properties.description
                        })
                    }
                    // marker
                    let center = layer.getBounds().getCenter();
                    var mapIcon = L.icon({
                        iconUrl: "custom/" + _this.vid.replace(':','-') + "/img/icon_map.png",
                        iconSize:     [25, 25],
                    });
                    let markerOptions = {
                       clickable: true,
                       color: "#356947",
                       alt: 'Marker ' + feature.properties.title,
                       icon: mapIcon
                    }
                    let marker = L.marker(center, markerOptions);
                    marker.options.icon.options.className = 'eth-map-marker';
                    // popup content
                    let popupContent = '<div class="map-info">';
                    popupContent += '<div class="map-info-title">' + feature.properties.title + '</div>';
                    if(feature.properties.attribution){
                        popupContent += '<div class="map-info-text">' + feature.properties.attribution + '</div>';
                    }
                    popupContent += '<div class="map-info-hint">' + _this.ethConfigService.getLabel(_this.config, 'searchThis') + '</div>';
                    popupContent += '</div>';
                    marker.bindPopup(popupContent);
                    // marker events
                    marker.on('mouseover', function (e) {
                        this.openPopup();
                        layer.setStyle({weight: openWeight});
                    });
                    // touch: long press (1sec)
                    marker.on('contextmenu', function (e) {
                        this.openPopup();
                        layer.setStyle({weight: openWeight});
                    });
                    marker.on('mouseout', function (e) {
                        this.closePopup();
                        layer.setStyle({weight: 1});
                    });
                    marker.on('click', function (e) {
                        let url = feature.properties.url;
                        if(url.indexOf('10.3931/e-rara') > -1 && url.indexOf('dx.doi.org') === -1){
                            url = 'https://dx.doi.org/' + url;
                        }
                        window.open(url, "_blank");
                    });
                    // add marker to layerGroup
                    let polygonAndItsCenter = L.layerGroup([layer, marker]);
                    polygonAndItsCenter.addTo(polygonsWithCenters);
                }
                _this.maps = [];
                let polygonsWithCenters = L.layerGroup();
                let geoJsonLayer = L.geoJSON(filteredFeatures, {
                    onEachFeature: onEachFeature,
                    style: {
                        "color": "#356947",
                        //"color": "#1D5B8B",
                        //"fillColor": "#356947",
                        "weight": 1,
                        "fillOpacity": opacity
                    }
                });
                polygonsWithCenters.addTo(map);
            })
            .catch( (e) => {
                console.error("***ETH*** an error occured: ethPlaceController ethPlaceService.getMapsFromGeoGraph");
                console.error(e.message);
                throw(e);
            })
    }

    searchSelectedPlaceRessources(poi){
        this.$location.hash("eth-top-anchor");
        let items = [];
        /*
        In primo 7 rows and max 30 boolean operators
        The system will display a message and provide suggestions when the following limits are exceeded:
        The query contains more than 30 boolean operators.
        The query contains more than 8 question marks.
        The query contains more than 8 asterisks and the word length is greater than 2 (such as abb* or ab*c).
        The query contains more than 4 asterisks and the word length is less than 3 (such as ab*).
        The entire query consists of a single letter and an asterisk (such as a*).
        */
        // https://dinkum.ethbib.ethz.ch/display/DAAS/V3%3A+Daten+aus+Primo+VE+und+Alma
        let c = 0;
        poi.contentItems.forEach( e => {
            if(c < 27 && e.docId && e.docId.indexOf('error:') == -1){
                items.push(e.docId);
                c++;
            }
        })
        let query = 'any,contains,';
        let queryValue = '';
        items.forEach( (e, i) => {
            queryValue += e;
            if (i < (items.length - 1)) {
                queryValue += ' OR ';
            }
        })
        query = query + queryValue;
        let options = {query: query, tab: this.tab, scope: this.myScope};
        if(this.parentCtrl.searchService.searchUtil.$stateParams.facet){
            options.facets = this.parentCtrl.searchService.searchUtil.$stateParams.facet;
        }
        document.getElementById('searchBar').value = queryValue;
        this.parentCtrl.searchService.searchUtil.$stateParams.query = query;
        this.parentCtrl.searchService.cheetah.$state.params.query = query;        
        this.parentCtrl.searchService.search(options, true);
    }


    // initial search for all resources
    searchAllPlaceRessources(search){
        //this.$location.hash("eth-top-anchor");
        let options = {query:'any,contains,' + search, tab: this.tab, scope: this.myScope};
        if(this.parentCtrl.searchService.searchUtil.$stateParams.facet){
            options.facets = this.parentCtrl.searchService.searchUtil.$stateParams.facet;
        }
        document.getElementById('searchBar').value = this.itemLabel;
        this.parentCtrl.searchService.searchUtil.$stateParams.query = 'any,contains,' + search;
        this.parentCtrl.searchService.cheetah.$state.params.query = 'any,contains,' + search;
        this.parentCtrl.searchService.search(options, true);
    }

    scrollTo(anchorName){
        //this.$location.hash(anchorName);
        this.$anchorScroll(anchorName);
        // prm-full-view
        //let fullView = angular.element(document.querySelector('prm-full-view'))
        //fullView.scrollToElementIdWithBeacon(anchorName)
    }

    loadPlacePage(qid){
        try{
            let query = '[wd/place]' + qid;
            let vid = window.appConfig.vid;
            let tab = window.appConfig['primo-view']['available-tabs'][0];
            let url = `/discovery/search?query=any,contains,${query}&tab=${tab}&vid=${vid}&explore=place`;
            location.href = url;
        }
        catch(e){
            console.error("***ETH*** an error occured: ethPersonCardsService.loadPlacePage(): \n\n");
            console.error(e.message);
            throw(e);
        }
    }    

    makePlacePageLink(qid){
        try{
            let query = '[wd/place]' + qid;
            let vid = window.appConfig.vid;
            let tab = window.appConfig['primo-view']['available-tabs'][0];
            let url = `/discovery/search?query=any,contains,${query}&tab=${tab}&vid=${vid}&explore=place`;
            return url;
        }
        catch(e){
            console.error("***ETH*** an error occured: ethPersonCardsService.makePlacePageLink(): \n\n");
            console.error(e.message);
            throw(e);
        }
    }    
    
    makePersonPageLink(gnd){
        try{
            let query = '[wd/person]' + gnd;
            let vid = window.appConfig.vid;
            let tab = window.appConfig['primo-view']['available-tabs'][0];
            let url = `/discovery/search?query=any,contains,${query}&tab=${tab}&vid=${vid}&explore=person`;
            return url;
        }
        catch(e){
            console.error("***ETH*** an error occured: ethPersonCardsService.makePersonPageLink(): \n\n");
            console.error(e.message);
            throw(e);
        }
    }

    handleNoWikidataData(){
        let query = this.search.replace('[wd/place]','');
        this.ethPlaceService.getNameForGND(query)
            .then((data) => { 
                if(!data || !data.preferredName)return;
                query = data.preferredName;
                this.parentCtrl.searchService.searchFieldsService.mainSearch = query;
                document.getElementById('searchBar').value = query;
                this.parentCtrl.searchService.searchUtil.$stateParams.query = 'any,contains,' + query;
                this.parentCtrl.searchService.cheetah.$state.params.query = 'any,contains,' + query;
                this.parentCtrl.searchService.cheetah.$location.$$search.query = 'any,contains,' + query;
                this.processDoCheck = false;
                this.$timeout(() => {
                    this.parentCtrl.searchService.search({query: 'any,contains,' + query, tab:this.tab, scope:this.myScope},true);
                }, 300);
            })
            .catch( e => {
                console.error("***ETH*** an error occured: ethPlaceController.$onInit()");
                console.error(e.message);
                throw(e)
            })
    }

    $doCheck() {
        try{
            if (!this.processDoCheck || !this.search || this.search.indexOf('[wd/place]') === -1) {
                return;
            }
            let noResultContainer = document.querySelector('prm-no-search-result');
            if(noResultContainer){
                noResultContainer.style.display = "none";
            }
            let domResultsCount = document.querySelector("[data-qa='change_result_size_per_page'] md-select-value span:first-child");
            if(domResultsCount && this.parentCtrl.searchInfo){
                this.oldTotalResults = this.parentCtrl.searchInfo.total;
                domResultsCount.textContent = this.parentCtrl.searchInfo.total + ' ' + this.ethConfigService.getLabel(this.config, 'results');
            }
        }
        catch(e){
            console.error("***ETH*** ethPlaceController.$doCheck:");
            console.error(e.message);
        }

    }

}
ethPlaceController.$inject = ['$location', '$timeout', '$scope', '$compile', '$element', '$anchorScroll', 'angularLoad', 'ethConfigService', 'ethSessionService', 'ethPlaceConfig', 'ethPlaceService'];
