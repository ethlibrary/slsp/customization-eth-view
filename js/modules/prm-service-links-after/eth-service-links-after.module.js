import {ethProvenienzModule} from './eth-provenienz/eth-provenienz.module';
import {ethProvenienzEraraLinkModule} from './eth-provenienz-erara-link/eth-provenienz-erara-link.module';
import {ethComposeEraraModule} from './eth-compose-erara/eth-compose-erara.module';
import {ethFullviewPersonModule} from './eth-fullview-person/eth-fullview-person.module';
import {ethPlaceLinkModule} from './eth-place-link/eth-place-link.module';
import {ethElendingLinkModule} from './eth-elending-link/eth-elending-link.module';
import {ethVideoLinkModule} from './eth-video-link/eth-video-link.module';

export const ethServiceLinksAfterModule = angular
    .module('ethServiceLinksAfterModule', [])
        .component('prmServiceLinksAfter',  {
            bindings: {parentCtrl: '<'},
            template: `
            <eth-compose-erara-component after-ctrl="$ctrl"></eth-compose-erara-component>
            <eth-provenienz-erara-link-component after-ctrl="$ctrl"></eth-provenienz-erara-link-component>
            <eth-video-link-component after-ctrl="$ctrl"></eth-video-link-component>
            <eth-elending-link-component after-ctrl="$ctrl"></eth-elending-link-component>
            <eth-provenienz-component after-ctrl="$ctrl"></eth-provenienz-component>
            <eth-place-link-component after-ctrl="$ctrl"></eth-place-link-component>
            <eth-fullview-person-component after-ctrl="$ctrl"></eth-fullview-person-component>
            `
        });

ethServiceLinksAfterModule.requires.push(ethProvenienzModule.name);
ethServiceLinksAfterModule.requires.push(ethProvenienzEraraLinkModule.name);
ethServiceLinksAfterModule.requires.push(ethComposeEraraModule.name);
ethServiceLinksAfterModule.requires.push(ethFullviewPersonModule.name);
ethServiceLinksAfterModule.requires.push(ethPlaceLinkModule.name);
ethServiceLinksAfterModule.requires.push(ethElendingLinkModule.name);
ethServiceLinksAfterModule.requires.push(ethVideoLinkModule.name);
