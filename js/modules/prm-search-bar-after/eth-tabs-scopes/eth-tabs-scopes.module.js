/**
* @ngdoc module
* @name ethTabsScopesModule
*
* @description
*
* Customization for the search bar:<br>
* - Tabs and scopes are displayed even if the search has not yet taken place (empty searchfield).
*
* <b>AngularJS Dependencies</b><br>
*
*
* <b>CSS/Image Dependencies</b><br>
*
*/
import {ethTabsScopesController} from './eth-tabs-scopes.controller';

export const ethTabsScopesModule = angular
    .module('ethTabsScopesModule', [])
        .controller('ethTabsScopesController', ethTabsScopesController)
        .component('ethTabsScopesComponent',  {
            bindings: {afterCtrl: '<'},
            controller: 'ethTabsScopesController'
        })
