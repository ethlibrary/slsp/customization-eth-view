/**
* @ngdoc module
* @name ethSwisscoveryModule
*
* @description
*
* Customization for the logo area:<br>
* - A link "ETH Library @ swisscovery" is added.<br>
* - (The hostname is added if it is not a production system.)
*
*
* <b>AngularJS Dependencies</b><br>
* Service /js/services {@link ETH.ethConfigService}<br>
* Service {@link ETH.ethSwisscoveryConfig}<br>
*
*
* <b>CSS/Image Dependencies</b><br>
* CSS eth-swisscovery.css
*
*/
import {ethConfigService} from '../../../services/eth-config.service';
import {ethSwisscoveryConfig} from './eth-swisscovery.config';
import {ethSwisscoveryController} from './eth-swisscovery.controller';
import {ethSwisscoveryHtml} from './eth-swisscovery.html';

export const ethSwisscoveryModule = angular
    .module('ethSwisscoveryModule', [])
        .factory('ethConfigService', ethConfigService)
        .factory('ethSwisscoveryConfig', ethSwisscoveryConfig)
        .controller('ethSwisscoveryController', ethSwisscoveryController)
        .component('ethSwisscoveryComponent',{
            controller: 'ethSwisscoveryController',
            template: ethSwisscoveryHtml
        })
