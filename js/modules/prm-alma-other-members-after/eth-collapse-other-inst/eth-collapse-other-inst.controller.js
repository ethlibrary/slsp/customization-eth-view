export class ethCollapseOtherInstController {
    constructor( $scope, $compile) {
        this.$scope = $scope;
        this.$compile = $compile;
    }

    $onInit() {
        try {
            this.parentCtrl = this.afterCtrl.parentCtrl;
            this.parentCtrl.isCollapsed = true;
            /*if (this.parentCtrl.serviceMode === 'howovp') {
                this.parentCtrl.isCollapsed = false;
            }
            else {
                this.parentCtrl.isCollapsed = true;
            }*/
        }
        catch (e) {
            console.error("***ETH*** an error occured: ethCollapseOtherInstController\n\n");
            console.error(e.message);
        }
    }
 }
ethCollapseOtherInstController.$inject =  ['$scope', '$compile'];
