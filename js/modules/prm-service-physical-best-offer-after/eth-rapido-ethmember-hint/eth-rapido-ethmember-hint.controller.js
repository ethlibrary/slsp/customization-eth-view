export class ethRapidoEthmemberHintController {

    constructor( $scope, $compile, ethSessionService, ethConfigService, ethRapidoEthmemberHintConfig, $timeout, $anchorScroll) {
        this.$scope = $scope;
        this.$compile = $compile;
        this.ethSessionService = ethSessionService;
        this.ethConfigService = ethConfigService;
        this.config = ethRapidoEthmemberHintConfig;
        this.$timeout = $timeout;
        this.$anchorScroll = $anchorScroll;
    }

    $onInit() {
        try{
            this.parentCtrl = this.afterCtrl.parentCtrl;
            // Only for digital offers
            if (!this.parentCtrl.isdigitaloffer)return;

            // Only for ETH-Member
            this.isETHMember = false;
            let token = this.ethSessionService.getDecodedToken();
            if(token.userGroup === 'ETH_Member' || token.userGroup === 'ETH_E06_GESS-Member' || token.userGroup === 'ETH_E72_INFK-Member' || token.userGroup === 'ETH_E64_MATH-Member'){
                this.isETHMember = true;
            }
            if(!this.isETHMember)return;
            this.$scope.$watch('this.$ctrl.parentCtrl.digitalbestoffer', (newValue, oldValue, scope) => {
                // Only if there is a digital offer (990005942000205503 = no offer)
                if(!!newValue && !!newValue.currency){
                    this.$timeout(() => {
                        // do it only once
                        let hintContainer = document.querySelector('.eth-rapido-ethmember-hint');
                        if(hintContainer){
                            angular.element(hintContainer).remove()
                        };
                        // Is there a digi button
                        let digiButtonTitle = document.querySelector('prm-opac [translate="AlmaDigitization"]');
                        let digiListItem = document.querySelector('alma-howovp [translate="AlmaDigitization"]');
                        let digiButtonItem = document.querySelector('prm-opac [translate="AlmaItemDigitization"]');
                        let html = '';
                        // 990044776050205503, 990002775510205503, 990002903850205503, 990041656210205503
                        if(digiButtonTitle || digiListItem || digiButtonItem){
                            html = `
                            <div class="eth-rapido-ethmember-hint">
                            <span>{{::$ctrl.ethConfigService.getLabel($ctrl.config, \'hint1\')}}</span>
                            <md-button class="md-primary eth-rapido-ethmember-hint__button" ng-click="$ctrl.scrollToGetit()">
                                ${this.ethConfigService.getLabel(this.config, 'linktext')}
                            </md-button>
                            <span>{{::$ctrl.ethConfigService.getLabel($ctrl.config, \'hint2\')}}</span>
                            </div>
                            `;
                        }
                        // no digi button visible
                        else{
                            // prm-opac, not alma-htgi-svc; not 991145621719705501
                            let location = document.querySelector('prm-opac prm-location');
                            // 990000799440205503, 990045844730205503 only request button, no digi button
                            let requestButtonTitle = document.querySelector('prm-opac [translate="AlmaRequest"]');
                            if(location && !requestButtonTitle){
                                html = `
                                <div class="eth-rapido-ethmember-hint">
                                {{::$ctrl.ethConfigService.getLabel($ctrl.config, \'hintCheck1\')}}
                                <md-button class="md-primary eth-rapido-ethmember-hint__button" ng-click="$ctrl.scrollToGetit()">
                                    ${this.ethConfigService.getLabel(this.config, 'linktextCheck')}
                                </md-button>{{::$ctrl.ethConfigService.getLabel($ctrl.config, \'hintCheck2\')}}
                                </div>
                                `;
                            }
                        }
                        if(html != ''){
                            let digitalTile = document.querySelectorAll('#rapidoOffer div.full-view-section-content prm-full-view-service-container prm-service-physical-best-offer .offer_details-wrapper');
                            angular.element(digitalTile).prepend(this.$compile(html)(this.$scope));
                        }
                    }, 1500);
                }
            }, true);
        }
        catch(e){
            console.error("***ETH*** an error occured: ethRapidoEthmemberHintController.onInit \n\n");
            console.error(e.message);
        }
    }

    // cdi_swepub_primary_oai_DiVA_org_uu_94377
    // cdi_webofscience_primary_000396575500006
    // if there are getit: getit has id getit_link1_0
    // if there are viewit and getit: viewit has id getit_link1_0, getit has id getit_link1_1
    // --> ethSetIdController sets id for getit heading
    scrollToGetit(){
        this.$anchorScroll('eth-getit-heading')
        return true;
    }

}

ethRapidoEthmemberHintController.$inject = ['$scope', '$compile', 'ethSessionService', 'ethConfigService', 'ethRapidoEthmemberHintConfig', '$timeout', '$anchorScroll'];
