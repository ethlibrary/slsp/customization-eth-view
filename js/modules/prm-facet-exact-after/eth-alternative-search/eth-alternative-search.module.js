/**
* @ngdoc module
* @name ethAlternativeSearchModule
*
* @description
*
* add facet group "Send my Search to" (e.g. Open Knowledge Map)
*
* <b>AngularJS Dependencies</b><br>
* Service /js/services {@link ETH.ethConfigService}<br>
* Service /js/services {@link ETH.ethAlternativeSearchConfig}<br>
*
* <b>CSS/Image Dependencies</b><br>
* CSS eth-alternative-search.css
* IMG /img/Logo_ConnectedPapers.jpg, img/Logo_Dimensions.jpg, img/Logo_Open_Knowledge_Maps.jpg,
*
*/
import {ethConfigService} from '../../../services/eth-config.service';
import {ethAlternativeSearchConfig} from './eth-alternative-search.config';
import {ethAlternativeSearchHtml} from './eth-alternative-search.html';
import {ethAlternativeSearchController} from './eth-alternative-search.controller';

export const ethAlternativeSearchModule = angular
    .module('ethAlternativeSearchModule', [])
        .factory('ethConfigService', ethConfigService)
        .factory('ethAlternativeSearchConfig', ethAlternativeSearchConfig)
        .controller('ethAlternativeSearchController', ethAlternativeSearchController)
        .component('ethAlternativeSearchComponent',{
            require: {prmFacet: '^^prmFacet' },
            restrict: 'E',
            bindings: {afterCtrl: '<'},
            controller: 'ethAlternativeSearchController',
            template: ethAlternativeSearchHtml
        })
