export class ethComposeEraraController {

    constructor( $scope, $compile, $timeout, ethComposeEraraService, ethConfigService, ethComposeEraraConfig ) {
        this.$scope = $scope;
        this.$compile = $compile;
        this.$timeout = $timeout;
        this.ethComposeEraraService = ethComposeEraraService;
        this.ethConfigService = ethConfigService;
        this.config = ethComposeEraraConfig;
    }

    $onInit() {
        try{
            // Online: 99117338116605503
            // Print: 990042488650205503
            // e-maps: 99117999030205503
            this.parentCtrl = this.afterCtrl.parentCtrl;
            if(!this.parentCtrl.item.pnx.display.mms){
                return;
            }
            let mmsid = this.parentCtrl.item.pnx.display.mms[0];
            this.mmsIdOnlineEMapRecord = null;
            this.mmsIdOnlineEraraRecord = null;
            this.mmsIdPrintEraraRecord = null;
            this.emapsUrl = null;

            // check if this a emap record and there is a erara record
            if(this.parentCtrl.item.pnx.display.type[0]==='map' && this.parentCtrl.item.pnx.display.lds50){
                this.parentCtrl.item.pnx.display.lds50.forEach(i => {
                    if(i.indexOf('E01emaps')>-1){
                        this.ethComposeEraraService.getEraraRecordForEMap(mmsid)
                            .then(data => {
                                if(data && data[0] && data[0]._fields && data[0]._fields.length > 1){
                                    this.mmsIdPrintEraraRecord = data[0]._fields[1];
                                    this.ethComposeEraraService.getOnlineEraraRecord(this.mmsIdPrintEraraRecord)
                                        .then(data => {
                                            if(data && data.docs && data.docs.length > 0){
                                                this.mmsIdOnlineEraraRecord = data.docs[0].pnx.control.sourcerecordid[0];
                                            }
                                        })
                                        .catch(e => {
                                            console.error("***ETH*** an error occured: ethComposeEraraController getOnlineEraraRecord1");
                                            console.error(e.message);
                                        })
                                }
                            })
                            .catch(e => {
                                console.error("***ETH*** an error occured: ethComposeEraraController getEraraRecordForEMap");
                                console.error(e.message);
                            })
                    }
                })
            }

            // check if this is a print erara resource and there is an online emaps record
            // or a erara record
            if(this.parentCtrl.item.pnx.control.sourcesystem[0]==='ILS'){
                if(mmsid.substr(mmsid.length-4,4)==='5503'){
                    if(this.parentCtrl.item.pnx.display.type[0]==='map'){
                        this.ethComposeEraraService.getEMapsRecord(mmsid)
                            .then(data => {
                                if(data && data[0] && data[0]._fields && data[0]._fields.length > 1){
                                    this.mmsIdOnlineEMapRecord = data[0]._fields[0];
                                    this.emapsUrl = data[0]._fields[1];
                                    this.insertAfterBriefResult(this.emapsUrl);
                                }
                            })
                            .catch(e => {
                                console.error("***ETH*** an error occured: ethComposeEraraController getEMapsRecord 1");
                                console.error(e.message);
                            })
                    }
                    this.ethComposeEraraService.getOnlineEraraRecord(mmsid)
                        .then(data => {
                            if(data && data.docs && data.docs.length > 0){
                                this.mmsIdOnlineEraraRecord = data.docs[0].pnx.control.sourcerecordid[0];
                            }
                        })
                        .catch(e => {
                            console.error("***ETH*** an error occured: ethComposeEraraController getOnlineEraraRecord 2");
                            console.error(e.message);
                        })
                }
            }

            // check if this is a digital erara resource and there is an print erara record of ETH
            // or emaps record
            if(this.parentCtrl.item.pnx.control.sourcesystem[0]==='Other' && this.parentCtrl.item.pnx.display.lds09){
                //"MMS_ID_PRINT_990042488650205503"
                this.parentCtrl.item.pnx.display.lds09.forEach(i => {
                    if(i.indexOf('MMS_ID_PRINT_') >-1 && i.substr(i.length-4,4)==='5503'){
                        this.mmsIdPrintEraraRecord = i.substring(i.indexOf('MMS_ID_PRINT_') + 13);
                        if(this.parentCtrl.item.pnx.display.type[0]==='map'){
                            this.ethComposeEraraService.getEMapsRecord(this.mmsIdPrintEraraRecord)
                                .then(data => {
                                    if(data && data[0] && data[0]._fields && data[0]._fields.length > 1){
                                        this.mmsIdOnlineEMapRecord = data[0]._fields[0];
                                        this.emapsUrl = data[0]._fields[1];
                                        this.insertAfterBriefResult(this.emapsUrl);
                                    }
                                })
                                .catch(e => {
                                    console.error("***ETH*** an error occured: ethComposeEraraController getEMapsRecord 2");
                                    console.error(e.message);
                                })
                            }
                    }
                })
            }
        }
        catch(e){
            console.error("***ETH*** an error occured: ethComposeEraraController $onInit\n\n");
            console.error(e.message);
            throw(e);
        }
    }

    loadRecord(mmsid) {
        let vid = window.appConfig.vid;
        let tab = window.appConfig['primo-view']['available-tabs'][0];
        let lang = this.ethConfigService.getLanguage();
        let url = `/discovery/fulldisplay?lang=${lang}&tab=${tab}&vid=${vid}&docid=alma${mmsid}`;
        location.href = url;
    }
// http://localhost:8003/discovery/fulldisplay?lang=de&tab=discovery_network&vid=41SLSP_ETH:ETH&docid=alma990017839020205508
// http://localhost:8003/discovery/fulldisplay?docid=alma991060436969705501&context=L&vid=41SLSP_ETH:ETH&lang=de&search_scope=DiscoveryNetwork&adaptor=Local%20Search%20Engine&tab=discovery_network&query=any,contains,990017839020205508
    insertAfterBriefResult(url){
        this.$timeout(() => {
            if(!document.getElementById('eth-geotiff-link')){
                let briefResultAfter = document.querySelector('prm-full-view-service-container prm-brief-result-after');
                //let availabilityLine = document.querySelector('prm-full-view-service-container prm-search-result-availability-line');
                let linktext = this.ethConfigService.getLabel(this.config, 'onlineGeoTIFF');
                let html = `
                <div><a id="eth-geotiff-link" class="eth-geotiff-link arrow-link md-primoExplore-theme" ng-href="${url}" ng-click="$event.stopPropagation();" target="_blank">
                    <span>${linktext}</span>
                    <prm-icon external-link icon-type="svg" svg-icon-set="primo-ui" icon-definition="open-in-new"></prm-icon>
                    <prm-icon link-arrow external-link icon-type="svg" svg-icon-set="primo-ui" icon-definition="chevron-right"></prm-icon>
                </a></div>
                `;
                angular.element(briefResultAfter).append(this.$compile(html)(this.$scope));
            }
        }, 300);

    }
}

ethComposeEraraController.$inject = ['$scope', '$compile', '$timeout', 'ethComposeEraraService','ethConfigService', 'ethComposeEraraConfig' ];
