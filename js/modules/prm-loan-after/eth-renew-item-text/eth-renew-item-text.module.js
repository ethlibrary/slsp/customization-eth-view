/**
* @ngdoc module
* @name ethRenewItemTextModule
*
* @description
* display "not renewable" messages in list of loans
*
* <b>AngularJS Dependencies</b><br>
*
* <b>CSS/Image Dependencies</b><br>
* CSS eth-renew-item-text.css
*
*
*/
import {ethRenewItemTextController} from './eth-renew-item-text.controller';
import {ethRenewItemTextHtml} from './eth-renew-item-text.html';

export const ethRenewItemTextModule = angular
   .module('ethRenewItemTextModule', [])
       .controller('ethRenewItemTextController', ethRenewItemTextController)
       .component('ethRenewItemTextComponent', {
           bindings: {afterCtrl: '<'},
           controller: 'ethRenewItemTextController',
           template: ethRenewItemTextHtml
       })
