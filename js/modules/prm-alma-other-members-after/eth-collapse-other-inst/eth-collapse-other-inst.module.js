    import {ethCollapseOtherInstController} from './eth-collapse-other-inst.controller';

    export const ethCollapseOtherInstModule = angular
        .module('ethCollapseOtherInstModule', [])
            .controller('ethCollapseOtherInstController', ethCollapseOtherInstController)
            .component('ethCollapseOtherInstComponent', {
                bindings: {afterCtrl: '<'},
                controller: 'ethCollapseOtherInstController'
            })
