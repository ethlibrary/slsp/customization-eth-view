export class ethTocLinkController {

    constructor(ethTocLinkService, ethConfigService, ethTocLinkConfig, $timeout) {
        this.ethTocLinkService = ethTocLinkService;
        this.ethConfigService = ethConfigService;
        this.config = ethTocLinkConfig;
        this.$timeout = $timeout;
    } 

    $onInit() {
        try{
            this.parentCtrl = this.afterCtrl.parentCtrl;
            this.deliveryLinks = [];
            this.dnbLinks = [];

            // render links only inside FullView
            if(!this.parentCtrl.isFullView)return;

            // get links from item.delivery
            // and filter for 'linktorsrc'
            // "addlink" ?
            let deliveryRsrcLinks = [];
            if(this.parentCtrl.item && this.parentCtrl.item.delivery && this.parentCtrl.item.delivery.link){
                deliveryRsrcLinks = this.parentCtrl.item.delivery.link.filter(l => {
                    return l.linkType === 'linktorsrc' || l.linkType === 'addlink';
                })
            }
            // push 'linktorsrc' links to this.deliveryLinks
            deliveryRsrcLinks.forEach( l => {
                if(l.linkURL && l.linkURL != ''){
                    this.deliveryLinks.push({"label": l.displayLabel, "uri": l.linkURL})
                }
            })
            // TOC from dnb sru (by isbn)
            // is there already a TOC link and is there an isbn?
            if(deliveryRsrcLinks.length > 0 || !this.parentCtrl.item.pnx.addata.isbn || this.parentCtrl.item.pnx.addata.isbn.length === 0){
                return;
            }
            let isbns = this.parentCtrl.item.pnx.addata.isbn;
            for(let i = 0; i < isbns.length; i++){
                // request toc
                this.ethTocLinkService.getTOC(isbns[i])
                    .then((data) => {
                        try{
                            if(!!data && !!data.links) {
                                data.links.forEach(l => {
                                    let already = this.dnbLinks.filter(i => {
                                        return l.uri === i.uri;
                                    })
                                    if(already.length === 0){
                                        this.dnbLinks.push({"label":l.partOfResource, "uri":l.uri, "isbn": isbns[i]});
                                    }
                                }) 
                            }
                        }
                        catch(e){
                            console.error("***ETH*** an error occured: ethTocLinkController:ethTocLinkService.getTOC(): \n\n");
                            console.error(e.message);
                        }
                    })
                }
            // copy dnb toc link also to link section
            this.$timeout(() => {
                let dnbNode = document.querySelector(".eth-toc-dnb-link");
                if(dnbNode){
                    let clonedNode = dnbNode.cloneNode(true);
                    let serviceLinksAfter = document.querySelector("prm-service-links-after");
                    if(clonedNode && serviceLinksAfter){
                        serviceLinksAfter.prepend(clonedNode);
                    }
                }
            }, 1000);
        }
        catch(e){
            console.error("***ETH*** an error occured: ethTocLinkController.onInit()\n\n");
            console.error(e.message);
        }
    }
}

ethTocLinkController.$inject = ['ethTocLinkService', 'ethConfigService', 'ethTocLinkConfig', '$timeout'];
