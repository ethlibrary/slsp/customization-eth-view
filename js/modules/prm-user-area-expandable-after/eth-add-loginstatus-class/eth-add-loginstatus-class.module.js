    import {ethAddLoginstatusClassController} from './eth-add-loginstatus-class.controller';
    import {ethSessionService} from '../../../services/eth-session.service';

    export const ethAddLoginstatusClassModule = angular
        .module('ethAddLoginstatusClassModule', [])
            .factory('ethSessionService', ethSessionService)
            .controller('ethAddLoginstatusClassController', ethAddLoginstatusClassController)
            .component('ethAddLoginstatusClassComponent',  {
                bindings: {afterCtrl: '<'},
                controller: 'ethAddLoginstatusClassController',

            })
