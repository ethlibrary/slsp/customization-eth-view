export class ethPersonsSuggestionController {

    constructor( $timeout, ethConfigService, ethPersonsSuggestionConfig, ethPersonsSuggestionService ) {
        this.$timeout = $timeout;
        this.ethConfigService = ethConfigService;
        this.config = ethPersonsSuggestionConfig;
        this.ethPersonsSuggestionService = ethPersonsSuggestionService;
    }

    $onInit() {
        try{
            this.parentCtrl = this.afterCtrl.afterCtrl.parentCtrl;
            this.exploreCtrl = this.afterCtrl;

            // this.persons: object array of persons
            this.tempPersons = [];
            this.persons = [];

            this.lang = this.ethConfigService.getLanguage();
            let query = this.parentCtrl.searchService.searchUtil.$stateParams.query;
            // this.search: input in Primo searchField
            this.search = query.substring(query.indexOf('contains,') + 9);

            this.getPersons(this.search);
        }
        catch(e){
            console.error("***ETH*** an error occured: ethPersonsSuggestionController.onInit()\n\n");
            console.error(e.message);
        }
    }

    getPersons(search){
        this.exploreCtrl.hasPersons = 'loading';
        this.ethPersonsSuggestionService.getPersonsFromWikidata(search, this.lang)
            .then((data) => {
                if(!data || !data.results || !data.results.bindings || data.results.bindings.length === 0){
                    this.exploreCtrl.hasPersons = 'no';
                    return null;
                }
                let lastItemValue = '';
                data.results.bindings.forEach(e => {
                    if(e.item.value != lastItemValue && e.item.value.indexOf(e.itemLabel.value) === -1){
                        // set properties of person object array
                        this.tempPersons.push({
                            'qid':e.item.value.substring(e.item.value.lastIndexOf('/')+1),
                            'label':e.itemLabel.value,
                            'description':e.itemDescription ? e.itemDescription.value : null,
                            'image':e.image ? e.image.value : null,
                            'birth':e.birth ? e.birth.value.substring(0,e.birth.value.indexOf('T')) : null,
                            'gndid':e.gnd ? e.gnd.value : null
                        });
                        lastItemValue = e.item.value;
                    }
                });
                if(data.results.bindings.length === 0){
                    this.exploreCtrl.hasPersons = 'no';
                    return;
                }
                this.exploreCtrl.hasPersons = 'yes';
                this.tempPersons = this.tempPersons.slice(0,50);
                // person list should not be rendered before primo results
                if(this.parentCtrl.itemlist && this.parentCtrl.itemlist.length === 0){
                    this.$timeout(() => {
                        this.persons = this.tempPersons;
                    }, 1500);
                }
                else{
                    this.persons = this.tempPersons;
                }
            })
            .catch( (e) => {
                console.error("***ETH*** an error occured: ethPersonsSuggestionController.getPersons");
                console.error(e.message);
            })
    }

    loadPersonPage(qid){
        let query = '[wd/person]' + qid;
        let vid = window.appConfig.vid;
        let tab = '';
        if(this.parentCtrl.searchService.searchUtil.$stateParams.tab){
            tab = this.parentCtrl.searchService.searchUtil.$stateParams.tab;
        }
        else{
            tab = window.appConfig['primo-view']['available-tabs'][0];
        }
        let scope = '';
        if(this.parentCtrl.searchService.searchUtil.$stateParams['search_scope']){
            scope = this.parentCtrl.searchService.searchUtil.$stateParams['search_scope'];
        }
        else{
            scope = window.appConfig['primo-view']['scopes'][0]['scope-id'];
        }
        let url = `/discovery/search?search_scope=${scope}&tab=${tab}&query=any,contains,${query}&vid=${vid}&lang=${this.lang}&explore=person`;
        // matomo
        if(!window._paq){
            window._paq = [];
        }
        window._paq.push(['setCustomUrl', 'https://eth.swisscovery.slsp.ch/explore/person']);
        window._paq.push(['trackPageView']);
        location.href = url;
    }

}
ethPersonsSuggestionController.$inject = ['$timeout', 'ethConfigService','ethPersonsSuggestionConfig', 'ethPersonsSuggestionService'];
