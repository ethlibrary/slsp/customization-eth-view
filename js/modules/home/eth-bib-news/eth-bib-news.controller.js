export class ethBibNewsController {

    constructor( $sce, ethConfigService, ethBibNewsConfig, ethBibNewsService ) {
        this.$sce = $sce;
        this.ethConfigService = ethConfigService;
        this.config = ethBibNewsConfig;
        this.ethBibNewsService = ethBibNewsService;
        this.lang = this.ethConfigService.getLanguage();
    }

    $onInit() {
        try{
            this.ethBibNewsService.getNews(this.lang)
                .then((data) => {
                    try{
                        this.allNewsUrl = data.link;
                        this.news = data.entries;
                        this.news.forEach(n => {
                            if(n.appjson && n.appjson.indexOf('library.ethz.ch')> -1){
                                n.image = n.appjson.replace('library.ethz.ch','aem-newsimage-redirector.replit.app');
                            }
                        })
                    }
                    catch(e){
                        console.error("***ETH*** an error occured: ethBibNewsController ethBibNewsService.getNews(): \n\n");
                        console.error(e.message);
                    }
                });
        }
        catch(e){
            console.error("***ETH*** an error occured: ethBibNewsController.onInit()\n\n");
            console.error(e.message);
        }
    }
    decodeHtmlEntities(text) {
        var div = document.createElement('div');
        div.innerHTML = text;
        return this.$sce.trustAsHtml(div.textContent);
 		//return  text ? String(text).replace(/&nbsp;/gm, ' ') : '';
    };
}

ethBibNewsController.$inject = ['$sce', 'ethConfigService', 'ethBibNewsConfig', 'ethBibNewsService' ];
