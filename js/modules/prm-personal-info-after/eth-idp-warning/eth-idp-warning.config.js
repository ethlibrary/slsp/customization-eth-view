export const ethIdpWarningConfig = function(){
    return {
        label: {
            text1: {
                de: 'Verknüpfen Sie Ihre SWITCH edu-ID mit Ihrem ETH-Benutzerkonto in',
                en: 'Link your SWICTH edu-ID to your ETH Zurich user account in'
            },
            text2: {
                de: 'www.password.ethz.ch',
                en: 'www.password.ethz.ch'
            },
            text3: {
                de: ', um von den Vorteilen für ETH-Angehörige zu profitieren.',
                en: 'to benefit from the advantages for members of ETH Zurich.'
            },
            text4: {
                de: 'Anleitung',
                en: 'Instructions'
            }
        },
        url: {
            password: {
                de: 'https://www.password.ethz.ch/',
                en: 'https://www.password.ethz.ch/authentication/login_en.html'
            },
            help: {
                de: 'https://library.ethz.ch/ausleihen-und-bestellen/medien-suchen-finden-und-nutzen/swisscovery.html#registrierung-eth',
                en: 'https://library.ethz.ch/en/borrowing-and-ordering/searching-for-finding-and-using-media/swisscovery.html#anleitung-registration-eth'
            }
        }
    }
}
