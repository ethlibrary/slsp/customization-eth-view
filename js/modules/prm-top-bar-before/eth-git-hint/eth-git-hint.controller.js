export class ethGitHintController {

    constructor($scope, $compile, ethGitHintService ) {
        this.$scope = $scope;
        this.$compile = $compile;
        this.ethGitHintService = ethGitHintService;
    }

    $onInit() {
        try{
            this.ethGitHintService.getMessage()
                .then((data) => {
                    try{
                        if(!data || !data.de){
                            return;
                        }
                        this.data = data;
                        this.lang = angular.element(document.querySelector('primo-explore')).injector().get('$rootScope').$$childHead.$ctrl.userSessionManagerService.getUserLanguage() ||
                            window.appConfig['primo-view']['attributes-map'].interfaceLanguage;
                        let topBarBefore = document.querySelector('prm-top-bar-before');
                        let html = `
                        <div class="eth-git-topbar" layout="row">
                            <!--svg fill="#a8322d" height="24" viewBox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg">
                                <path d="M0 0h24v24H0V0z" fill="none"/>
                                <path d="M11 15h2v2h-2zm0-8h2v6h-2zm.99-5C6.47 2 2 6.48 2 12s4.47 10 9.99 10C17.52 22 22 17.52 22 12S17.52 2 11.99 2zM12 20c-4.42 0-8-3.58-8-8s3.58-8 8-8 8 3.58 8 8-3.58 8-8 8z"/>
                            </svg-->
                            <span ng-bind-html="$ctrl.data[$ctrl.lang]"></span>
                        </div>
                        `;
                        angular.element(topBarBefore).prepend(this.$compile(html)(this.$scope));
                    }
                    catch(e){
                        console.error("***ETH*** an error occured: ethGitHintController getMessage() callback \n\n");
                        console.error(e.message);
                        throw (e)
                    }
                })
        }
        catch(e){
            console.error("***ETH*** an error occured: ethGitHintController\n\n");
            console.error(e.message);
        }
    }
}
ethGitHintController.$inject = ['$scope', '$compile', 'ethGitHintService'];
