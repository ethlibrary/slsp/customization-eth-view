/**
* @ngdoc module
* @name ethComposeEraraModule
*
* @description
*
* Customization for the links section:<br>
* - render links for erara online and print resources
*
* <b>AngularJS Dependencies</b><br>
* Service {@link ETH.ethComposeEraraService}<br>
* Service /js/services {@link ETH.ethConfigService}<br>
* Service {@link ETH.ethComposeEraraConfig}<br>
*
*/
import {ethConfigService} from '../../../services/eth-config.service';
import {ethComposeEraraConfig} from './eth-compose-erara.config';
import {ethComposeEraraController} from './eth-compose-erara.controller';
import {ethComposeEraraHtml} from './eth-compose-erara.html';
import {ethComposeEraraService} from './eth-compose-erara.service';

export const ethComposeEraraModule = angular
    .module('ethComposeEraraModule', [])
        .factory('ethConfigService', ethConfigService)
        .factory('ethComposeEraraConfig', ethComposeEraraConfig)
        .factory('ethComposeEraraService', ethComposeEraraService)
        .controller('ethComposeEraraController', ethComposeEraraController)
        .component('ethComposeEraraComponent', {
            bindings: {afterCtrl: '<'},
            controller: 'ethComposeEraraController',
            template: ethComposeEraraHtml
        })
