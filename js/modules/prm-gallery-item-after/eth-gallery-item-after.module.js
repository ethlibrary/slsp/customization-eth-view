import {ethAddAuthorDateModule} from './eth-add-author-date/eth-add-author-date.module';

export const ethGalleryItemAfterModule = angular
    .module('ethGalleryItemAfterModule', [])
        .component('prmGalleryItemAfter',  {
            bindings: {parentCtrl: '<'},
            template: `<eth-add-author-date-component after-ctrl="$ctrl"></eth-add-author-date-component>`
        });

ethGalleryItemAfterModule.requires.push(ethAddAuthorDateModule.name);
