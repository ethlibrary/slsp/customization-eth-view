/**
* @ngdoc module
* @name ethTocLinkModule
*
* @description
*
* - eventually render a link to a resource like TOC or e-rara document
* - checks beacon.findbuch for a TOC (by ISBN)
*
* <b>AngularJS Dependencies</b><br>
* Service {@link ETH.ethTocLinkService}<br>
* Service /js/services {@link ETH.ethConfigService}<br>
*
*
* <b>CSS/Image Dependencies</b><br>
* CSS eth-toc-link.css
*
*
*/
import {ethTocLinkController} from './eth-toc-link.controller';
import {ethTocLinkHtml} from './eth-toc-link.html';
import {ethTocLinkService} from './eth-toc-link.service';
import {ethTocLinkConfig} from './eth-toc-link.config';
import {ethConfigService} from '../../../services/eth-config.service';

export const ethTocLinkModule = angular
    .module('ethTocLinkModule', [])
        .factory('ethTocLinkService', ethTocLinkService)
        .factory('ethConfigService', ethConfigService)
        .factory('ethTocLinkConfig', ethTocLinkConfig)
        .controller('ethTocLinkController', ethTocLinkController)
        .component('ethTocLinkComponent',  {
            bindings: {afterCtrl: '<'},
            controller: 'ethTocLinkController',
            template: ethTocLinkHtml
        })
