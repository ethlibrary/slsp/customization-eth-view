/**
* @ngdoc module
* @name ethRapidoOfferController
*
* @description
*
* changes for the rapido tile
* - if a physical item is not available, button is changed to a reservation button
* - for items of a reading room, a hint is added
* - for digital offers a text is added: max no pages 50 ...
*
* <b>AngularJS Dependencies</b><br>
* Service /js/services {@link ETH.ethConfigService}<br>
* Service {@link ETH.ethRapidoOfferConfig}<br>
*
* <b>CSS/Image Dependencies</b><br>
*
*/

import {ethRapidoOfferController} from './eth-rapido-offer.controller';

export const ethRapidoOfferModule = angular
    .module('ethRapidoOfferModule', [])
        .controller('ethRapidoOfferController', ethRapidoOfferController)
        .component('ethRapidoOfferComponent',  {
            bindings: {afterCtrl: '<'},
            controller: 'ethRapidoOfferController'
    })
