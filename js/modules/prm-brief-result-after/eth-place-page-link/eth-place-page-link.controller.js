export class ethPlacePageLinkController {

    constructor( ethConfigService, ethPlacePageLinkConfig, ethPlacePageLinkService ) {
        this.ethConfigService = ethConfigService;
        this.config = ethPlacePageLinkConfig;
        this.ethPlacePageLinkService = ethPlacePageLinkService;
    }

    $onInit() {
        try{
            this.parentCtrl = this.afterCtrl.parentCtrl;

            // render links only inside FullView
            if(!this.parentCtrl.isFullView)return;

            this.aPlacePageLinks = [];
            // GND ID
            if(this.parentCtrl.item.pnx.display.lds03 && this.parentCtrl.item.pnx.display.lds03.length > 0){
                let lds03 = this.parentCtrl.item.pnx.display.lds03;
                let aGndIds = [];
                this.placePages = [];
                for(let i = 0; i < lds03.length; i++){
                    // ALMA Ressources: link in value
                    if(lds03[i].indexOf('/gnd/') > -1){
                        let part = lds03[i].substring(lds03[i].indexOf('/gnd/') + 5);
                        part = part.substring(0, part.indexOf('">'));
                        if(part.indexOf('(DE-588)')>-1){
                            part = part.replace('(DE-588)','')
                        }
                        if(aGndIds.indexOf(part)===-1)aGndIds.push(part);
                    }
                    // External data: text
                    else if (lds03[i].indexOf(': ') > -1) {
                        let part = lds03[i].substring(lds03[i].lastIndexOf(': ') + 2);
                        if(part.indexOf('(DE-588)')>-1){
                            part = part.replace('(DE-588)','')
                        }
                        if(aGndIds.indexOf(part)===-1)aGndIds.push(part);
                    }
                }
                this.ethPlacePageLinkService.getPlaces(aGndIds)
                    .then((data) => {
                        try{
                            if(!data || !data.results || data.results.length === 0)return;
                            let vid = window.appConfig.vid;
                            let tab = window.appConfig['primo-view']['available-tabs'][0];
                            data.results.forEach(p => {
                                if(p.qid){
                                    let query = '[wd/place]' + p.qid;
                                    p.url = `/discovery/search?tab=${tab}&query=any,contains,${query}&vid=${vid}`;
                                    this.placePages.push(p);
                                }
                            })
                        }
                        catch(e){
                            console.error("***ETH*** an error occured: ethPlacePageLinkController:ethPlacePageLinkService.getPlaces(): \n\n");
                            console.error(e.message);
                        }
                    });
            }
        }
        catch(e){
            console.error("***ETH*** an error occured: ethPlacePageLinkController.onInit()\n\n");
            console.error(e.message);
        }
    }

}

ethPlacePageLinkController.$inject = ['ethConfigService', 'ethPlacePageLinkConfig', 'ethPlacePageLinkService'];
