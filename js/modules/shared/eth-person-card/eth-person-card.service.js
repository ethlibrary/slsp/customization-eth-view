/**
* @ngdoc service
* @name ethPersonCardService
*
* @description
*
* provides methods for response processing
*
* <b>Used by</b><br>
*
* Module {@link ETH.ethFullviewPersonModule}<br>
* Module {@link ETH.ethPersonModule}<br>
*
*
 */
export const ethPersonCardService = ['$http', function($http){    

    function processPrometheusResponse(prometheusResult, ethConfigService, config){
        try{
            let sourcesWhitelist = ["ba.e-pics.ethz.ch","performing-arts.eu",
                                    "/www.historische-kommission-muenchen-editionen.de/beacond/filmportal.php",
                                    "deutsche-biographie.de", "deutsche-digitale-bibliothek.de",
                                    "www.hdg.de/lemo/html/biografien/", "www.perlentaucher.de/autor/","archinform.net/gnd/",
                                    "www.gutenberg.org/ebooks/author/","archivdatenbank-online.ethz.ch/hsa/","geschichtsquellen.de"
                                    ];
            let whitelistedPrometheusLinks = [];
            let isDB = false;
            for(var j = 0; j < prometheusResult[0].resp[1].length; j++){
                let isWhitelisted = false;
                let url = prometheusResult[0].resp[3][j];
                // Für NDB und ADB nur einen Link
                if (url.indexOf("http://www.deutsche-biographie.de")>-1){
                    if(!isDB){
                        isDB = true;
                    }
                    else{
                        continue;
                    }
                }

                for(var k=0;k<sourcesWhitelist.length;k++){
                    if(url.indexOf(sourcesWhitelist[k])>-1)
                        isWhitelisted = true;
                }
                if (!isWhitelisted)
                        continue;
                let label = prometheusResult[0].resp[1][j];
                if (url.indexOf("deutsche-biographie.de")>-1) {
                        label = "Deutsche Biographie";
                }
                else if (url.indexOf("ba.e-pics.ethz.ch")>-1) {
                        label = "Bilder im E-Pics Bildarchiv";
                }
                else if (url.indexOf("archivdatenbank-online.ethz.ch/hsa/")>-1) {
                    label = "ETH Hochschularchiv";
                }
                else if (url.indexOf("www.gutenberg.org")>-1) {
                        label = "Projekt Gutenberg";
                }
                else if (url.indexOf("www.perlentaucher.de")>-1) {
                        label = "perlentaucher.de";
                }
                else if (url.indexOf("www.hdg.de")>-1) {
                        label = "LeMO Biographie";
                }
                else if (url.indexOf("archinform.net")>-1) {
                        label = "Architekturdatenbank archINFORM";
                }
                whitelistedPrometheusLinks.push({'url': url, 'label': label});
            }
            if(whitelistedPrometheusLinks.length > 0 && prometheusResult[0].gnd){
                whitelistedPrometheusLinks.push({'url': 'https://prometheus.lmu.de/gnd/' + prometheusResult[0].gnd , 'label': ethConfigService.getLabel(config, 'linkTextAllLMU')});
            }
            return whitelistedPrometheusLinks;
        }
        catch(e){
            console.error("***ETH*** an error occured: ethPersonCardsService.processPrometheusResponse(): \n\n");
            console.error(e.message);
            throw(e);
        }
    }

    function processEntityfactsResponse(entityfactsResult){
        try{
            let entityfacts = {};
            entityfacts.relatedPersons = [];
            if (entityfactsResult[0].resp['@type'] !== 'person')return null;
            if(entityfactsResult[0].resp.preferredName)entityfacts.preferredName = entityfactsResult[0].resp.preferredName;
            if(entityfactsResult[0].resp.biographicalOrHistoricalInformation)entityfacts.biographicalOrHistoricalInformation = entityfactsResult[0].resp.biographicalOrHistoricalInformation;
            if(entityfactsResult[0].resp.professionOrOccupation)entityfacts.professionOrOccupation = entityfactsResult[0].resp.professionOrOccupation[0].preferredName;
            if(entityfactsResult[0].resp.dateOfBirth)entityfacts.dateOfBirth = entityfactsResult[0].resp.dateOfBirth;
            if(entityfactsResult[0].resp.dateOfDeath)entityfacts.dateOfDeath = entityfactsResult[0].resp.dateOfDeath;
            if(entityfactsResult[0].resp.depiction)entityfacts.depiction = entityfactsResult[0].resp.depiction;
            if(entityfactsResult[0].resp.familialRelationship)entityfacts.relatedPersons = entityfacts.relatedPersons.concat(entityfactsResult[0].resp.familialRelationship);
            if(entityfactsResult[0].resp.relatedPerson)entityfacts.relatedPersons = entityfacts.relatedPersons.concat(entityfactsResult[0].resp.relatedPerson);
            entityfacts.relatedPersons = entityfacts.relatedPersons.map(p => {
                if(p['@id'])
                    p.gnd = p['@id'].substring(p['@id'].lastIndexOf('/')+1);
                else
                    p.gnd = null;
                return p;
            })
            entityfacts.relatedPersons = entityfacts.relatedPersons.filter(p => {
                if(p.preferredName.indexOf('Familie')>-1)return false;
                return p.gnd;
            })
            
            if(entityfactsResult[0].resp.placeOfBirth){
                entityfacts.placeOfBirth = entityfactsResult[0].resp.placeOfBirth[0];
                if(entityfactsResult[0].resp.placeOfBirth[0]['@id'])
                    entityfacts.placeOfBirth.gnd = entityfactsResult[0].resp.placeOfBirth[0]['@id'].substring(entityfactsResult[0].resp.placeOfBirth[0]['@id'].lastIndexOf('/')+1);
            }
            if(entityfactsResult[0].resp.placeOfDeath){
                entityfacts.placeOfDeath = entityfactsResult[0].resp.placeOfDeath[0];
                if(entityfactsResult[0].resp.placeOfDeath[0]['@id'])
                    entityfacts.placeOfDeath.gnd = entityfactsResult[0].resp.placeOfDeath[0]['@id'].substring(entityfactsResult[0].resp.placeOfDeath[0]['@id'].lastIndexOf('/')+1);
            }
            if(entityfactsResult[0].resp.placeOfActivity){
                entityfacts.placeOfActivity = [];
                entityfactsResult[0].resp.placeOfActivity.forEach( p => {
                    if(p['@id']){
                        p.gnd = p['@id'].substring(p['@id'].lastIndexOf('/')+1);
                        entityfacts.placeOfActivity.push(p)
                    }
                });
            }
            return entityfacts;
        }
        catch(e){
            console.error("***ETH*** an error occured: ethPersonCardsService.processEntityfactsResponse(): \n\n");
            console.error(e.message);
            throw(e);
        }
    }

    function processWikiResponse(wikiResult, ethConfigService, config, lang){
        try{
            if(!wikiResult[0] || !wikiResult[0].resp.results.bindings || wikiResult[0].resp.results.bindings.length === 0){
                return;
            }
            let wiki = {};
            let binding = wikiResult[0].resp.results.bindings[0];
            wiki.qid = binding.item ? binding.item.value.substring(binding.item.value.lastIndexOf('/')+1) : null;
            wiki.label = binding.itemLabel ? binding.itemLabel.value : null;
            wiki.image_url = binding.image ? binding.image.value : null;
            wiki.description = binding.itemDescription ? binding.itemDescription.value : null;
            wiki.gnd = binding.gnd ? binding.gnd.value : null;
            wiki.birth = binding.birth ? binding.birth.value : null;
            wiki.death = binding.death ? binding.death.value : null;
            wiki.birthplace = binding.birthplaceLabel ? binding.birthplaceLabel.value : null;
            wiki.deathplace = binding.deathplaceLabel ? binding.deathplaceLabel.value : null;
            wiki.aVariants = binding.aliasList ? binding.aliasList.value.split('|') : null;
            if(wiki.aVariants && wiki.aVariants.indexOf(wiki.label) === -1){
                wiki.aVariants.unshift(wiki.label);
            }
            wiki.links = [];
            //if(binding.gnd && binding.gnd.value)wiki.links.push({'url': 'http://persondata.toolforge.org/redirect/gnd/' + lang + '/' + binding.gnd.value , 'label': 'Wikipedia'});
            if(binding.item && binding.item.value)wiki.links.push({'url': binding.item.value, 'label': 'Wikidata'});
            if(binding.wc && binding.wc.value)wiki.links.push({'url': 'https://commons.wikimedia.org/wiki/Category:' + binding.wc.value , 'label': 'Wikimedia Commons'});
            if(binding.hls && binding.hls.value)wiki.links.push({'url': 'http://www.hls-dhs-dss.ch/textes/d/D' + binding.hls.value + '.php', 'label': 'Historisches Lexikon der Schweiz'});
            if(binding.oclc && binding.oclc.value)wiki.links.push({'url': 'https://entities.oclc.org/worldcat/entity/' + binding.oclc.value, 'label': 'WorldCat'});
            if(binding.ah && binding.ah.value)wiki.links.push({'url': 'https://katalog.arthistoricum.net/id/' + binding.ah.value, 'label': 'arthistoricum.net'});
            if(binding.kal && binding.kal.value)wiki.links.push({'url': 'https://kalliope-verbund.info/gnd/' + binding.kal.value, 'label': 'Kalliope-Verbund'});
            if(binding.gnd && binding.gnd.value)wiki.links.push({'url': 'https://d-nb.info/gnd/' + binding.gnd.value, 'label': 'GND (Gemeinsame Normdatei der Deutschen Nationalbibliothek)'});
            if(binding.sfa && binding.sfa.value)wiki.links.push({'url': 'https://www.swiss-archives.ch/archivplansuche.aspx?ID=' + binding.sfa.value, 'label': 'Schweizerisches Bundesarchiv'});
            if(binding.loc && binding.loc.value)wiki.links.push({'url': 'http://id.loc.gov/authorities/names/' + binding.loc.value + '.html', 'label': 'Library of Congress'});
            wiki.profiles = [];
            if(binding.orcid && binding.orcid.value)wiki.profiles.push({'url': 'https://orcid.org/' + binding.orcid.value, 'label': ethConfigService.getLabel(config, 'linkOrcid')});
            if(binding.scholar && binding.scholar.value)wiki.profiles.push({'url': 'https://scholar.google.com/citations?user=' + binding.scholar.value, 'label': ethConfigService.getLabel(config, 'linkScholar')});
            if(binding.scopus && binding.scopus.value)wiki.profiles.push({'url': 'https://www.scopus.com/authid/detail.uri?authorId=' + binding.scopus.value, 'label': ethConfigService.getLabel(config, 'linkScopus')});
            if(binding.researchgate && binding.researchgate.value)wiki.profiles.push({'url': 'https://www.researchgate.net/profile/' + binding.researchgate.value, 'label': ethConfigService.getLabel(config, 'linkResearchgate')});
            if(binding.dimension && binding.dimension.value)wiki.profiles.push({'url': 'https://app.dimensions.ai/discover/publication?and_facet_researcher=ur.' + binding.dimension.value, 'label': ethConfigService.getLabel(config, 'linkDimension')});
            return wiki;
        }
        catch(e){
            console.error("***ETH*** an error occured: ethPersonCardsService.processWikiResponse(): \n\n");
            console.error(e.message);
            throw(e);
        }
    }

    
    function processWikipediaResponse(wikiResult, lang){
        try{
            if(!wikiResult[0] || !wikiResult[0].resp.results.bindings || wikiResult[0].resp.results.bindings.length === 0 || !wikiResult[0].resp.results.bindings[0].wikipediaUrlList){
                return;
            }
            let strWikipediaUrls = wikiResult[0].resp.results.bindings[0].wikipediaUrlList.value;
            if(!strWikipediaUrls || strWikipediaUrls === '')return;
            let wikipediaUrls = strWikipediaUrls.split(';');
            //console.error(wikipediaUrls)
            let search = lang + '.wikipedia.org';
            let displayUrl = wikipediaUrls.filter(u => {
                return u.indexOf(search) > -1;
            })
            if(displayUrl.length > 0){
                return displayUrl[0];
            }
            else{
                search = 'en.wikipedia.org';
                displayUrl = wikipediaUrls.filter(u => {
                    return u.indexOf(search) > -1;
                })
                if(displayUrl.length > 0){
                    return displayUrl[0];
                }
                else{
                    search = '.wikipedia.org';
                    displayUrl = wikipediaUrls.filter(u => {
                        return u.indexOf(search) > -1;
                    })
                    if(displayUrl.length > 0){
                        return displayUrl[0];
                    }
                    else{
                        return null;
                    }
                }            
            }
        }
        catch(e){
            console.error("***ETH*** an error occured: ethPersonCardsService.processWikipediaResponse(): \n\n");
            console.error(e.message);
            throw(e);
        }
    }

    function processRelatedPersonsResponse(wikiResult){
        try{
            if(!wikiResult[0] || !wikiResult[0].resp.results.bindings || wikiResult[0].resp.results.bindings.length === 0){
                return;
            }
            let persons = [];
            let lastItemValue = '';
            for(let i = 0; i < wikiResult[0].resp.results.bindings.length; i++){
                let binding = wikiResult[0].resp.results.bindings[i];
                if(binding.item.value != lastItemValue && binding.item.value.indexOf(binding.itemLabel.value) === -1){
                    let person = {};
                    lastItemValue = binding.item.value;
                    person.item = binding.item ? binding.item.value : null;
                    person.qid = binding.item ? binding.item.value.substring(binding.item.value.lastIndexOf('/')+1) : null;
                    person.label = binding.itemLabel ? binding.itemLabel.value : null;
                    person.gnd = binding.gnd ? binding.gnd.value : null;
                    person.image_url = binding.image ? binding.image.value : null;
                    person.description = binding.itemDescription ? binding.itemDescription.value : null;
                    if(binding.teacherBirths){
                        person.birth = binding.teacherBirths.value;
                    }
                    else if(binding.studentBirths){
                        person.birth = binding.studentBirths.value;
                    }
                    else{
                        person.birth = null;
                    }
                    if(binding.teacherDeaths){
                        person.death = binding.teacherDeaths.value;
                    }
                    else if(binding.studentDeaths){
                        person.death = binding.studentDeaths.value;
                    }
                    else{
                        person.death = null;
                    }
                    persons.push(person);
                }
            }
            return persons;
        }
        catch(e){
            console.error("***ETH*** an error occured: ethPersonCardsService.processRelatedPersonsResponse(): \n\n");
            console.error(e.message);
            throw(e);
        }
    }

    function processwikiArchivesAtResponse(wikiResult){
        try{
            let wikiArchivesAtLinks = [];
            let bindings = wikiResult[0].resp.results.bindings;
            for(let i = 0; i < bindings.length; i++){
                let url = bindings[i].ref ? bindings[i].ref.value: null;
                let label =  bindings[i].archivedLabel ? bindings[i].archivedLabel.value : null;
                let inventoryno =  bindings[i].inventoryno ? bindings[i].inventoryno.value : null;
                wikiArchivesAtLinks.push({'url': url, 'label': label, 'inventoryno': inventoryno});
            }
            return wikiArchivesAtLinks;
        }
        catch(e){
            console.error("***ETH*** an error occured: ethPersonCardsService.processwikiArchivesAtResponse(): \n\n");
            console.error(e.message);
            throw(e);
        }
    }

    function processMetagridResponse(metagridResult, ethConfigService, config){
        try{
            let resources = metagridResult[0].resp.concordances[0].resources;
            let whitelistedMetagridLinks = [];
            let whitelistedMetagridLinksSorted = [];

            if (!!resources[0] && resources.length > 0) {
                let name = resources[0].last_name + ', ' + resources[0].first_name;

                for(var j = 0; j < resources.length; j++){
                    let resource = resources[j];
                    // https://api.metagrid.ch/providers.json
                    let slug = resource.provider.slug;
                    let url = resource.link.uri;
                    // check whitelist for Metagrid links
                    if (config.whitelistMetagrid.indexOf(slug) === -1) {
                        continue;
                    }
                    let label = ethConfigService.getLabel(config, slug);
                    whitelistedMetagridLinks.push({'slug': slug,'url': url, 'label': label});
                }
                // Dodis and HLS first
                let dodis = whitelistedMetagridLinks.filter(e => {
                    return e.slug === 'dodis';
                });
                whitelistedMetagridLinksSorted = whitelistedMetagridLinksSorted.concat(dodis);
                let hls = whitelistedMetagridLinks.filter(e => {
                    return e.slug === 'hls-dhs-dss';
                });
                whitelistedMetagridLinksSorted = whitelistedMetagridLinksSorted.concat(hls);
                let rest = whitelistedMetagridLinks.filter(e => {
                    return e.slug !== 'hls-dhs-dss' && e.slug !== 'dodis';
                });
                whitelistedMetagridLinksSorted = whitelistedMetagridLinksSorted.concat(rest);
            }
            return whitelistedMetagridLinksSorted;
        }
        catch(e){
            console.error("***ETH*** an error occured: ethPersonCardsService.processMetagridResponse(): \n\n");
            console.error(e.message);
            throw(e);
        }
    }

    function processPersonsResponse(results, ethConfigService, config, lang){
        try{
            if(!lang)lang = 'de';
            let person = {};
            person.gnd = "";
            let resultWithGnd = results.filter(e=>{
                return e.gnd && e.gnd != "";
            })
            if(resultWithGnd.length > 0){
                person.gnd = resultWithGnd[0].gnd;
            }
            // DNB Entityfacts
            let entityfactsResult = results.filter(e => {
                return e.provider === 'hub.culturegraph.org';
            });
            if(entityfactsResult.length > 0){
                person.entityfacts = this.processEntityfactsResponse(entityfactsResult);
            }

            // Metagrid
            let metagridResult = results.filter(e => {
                return e.provider === 'api.metagrid.ch';
            });
            if(metagridResult.length > 0 && metagridResult[0].resp.concordances && metagridResult[0].resp.concordances.length > 0){
                person.metagridLinks = this.processMetagridResponse(metagridResult, ethConfigService, config);
            }

            // prometheus.lmu
            let prometheusResult = results.filter(e => {
                return e.provider === 'prometheus.lmu.de';
            });
            if(prometheusResult.length > 0 && prometheusResult[0].resp && prometheusResult[0].resp[1]){
                person.prometheusLinks = this.processPrometheusResponse(prometheusResult, ethConfigService, config);
            }
            // Wikidata teachers
            let wikiTeacherResult = results.filter(e => {
                return e.provider === 'query.wikidata.org' && e.resp.head.vars.indexOf("teacherBirths") > -1;
            });
            if(wikiTeacherResult.length > 0){
                person.teachers = this.processRelatedPersonsResponse(wikiTeacherResult);
            }
            // Wikidata students
            let wikiStudentResult = results.filter(e => {
                return e.provider === 'query.wikidata.org' && e.resp.head.vars.indexOf("studentBirths") > -1;
            });
            if(wikiStudentResult.length > 0){
                person.students = this.processRelatedPersonsResponse(wikiStudentResult);
            }
            // Wikidata teachers
            let wikiWikipediaResult = results.filter(e => {
                return e.provider === 'query.wikidata.org' && e.resp.head.vars.indexOf("wikipediaUrlList") > -1;
            });
            if(wikiWikipediaResult.length > 0){
                person.wikipediaUrl = this.processWikipediaResponse(wikiWikipediaResult, lang);
            }
            // Wikidata bio and Links
            let wikiResult = results.filter(e => {
                return e.provider === 'query.wikidata.org' && e.resp.head.vars.indexOf("birth") > -1;
            });
            if(wikiResult.length > 0){
                person.wiki = this.processWikiResponse(wikiResult, ethConfigService, config, lang);
            }

            // Wikidata archives at
            let wikiArchivesAtResult = results.filter(e => {
                return e.provider === 'query.wikidata.org' && e.resp.head.vars.indexOf("refnode") > -1;
            });

            if(wikiArchivesAtResult.length > 0){
                person.wikiArchivesAtLinks = this.processwikiArchivesAtResponse(wikiArchivesAtResult);
            }
            // person page url
            let vid = window.appConfig.vid;
            let tab = window.appConfig['primo-view']['available-tabs'][0];
            if(person.wiki && person.wiki.qid && person.wiki.qid != ''){
                person.url = `/discovery/search?tab=${tab}&vid=${vid}&lang=${lang}&query=any,contains,[wd/person]` + person.wiki.qid + '&explore=person';
            }
            else if(person.gnd) {
                person.url = `/discovery/search?tab=${tab}&vid=${vid}&lang=${lang}&query=any,contains,[wd/person]` + person.gnd + '&explore=person';
            }
            return person;
        }
        catch(e){
            console.error("***ETH*** an error occured: ethPersonCardsService.processPersonsResponse(): \n\n");
            console.error(e.message);
            throw(e);
        }
    }

    function makePersonPageLink(identifier, lang){
        try{
            let query = '[wd/person]' + identifier;
            let vid = window.appConfig.vid;
            let tab = window.appConfig['primo-view']['available-tabs'][0];
            let url = `/discovery/search?query=any,contains,${query}&tab=${tab}&vid=${vid}&lang=${lang}&explore=person`;
            return url;
        }
        catch(e){
            console.error("***ETH*** an error occured: ethPersonCardsService.makePersonPageLink(): \n\n");
            console.error(e.message);
            throw(e);
        }
    }

    function makePlacePageLink(identifier, lang){
        try{
            let query = '[wd/place]' + identifier;
            let vid = window.appConfig.vid;
            let tab = window.appConfig['primo-view']['available-tabs'][0];
            let url = `/discovery/search?query=any,contains,${query}&tab=${tab}&vid=${vid}&lang=${lang}&explore=place`;
            return url;
        }
        catch(e){
            console.error("***ETH*** an error occured: ethPersonCardsService.makePlacePageLink(): \n\n");
            console.error(e.message);
            throw(e);
        }
    }

    function getGndByIdRef(idref){
        let url = 'https://daas.library.ethz.ch/rib/v3/persons/gnd/sudoc/' + idref;
        return $http.get(url)
            .then(
                function(response){
                    if(response.data && response.data.gnd){
                        return response.data.gnd;
                    }
                    else{
                        return null;
                    }
                },
                function(httpError){
                    if (httpError.status === 404)return null;
                    let error = "***ETH*** an error occured: ethPersonCardService.getGndByIdRef: " + httpError.status;
                    if (httpError.data && httpError.data.errorMessage) {
                        error += ' - ' + httpError.data.errorMessage;
                    }
                    console.error(error);
                    return null;
                }
            )
    }


    return {
        processWikiResponse: processWikiResponse,
        processwikiArchivesAtResponse: processwikiArchivesAtResponse,
        processRelatedPersonsResponse: processRelatedPersonsResponse,
        processWikipediaResponse: processWikipediaResponse,
        processPrometheusResponse: processPrometheusResponse,
        processEntityfactsResponse: processEntityfactsResponse,
        processMetagridResponse: processMetagridResponse,
        processPersonsResponse: processPersonsResponse,
        makePersonPageLink: makePersonPageLink,
        makePlacePageLink: makePlacePageLink,
        getGndByIdRef: getGndByIdRef        
    };
}]
