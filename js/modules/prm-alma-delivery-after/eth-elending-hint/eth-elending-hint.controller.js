export class ethElendingHintController {

    constructor( ethConfigService, ethElendingHintConfig ) {
        this.ethConfigService = ethConfigService;
        this.config = ethElendingHintConfig;
    }

    $onInit() {
        try{
            this.parentCtrl = this.afterCtrl.parentCtrl;
            this.source = '';

            if (!this.parentCtrl.item || !this.parentCtrl.item.pnx) {
                console.error("***ETH*** ethElendingHintController.$onInit: item or item.pnx not available");
                return;
            }

            if(this.parentCtrl.item.pnx.display.source && this.parentCtrl.item.pnx.display.source.length > 0){
                this.source = this.parentCtrl.item.pnx.display.source[0];
            }
        }
        catch(e){
            console.error("***ETH*** an error occured: ethElendingHintController $onInit\n\n");
            console.error(e.message);
        }
    }
}

ethElendingHintController.$inject = ['ethConfigService', 'ethElendingHintConfig' ];
