/**
* @ngdoc module
* @name ethBrowzineModule
*
* @description
*
* Customization for the availability line:<br>
* - fetch journal informations from browzine<br>
* - eventually renders a Link to the journal in browzine<br>
*
*
* <b>AngularJS Dependencies</b><br>
* Service {@link ETH.ethBrowzineConfig}<br>
* Service {@link ETH.ethBrowzineService}<br>
* Service /js/services {@link ETH.ethConfigService}<br>
* Service /js/services {@link ETH.ethSessionService}<br>
*
*
* <b>CSS/Image Dependencies</b><br>
* CSS eth-browzine.css
*
*
*/
import {ethConfigService} from '../../../services/eth-config.service';
import {ethSessionService} from '../../../services/eth-session.service';
import {ethBrowzineService} from './eth-browzine.service';
import {ethBrowzineConfig} from './eth-browzine.config';
import {ethBrowzineController} from './eth-browzine.controller';
import {ethBrowzineHtml} from './eth-browzine.html';

export const ethBrowzineModule = angular
    .module('ethBrowzineModule', [])
        .factory('ethConfigService', ethConfigService)
        .factory('ethSessionService', ethSessionService)
        .factory('ethBrowzineService', ethBrowzineService)
        .factory('ethBrowzineConfig', ethBrowzineConfig)
        .controller('ethBrowzineController', ethBrowzineController)
        .component('ethBrowzineComponent', {
            bindings: {afterCtrl: '<'},
            require: {
                briefResult: '^prmBriefResultContainer'
            },
            controller: 'ethBrowzineController',
            template: ethBrowzineHtml
        })
