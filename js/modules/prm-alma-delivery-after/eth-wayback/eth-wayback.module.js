/**
* @ngdoc module
* @name ethWaybackModule
*
* @description
*
* Changes link text of Wayback link (webarchive of HSA) in view online section
* and renders an additional hint.
*
* <b>AngularJS Dependencies</b><br>
* Service /js/services {@link ETH.ethConfigService}<br>
* Service {@link ETH.ethWaybackConfig}<br>
*
* <b>CSS/Image Dependencies</b><br>
* wayback.css
*
*/
import {ethConfigService} from '../../../services/eth-config.service';
import {ethWaybackConfig} from './eth-wayback.config';
import {ethWaybackController} from './eth-wayback.controller';

export const ethWaybackModule = angular
    .module('ethWaybackModule', [])
        .factory('ethConfigService', ethConfigService)
        .factory('ethWaybackConfig', ethWaybackConfig)
        .controller('ethWaybackController', ethWaybackController)
        .component('ethWaybackComponent', {
            bindings: {afterCtrl: '<'},
            controller: 'ethWaybackController'
        })
