export class ethFullviewPersonController {

    constructor( $scope, $compile, $timeout, ethConfigService, ethSessionService, ethPersonCardConfig, ethPersonCardService, ethFullviewPersonService ) {
        this.$scope = $scope;
        this.$compile = $compile;
        this.$timeout = $timeout;
        this.ethConfigService = ethConfigService;
        this.ethSessionService = ethSessionService;
        this.config = ethPersonCardConfig;
        this.ethPersonCardService = ethPersonCardService;
        this.ethFullviewPersonService = ethFullviewPersonService;
    }

    $onInit() {
        try{
            this.parentCtrl = this.afterCtrl.parentCtrl;
            this.personsDone = false;
            this.lang = this.ethConfigService.getLanguage();
            this.isFullView = true;
            this.persons = [];            
            let gndIds = [];

            // GND ID - lds03
            if(this.parentCtrl.item.pnx.display.lds03 && this.parentCtrl.item.pnx.display.lds03.length > 0){
                let lds03 = this.parentCtrl.item.pnx.display.lds03;
                for(let i = 0; i < lds03.length; i++){
                    // ALMA Ressources: link in value
                    if(lds03[i].indexOf('/gnd/') > -1){
                        let part = lds03[i].substring(lds03[i].indexOf('/gnd/') + 5);
                        part = part.substring(0, part.indexOf('">'));
                        if(part.indexOf('(DE-588)')>-1){
                            part = part.replace('(DE-588)','')
                        }
                        if(gndIds.indexOf(part)===-1)gndIds.push(part);
                    }
                    // External data: text
                    else if (lds03[i].indexOf(': ') > -1) {
                        let part = lds03[i].substring(lds03[i].lastIndexOf(': ') + 2);
                        if(part.indexOf('(DE-588)')>-1){
                            part = part.replace('(DE-588)','')
                        }
                        if(gndIds.indexOf(part)===-1)gndIds.push(part);
                    }
                }
                this.ethFullviewPersonService.getPersons(gndIds, this.lang)
                    .then((data) => {
                        try{
                            if(!data || !data.gnd)return;
                            for(let i = 0; i < data.gnd.length; i++){
                                let filteredResults = data.results.filter(e => {
                                    return e.gnd === data.gnd[i];
                                });
                                if(filteredResults.length > 0){
                                    let person = this.ethPersonCardService.processPersonsResponse(filteredResults, this.ethConfigService, this.config, this.lang);
                                    this.persons.push(person);
                                }
                            }
                            this.renderPersonLinks(this.persons);
                        }
                        catch(e){
                            console.error("***ETH*** an error occured: ethFullviewPersonController:ethFullviewPersonService.getPersons(): \n\n");
                            console.error(e.message);
                        }
                    });
            }

            // IdRef - lds90
            let idRefs = [];
            if((this.parentCtrl.item.pnx.display.lds90 && this.parentCtrl.item.pnx.display.lds90.length > 0)){
                let lds90 = this.parentCtrl.item.pnx.display.lds90;
                for(let i = 0; i < lds90.length; i++){
                    let part = lds90[i].substring(lds90[i].indexOf('idref.fr/') + 9);
                    part = part.substring(0, part.indexOf('">'));
                    if(idRefs.indexOf(part)===-1)idRefs.push(part);
                }
            }
            for(var i = 0; i < idRefs.length; i++){
                let idref = idRefs[i];
                if (idref === "")continue;
                // get gnd by idref (via viaf)
                this.ethPersonCardService.getGndByIdRef(idref)
                    .then((gnd) => {
                        if(gnd){
                            if(gndIds.indexOf(gnd)===-1){
                                gndIds.push(gnd);
                                this.ethFullviewPersonService.getPersons([gnd], this.lang)
                                    .then((data) => {
                                        try{
                                            if(!data || !data.gnd)return;
                                            for(let i = 0; i < data.gnd.length; i++){
                                                let filteredResults = data.results.filter(e => {
                                                    return e.gnd === data.gnd[i];
                                                });
                                                if(filteredResults.length > 0){
                                                    let person = this.ethPersonCardService.processPersonsResponse(filteredResults, this.ethConfigService, this.config, this.lang);
                                                    this.persons.push(person);
                                                    this.renderPersonLinks([person]);
                                                }
                                            }
                                        }
                                        catch(e){
                                            console.error("***ETH*** an error occured: ethFullviewPersonController:ethFullviewPersonService.getPersons(): \n\n");
                                            console.error(e.message);
                                        }
                                    })
                                }
                        }
                    })                
            }

        }
        catch(e){
            console.error("***ETH*** an error occured: ethFullviewPersonController.onInit()\n\n");
            console.error(e.message);
            throw(e);
        }
    }

    renderPersonLinks(persons){
        // insert a person page link
        try{
            let linktext1 = this.ethConfigService.getLabel(this.config, 'personPageLink');
            this.$timeout(() => {
                let serviceLinks = document.querySelector('prm-service-links .spaced-rows');
                persons.forEach(p => {
                    if(p.url && !document.getElementById('personpage' + p.gnd)){
                        let name = null;
                        if(p.wiki && p.wiki.label){
                            name = p.wiki.label;
                        }
                        else if(p.entityfacts && p.entityfacts.preferredName){
                            name = p.entityfacts.preferredName;
                        }
                        if(name){
                            let html = `
                            <div><a id="personpage${p.gnd}" class="eth-personpage-link arrow-link md-primoExplore-theme" ng-href="${p.url}" ng-click="$event.stopPropagation();">
                                <span>${linktext1} "${name}"</span>
                                <prm-icon link-arrow external-link icon-type="svg" svg-icon-set="primo-ui" icon-definition="chevron-right"></prm-icon>
                            </a></div>
                            `;
                            angular.element(serviceLinks).append(this.$compile(html)(this.$scope));
                        }
                    }
                })
            }, 300);
        }
        catch(e){
            console.error("***ETH*** an error occured: ethFullviewPersonController:renderPersonLinks:insert person page links: \n\n");
            console.error(e.message);
        }

    }
}

ethFullviewPersonController.$inject = ['$scope', '$compile', '$timeout', 'ethConfigService', 'ethSessionService', 'ethPersonCardConfig', 'ethPersonCardService', 'ethFullviewPersonService' ];
