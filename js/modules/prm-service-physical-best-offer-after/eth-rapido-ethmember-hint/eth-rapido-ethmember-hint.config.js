export const ethRapidoEthmemberHintConfig = function(){
    return {
        label: {
            hint1:{
                de: 'Kostenlose Digitalisierung – bitte',
                en: 'Digitization without costs – please use the'
            },
            linktext:{
                de: 'oberen Bestellbereich',
                en: 'upper ordering section'
            },
            hint2:{
                de: 'verwenden.',
                en: '.'
            },
            hintCheck1:{
                de: 'Überprüfen Sie bitte',
                en: 'Please check'
            },
            linktextCheck:{
                de: 'im oberen Bestellbereich',
                en: 'in the upper ordering section'
            },
            hintCheck2:{
                de: ', ob eine Bibliothek der ETH Zürich "Digitalisierung" anbietet (kostenloser Service).',
                en: ' if one of the libraries at ETH Zurich offers "Digitization" (free service).'
            }

        }
    }
}
