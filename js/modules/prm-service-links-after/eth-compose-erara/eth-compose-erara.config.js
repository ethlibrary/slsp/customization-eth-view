export const ethComposeEraraConfig = function(){
    return {
        label: {
            print:{
                de: 'Print-Exemplar in ETH-Bibliothek @ swisscovery',
                en: 'Print Item in ETH Library @ swisscovery'
            },
            online: {
                de: 'Online-Exemplar in ETH-Bibliothek @ swisscovery (e-rara)',
                en: 'Online Item in ETH Library @ swisscovery (e-rara)'
            },
            onlineEMap: {
                de: 'Georeferenziertes Online-Exemplar in ETH-Bibliothek @ swisscovery (e-maps)',
                en: 'Georeferenced online Item in ETH Library @ swisscovery (e-maps)'
            },
            onlineGeoTIFF: {
                de: 'GeoTIFF via e-maps',
                en: 'GeoTIFF via e-maps'
            }
        }
    }
}
