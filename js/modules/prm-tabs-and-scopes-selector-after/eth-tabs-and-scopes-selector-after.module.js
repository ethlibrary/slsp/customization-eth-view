import {ethChangeTabsModule} from './eth-change-tabs/eth-change-tabs.module';

export const ethTabsAndScopesSelectorAfterModule = angular
    .module('ethTabsAndScopesSelectorAfterModule', [])
        .component('prmTabsAndScopesSelectorAfter',  {
            template: '<eth-change-tabs-component after-ctrl="$ctrl"></eth-change-tabs-component>'
        });

ethTabsAndScopesSelectorAfterModule.requires.push(ethChangeTabsModule.name);
