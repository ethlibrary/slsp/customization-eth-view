/**
* @ngdoc module
* @name ethSketchfabLinkModule
*
* @description
*
* Customization for the brief result section:<br>
* - render links for sketchfab
*
*
*/
import {ethConfigService} from '../../../services/eth-config.service';
import {ethSketchfabLinkConfig} from './eth-sketchfab-link.config';
import {ethSketchfabLinkController} from './eth-sketchfab-link.controller';
import {ethSketchfabLinkHtml} from './eth-sketchfab-link.html';

export const ethSketchfabLinkModule = angular
    .module('ethSketchfabLinkModule', [])
        .factory('ethConfigService', ethConfigService)
        .factory('ethSketchfabLinkConfig', ethSketchfabLinkConfig)
        .controller('ethSketchfabLinkController', ethSketchfabLinkController)
        .component('ethSketchfabLinkComponent', {
            bindings: {afterCtrl: '<'},
            controller: 'ethSketchfabLinkController',
            template: ethSketchfabLinkHtml
        })
