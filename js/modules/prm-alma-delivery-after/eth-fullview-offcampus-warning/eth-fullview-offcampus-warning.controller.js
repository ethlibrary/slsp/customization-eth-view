export class ethFullviewOffcampusWarningController {

    constructor( $location, ethConfigService, ethSessionService, ethFullviewOffcampusWarningConfig ) {
        this.$location = $location;
        this.ethConfigService = ethConfigService;
        this.ethSessionService = ethSessionService;
        this.config = ethFullviewOffcampusWarningConfig;
        this.isOffCampusWarning = false;
    }

    $onInit() {
        try{
            this.parentCtrl = this.afterCtrl.parentCtrl;
            this.processDoCheck = false;
            this.deliveryCategory = [];

            if(this.parentCtrl.item.pnx.addata.openaccess && this.parentCtrl.item.pnx.addata.openaccess[0] === 'true'){
                return;
            }
            let delivery = this.parentCtrl.item.delivery;
            if(delivery && delivery.deliveryCategory && (delivery.deliveryCategory.indexOf('Alma-E') > -1 || delivery.deliveryCategory.indexOf('Remote Search Resource') > -1)){
                this.deliveryCategory = delivery.deliveryCategory;
                this.processDoCheck = true;
            }
        }
        catch(e){
            console.error("***ETH*** an error occured: ethFullviewOffcampusWarningController.$onInit\n\n");
            console.error(e.message);
            throw(e)
        }
    }

    $doCheck() {
        try{
            if(!this.processDoCheck){
                return;
            }
            let delivery = this.parentCtrl.item.delivery;
            //check if electronicServices are already loaded
            if(!delivery || !delivery.electronicServices || delivery.electronicServices.length === 0){
                return;
            }
            // check if 'Remote Search Resource' and not CDI
            if(delivery.deliveryCategory.indexOf('Remote Search Resource') > -1 && delivery.electronicServices[0].ilsApiId.indexOf("cdi_") === -1){
                this.processDoCheck = false;
                return;
            }
            // check if there is a non licenced ressource
            let aFilteredElectronicServices = delivery.electronicServices.filter( e => {
                if(e.publicNote && e.publicNote === "Onlinezugriff via World Wide Web"){
                    return true;
                }
            })
            if(aFilteredElectronicServices.length > 0){
                this.processDoCheck = false;
                return;
            }

            this.isOffCampusWarning = this.evaluateIsOffCampusWarning();
            this.processDoCheck = false;
        }
        catch(e){
            console.error("***ETH*** an error occured: ethFullviewOffcampusWarningController.$doCheck\n\n");
            console.error(e.message);
            throw(e)
        }
    }

    evaluateIsOffCampusWarning(){
        // JWT not available yet
        if (!this.ethSessionService.getDecodedToken())return false;
        if (this.ethSessionService.isOnCampus()) {
            return false;
        }
        return true;
    }
}

ethFullviewOffcampusWarningController.$inject = ['$location', 'ethConfigService', 'ethSessionService', 'ethFullviewOffcampusWarningConfig' ];
