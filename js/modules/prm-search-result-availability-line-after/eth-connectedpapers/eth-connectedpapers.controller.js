export class ethConnectedPapersController {

    constructor( ethConnectedPapersService, ethSessionService , ethConfigService, ethConnectedPapersConfig ) {
        this.ethConnectedPapersService = ethConnectedPapersService;
        this.ethSessionService = ethSessionService;
        this.ethConfigService = ethConfigService;
        this.config = ethConnectedPapersConfig;
    }

    $onInit() {
        try{
            this.parentCtrl = this.afterCtrl.parentCtrl;
            this.url = null;
            if (!this.parentCtrl.result || !this.parentCtrl.result.pnx) {
                console.error("***ETH*** ethConnectedPapersController.$onInit: result or result.pnx not available");
                return;
            }
            // check onCampus?
            /*if (!this.ethSessionService.isOnCampus())
                return;
            */
            // check type = article and book_chapter
            if(!this.parentCtrl.result.pnx.display.type || !this.parentCtrl.result.pnx.display.type.length > 0 || (this.parentCtrl.result.pnx.display.type[0] !== 'article' && this.parentCtrl.result.pnx.display.type[0] !== 'book_chapter')){
                return;
            }
            // check DOI
            if (!this.parentCtrl.result.pnx.addata || !this.parentCtrl.result.pnx.addata.doi || this.parentCtrl.result.pnx.addata.doi.length == 0) {
                return;
            }
            // url for connected papers
            this.url = 'https://connectedpapers.com/api/redirect/doi/' + this.parentCtrl.result.pnx.addata.doi[0] + '?utm_source=primo';
        }
        catch(e){
            console.error("***ETH*** an error occured: ethConnectedPapersController\n\n");
            console.error(e.message);
        }
    }
}

ethConnectedPapersController.$inject = ['ethConnectedPapersService', 'ethSessionService', 'ethConfigService', 'ethConnectedPapersConfig' ];
