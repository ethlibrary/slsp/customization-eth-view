/**
* @ngdoc module
* @name ethIllLinkModule
*
* @description
*
* Customization of the "how to getit" section:<br>
* - add link to an interlibrary loan form
*
* <b>AngularJS Dependencies</b><br>
* Service /js/services {@link ETH.ethConfigService}<br>
* Service {@link ETH.ethIllLinkConfig}<br>
*
* <b>CSS/Image Dependencies</b><br>
* CSS eth-ill-link.css
*
*/
import {ethConfigService} from '../../../services/eth-config.service';
import {ethIllLinkConfig} from './eth-ill-link.config';
import {ethIllLinkController} from './eth-ill-link.controller';
import {ethIllLinkHtml} from './eth-ill-link.html';

export const ethIllLinkModule = angular
    .module('ethIllLinkModule', [])
        .factory('ethConfigService', ethConfigService)
        .factory('ethIllLinkConfig', ethIllLinkConfig)
        .controller('ethIllLinkController', ethIllLinkController)
        .component('ethIllLinkComponent', {
            bindings: {afterCtrl: '<'},
            controller: 'ethIllLinkController',
            template: ethIllLinkHtml
        })
