/**
* @ngdoc service
* @name ethPersonsSuggestionService
*
* @description
*
* getPersonsFromWikidata(): getting informations about persons from wikidata
*
* <b>Used by</b><br>
* Module {@link ETH.ethPersonsSuggestionModule}<br>
*
*
 */
export const ethPersonsSuggestionService = ['$http', '$sce', function($http, $sce){

    function getPersonsFromRIB(search, lang){
        let wLang = 'en';
        if(lang==='de'){
            wLang = 'de';
        }
        let url = 'https://daas.library.ethz.ch/rib/v3/persons?search=';
        url = url + search + '&lang=' + wLang;
        $sce.trustAsResourceUrl(url);

        return $http.get(url)
            .then(
                function(response){
                    return response.data;
                },
                function(httpError){
                    if (httpError.status === 404)return null;
                    let error = "***ETH*** an error occured: ethPersonsSuggestionService.getPersonsFromWikidata(): " + httpError.status;
                    if (httpError.data && httpError.data.errorMessage) {
                        error += ' - ' + httpError.data.errorMessage;
                    }
                    console.error(error);
                    return null;
                }
            );
    }

    // default order of mwapi "Search" is relevance (srsort)
    // https://www.mediawiki.org/wiki/API:Search
    // https://www.bobdc.com/blog/sparql-full-text-wikipedia-sea/
    //(group_concat(DISTINCT(?professionLabel) ;separator = "| ") as ?professions )
    // ORDER BY ASC(?ordinal)
    function getPersonsFromWikidata(search, lang){
        const endpointUrl = 'https://query.wikidata.org/sparql';
        let wLang = 'en,de';
        if(lang==='de'){
            wLang = 'de,en';
        }
        let sparqlQuery = `
        SELECT DISTINCT ?item ?itemLabel ?itemDescription ?birth ?death ?image ?gnd ?viaf ?ordinal ?isImage
        WHERE {
          SERVICE wikibase:mwapi {
              bd:serviceParam wikibase:api "Search" .
              bd:serviceParam wikibase:endpoint "www.wikidata.org" .
              bd:serviceParam mwapi:srsearch "${search}" .
              ?item wikibase:apiOutputItem mwapi:title .
              ?ordinal wikibase:apiOrdinal true .
            }
            ?item wdt:P227 ?gnd.
            ?item wdt:P214 ?viaf.
            ?item wdt:P31 wd:Q5.
            OPTIONAL {?item wdt:P569 ?birth.}
            OPTIONAL {?item wdt:P570 ?death.}
            OPTIONAL {?item wdt:P18 ?image.}
            BIND(BOUND(?image) AS ?isImage).
            SERVICE wikibase:label { bd:serviceParam wikibase:language "${wLang}"}
        }
        GROUP BY ?item ?itemLabel ?itemDescription ?birth ?death ?image ?gnd ?viaf ?ordinal ?isImage
        ORDER BY DESC(?isImage) ASC(?ordinal)
        LIMIT 100
        `;
        let fullUrl = endpointUrl + '?query=' + sparqlQuery;
        $sce.trustAsResourceUrl(fullUrl);
        return $http.get(fullUrl, {headers:{ 'Accept': 'application/sparql-results+json'}})
            .then(
                function(response){
                    return response.data;
                },
                function(httpError){
                    let error = "***ETH*** an error occured: ethPersonsSuggestionService getPersons error callback: " + httpError.status;
                    if (httpError.data && httpError.data.errorMessage) {
                        error += ' - ' + httpError.data.errorMessage;
                    }
                    console.error(error);
                    return null;
                }
            )
    }

    return {
        getPersonsFromWikidata: getPersonsFromRIB
    };

}]
