/**
* @ngdoc module
* @name ethExploreModule
*
* @description
*
* Explore feature
*
*/
import {ethConfigService} from '../../../services/eth-config.service';
import {ethExploreConfig} from './eth-explore.config';
import {ethExploreController} from './eth-explore.controller';

import {ethPersonsSuggestionModule} from './eth-persons-suggestion/eth-persons-suggestion.module';
import {ethPlacesSuggestionModule} from './eth-places-suggestion/eth-places-suggestion.module';

export const ethExploreModule = angular
    .module('ethExploreModule', [])
        .factory('ethConfigService', ethConfigService)
        .factory('ethExploreConfig', ethExploreConfig)
        .controller('ethExploreController', ethExploreController)
        .component('ethExploreComponent',  {
                bindings: {afterCtrl: '<'},
                controller: 'ethExploreController'
            });

ethExploreModule.requires.push(ethPlacesSuggestionModule.name);
ethExploreModule.requires.push(ethPersonsSuggestionModule.name);          