/**
* @ngdoc service
* @name ethSessionService
*
* @description
*
* Service to get informations about the current session:
* - getDecodedToken()
* - isOnCampus()
* - isGuest()
* - getViewId()
*
* <b>Used by</b><br>
*
* Module {@link ETH.ethLibkeyModule}<br>
* Module {@link ETH.ethFullviewLodLinksModule}<br>
* Module {@link ETH.ethOffcampusWarningModule}<br>
* Module {@link ETH.ethFullviewOffcampusWarningModule}<br>
* Module {@link ETH.ethBrowzineModule}<br>
* Module {@link ETH.ethUnpaywallModule}<br>
* Module {@link ETH.ethProvenienzModule}<br>
*
 */
export const ethSessionService = ['jwtHelper', function( jwtHelper ){

    function getDecodedToken(){
        try{
            if (!sessionStorage){
                console.error("***ETH*** no session storage")
                return null;
            }
            let jwt = sessionStorage.getItem('primoExploreJwt');
            if (!jwt){
                return null;
            }
            return jwtHelper.decodeToken(jwt);
        }
        catch(e){
            console.error("***ETH*** an error occured: ethSessionService.getDecodedToken:");
            console.error(e.message);
        }
    }

    function isOnCampus(){
        try{
            let decodedToken = getDecodedToken();
            if (!decodedToken) {
                return null;
            }
            if (decodedToken.onCampus === 'true') {
                return true;
            }
            else{
                return false;
            }
        }
        catch(e){
            console.error("***ETH*** an error occured: ethSessionService.isOnCampus:");
            console.error(e.message);
        }
    }

    function getViewId(){
        try{
            let decodedToken = getDecodedToken();
            if (!decodedToken) {
                return null;
            }
            return decodedToken.viewId;
        }
        catch(e){
            console.error("***ETH*** an error occured: ethSessionService.getViewId:");
            console.error(e.message);
        }
    }

    function isGuest(){
        try{
            let decodedToken = getDecodedToken();
            if (!decodedToken) {
                return null;
            }
            let userName= decodedToken.userGroup !== 'GUEST'? decodedToken.userName : '';
            if (userName){
                return false;
            }
            else{
                return true;
            }
        }
        catch(e){
            console.error("***ETH*** an error occured: ethSessionService.isGuest:");
            console.error(e.message);
        }
    }

    return {
        isGuest: isGuest,
        isOnCampus: isOnCampus,
        getDecodedToken: getDecodedToken,
        getViewId: getViewId
    };
}]
