export const ethPlaceLinkConfig = function(){
    return {
        label: {
            heading: {
                de: 'Geografischer Bezug',
                en: 'Geographical reference'
            }
        },
        url: {
            ethorama: {
                de: 'http://ethorama.library.ethz.ch/de/',
                en: 'http://ethorama.library.ethz.ch/en/'
            }
        }
    }
}
