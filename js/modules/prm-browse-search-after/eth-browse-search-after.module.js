import {ethBrowseStartpageModule} from './eth-browse-startpage/eth-browse-startpage.module';

export const ethBrowseSearchAfterModule = angular
    .module('ethBrowseSearchAfterModule', [])
        .component('prmBrowseSearchAfter',  {
            bindings: {parentCtrl: '<'},
            template: `<eth-browse-startpage-component after-ctrl="$ctrl"></eth-browse-startpage-component>`
        });

ethBrowseSearchAfterModule.requires.push(ethBrowseStartpageModule.name);
