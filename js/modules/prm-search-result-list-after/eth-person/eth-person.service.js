/**
* @ngdoc service
* @name ethPersonService
*
* @description
*
* getPerson(): checks prometheus lmu, Metagrid, Entityfacts (DNB), Wikidata for person informations by qid
* searchPrimo(): search in Primo
*
*
* <b>Used by</b><br>
* Module {@link ETH.ethPersonModule}<br>
*
*
 */
export const ethPersonService = ['$http', '$sce', function($http, $sce){
    let baseurl = 'https://daas.library.ethz.ch/rib/v3/persons';

    function getPerson(id, lang){
        if(id.substring(0,1) === 'Q'){
            return this.getPersonByQid(id, lang);
        }
        else{
            return this.getPersonByGnd(id, lang);
        }
    }

    function getPersonByQid(qid, lang){
        let url = baseurl + "/person-qid?qid=" + qid + '&lang=' + lang;
        $sce.trustAsResourceUrl(url);

        return $http.get(url)
            .then(
                function(response){
                    return response.data;
                },
                function(httpError){
                    if (httpError.status === 404)return null;
                    let error = "***ETH*** an error occured: ethPersonService.getPersonByQid(): " + httpError.status;
                    if (httpError.data && httpError.data.errorMessage) {
                        error += ' - ' + httpError.data.errorMessage;
                    }
                    console.error(error);
                    return null;
                }
            );
    }

    function getPersonByGnd(gnd, lang){
        let url = baseurl + "/person-gnd?gnd=" + gnd + '&lang=' + lang;
        $sce.trustAsResourceUrl(url);

        return $http.get(url)
            .then(
                function(response){
                    return response.data;
                },
                function(httpError){
                    if (httpError.status === 404)return null;
                    let error = "***ETH*** an error occured: ethPersonService.getPersonByGnd(): " + httpError.status;
                    if (httpError.data && httpError.data.errorMessage) {
                        error += ' - ' + httpError.data.errorMessage;
                    }
                    console.error(error);
                    return null;
                }
            );
    }

    function searchPrimo(q, tab, scope, lang){
        if(!lang)lang = 'de';
        let baseurl = 'https://daas.library.ethz.ch/rib/v3/search';
        let url = baseurl + '?tab=' + tab + '&scope=' + scope + '&lang=' + lang + '&limit=1&skipDelivery=true&disableSplitFacets=false&q=' + encodeURIComponent(q);
        $sce.trustAsResourceUrl(url);
        return $http.get(url)
            .then(
                function(response){
                    return response.data;
                },
                function(httpError){
                    console.error(httpError)
                    if (httpError.status === 404)return null;
                    let error = "***ETH*** an error occured: ethPersonService.searchPrimo(): " + httpError.status;
                    if (httpError.data && httpError.data.errorMessage) {
                        error += ' - ' + httpError.data.errorMessage;
                    }
                    return null;
                }
            );
    }

    return {
        getPerson: getPerson,
        getPersonByQid: getPersonByQid,
        getPersonByGnd: getPersonByGnd,
        searchPrimo: searchPrimo
    };

}]
