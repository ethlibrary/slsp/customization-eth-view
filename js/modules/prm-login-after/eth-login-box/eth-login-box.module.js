import { ethLoginBoxController } from './eth-login-box.controller';
import { ethConfigService } from '../../../services/eth-config.service';
import { ethLoginBoxConfig } from './eth-login-box.config';

export const ethLoginBoxModule = angular
    .module('ethLoginBoxModule', [])
    .factory('ethConfigService', ethConfigService)
    .factory('ethLoginBoxConfig', ethLoginBoxConfig)
    .controller('ethLoginBoxController', ethLoginBoxController)
    .component('ethLoginBoxComponent', {
        bindings: { afterCtrl: '<' },
        controller: 'ethLoginBoxController'
    })