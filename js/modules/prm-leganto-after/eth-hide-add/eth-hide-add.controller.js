export class ethHideAddController { 

    constructor($timeout, $scope, $compile, ethConfigService, ethHideAddConfig ) {
        this.$timeout = $timeout;
        this.$scope = $scope;
        this.$compile = $compile;
        this.ethConfigService = ethConfigService;
        this.config = ethHideAddConfig;
    }

    $onInit() {
        try{
            this.processDoCheck = true;
        }
        catch(e){
            console.error("***ETH*** an error occured: ethHideAddController\n\n");
            console.error(e.message);
        }
    }


    $doCheck() {
        try{
            if(!this.processDoCheck){
                return;
            }
            let addButton = document.querySelector("prm-leganto button");
            let condValue = "$ctrl.legantoService.showTemplateType === 'noListsExist'";
            let textContainer = document.querySelector(`prm-leganto div[ng-if^="${condValue}"]`);
            if(textContainer){
                angular.element(addButton).attr('disabled','disabled').attr('ng-disabled','disabled');
                let html = `
                <div class='eth-leganto-hint'>
                    <a ng-href="{{::$ctrl.ethConfigService.getUrl($ctrl.config, 'leganto')}}" target="_blank">
                        {{::$ctrl.ethConfigService.getLabel($ctrl.config, 'info')}}
                    </a>
                </div>
                `;
                angular.element(textContainer).append(this.$compile(html)(this.$scope));
                angular.element(textContainer).addClass('eth-leganto-container');
                this.processDoCheck = false;
            }
        }
        catch(e){
            console.error("***ETH*** an error occured: ethHideAddController.$doCheck\n\n");
            console.error(e.message);
            throw(e)
        }
    }


}

ethHideAddController.$inject = ['$timeout', '$scope', '$compile', 'ethConfigService', 'ethHideAddConfig' ];
