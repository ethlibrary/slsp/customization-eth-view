export class ethRapidoHintController {

    constructor( $location, ethConfigService, ethSessionService, ethRapidoHintConfig ) {
        this.$location = $location;
        this.ethConfigService = ethConfigService;
        this.ethSessionService = ethSessionService;
        this.config = ethRapidoHintConfig;
        this.isRapidoHint = true;
    }

    $onInit() {
        try{
            this.isRapidoHint = this.evaluateIsRapidoHint();
        }
        catch(e){
            console.error("***ETH*** an error occured: ethRapidoHintController\n\n");
            console.error(e.message);
        }
    }

    evaluateIsRapidoHint(){
        if (sessionStorage && sessionStorage.getItem('eth-topbar-hide') === '1'){
            return false;
        }
        else{
            return true;
        }
    }

    onClose(){
        let topbar = document.querySelector('.eth-topbar');
        angular.element(topbar).remove();
        if (sessionStorage) {
            sessionStorage.setItem('eth-topbar-hide', '1');
        }
        return;
    }

}
ethRapidoHintController.$inject = ['$location', 'ethConfigService', 'ethSessionService', 'ethRapidoHintConfig' ];
