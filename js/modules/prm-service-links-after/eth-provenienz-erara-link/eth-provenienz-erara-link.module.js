/**
* @ngdoc module
* @name ethProvenienzEraraLinkModule
*
* @description
*
* Customization for the links section:<br>
* - add link to e-rara source
*
* <b>AngularJS Dependencies</b><br>
* Service /js/services {@link ETH.ethConfigService}<br>
* Service {@link ETH.ethProvenienzEraraLinkConfig}<br>
*
*/
import {ethConfigService} from '../../../services/eth-config.service';
import {ethProvenienzEraraLinkConfig} from './eth-provenienz-erara-link.config';
import {ethProvenienzEraraLinkController} from './eth-provenienz-erara-link.controller';
import {ethProvenienzEraraLinkHtml} from './eth-provenienz-erara-link.html';

export const ethProvenienzEraraLinkModule = angular
    .module('ethProvenienzEraraLinkModule', [])
        .factory('ethConfigService', ethConfigService)
        .factory('ethProvenienzEraraLinkConfig', ethProvenienzEraraLinkConfig)
        .controller('ethProvenienzEraraLinkController', ethProvenienzEraraLinkController)
        .component('ethProvenienzEraraLinkComponent', {
            bindings: {afterCtrl: '<'},
            controller: 'ethProvenienzEraraLinkController',
            template: ethProvenienzEraraLinkHtml
        })
