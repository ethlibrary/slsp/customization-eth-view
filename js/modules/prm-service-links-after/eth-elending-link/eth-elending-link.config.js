export const ethElendingLinkConfig = function(){
    return {
        elending: {
            label: {
                informations:{
                    de: 'Weitere Informationen zum E-Lending',
                    en: 'More information about E-Lending'
                }
            },
            url:{
                informations: {
                    de: 'https://documentation.library.ethz.ch/display/WWE/E-Lending',
                    en: 'https://documentation.library.ethz.ch/display/WWE/E-Lending+Service'
                }
            }
        }
    }
}
