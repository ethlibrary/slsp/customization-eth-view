export class ethBrowzineController {

    constructor( ethBrowzineService, ethSessionService , ethConfigService, ethBrowzineConfig ) {
        this.ethBrowzineService = ethBrowzineService;
        this.ethSessionService = ethSessionService;
        this.ethConfigService = ethConfigService;
        this.config = ethBrowzineConfig;
    }

    $onInit() {
        try{
            this.browzine = {};
            this.parentCtrl = this.afterCtrl.parentCtrl;

            if (!this.parentCtrl.result || !this.parentCtrl.result.pnx) {
                console.error("***ETH*** ethBrowzineController.$onInit: result or result.pnx not available");
                return;
            }
            // check onCampus
            if (!this.ethSessionService.isOnCampus())
                return;
            // check type = journal; CDI: eJournal
            if(!this.parentCtrl.result.pnx.display.type || !this.parentCtrl.result.pnx.display.type.length > 0 || (this.parentCtrl.result.pnx.display.type[0] !== 'journal' && this.parentCtrl.result.pnx.display.type[0] !== 'eJournal' && this.parentCtrl.result.pnx.display.type[0] !== 'Zeitschrift')){
                return;
            }
            // check issn
            if (!this.parentCtrl.result.pnx.addata || !this.parentCtrl.result.pnx.addata.issn || !this.parentCtrl.result.pnx.addata.issn.length > 0) {
                return;
            }
            this.ethBrowzineService.getJournal( this.parentCtrl.result.pnx.addata.issn[0].replace("-", "") )
                .then((data) => {
                    try{
                        this.briefResult.eth_browzine = {};
                        if(!data || !data.length > 0){
                            return;
                        }
                        if(data[0].browzineWebLink)this.browzine.browzineWebLink = data[0].browzineWebLink;
                        if(data[0].coverImageUrl)this.browzine.coverImageUrl = data[0].coverImageUrl;
                        this.briefResult.eth_browzine = this.browzine;
                    }
                    catch(e){
                        console.error("***ETH*** an error occured: ethBrowzineController getJournal(): \n\n");
                        console.error(e.message);
                    }
                });
        }
        catch(e){
            console.error("***ETH*** an error occured: ethBrowzineController\n\n");
            console.error(e.message);
        }
    }
}

ethBrowzineController.$inject = ['ethBrowzineService', 'ethSessionService', 'ethConfigService', 'ethBrowzineConfig' ];
