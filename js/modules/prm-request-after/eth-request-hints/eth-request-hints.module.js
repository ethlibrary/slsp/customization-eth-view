/**
* @ngdoc module
* @name ethRequestHintsModule
*
* @description
*
* - A link to the fees and a hint to the fees specific to user groups are added.<br>
* - A hint on the other pickup locations is added<br>
*
*
* <b>AngularJS Dependencies</b><br>
* Service /js/services {@link ETH.ethConfigService}<br>
* Service {@link ETH.ethRequestHintsConfig}<br>
*
*
* <b>CSS/Image Dependencies</b><br>
*  eth-request-hints.css
*
*/
import {ethConfigService} from '../../../services/eth-config.service';
import {ethRequestHintsConfig} from './eth-request-hints.config';
import {ethRequestHintsController} from './eth-request-hints.controller';
import {ethRequestHintsHtml} from './eth-request-hints.html';

export const ethRequestHintsModule = angular
    .module('ethRequestHintsModule', [])
        .factory('ethConfigService', ethConfigService)
        .factory('ethRequestHintsConfig', ethRequestHintsConfig)
        .controller('ethRequestHintsController', ethRequestHintsController)
        .component('ethRequestHintsComponent',{
            bindings: {afterCtrl: '<'},
            controller: 'ethRequestHintsController',
            template: ethRequestHintsHtml
        })
