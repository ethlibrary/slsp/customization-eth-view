/**
* @ngdoc module
* @name ethSectionOrderingModule
*
* @description
* - The order of sections is changed: virtualBrowse is removed, action_list is moved to the end.
*
*
* <b>AngularJS Dependencies</b><br>
*
*
* <b>CSS/Image Dependencies</b><br>
* CSS eth-section-ordering.css
*
*
*/
import {ethSectionOrderingController} from './eth-section-ordering.controller';

export const ethSectionOrderingModule = angular
    .module('ethSectionOrderingModule', [])
        .controller('ethSectionOrderingController', ethSectionOrderingController)
        .component('ethSectionOrderingComponent',  {
            bindings: {afterCtrl: '<'},
            controller: 'ethSectionOrderingController'
        })
