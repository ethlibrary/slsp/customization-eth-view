//-------- ---------------------------------------

// 99117220533105503

export class slspTranslatePublicNoteController {
    constructor($scope, $compile) {
        this.$scope = $scope;
        this.$compile = $compile;
    }

    $onInit() {
        try{
            this.processDoCheck = true;
        }
        catch(e) {
            console.error("***ETH*** an error occured: slspTranslatePublicNoteController");
            console.error(e.message);
        }
    }

    $doCheck() {
        try {
            if(!this.processDoCheck){
                return;
            }
            let noteDe = document.querySelectorAll(".note-de");
            if(!noteDe || noteDe.length === 0) {
                return;
            }
            let noteEn = document.querySelectorAll(".note-en");
            let noteFr = document.querySelectorAll(".note-fr");
            let noteIt = document.querySelectorAll(".note-it");
            let noteBreak = document.querySelectorAll('p[ng-if="item.authNote"] br, p[ng-if="item.publicNote"] br');
            angular.element(noteBreak).remove();

            let lang = 'de';
            let sms = this.$scope.$root.$$childHead.$ctrl.userSessionManagerService;
            if (sms) {
                lang = sms.getUserLanguage();
            }
            if (lang !== 'de') {
                angular.element(noteDe).remove();
            }
            if (lang !== 'en') {
                angular.element(noteEn).remove();
            }
            if (lang !== 'fr') {
                angular.element(noteFr).remove();
            }
            if (lang !== 'it') {
                angular.element(noteIt).remove();
            }
            this.processDoCheck = false;

        } catch (e) {
            console.error('***SLSP*** Ein Fehler ist aufgetreten: TranslatePublicNoteController \n\n');
            console.error(e.message);
        }
    }
}

slspTranslatePublicNoteController.$inject = ['$scope', '$compile'];
