import {ethRenewItemTextModule} from './eth-renew-item-text/eth-renew-item-text.module';
import {ethTitleAsTextModule} from './eth-title-as-text/eth-title-as-text.module';
import {slspHideRapidoLoanLinkModule} from './slsp-hide-rapido-loan-link/slsp-hide-rapido-loan-link.module';
import {ethLoansHintModule} from './eth-loans-hint/eth-loans-hint.module';

export const ethLoanAfterModule = angular
    .module('ethLoanAfterModule', [])
        .component('prmLoanAfter',  {
            bindings: {parentCtrl: '<'},
            template: `
            <eth-renew-item-text-component after-ctrl="$ctrl"></eth-renew-item-text-component>
            <eth-title-as-text-component after-ctrl="$ctrl"></eth-title-as-text-component>
            <eth-loans-hint-component after-ctrl="$ctrl"></eth-loans-hint-component>
            <slsp-hide-rapido-loan-link-component after-ctrl="$ctrl"></slsp-hide-rapido-loan-link-component>
            `
        });
ethLoanAfterModule.requires.push(ethRenewItemTextModule.name);
ethLoanAfterModule.requires.push(ethTitleAsTextModule.name);
ethLoanAfterModule.requires.push(ethLoansHintModule.name);
ethLoanAfterModule.requires.push(slspHideRapidoLoanLinkModule.name);
