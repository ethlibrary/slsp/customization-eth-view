export class ethRenewItemTextController {

    $onInit() {
        try{
            this.parentCtrl = this.afterCtrl.parentCtrl;
            this.renewText = '';
            if(this.parentCtrl.item.renewstatuses && this.parentCtrl.item.renewstatuses.renewstatus && this.parentCtrl.item.renewstatuses.renewstatus.length > 0) {
                this.renewText = this.parentCtrl.item.renewstatuses.renewstatus[0];
            }
        }
        catch(e) {
            console.error("***ETH*** an error occured: ethRenewItemTextController");
            console.error(e.message);
        }
    }
}
