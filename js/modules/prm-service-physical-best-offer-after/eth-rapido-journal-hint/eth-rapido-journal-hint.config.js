export const ethRapidoJournalHintConfig = function(){
    return {
        label: {
            hint1:{
                de: 'Möchten Sie diese Zeitschrift an einen Abholort an der ETH Zürich bestellen? Verwenden Sie bitte den',
                en: 'Would you like to order this journal to a pickup location at the ETH Zurich? Please use the'
            },
            hint2:{
                de: '.',
                en: '.'
            },
            linktext:{
                de: 'oberen Bestellbereich',
                en: 'upper ordering section'
            }
        },
        url:{
            cms: {
                de: 'https://library.ethz.ch/recherchieren-und-nutzen/ausleihen-und-nutzen/bestellformulare/journal-bestellen.html',
                en: 'https://library.ethz.ch/en/searching-and-using/borrowing-and-using/order-forms/journal-bestellen.html'
            }
        }
    }
}
