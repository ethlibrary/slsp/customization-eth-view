export const ethReferencesHintConfig = function(){
    return {
        label: {
            heading: {
                de: 'Tipp',
                en: 'Tip'
            },
            text1: {
                de: 'Verwenden Sie ein',
                en: 'Use a'
            },
            linktext: {
                de: 'Literaturverwaltungsprogramm',
                en: 'reference management software'
            },
            text2: {
                de: ', wenn Sie viele Referenzen organisieren und verwalten möchten.',
                en: ', if you have many references to organise and manage.'
            }
        },
        url:{
            link: {
                de: 'https://library.ethz.ch/publizieren-und-archivieren/schreiben-literatur-verwalten/literatur-verwalten.html',
                en: 'https://library.ethz.ch/en/publishing-and-archiving/writing-managing-references/managing-references.html'
            }
        }
    }
}
