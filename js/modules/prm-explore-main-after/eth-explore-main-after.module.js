import {ethChatModule} from './eth-chat/eth-chat.module';

export const ethExploreMainAfterModule = angular
    .module('ethExploreMainAfterModule', [])
        .component('prmExploreMainAfter',  {
            template: `<eth-chat-component></eth-chat-component>`
        });

ethExploreMainAfterModule.requires.push(ethChatModule.name);
