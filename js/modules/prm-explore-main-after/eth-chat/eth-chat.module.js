/**
* @ngdoc module
* @name ethChatModule
*
* @description
*
* adds chat button and chat box for libraryh3lp
*
* <b>AngularJS Dependencies</b><br>
*
*
* <b>CSS/Image Dependencies</b><br>
* CSS eth-chat.css
*
*/
import {ethChatController} from './eth-chat.controller';
import {ethChatHtml} from './eth-chat.html';

export const ethChatModule = angular
    .module('ethChatModule', [])
        .controller('ethChatController', ethChatController)
        .component('ethChatComponent',{
            controller: 'ethChatController',
            template: ethChatHtml
        })
