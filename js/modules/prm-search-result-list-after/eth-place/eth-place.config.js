export const ethPlaceConfig = function(){
    return {
        whitelist:
            ["Wikidata","P1225","P1255","P1566","P1667","P402","P4672","P902","Dodis","ssrq","Ortsnamen","Ortsnamen2","Swisstopo","HLS","lobid","Kalliope","ddb","GeoNames","Encyclopædia Britannica","OSM relation","VIAF","SSRQ","GND","SUDOC"],
        label: {
            results: {
                de: 'Ergebnisse',
                en: 'Results'
            },
            selectedResults: {
                de: 'Ausgewählte Dokumente',
                en: 'Selected documents'
            },
            moreInformations: {
                de: 'Mehr Informationen zum Ort',
                en: 'More information about the place'
            },
            wdLinks: {
                de: 'Externe Links aus Wikidata',
                en: 'External links from Wikidata'
            },
            geolinkerLinks: {
                de: 'Externe Links der Geolinker API',
                en: 'External links from Geolinker API'
            },
            geolinker: {
                de: 'Werden mehrere Links eines Portals angezeigt, so sind dies Links zu verschiedenen Ortstypen mit dem gleichen Namen (z.B. Gemeinde, Bezirk, Kanton).',
                en: 'If several links of a portal are displayed, these are links to different location types with the same name (e.g. municipality, district, canton).'
            },
            titleEthorama1: {
                de: 'ETHorama',
                en: 'ETHorama'
            },
            textEthorama1: {
                de: 'Auf der Plattform',
                en: 'Places and selected documents can be searched via a map on the'
            },
            textEthorama2: {
                de: 'sind Orte mit ausgewählten Dokumente via Karte recherchierbar.',
                en: 'portal.'
            },
            linktextEthorama: {
                de: ' in ETHorama mit allen verbundenen Dokumenten',
                en: ' in ETHorama with all related documents'
            },
            titleEthorama2: {
                de: 'ETHorama Themensammlungen',
                en: 'ETHorama thematic collections'
            },
            titleEthorama3: {
                de: 'ETHorama Historische Reisen',
                en: 'ETHorama historical travels'
            },
            titleNearto: {
                de: 'Orte im Umfeld (1000m)',
                en: 'Places nearby (1000m)'
            },
            titlePersons: {
                de: 'Personen aus der GND',
                en: 'Related Persons from the GND'
            },
            titleMaps: {
                de: 'Karten',
                en: 'Maps'
            },
            titleGeoTopics: {
                de: 'Schlagwörter',
                en: 'Topics'
            },
            eraraMap: {
                de: '(e-rara)',
                en: '(e-rara)'
            },
            emapsMap: {
                de: '(e-maps, georeferenziert)',
                en: '(e-maps, georeferenced)'
            },
            searchThis: {
                de: 'Klicken Sie bitte auf den Marker, um Details zu dieser Karte zu sehen.',
                en: 'Please click on the marker to see details of this map.'
            }
        },
        url: {
            ethorama: {
                de: 'https://ethorama.library.ethz.ch/de/orte/',
                en: 'https://ethorama.library.ethz.ch/en/locations/'
            },
            ethoramaDossier: {
                de: 'https://ethorama.library.ethz.ch/de/themen/',
                en: 'https://ethorama.library.ethz.ch/en/topics/'
            },
            ethoramaRoute: {
                de: 'https://ethorama.library.ethz.ch/de/reisen/',
                en: 'https://ethorama.library.ethz.ch/en/routes/'
            },
            info: {
                de: 'https://library.ethz.ch',
                en: 'https://library.ethz.ch'
            }
        }
    }
}
