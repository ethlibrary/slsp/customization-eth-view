/**
* @ngdoc module
* @name ethFullviewArchivesGetitModule
*
* @description
*
* - Text and links for resources of archives MFA, TMA, HSA (cmistar)
* - HSA contains online resources (-> viewit-after), the other archives not ( -> htgi-svc-after ).
*
* <b>AngularJS Dependencies</b><br>
* Service /js/services {@link ETH.ethConfigService}<br>
* Service {@link ETH.ethFullviewArchivesGetitConfig}<br>
*
*
* <b>CSS/Image Dependencies</b><br>
* CSS eth-fullview-archives-getit.css
*
*
*/
import {ethConfigService} from '../../../services/eth-config.service';
import {ethFullviewArchivesGetitConfig} from './eth-fullview-archives-getit.config';
import {ethFullviewArchivesGetitController} from './eth-fullview-archives-getit.controller';
import {ethFullviewArchivesGetitHtml} from './eth-fullview-archives-getit.html';

export const ethFullviewArchivesGetitModule = angular
    .module('ethFullviewArchivesGetitModule', [])
        .factory('ethConfigService', ethConfigService)
        .factory('ethFullviewArchivesGetitConfig', ethFullviewArchivesGetitConfig)
        .controller('ethFullviewArchivesGetitController', ethFullviewArchivesGetitController)
        .component('ethFullviewArchivesGetitComponent',{
            bindings: {afterCtrl: '<'},
            controller: 'ethFullviewArchivesGetitController',
            template: ethFullviewArchivesGetitHtml
        })
