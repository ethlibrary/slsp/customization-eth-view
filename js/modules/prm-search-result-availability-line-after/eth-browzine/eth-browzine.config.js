export const ethBrowzineConfig = function(){
    return {
        label: {
            linktext: {
                de: 'In BrowZine anzeigen',
                en: 'View in BrowZine'
            }
        }
    }
}
