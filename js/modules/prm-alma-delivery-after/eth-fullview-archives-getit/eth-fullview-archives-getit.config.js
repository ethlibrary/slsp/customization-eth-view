export const ethFullviewArchivesGetitConfig = function(){
    return {
        hsa: {
            label: {
                linktext1: {
                    de: 'Bestellen über den Virtuellen Lesesaal des Hochschularchivs der ETH Zürich',
                    en: 'Consult in the Virtual Reading Room of the ETH Zurich University Archives'
                },
                text1:{
                    de: 'Informationen zum',
                    en: 'How to'
                },
                linktext2: {
                    de: 'Bestellen und Konsultieren',
                    en: 'order and consult'
                },
                text2:{
                    de: 'von Unterlagen',
                    en: 'documents'
                }
            },
            url:{
                manual: {
                    de: 'https://vls.hsa.ethz.ch/client/#/de/informationen/bestellen-und-konsultieren',
                    en: 'https://vls.hsa.ethz.ch/client/#/en/information/ordering-and-consulting'
                },
                url: {
                    de: 'https://vls.hsa.ethz.ch/client/#/de/archiv/einheit/',
                    en: 'https://vls.hsa.ethz.ch/client/#/en/archive/unit/'
                }
            }
        },
        mfa:{
            label: {
                linktext1: {
                    de: 'Bestellen über den Virtuellen Lesesaal des Max Frisch-Archivs',
                    en: 'Consult in the Virtual Reading Room of the Max Frisch Archive'
                },
                text1:{
                    de: 'Informationen zum',
                    en: 'How to'
                },
                linktext2: {
                    de: 'Bestellen und Konsultieren',
                    en: 'order and consult'
                },
                text2:{
                    de: 'von Unterlagen',
                    en: 'documents'
                }
            },
            url:{
                manual: {
                    de: 'https://vls.mfa.ethz.ch/client/#/de/informationen/bestellen-und-konsultieren',
                    en: 'https://vls.mfa.ethz.ch/client/#/en/information/ordering-and-consulting'
                },
                url: {
                    de: 'https://vls.mfa.ethz.ch/client/#/de/archiv/einheit/',
                    en: 'https://vls.mfa.ethz.ch/client/#/en/archive/unit/'
                }
            }
        },
        tma:{
            label: {
                linktext1: {
                    de: 'Bestellen über den Virtuellen Lesesaal des Thomas-Mann-Archivs',
                    en: 'Consult in the Virtual Reading Room of the Thomas Mann Archive'
                },
                text1:{
                    de: 'Informationen zum',
                    en: 'How to'
                },
                linktext2: {
                    de: 'Bestellen und Konsultieren',
                    en: 'order and consult'
                },
                text2:{
                    de: 'von Unterlagen',
                    en: 'documents'
                }
            },
            url:{
                manual: {
                    de: 'https://vls.tma.ethz.ch/client/#/de/informationen/bestellen-und-konsultieren',
                    en: 'https://vls.tma.ethz.ch/client/#/en/information/ordering-and-consulting'
                },
                url: {
                    de: 'https://vls.tma.ethz.ch/client/#/de/archiv/einheit/',
                    en: 'https://vls.tma.ethz.ch/client/#/en/archive/unit/'
                }
            }
        }

    }
}
