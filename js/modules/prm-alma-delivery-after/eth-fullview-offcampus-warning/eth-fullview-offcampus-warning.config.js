export const ethFullviewOffcampusWarningConfig = function(){
    return {
        label: {
            offcampusWarningText: {
                de: 'Lizenzierte elektronische Ressourcen sind nur im Netzwerk der ETH Zürich zugänglich.',
                en: 'Licensed electronic resources of ETH Library are accessible only within the network of ETH Zurich.'
            },
            offcampusWarningLinktext: {
                de: 'Informationen zur Nutzung elektronischer Ressourcen',
                en: 'Information about using electronic resources'
            }
        },
        url:{
            offcampusWarningUrl: {
                de: 'https://unlimited.ethz.ch/display/WWE/Elektronische+Medien+der+ETH-Bibliothek+nutzen',
                en: 'https://unlimited.ethz.ch/display/WWE/Use+of+electronic+media+the+ETH+Library'
            }
        }
    }
}
