export class ethFullviewArchivesGetitController {

    constructor( ethConfigService, ethFullviewArchivesGetitConfig ) {
        this.ethConfigService = ethConfigService;
        this.config = ethFullviewArchivesGetitConfig;
    }

    $onInit() {
        try{
            this.parentCtrl = this.afterCtrl.parentCtrl;
            if(!this.parentCtrl.item.pnx.display.source || this.parentCtrl.item.pnx.display.source.length === 0)return;

            this.source= this.parentCtrl.item.pnx.display.source[0];
            if(this.source !== 'ETH_Hochschularchiv' && this.source !== 'ETH_MaxFrischArchiv' && this.source !== 'ETH_ThomasMannArchiv'){
                return;
            }

            let section = document.getElementById('full-view-container');
            // get guid
            let sourceid = this.parentCtrl.item.pnx.control.originalsourceid[0];
            if(this.source === 'ETH_ThomasMannArchiv' || this.source === 'ETH_MaxFrischArchiv'){
                this.guid = sourceid.substring(sourceid.lastIndexOf(':') + 1);
                // hide default content
                section.classList.add('eth-archives-hide');
            }
            // Hochschularchiv: sometimes there is an online link, sometimes not
            else if(this.source === 'ETH_Hochschularchiv'){
                if(this.parentCtrl.item.delivery.GetIt1 && this.parentCtrl.item.delivery.GetIt1.length > 0 && this.parentCtrl.item.delivery.GetIt1[0].links && this.parentCtrl.item.delivery.GetIt1[0].links.length > 0){
                    // is there an online resource?
                    let aLink = this.parentCtrl.item.delivery.GetIt1[0].links.filter( l => {
                        if(l.isLinktoOnline && l.link !== ''){
                            return true;
                        }
                        return false;
                    });
                    if (aLink.length > 0) {
                        return;
                    }
                }
                this.guid = sourceid.substring(sourceid.lastIndexOf(':') + 1);
                // hide default content
                section.classList.add('eth-archives-hide');
                // Fullview: change Heading
                let fullView = document.getElementById('fullView');
                if(fullView){
                    fullView.classList.add('eth-fullview-hsa-no-online');
                    fullView.classList.add('eth-fullview-hsa-no-online-' + this.ethConfigService.getLanguage());
                }
            }
        }
        catch(e){
            console.error("***ETH*** an error occured: ethFullviewArchivesGetitController\n\n");
            console.error(e.message);
        }
    }

}

ethFullviewArchivesGetitController.$inject = ['ethConfigService', 'ethFullviewArchivesGetitConfig' ];
