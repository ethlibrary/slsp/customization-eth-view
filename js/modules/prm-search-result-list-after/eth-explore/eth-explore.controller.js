export class ethExploreController {

    constructor( $scope, $compile, $anchorScroll, $timeout, $location, ethConfigService, ethExploreConfig ) {
        this.$scope = $scope;
        this.$compile = $compile;
        this.$anchorScroll = $anchorScroll;        
        this.$timeout = $timeout;
        this.$location = $location;
        this.ethConfigService = ethConfigService;
        this.config = ethExploreConfig;
    }

    $onInit() {
        try{
            this.parentCtrl = this.afterCtrl.parentCtrl;
            this.processDoCheck = false;
            this.isExploreRendered = false;

            this.hasPois = null;
            this.hasPersons = null;

            // show explore suggestions?
            if(!this.isExplore()){
                return;
            }

            // prepare explore navigation - toggle and link container
            this.htmlNavi = `
            <div ng-if="$ctrl.hasPersons=='yes' || $ctrl.hasPois=='yes'" class="eth-explore__wrapper">
                <h3 class="section-title-header eth-explore__header">
                    ${this.ethConfigService.getLabel(this.config, 'linksHeading')}
                </h3>
                <div ng-if="$ctrl.hasPersons=='loading' || $ctrl.hasPois=='loading'">
                    <div class="loading-indicator"><div class="loading-progress"></div></div>
                </div>
                <p ng-if="$ctrl.hasPois=='yes'">
                    <a class="eth-explore__link" ng-click="$ctrl.trackExploreNaviLink('places');$ctrl.$location.hash('eth-places__suggestion-anchor');$ctrl.$anchorScroll();">
                        {{$ctrl.ethConfigService.getLabel($ctrl.config, 'poisLinkText')}}
                    </a>
                </p>
                <p ng-if="$ctrl.hasPersons=='yes'">
                    <a class="eth-explore__link" ng-click="$ctrl.trackExploreNaviLink('persons');$ctrl.$location.hash('eth-persons__suggestion-anchor');$ctrl.$anchorScroll();">
                        ${this.ethConfigService.getLabel(this.config, 'personsLinkText')}
                    </a>
                </p>
            </div>
            `
            // insert suggestion container (do it here because of timing)    
            let htmlSuggestions =  `
                <eth-places-suggestion-component after-ctrl="$ctrl"></eth-places-suggestion-component>
                <eth-persons-suggestion-component after-ctrl="$ctrl"></eth-persons-suggestion-component>
                `
            let suggestionsContainer = angular.element(document.querySelector('eth-explore-component'));
            suggestionsContainer.append(this.$compile(htmlSuggestions)(this.$scope));
            // render explore navigation
            this.$timeout(() => {
                const mediaQueryList = window.matchMedia("only screen and (min-width: 960px)");
                if (mediaQueryList.matches) {
                    // render at top of facets
                    this.processDoCheck = true;
                    // fallback: if there is no facet container, render at top of sidebar wrapper
                    this.insertNaviInSidebarWrapper();
                }
                else {
                    this.insertNaviSmall();
                }
            }, 800);                
        }
        catch(e){
            console.error("***ETH*** an error occured: ethExploreController.onInit()\n\n");
            console.error(e.message);
        }
    }

    $doCheck() {
        try {
            // render at top of facets
            if(!this.processDoCheck)return;
            if(!document.querySelector('.sidebar-title') || !document.querySelector('.sidebar-title').parentNode)return;
            if(!document.querySelector('.eth-explore__wrapper') && !this.isExploreRendered){
                this.isExploreRendered = true;
                let container = angular.element(document.querySelector('.sidebar-title').parentNode);
                if(container)container.append(this.$compile(this.htmlNavi)(this.$scope));
            }
            this.processDoCheck = false;
        } catch (e) {
            console.error("***ETH*** an error occured: ethExploreController.$doCheck()\n\n");
            console.error(e.message);
        }
    }

    insertNaviSmall(){
        let container = angular.element(document.querySelector('prm-search-result-list'));
        container.prepend(this.$compile(this.htmlNavi)(this.$scope));
    }

    // fallback: if there is no facet container, render at top of sidebar wrapper
    insertNaviInSidebarWrapper(){
        this.$timeout(() => {
            if(!document.querySelector('.eth-explore__wrapper')  && !this.isExploreRendered && document.querySelector('.sidebar-inner-wrapper')){
                this.processDoCheck = false;
                this.isExploreRendered = true;
                let container = angular.element(document.querySelector('.sidebar-inner-wrapper'));
                container.prepend(this.$compile(this.htmlNavi)(this.$scope));
            }
        }, 5000)
    }


    // show EXPLORE suggestions?
    isExplore(){
        // facet -> no explore
        let facet = this.parentCtrl.searchService.searchUtil.$stateParams.facet;
        let mfacet = this.parentCtrl.searchService.searchUtil.$stateParams.mfacet;
        let pfilter = this.parentCtrl.searchService.searchUtil.$stateParams.pfilter;
        if((facet && facet.length > 0) || (mfacet && mfacet.length > 0) || pfilter){
            return false;
        }
        // advanced search
        let mode = this.parentCtrl.searchService.searchUtil.$stateParams.mode;
        if(mode && mode === 'advanced'){
            return false;
        }
        // place page / person page / fullview -> no explore
        let query = this.parentCtrl.searchService.searchUtil.$stateParams.query;
        if(!query || query === ''){
            return false;
        }
        this.search = query.substring(query.indexOf('contains,') + 9);
        if(this.search.indexOf('[wd/') > -1){
            return false;
        }
        // mmsid 990003210670205503 in search term
        if(this.search.indexOf('99') > -1 && this.search.indexOf('550') > -1){
            return false;
        }
        // only first tab
        if(this.parentCtrl.searchService.searchUtil.$stateParams.tab && this.parentCtrl.searchService.searchUtil.$stateParams.tab !== 'discovery_network'){
            return false;
        }
        return true;
    }

    trackExploreNaviLink(target){
        // matomo
        if(!window._paq){
            window._paq = [];
        }
        window._paq.push(['setCustomUrl', 'https://eth.swisscovery.slsp.ch/explore/navigation/' + target]);
        window._paq.push(['trackPageView']);
        return true;
    }

}

ethExploreController.$inject = ['$scope', '$compile', '$anchorScroll', '$timeout', '$location', 'ethConfigService', 'ethExploreConfig'];
