/**
* @ngdoc module
* @name ethHideAddModule
*
* @description
*
* if there is no leganto reading list, disable button (which adds item to reading list)
*
*
*/
import {ethHideAddController} from './eth-hide-add.controller';
import {ethConfigService} from '../../../services/eth-config.service';
import {ethHideAddConfig} from './eth-hide-add.config';


export const ethHideAddModule = angular
    .module('ethHideAddModule', [])
        .factory('ethConfigService', ethConfigService)
        .factory('ethHideAddConfig', ethHideAddConfig)
        .controller('ethHideAddController', ethHideAddController)
        .component('ethHideAddComponent', {
            bindings: {afterCtrl: '<'},
            controller: 'ethHideAddController'
        })
