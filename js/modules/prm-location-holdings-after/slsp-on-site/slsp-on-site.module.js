    import { slspOnSiteController } from './slsp-on-site.controller';
    import { slspOnSiteHtml } from './slsp-on-site.html';

    export const slspOnSiteModule = angular
        .module('slspOnSiteModule', [])
        .controller('slspOnSiteController', slspOnSiteController)
        .component('slspOnSiteComponent', {
            bindings: { afterCtrl: '<' },
            controller: 'slspOnSiteController',
            template: slspOnSiteHtml
        })
