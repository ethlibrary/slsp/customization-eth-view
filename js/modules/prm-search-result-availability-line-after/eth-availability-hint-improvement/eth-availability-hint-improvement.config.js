export const ethAvailabilityHintImprovementConfig = function(){
    return {
        hsa: {
            label: {
                linktextNoOnline:{
                    de: 'Verfügbare Services überprüfen',
                    en: 'Check for available services'
                }
            }
        },
        elending: {
            label: {
                access:{
                    de: 'Zugriff via E-Lending (E-Book-Ausleihe für Nicht-ETH-Angehörige)',
                    en: 'Access via E-Lending (E-Book loan for non-members of ETH Zurich)'
                }
            }
        },
        noInventory:{
            label: {
                online1:{
                    de: 'Nicht an der ETH Zürich verfügbar',
                    en: 'Not available at ETH Zurich'
                },
                online2:{
                    de: 'Online-Zugriff nur an anderen swisscovery-Bibliotheken',
                    en: 'Online access only at other swisscovery libraries'
                },
                print1:{
                    de: 'Nicht an der ETH Zürich verfügbar',
                    en: 'Not available at ETH Zurich'
                },
                print2:{
                    de: 'Verfügbare Services anderer swisscovery-Bibliotheken prüfen',
                    en: 'Check for available services at other swisscovery libraries'
                }
            }
        }
    }
}
