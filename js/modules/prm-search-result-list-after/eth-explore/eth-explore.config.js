export const ethExploreConfig = function(){
    return {
        label: {
            linksHeading: {
                de: 'Suchen Sie ...',
                en: 'Are you looking for ...'
            },
            poisLinkText: {
                de: '... einen Ort in der Schweiz?',
                en: '... a place in Switzerland?'
            },
            personsLinkText: {
                de: '... eine Person?',
                en: '... a person?'
            },
            noResults: {
                de: 'Keine Vorschläge',
                en: 'No suggestions'
            },
        }
    }
}
