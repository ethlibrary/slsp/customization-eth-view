export class tmiComposeNbController {

    constructor( tmiComposeNbService, ethConfigService, tmiComposeNbConfig, $timeout ) {
        this.tmiComposeNbService = tmiComposeNbService;
        this.ethConfigService = ethConfigService;
        this.config = tmiComposeNbConfig;
        this.$timeout = $timeout;
    }

    $onInit() {
        try{
            // Alle Online: ETH_NachlassbibliothekTM
            this.parentCtrl = this.afterCtrl.parentCtrl;
            if(!this.parentCtrl.isFullView){
                return;
            }
            // check if this is a digital NB resource and get MMS ID for print record
            this.mmsIdNBRecordPrint = null;
            let originalSourceId = null;
            let nebisId = null;
            if(this.parentCtrl.item.pnx.display && this.parentCtrl.item.pnx.display.source && this.parentCtrl.item.pnx.display.source.length > 0 && this.parentCtrl.item.pnx.display.source[0] === 'eth_nachlassbibliothek'){
                if(this.parentCtrl.item.pnx.control.originalsourceid && this.parentCtrl.item.pnx.control.originalsourceid[0])
                {
                    // oai:agora.ch:004261444_08
                    originalSourceId = this.parentCtrl.item.pnx.control.originalsourceid[0];
                    if(originalSourceId.indexOf('_')>-1){
                        originalSourceId = originalSourceId.substring(0,originalSourceId.indexOf('_'));
                    }
                    nebisId = 'ebi01_prod' + originalSourceId.substring(originalSourceId.lastIndexOf(':') + 1);

                    // https://daas.library.ethz.ch/rib/v3/mapping/redirect?id=ebi01_prod004464904&result=map
                    this.tmiComposeNbService.getPrintData(nebisId)
                        .then(data => {
                            if(data.map && data.map.length > 0){
                                this.mmsIdNBRecordPrint = data.map[0].almaSearch;
                            }
                        })
                        .catch(e => {
                            console.error("***TMI*** an error occured: tmiComposeNbController tmiComposeNbService.getPrintData");
                            console.error(e.message);
                        })
                }
            }

            // check if this is a print NB resource and get MMS ID for online record
            this.mmsIdNBRecordOnline = [];
            let oaiId = null;

            if(this.parentCtrl.item.pnx.display.source && this.parentCtrl.item.pnx.display.source[0] === 'Alma' && this.parentCtrl.item.pnx.display.lds02 && this.parentCtrl.item.pnx.display.lds02.length > 0){
                this.parentCtrl.item.pnx.display.lds02.forEach(i => {
                    if(i.indexOf('(NEBIS)')>-1){
                        // (NEBIS)004464904EBI01 -> oai:agora.ch:004464904
                        oaiId = "oai:agora.ch:" + i.substring(7, i.length - 5);
                    }
                })
                if(oaiId){
                    // multiple result 'Sämtliche Romane und Novellen Dostoevskij'
                    // oai:agora.ch:004261444
                    this.tmiComposeNbService.getOnlineData(oaiId)
                        .then(data => {
                            if(data && data.docs && data.docs.length > 0){
                                data.docs.forEach(d => {
                                    this.mmsIdNBRecordOnline.push(
                                        {
                                            id:d.pnx.control.sourcerecordid[0],
                                            title:d.pnx.display.title[0]
                                        }
                                    )
                                })
                                if(this.mmsIdNBRecordOnline.length > 1){
                                    this.mmsIdNBRecordOnline.sort(function(a, b){
                                        // Sämtliche Romane und Novellen, Band 1
                                        let volA = a.title.substring(a.title.indexOf('Band') + 4);
                                        let volB = b.title.substring(a.title.indexOf('Band') + 4);
                                        return volA - volB;
                                    })
                                }
                            }
                        })
                        .catch(e => {
                            console.error("***TMI*** an error occured: tmiComposeNbController tmiComposeNbService.getOnlineData");
                            console.error(e.message);
                        })
                }
            }
        }
        catch(e){
            console.error("***TMI*** an error occured: tmiComposeNbController $onInit\n\n");
            console.error(e.message);
            throw(e);
        }
    }

    loadPage(mmsid) {
        let vid = window.appConfig.vid;
        let tab = window.appConfig['primo-view']['available-tabs'][0];
        let lang = this.ethConfigService.getLanguage();
        let url = `/discovery/fulldisplay?lang=${lang}&tab=${tab}&vid=${vid}&docid=alma${mmsid}`;
        location.href = url;
    }
}

tmiComposeNbController.$inject = ['tmiComposeNbService','ethConfigService', 'tmiComposeNbConfig', '$timeout' ];
