export const ethPersonCardConfig = function(){
    return {
        whitelistMetagrid:
            ["sudoc","hallernet", "fotostiftung", "sikart","elites-suisses-au-xxe-siecle","bsg", "dodis", "helveticarchives", "helveticat", "hls-dhs-dss", "histoirerurale","lonsea","ssrq","alfred-escher","geschichtedersozialensicherheit"],
        label: {
            'sudoc': {
                de: 'Sudoc (Système Universitaire de Documentation)',
                en: 'Sudoc (Système Universitaire de Documentation)',
                fr: 'Sudoc (Système Universitaire de Documentation)',
                it: 'Sudoc (Système Universitaire de Documentation)'
            },
            'hallernet': {
                de: 'Editions- und Forschungsplattform hallerNet',
                en: 'Editions- und Forschungsplattform hallerNet',
                fr: 'Editions- und Forschungsplattform hallerNet',
                it: 'Editions- und Forschungsplattform hallerNet'
            },
            'fotostiftung': {
                de: 'Fotostiftung Schweiz',
                en: 'Fotostiftung Schweiz',
                fr: 'Fotostiftung Schweiz',
                it: 'Fotostiftung Schweiz'
            },
            'sikart': {
                de: 'SIKART',
                en: 'SIKART',
                fr: 'SIKART',
                it: 'SIKART'
            },
            'elites-suisses-au-xxe-siecle': {
                de: 'Schweizerische Eliten im 20. Jahrhundert',
                en: 'Swiss elites database',
                fr: 'Elites suisses au XXe siècle',
                it: 'Elites suisses au XXe siècle'
            },
            'bsg': {
                de: 'Bibliographie der Schweizergeschichte',
                en: 'Bibliography on Swiss History',
                fr: 'Bibliographie de l\'histoire suisse',
                it: 'Bibliografia della storia svizzera'

            },
            'dodis': {
                de: 'Diplomatische Dokumente der Schweiz',
                en: 'Diplomatic Documents of Switzerland',
                fr: 'Documents diplomatiques suisses',
                it: 'Documenti diplomatici svizzeri'
            },
            'helveticat': {
                de: 'Helveticat',
                en: 'Helveticat',
                fr: 'Helveticat',
                it: 'Helveticat'
            },
            'helveticarchives': {
                de: 'Helveticat',
                en: 'Helveticat',
                fr: 'Helveticat',
                it: 'Helveticat'
            },
            'hls-dhs-dss': {
                de: 'Historisches Lexikon der Schweiz',
                en: 'Historical Dictionary of Switzerland',
                fr: 'Dictionnaire historique de la Suisse',
                it: 'Dizionario storico della Svizzera'
            },
            'histoirerurale': {
                de: 'Archiv für Agrargeschichte',
                en: 'Archives of rural history',
                fr: 'Archives de l\'histoire rurale',
                it: 'Archivio della storia rurale'
            },
            'lonsea': {
                de: 'Lonsea',
                en: 'Lonsea',
                fr: 'Lonsea',
                it: 'Lonsea'
            },
            'ssrq': {
                de: 'Sammlung Schweizerischer Rechtsquellen',
                en: 'Collection of Swiss Law Sources',
                fr: 'Collection des sources du droit suisse',
                it: 'Collana Fonti del diritto svizzero'
            },
            'alfred-escher': {
                de: 'Alfred Escher-Briefedition',
                en: 'Alfred Escher letters edition',
                fr: 'Edition des lettres Alfred Escher',
                it: 'Edizione lettere Alfred Escher'
            },
            'geschichtedersozialensicherheit': {
                de: 'Geschichte der sozialen Sicherheit',
                en: 'Geschichte der sozialen Sicherheit',
                fr: 'Histoire de la sécurité sociale',
                it: 'Storia della sicurezza sociale'
            },
            img_license: {
                de: 'Lizenz für das Bild siehe',
                en: 'Information regarding the license status of embedded media files',
            },
            toc: {
                de: 'Inhaltsverzeichnis aus anderem Bibliotheksverbund',
                en: 'Table of contents from other library network'
            },
            result: {
                de: 'Ergebnis',
                en: 'result'
            },
            results: {
                de: 'Ergebnisse',
                en: 'results'
            },
            resultsGndCDI: {
                de: 'Ergebnisse für die Suche nach GND',
                en: 'results of search for GND'
            },
            resultsBirthCDI: {
                de: 'Ergebnisse für die Suche nach Name, aber eingeschränkt',
                en: 'results of search for name, but restricted '
            },
            resultsLabelCDI: {
                de: 'Ergebnisse für die Suche nach Name',
                en: 'results of search by name'
            },
            resultsVariantsCDI: {
                de: 'Ergebnisse für die Suche nach Name und Namensvarianten',
                en: 'results of search for name and name variants'
            },
            precision: {
                de: 'Genauere oder mehr Ergebnisse',
                en: 'More precise or more results'
            },
            personPageLink: {
                de: 'Mehr Informationen zu',
                en: 'More information about'
            },
            moreInformations: {
                de: 'Mehr Informationen zur Person',
                en: 'More information about the person'
            },
            infoGndWd: {
                de: 'Informationen aus Wikidata und der GND',
                en: 'Information from Wikidata and the GND'
            },
            archives: {
                de: 'Archive',
                en: 'Archives'
            },
            researcherProfile: {
                de: 'Forscherprofile via IDs aus Wikidata',
                en: 'Researcher profiles via IDs from Wikidata'
            },
            linkOrcid: {
                de: 'ORCID Profil',
                en: 'ORCID record'
            },
            linkScholar: {
                de: 'Google Scholar Profil',
                en: 'Google Scholar profile'
            },
            linkScopus: {
                de: 'Scopus author details',
                en: 'Scopus author details'
            },
            linkResearchgate: {
                de: 'ResearchGate Profil',
                en: 'ResearchGate profile'
            },
            linkDimension: {
                de: 'Dimensions Profil',
                en: 'Dimensions profile'
            },
            linksWD: {
                de: 'Links aus Wikidata',
                en: 'Links from Wikidata'
            },
            linksMetagrid: {
                de: 'Links von Metagrid',
                en: 'Links from Metagrid'
            },
            linksPrometheus: {
                de: 'Links vom BEACON Aggregator',
                en: 'Links from BEACON Aggregator'
            },
            linksPrometheusHint1: {
                de: 'Links vom ',
                en: 'Links from the '
            },
            linksPrometheusHint2: {
                de: 'BEACON',
                en: 'BEACON'
            },
            linksPrometheusHint3: {
                de: 'Aggregator des',
                en: 'Aggregator of the'
            },
            linksPrometheusHint4: {
                de: 'LMU Center for Digital Humanities',
                en: 'LMU Center for Digital Humanities'
            },
            linkTextAllLMU: {
                de: 'Mehr Links',
                en: 'More Links'
            },
            students: {
                de: 'Student*innen oder Doktorand*innen (aus Wikidata)',
                en: 'Students or doctoral students (from Wikidata)'
            },
            teachers: {
                de: 'Lehrer*innen (aus Wikidata)',
                en: 'Teachers (from Wikidata)'
            },
            relatedPersons: {
                de: 'Personen aus der GND',
                en: 'Related Persons from the GND'
            },
            born: {
                de: 'Geboren',
                en: 'Born'
            },
            died: {
                de: 'Gestorben',
                en: 'Died'
            },
            placeOfActivity: {
                de: 'Wirkungsorte',
                en: 'Place of activity'
            },
            precisionIntroGND1: {
                de: 'Sie sehen',
                en: 'You see'
            },
            precisionIntroGND2: {
                de: 'für die',
                en: 'of the'
            },
            precisionIntroGND3: {
                de: 'Suche nach einer eindeutigen Identifikationsnummer (GND) der Person.',
                en: 'search for a unique identification number (GND) of the person.'
            },
            precisionIntroGND4: {
                de: 'Achtung: Sie sehen möglicherweise nicht alle Ergebnisse, da die Identifikationsnummer (GND) der Person nicht allen Ressourcen zugeordnet ist.',
                en: 'Attention: You may not see all results because the identification number (GND) of the person is not assigned to all resources.'
            },
            precisionIntroBirth1: {
                de: 'Sie sehen',
                en: 'You see'
            },
            precisionIntroBirth2: {
                de: 'für die',
                en: 'of the'
            },
            precisionIntroBirth3: {
                de: 'Suche nach dem Namen, eingeschränkt durch Identifikationsnummer (GND) der Person ODER das Geburtsjahr.',
                en: 'search for the name, restricted by the identification number (GND) of the person OR the year of birth.'
            },
            precisionIntroBirth4: {
                de: 'Achtung: Sie sehen möglicherweise nicht alle Ergebnisse für diese Person, da Geburtsjahr oder Identifikationsnummer (GND) nicht allen Ressourcen zugeordnet sind.',
                en: 'Attention: You may not see all results for this person because the year of birth or identification number (GND) is not assigned to all resources.'
            },
            precisionIntroLabel1: {
                de: 'Sie sehen',
                en: 'You see'
            },
            precisionIntroLabel2: {
                de: 'für die',
                en: 'of the'
            },
            precisionIntroLabel3: {
                de: 'Suche nach dem Namen.',
                en: 'search for the name.'
            },
            precisionIntroLabel4: {
                de: 'Sie sehen möglicherweise auch falsche Ergebnisse, wenn mehrere Personen denselben Namen tragen.',
                en: 'You may also see wrong results if several people have the same name.'
            },
            precisionChoose: {
                de: 'Wählen Sie eine Suchoption',
                en: 'Select one of the search options'
            },
            precisionGnd1: {
                de: 'Suche nur nach',
                en: 'Search for'
            },
            precisionGnd2: {
                de: 'eindeutiger Identifikationsnummer (GND)',
                en: 'the unique identification number (GND)'
            },
            precisionGnd3: {
                de: 'der Person.',
                en: 'of the person (GND)'
            },
            precisionGnd4: {
                de: 'Sie sehen möglicherweise nicht alle Ergebnisse für diese Person, da die Identifikationsnummer (GND) nicht bei allen Ressourcen zugeordnet ist.',
                en: 'You may not see all results for this person because the identification number (GND) is not assigned to all resources.'
            },
            precisionBest1: {
                de: 'Suche nach dem',
                en: 'Search for the'
            },
            precisionBest2: {
                de: 'Namen',
                en: 'name'
            },
            precisionBest3: {
                de: ', eingeschränkt durch',
                en: ', restricted by the'
            },
            precisionBest4: {
                de: 'Identifikationsnummer (GND) der Person',
                en: 'identification number (GND) of the person'
            },
            precisionBest5: {
                de: 'ODER das',
                en: 'OR the'
            },
            precisionBest6: {
                de: 'Geburtsjahr.',
                en: 'year of birth.'
            },
            precisionBest7: {
                de: 'Sie sehen möglicherweise nicht alle Ergebnisse für diese Person, da Geburtsjahr oder Identifikationsnummer (GND) nicht allen Ressourcen zugeordnet sind.',
                en: 'You may not see all results for this person because the year of birth or identification number (GND) is not assigned to all resources.'
            },
            precisionName1: {
                de: 'Suche',
                en: 'Search by'
            },
            precisionName2: {
                de: 'nur nach dem Namen',
                en: 'name only'
            },
            precisionName3: {
                de: 'Sie sehen möglicherweise auch falsche Ergebnisse, wenn mehrere Personen denselben Namen tragen.',
                en: 'You may also see wrong results if several people have the same name.'
            },
            precisionNameVariants1: {
                de: 'Suche nach',
                en: 'Search for'
            },
            precisionNameVariants2: {
                de: 'dem Namen',
                en: 'the name'
            },
            precisionNameVariants3: {
                de: 'und den',
                en: 'and'
            },
            precisionNameVariants4: {
                de: 'Namensvarianten',
                en: 'name variants'
            },
            precisionNameVariants5: {
                de: '(aus Wikidata)',
                en: '(from Wikidata)'
            },
            precisionNameVariants6: {
                de: 'Sie sehen möglicherweise auch falsche Ergebnisse, wenn mehrere Personen eine der Namensvarianten tragen.',
                en: 'You may also see wrong results if several people have one of the name variants.'
            },
            birthday: {
                de: 'Geburtstag',
                en: 'Birthday'
            }
        },
        url: {
            info: {
                de: 'https://library.ethz.ch',
                en: 'https://library.ethz.ch'
            },
            beacon: {
                de: 'https://de.wikipedia.org/wiki/Wikipedia:BEACON',
                en: 'https://de.wikipedia.org/wiki/Wikipedia:BEACON'
            },
            lmu: {
                de: 'https://www.itg.uni-muenchen.de/index.html',
                en: 'https://www.itg.uni-muenchen.de/index.html'
            }            
        }
    }
}
