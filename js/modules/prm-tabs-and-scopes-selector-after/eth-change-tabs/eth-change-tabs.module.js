/**
* @ngdoc module
* @name ethChangeTabsModule
*
* @description
*
* Customization for the search bar:<br>
* - Changing tabs triggers search.
*
* <b>AngularJS Dependencies</b><br>
*
*
* <b>CSS/Image Dependencies</b><br>
*
*/

import {ethChangeTabsController} from './eth-change-tabs.controller';

export const ethChangeTabsModule = angular
    .module('ethChangeTabsModule', [])
        .controller('ethChangeTabsController', ethChangeTabsController)
        .component('ethChangeTabsComponent',{
            bindings: {parentCtrl: '<'},
            controller: 'ethChangeTabsController',
        })
