export class ethElendingLinkController {

    constructor( ethConfigService, ethElendingLinkConfig ) {
        this.ethConfigService = ethConfigService;
        this.config = ethElendingLinkConfig;
    }

    $onInit() {
        try{
            this.parentCtrl = this.afterCtrl.parentCtrl;
            this.source = '';

            if (!this.parentCtrl.item || !this.parentCtrl.item.pnx) {
                console.error("***ETH*** ethElendingLinkController.$onInit: item or item.pnx not available");
                return;
            }

            if(this.parentCtrl.item.pnx.display.source && this.parentCtrl.item.pnx.display.source.length > 0){
                this.source = this.parentCtrl.item.pnx.display.source[0];
            }
        }
        catch(e){
            console.error("***ETH*** an error occured: ethElendingLinkController $onInit\n\n");
            console.error(e.message);
        }
    }
}

ethElendingLinkController.$inject = ['ethConfigService', 'ethElendingLinkConfig' ];
