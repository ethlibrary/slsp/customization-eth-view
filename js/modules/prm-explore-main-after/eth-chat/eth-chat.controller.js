export class ethChatController {

    constructor( $sce ) {
        this.$sce = $sce;
    }

    $onInit() {
        try{
            this.$sce.trustAsResourceUrl('https://eu.libraryh3lp.com/chat/ethbibs-queue@chat.eu.libraryh3lp.com?skin=15125');
        }
        catch(e){
            console.error("***ETH*** an error occured: ethChatController.onInit()\n\n");
            console.error(e.message);
        }
    }
}

ethChatController.$inject = ['$sce'];
