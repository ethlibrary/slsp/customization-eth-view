/**
* @ngdoc service
* @name ethPlaceLinkService
*
* @description
*
* getTopicPlaces(): get informations about geo topics(651 place) from geodata graph
* enrichPOIsWithQID(): enriches poi objects by qid and wikidata label
* getPlaceFromETHorama(): getPlace informations from ETHorama Database
*
*
* <b>Used by</b><br>
* Module {@link ETH.ethPlaceLinkModule}<br>
*
*
 */
export const ethPlaceLinkService = ['$http', '$sce', function($http, $sce){

    function getTopicPlaces(aGndId){
        let baseurl = 'https://daas.library.ethz.ch/rib/v3/graph/places-by-gnd-list';
        let url = baseurl + '?gnd=' + aGndId.join(',');
        $sce.trustAsResourceUrl(url);
        return $http.get(url)
            .then(
                function(response){
                    return response.data;
                },
                function(httpError){
                    if (httpError.status === 404 || httpError.status === 500 || httpError.statusText === 'Bad Request')return null;
                    let error = "***ETH*** an error occured: ethPlacePageLinkService.getPlaces():" + httpError.status;
                    if (httpError.data && httpError.data.errorMessage) {
                        error += ' - ' + httpError.data.errorMessage;
                    }
                    console.error(error);
                    return null;
                }
            )
    }

    function enrichPOIsWithQID(pois){
        const endpointUrl = 'https://de.wikipedia.org/w/api.php?action=query&prop=pageprops&format=json&origin=*';
        let titles = ''
        let counter = 1;
        pois.forEach(poi => {
            if(counter <= 50){
                let link = '';
                if(poi.references && poi.references.wikipedia && poi.references.wikipedia['de']){
                    link = poi.references.wikipedia['de'];
                }
                else if(poi.references && poi.references.wikipedia && poi.references.wikipedia['en']){
                    link = poi.references.wikipedia['en'];
                }
                if(link !== ''){
                    if(counter > 1){
                        titles += '|';
                    }
                    let temp = link.substring(link.lastIndexOf('/') + 1).replace('%28','(').replace('%29',')');
                    if(temp.indexOf('#')>-1){
                        temp = temp.substring(0,temp.indexOf('#'));
                    }
                    titles += temp.trim();
                    poi.wikiTitle = temp.trim();
                    counter += 1;
                }
            }
        });
        if(titles === '')return null;
        let fullUrl = endpointUrl + '&titles=' + titles;
        $sce.trustAsResourceUrl(fullUrl);
        return $http.get(fullUrl)
            .then(
                function(response){
                    let mapTitleQid = [];
                    for (const key in response.data.query.pages) {
                      let page = response.data.query.pages[key];
                      if(page.pageprops){
                        let title = page.title;
                        let qid = page.pageprops['wikibase_item'];
                        if(response.data.query.normalized){
                            let normalized = response.data.query.normalized.filter(e => {
                              return e.to === title;
                            });
                            if(normalized.length > 0){
                              title = normalized[0].from;
                            }
                        }
                        mapTitleQid[title] = qid;
                      }
                    }
                    pois.forEach(poi => {
                        if(poi.wikiTitle){
                            if(mapTitleQid[poi.wikiTitle]){
                                poi.qid = mapTitleQid[poi.wikiTitle];
                            }
                            else{
                                let decodedWikiTitle = decodeURIComponent(poi.wikiTitle);
                                if(mapTitleQid[decodedWikiTitle]){
                                    poi.qid = mapTitleQid[decodedWikiTitle];
                                    poi.wikiTitle = decodedWikiTitle
                                }
                            }
                        }
                    })
                    return pois;
                },
                function(httpError){
                    let error = "***ETH*** an error occured: ethPlaceLinkService.enrichPOIsWithQID:" + httpError.status;
                    if (httpError.data && httpError.data.errorMessage) {
                        error += ' - ' + httpError.data.errorMessage;
                    }
                    console.error(error);
                    return null;
                }
            )
    }

    function getPlaceFromETHorama(docId){
        let baseurl = 'https://api.library.ethz.ch/ethorama/v1/pois';
        let url = baseurl + '?apikey=BKFefOQWF3VGq2sreNcyLqK7Gob61xO9jnLQAd0wy82ktIYn&pageSize=100&details=false&docId=' + docId;
        $sce.trustAsResourceUrl(url);
        return $http.get(url)
            .then(
                function(response){
                    return response.data;
                },
                function(httpError){
                    if (httpError.status === 404 || httpError.status === 500 || httpError.statusText === 'Bad Request')return null;
                    let error = "***ETH*** an error occured: ethPlaceLinkService.getPlaceFromETHorama():" + httpError.status;
                    if (httpError.data && httpError.data.errorMessage) {
                        error += ' - ' + httpError.data.errorMessage;
                    }
                    console.error(error);
                    return null;
                }
            )
    }

    return {
        getTopicPlaces: getTopicPlaces,
        enrichPOIsWithQID: enrichPOIsWithQID,
        getPlaceFromETHorama: getPlaceFromETHorama
    };

}]
