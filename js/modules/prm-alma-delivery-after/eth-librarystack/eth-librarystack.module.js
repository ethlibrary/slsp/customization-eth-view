/**
* @ngdoc module
* @name ethLibrarystackModule
*
* @description
*
* adds a note for librarystack resources
*
*
* <b>AngularJS Dependencies</b><br>
* Service /js/services {@link ETH.ethConfigService}<br>
* Service {@link ETH.ethLibrarystackConfig}<br>
*
* <b>CSS/Image Dependencies</b><br>
* librarystack.css
*
*/
import {ethConfigService} from '../../../services/eth-config.service';
import {ethLibrarystackConfig} from './eth-librarystack.config';
import {ethLibrarystackController} from './eth-librarystack.controller';

export const ethLibrarystackModule = angular
    .module('ethLibrarystackModule', [])
        .factory('ethConfigService', ethConfigService)
        .factory('ethLibrarystackConfig', ethLibrarystackConfig)
        .controller('ethLibrarystackController', ethLibrarystackController)
        .component('ethLibrarystackComponent', {
            bindings: {afterCtrl: '<'},
            controller: 'ethLibrarystackController'
        })
