/**
* @ngdoc module
* @name ethAvailabilityHintImprovementModule
*
* @description
*
* Customization for the availability line:<br>
* - show type/access right of research collection items<br>
* - show hint for E-Lending resource
* - HSA: some resources are without digitalisat, but Primo shows them as online resources. Correct this.
* - improve availability text, if availability = "no_inventory"
*
* <b>AngularJS Dependencies</b><br>
* Service /js/services {@link ETH.ethConfigService}<br>
* Service {@link ETH.ethAvailabilityHintImprovementConfig}<br>
*
* <b>CSS/Image Dependencies</b><br>
* CSS eth-availability-hint-improvement.css
*
*
*/
import {ethConfigService} from '../../../services/eth-config.service';
import {ethAvailabilityHintImprovementConfig} from './eth-availability-hint-improvement.config';
import {ethAvailabilityHintImprovementController} from './eth-availability-hint-improvement.controller';
import {ethAvailabilityHintImprovementHtml} from './eth-availability-hint-improvement.html';

export const ethAvailabilityHintImprovementModule = angular
    .module('ethAvailabilityHintImprovementModule', [])
        .factory('ethConfigService', ethConfigService)
        .factory('ethAvailabilityHintImprovementConfig', ethAvailabilityHintImprovementConfig)
        .controller('ethAvailabilityHintImprovementController', ethAvailabilityHintImprovementController)
        .component('ethAvailabilityHintImprovementComponent', {
            bindings: {afterCtrl: '<'},
            controller: 'ethAvailabilityHintImprovementController',
            template: ethAvailabilityHintImprovementHtml
        })
