/**
* @ngdoc service
* @key ethSearchService
*
* @description
*
* Service to get context informations of the current search (tab, scope etc).
*
* <b>Used by</b><br>
*
* Eth Component {@link ETH.ethAltmetricModule}<br>
*
*/
export const ethSearchService = ['$rootScope', function( $rootScope ){

    function getTab(searchService) {
        return searchService.searchFieldsService._tab;
    }

    function getScope(searchService) {
        return searchService.searchFieldsService._scope;
    }

    function getMainSearch (searchService) {
        return searchService.searchFieldsService._mainSearch;
    }

    return {
        getTab: getTab,
        getScope: getScope,
        getMainSearch: getMainSearch
    }
}]
