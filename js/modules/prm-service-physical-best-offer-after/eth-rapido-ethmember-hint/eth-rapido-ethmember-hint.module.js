/**
* @ngdoc module
* @name ethRapidoEthmemberHintModule
*
* @description
*
* fees hint at top of rapido section, if
* - ETH Member and Digi Button inside Getit
*
* <b>AngularJS Dependencies</b><br>
* Service /js/services {@link ETH.ethConfigService}<br>
* Service {@link ETH.ethRapidoEthmemberHintConfig}<br>
*
* ethSetIdModule (opac-after) sets id attribute, which is used as target
*
* <b>CSS/Image Dependencies</b><br>
* CSS eth-rapido-ethmember-hint.css
*
*/

import {ethConfigService} from '../../../services/eth-config.service';
import {ethRapidoEthmemberHintConfig} from './eth-rapido-ethmember-hint.config';
import {ethRapidoEthmemberHintController} from './eth-rapido-ethmember-hint.controller';

export const ethRapidoEthmemberHintModule = angular
    .module('ethRapidoEthmemberHintModule', [])
        .factory('ethConfigService', ethConfigService)
        .factory('ethRapidoEthmemberHintConfig', ethRapidoEthmemberHintConfig)
        .controller('ethRapidoEthmemberHintController', ethRapidoEthmemberHintController)
        .component('ethRapidoEthmemberHintComponent', {
            bindings: {afterCtrl: '<'},
            controller: 'ethRapidoEthmemberHintController'
        })
