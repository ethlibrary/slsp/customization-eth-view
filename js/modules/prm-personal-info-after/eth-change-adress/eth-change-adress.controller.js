export class ethChangeAdressController {
    constructor( ethConfigService, ethChangeAdressConfig ) {
        this.ethConfigService = ethConfigService;
        this.config = ethChangeAdressConfig;
    }

    $onInit() {
        try{
            let targetElement = angular.element(document.querySelector('prm-personal-info > div.layout-wrap.layout-align-center-start.layout-row > md-card:nth-child(2) md-card-content'));
            let element = angular.element(document.querySelector('eth-change-adress-component'));
            targetElement.append(element);
        }
        catch(e){
            console.error("***ETH*** an error occured: ethChangeAdressController\n\n");
            console.error(e.message);
        }
    }

}

ethChangeAdressController.$inject = ['ethConfigService', 'ethChangeAdressConfig' ];
