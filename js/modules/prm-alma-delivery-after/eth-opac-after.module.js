import {ethPartofHintModule} from './eth-partof-hint/eth-partof-hint.module';
import {ethSetIdModule} from './eth-set-id/eth-set-id.module';
import { slspOvpReservationModule } from './slsp-ovp-reservation/slsp-ovp-reservation.module';

export const ethOpacAfterModule = angular
    .module('ethOpacAfterModule', [])
        .component('prmOpacAfter',  {
            bindings: {parentCtrl: '<'},
            template: `
            <eth-set-id-component after-ctrl="$ctrl"></eth-set-id-component>
            <eth-partof-hint-component after-ctrl="$ctrl"></eth-partof-hint-component>
            <slsp-ovp-reservation-component after-ctrl="$ctrl"></slsp-ovp-reservation-component>
            `
        });
ethOpacAfterModule.requires.push(ethPartofHintModule.name);
ethOpacAfterModule.requires.push(ethSetIdModule.name);
ethOpacAfterModule.requires.push(slspOvpReservationModule.name);
