export class ethReferencesHintController {

    constructor( $scope, $compile, ethConfigService, ethSessionService, ethReferencesHintConfig ) {
        this.$scope = $scope;
        this.$compile = $compile;
        this.ethConfigService = ethConfigService;
        this.ethSessionService = ethSessionService;
        this.config = ethReferencesHintConfig;
    }

    $onInit() {
        try{
            let contLabels = document.querySelector('prm-favorites-labels');
            if (!contLabels) {
                console.error("***ETH*** prmFavoritesLabelsAfter.$onInit: prm-favorites-labels not available");
                return;
            }
            let html = `
            <div class="eth-references-hint sidebar-inner-wrapper padding-left-large eth-favorites-tip" layout="column">
                <h2 class="sidebar-title">
                    {{$ctrl.ethConfigService.getLabel($ctrl.config, 'heading')}}
                </h2>
                <div class="sidebar-section layout-align-start-start layout-column" layout="column" layout-align="start start">
                    <p>
                        {{$ctrl.ethConfigService.getLabel($ctrl.config, 'text1')}}
                        <a href="{{::$ctrl.ethConfigService.getUrl($ctrl.config, 'link')}}" target="_blank" rel="noopener">
                            <span>{{::$ctrl.ethConfigService.getLabel($ctrl.config, 'linktext')}}</span>
                            <prm-icon external-link icon-type="svg" svg-icon-set="primo-ui" icon-definition="open-in-new"></prm-icon>
                        </a>
                        {{$ctrl.ethConfigService.getLabel($ctrl.config, 'text2')}}
                    </p>
                </div>
            </div>`;
            angular.element(contLabels).prepend(this.$compile(html)(this.$scope));
        }
        catch(e){
            console.error("***ETH*** an error occured: ethReferencesHintController.onInit()\n\n");
            console.error(e.message);
        }
    }
}

ethReferencesHintController.$inject = ['$scope', '$compile', 'ethConfigService', 'ethSessionService', 'ethReferencesHintConfig'];
