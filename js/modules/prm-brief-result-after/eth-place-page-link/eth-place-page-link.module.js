/**
* @ngdoc module
* @name ethPlacePageLinkModule
*
* @description
*
* - checks lds03 for gnd links
* - checks whether there are places for gnd ids in the geodata graph
* - if so: render links to place pages
*
* <b>AngularJS Dependencies</b><br>
* Service /js/services {@link ETH.ethConfigService}<br>
* Service {@link ETH.ethPlacePageLinkConfig}<br>
* Service {@link ETH.ethPlacePageLinkService}<br>
*
*
* <b>CSS/Image Dependencies</b><br>
* CSS eth-place-page-link.css
*
*/
import {ethConfigService} from '../../../services/eth-config.service';
import {ethPlacePageLinkConfig} from './eth-place-page-link.config';
import {ethPlacePageLinkService} from './eth-place-page-link.service';
import {ethPlacePageLinkController} from './eth-place-page-link.controller';
import {ethPlacePageLinkHtml} from './eth-place-page-link.html';

export const ethPlacePageLinkModule = angular
    .module('ethPlacePageLinkModule', [])
        .factory('ethConfigService', ethConfigService)
        .factory('ethPlacePageLinkConfig', ethPlacePageLinkConfig)
        .factory('ethPlacePageLinkService', ethPlacePageLinkService)
        .controller('ethPlacePageLinkController', ethPlacePageLinkController)
        .component('ethPlacePageLinkComponent',{
            bindings: {afterCtrl: '<'},
            controller: 'ethPlacePageLinkController',
            template: ethPlacePageLinkHtml
        })
