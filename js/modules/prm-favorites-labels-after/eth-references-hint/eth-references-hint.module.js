/**
* @ngdoc module
* @name ethReferencesHintModule
*
* @description
*
* Customization for the favorites labels (right column of favorites page):<br>
* - Hint about reference management is added.
*
*
* <b>AngularJS Dependencies</b><br>
* Service /js/services {@link ETH.ethSessionService}<br>
* Service /js/services {@link ETH.ethConfigService}<br>
* Service /js/services {@link ETH.ethReferencesHintConfig}<br>
*
*
* <b>AngularJS Dependencies</b><br>
*
*
* <b>CSS/Image Dependencies</b><br>
* CSS eth-references-hint.css
*
*/

import {ethConfigService} from '../../../services/eth-config.service';
import {ethSessionService} from '../../../services/eth-session.service';
import {ethReferencesHintConfig} from './eth-references-hint.config';
import {ethReferencesHintController} from './eth-references-hint.controller';

export const ethReferencesHintModule = angular
        .module('ethReferencesHintModule', [])
            .factory('ethConfigService', ethConfigService)
            .factory('ethSessionService', ethSessionService)
            .factory('ethReferencesHintConfig', ethReferencesHintConfig)
            .controller('ethReferencesHintController', ethReferencesHintController)
            .component('ethReferencesHintComponent',{
                controller: 'ethReferencesHintController'
            })
