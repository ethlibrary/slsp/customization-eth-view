import {ethPageTypeModule} from './eth-page-type/eth-page-type.module';

export const ethSearchAfterModule = angular
    .module('ethSearchAfterModule', [])
        .component('prmSearchAfter',  {
            bindings: {parentCtrl: '<'},
            template: `<eth-page-type-component after-ctrl="$ctrl"></eth-page-type-component>`
        });

ethSearchAfterModule.requires.push(ethPageTypeModule.name);
