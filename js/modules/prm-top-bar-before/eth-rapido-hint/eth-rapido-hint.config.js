export const ethRapidoHintConfig = function(){
    return {
        label: {
            text: {
                de: 'Bestellmöglichkeiten erklärt:',
                en: 'Ordering options explained:'
            },
            linktext1: {
                de: 'Merkblatt für ETH-Mitarbeitende und ETH-Doktorierende',
                en: 'Factsheet for ETH Zurich employees and doctoral students'
            },
            linktext2: {
                de: 'Merkblatt für ETH-Studierende und Nicht-ETH-Angehörige',
                en: 'Factsheet for ETH Zurich students and non-​members of ETH Zurich'
            },
            or: {
                de: 'oder',
                en: 'or'
            },
            close: {
                de: 'Schliessen',
                en: 'Close'
            }
        },
        url:{
            url1: {
                de: 'https://ethz.ch/content/dam/ethz/associates/ethlibrary-dam/documents/ausleihen-und-bestellen/Anleitung_Bestellung_swisscovery_ETH-MA.pdf',
                en: 'https://ethz.ch/content/dam/ethz/associates/ethlibrary-dam/documents/ausleihen-und-bestellen/Anleitung_Bestellung_swisscovery_ETH_employees_EN.pdf'
            },
            url2: {
                de: 'https://ethz.ch/content/dam/ethz/associates/ethlibrary-dam/documents/ausleihen-und-bestellen/Anleitung_Bestellung_swisscovery_ETH-Studierende_Nicht-ETH.pdf',
                en: 'https://ethz.ch/content/dam/ethz/associates/ethlibrary-dam/documents/ausleihen-und-bestellen/Anleitung_Bestellung_swisscovery_ETH_students_Non-members_EN.pdf'
            }
        }
    }
}
