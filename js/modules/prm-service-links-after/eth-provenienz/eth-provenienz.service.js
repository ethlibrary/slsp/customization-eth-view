/**
* @ngdoc service
* @name ethProvenienzService
*
* @description
* Service to get provenience informations from e-pics (via RIB)
*
* <b>Used by</b><br>
*
* Module {@link ETH.ethProvenienzModule}<br>
*
 */
export const ethProvenienzService = ['$http', '$sce', function($http, $sce){
    // https://daas.library.ethz.ch/rib/v3/enrichments/provenienz?doi=10.3931/e-rara-9423
    function getItems(doi){
        let url = "https://daas.library.ethz.ch/rib/v3/enrichments/provenienz?doi=" + doi;
        $sce.trustAsResourceUrl(url);

        return $http.get(url)
            .then(
                function(response){
                    return response.data;
                },
                function(httpError){
                    if (httpError.status === 404)return null;
                    let error = "***ETH*** an error occured: ethProvenienzService.getItems: " + httpError.status;
                    if (httpError.data && httpError.data.errorMessage) {
                        error += ' - ' + httpError.data.errorMessage;
                    }
                    console.error(error);
                    return null;
                }
            );
    }

    return {
        getItems: getItems
    };
}];
