import {ethAddLoginstatusClassModule} from './eth-add-loginstatus-class/eth-add-loginstatus-class.module';

export const ethUserAreaExpandableAfterModule = angular
    .module('ethUserAreaExpandableAfterModule', [])
        .component('prmUserAreaExpandableAfter',  {
            bindings: {parentCtrl: '<'},
            template: `<eth-add-loginstatus-class-component after-ctrl="$ctrl"></eth-add-loginstatus-class-component><eth-user-area-expandable-after parent-ctrl="$parent.$ctrl"></eth-user-area-expandable-after>`,
        });

ethUserAreaExpandableAfterModule.requires.push(ethAddLoginstatusClassModule.name);
