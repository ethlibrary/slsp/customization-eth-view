/**
* @ngdoc module
* @name ethLibkeyModule
*
* @description
*
* Customization for the availability line:<br>
* - requests a PDF download link from the browzine article endpoint (libkey)<br>
*
* <b>AngularJS Dependencies</b><br>
* Service {@link ETH.ethLibkeyService}<br>
* Service /js/services {@link ETH.ethSessionService}<br>
*
*
* <b>CSS/Image Dependencies</b><br>
* CSS eth-libkey.css
* IMG /img/pdf-download-icon.svg
*
*
*/
import {ethConfigService} from '../../../services/eth-config.service';
import {ethLibkeyConfig} from './eth-libkey.config';
import {ethLibkeyController} from './eth-Libkey.controller';
import {ethLibkeyService} from './eth-libkey.service';
import {ethSessionService} from '../../../services/eth-session.service';

export const ethLibkeyModule = angular
    .module('ethLibkeyModule', [])
        .factory('ethConfigService', ethConfigService)
        .factory('ethLibkeyConfig', ethLibkeyConfig)
        .factory('ethLibkeyService', ethLibkeyService)
        .factory('ethSessionService', ethSessionService)
        .controller('ethLibkeyController', ethLibkeyController)
        .component('ethLibkeyComponent', {
            bindings: {afterCtrl: '<'},
            controller: 'ethLibkeyController'
        })
