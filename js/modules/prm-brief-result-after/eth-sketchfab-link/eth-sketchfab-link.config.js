export const ethSketchfabLinkConfig = function(){
    return {
        label: {
            linktext:{
                de: '3D-Modell',
                en: '3D model'
            }
        }
    }
}
