export const ethOpenurlInterlibraryConfig = function(){
    return {
        label: {
            text:{
                de: 'Bestellen Sie eine Kopie des Artikels per',
                en: 'Order a copy of the article via'
            },
            linktext:{
                de: 'Fernleihe',
                en: 'Interlibrary loan'
            }
        },
        url:{
            link: {
                de: 'https://library.ethz.ch/recherchieren-und-nutzen/ausleihen-und-nutzen/bestellformulare/fernleihe-kopien-bestellen.html',
                en: 'https://library.ethz.ch/en/searching-and-using/borrowing-and-using/order-forms/interlibrary-loans-ordering-copies.html'
            }
        }
    }
}
