export const ethProvenienzConfig = function(){
    return {
        label: {
            heading: {
                de: 'Provenienz-Merkmale dieses Buches',
                en: 'Provenance marks in this book'
            },
            owner: {
                de: 'Besitzer',
                en: 'Owner'
            },
            dating: {
                de: 'Datierung',
                en: 'Dating'
            },
            swisscovery: {
                de: 'Provenienz in swisscovery',
                en: 'Provenance in swisscovery'
            }
        }
    }
}
