/**
* @ngdoc service
* @name ethLibkeyService
*
* @description
* Service to retrieve article informations from Browzine (Libkey)
*
* <b>Used by</b><br>
* 
* Module {@link ETH.ethLibkeyModule}<br>
*
*
 */
export const ethLibkeyService = ['$http', '$sce', function($http, $sce){

    function getArticle(doi, onCampus) {
        let url = 'https://daas.library.ethz.ch/rib/v3/enrichments/fulltext-offcampus';
        if(onCampus){
          url = 'https://daas.library.ethz.ch/rib/v3/enrichments/fulltext-oncampus';
        }
        url = url + '?doi=' + doi;
        return $http.get(url,{headers:{ "X-From-ExL-API-Gateway": undefined }})
            .then(
                function(response){
                    return response.data;
                },
                function(httpError){
                    if (httpError.status === 404)return null;
                    let error = "***ETH*** an error occured: browzineService.getLibkeyViaProxy error callback: " + httpError.status;
                    if (httpError.data && httpError.data.errorMessage) {
                        error += ' - ' + httpError.data.errorMessage;
                    }
                    console.error(error);
                    return null;
                }
            )
    }  

    return {
        getArticle: getArticle
    };
}];
