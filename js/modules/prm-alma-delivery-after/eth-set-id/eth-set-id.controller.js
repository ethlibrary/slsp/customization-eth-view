export class ethSetIdController {

    constructor($timeout, $anchorScroll) {
        this.$timeout = $timeout;
        this.$anchorScroll = $anchorScroll;
    }

    $onInit() {
        try{
            this.parentCtrl = this.afterCtrl.parentCtrl;
            if(!window.$anchorScroll){
                window.$anchorScroll = this.$anchorScroll;
            }
            this.$timeout(() => {
                let headingViewit = document.querySelector('[translate="nui.getit.service_viewit"].section-title');
                let headingGetit = document.querySelector('[translate="nui.getit.service_getit"].section-title');
                let headingHtgi = document.querySelector('[translate="nui.getit.service_howtogetit"].section-title');
                if(headingViewit && !headingGetit){
                    angular.element(headingViewit).attr('id','eth-getit-heading')
                }
                if(headingHtgi && !headingGetit){
                    angular.element(headingHtgi).attr('id','eth-getit-heading')
                }
                if(headingGetit){
                    angular.element(headingGetit).attr('id','eth-getit-heading')
                }
            }, 100);
        }
        catch(e){
            console.error("***ETH*** an error occured: ethSetIdController $onInit\n\n");
            console.error(e.message);
        }
    }
}
ethSetIdController.$inject = ['$timeout', '$anchorScroll'];
