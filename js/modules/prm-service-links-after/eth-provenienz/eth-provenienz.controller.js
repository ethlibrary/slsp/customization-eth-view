export class ethProvenienzController {

    constructor( ethProvenienzService, ethConfigService, ethProvenienzConfig, ethSessionService ) {
        this.ethProvenienzService = ethProvenienzService;
        this.ethConfigService = ethConfigService;
        this.config = ethProvenienzConfig;
        this.ethSessionService = ethSessionService
    }

    $onInit() {
        try{
            this.parentCtrl = this.afterCtrl.parentCtrl;
            if(!this.parentCtrl.item.delivery || !this.parentCtrl.item.delivery.availabilityLinksUrl || this.parentCtrl.item.delivery.availabilityLinksUrl.length === 0){
                return;
            }
            let doi = this.parentCtrl.item.delivery.availabilityLinksUrl[0].substring(this.parentCtrl.item.delivery.availabilityLinksUrl[0].indexOf('doi.org/') + 8);
            // only (online) items from e-rara
            if(doi.indexOf('10.3931/e-rara-') === -1){
                return;
            }
            // only ETH , 99117337824305503
            if(!this.parentCtrl.item.delivery.recordOwner || this.parentCtrl.item.delivery.recordOwner !== '41SLSP_ETH'){
                return;
            }
            this.ethProvenienzService.getItems(doi)
                .then((data) => {
                    try{
                        if(!data){
                            return;
                        }
                        this.items = data.items;
                    }
                    catch(e){
                        console.error("***ETH*** an error occured: ethProvenienzController getItems(): \n\n");
                        console.error(e.message);
                    }
                });
            }
            catch(e){
                console.error("***ETH*** an error occured: ethProvenienzController\n\n");
                console.error(e.message);
            }
    };
}

ethProvenienzController.$inject = ['ethProvenienzService', 'ethConfigService', 'ethProvenienzConfig', 'ethSessionService'];
