export class ethProvenienzEraraLinkController {

    constructor( ethConfigService, ethProvenienzEraraLinkConfig, ethSessionService ) {
        this.ethConfigService = ethConfigService;
        this.config = ethProvenienzEraraLinkConfig;
        this.ethSessionService = ethSessionService;
    }

    $onInit() {
        try{
            this.parentCtrl = this.afterCtrl.parentCtrl;
            this.eraraLink = '';
            this.swisscoveryQuery = '';

            if (!this.parentCtrl.item || !this.parentCtrl.item.pnx) {
                console.error("***ETH*** ethProvenienzEraraLinkController.$onInit: item or item.pnx not available");
                return;
            }
            if(!this.parentCtrl.item.pnx.display.source || this.parentCtrl.item.pnx.display.source.length == 0 || this.parentCtrl.item.pnx.display.source[0] != 'eth_epics_provenienz'){
                return;
            }
            if(!this.parentCtrl.item.pnx.display.lds09){
                return;
            }
            let aEraraLink = this.parentCtrl.item.pnx.display.lds09.filter( l => {
                if(l.indexOf('dx.doi.org/10.3931/e-rara-') > -1){
                    return true;
                }
                return false;
            })
            if(aEraraLink.length > 0){
                this.eraraLink = aEraraLink[0];
                this.swisscoveryQuery = this.eraraLink.substring(this.eraraLink.indexOf('dx.doi.org/')+11);
            }
        }
        catch(e){
            console.error("***ETH*** an error occured: ethProvenienzEraraLinkController $onInit\n\n");
            console.error(e.message);
        }
    }
}

ethProvenienzEraraLinkController.$inject = ['ethConfigService', 'ethProvenienzEraraLinkConfig', 'ethSessionService' ];
