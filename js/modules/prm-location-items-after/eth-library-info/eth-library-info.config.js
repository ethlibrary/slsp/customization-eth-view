/*
ETH-BAU (Zürich) E03
ETH-BIB (Zürich) E01
ETH-ERD (Zürich) E04
ETH-GESS (Zürich) E06
ETH-GRUEN (Zürich) E05
ETH-GS (Zürich)	E76
ETH-GTA (Zürich) E79
ETH-HCI (Zürich) E33
ETH-HDB (Zürich) E16
ETH-INFK (Zürich) E72
ETH-MATH (Zürich) E64
ETH-PHY (Zürich) E09
Literaturarchiv ETH-TMA (Zürich) E73BI
Literaturarchiv ETH-MFA (Zürich) E73MF
*/
export const ethLibraryInfoConfig = function(){
    return {
        E01:{
            url:{
                cms: {
                    de: 'https://library.ethz.ch/standorte-und-medien/standorte-und-oeffnungszeiten/lesesaal.html',
                    en: 'https://library.ethz.ch/en/locations-and-media/locations-and-opening-hours/reading-room.html'
                },
                maps: {
                    de: 'https://goo.gl/maps/EZdHzgUSumjxEPGP8',
                    en: 'https://goo.gl/maps/EZdHzgUSumjxEPGP8'
                }
            }
        },
        E03:{
            url:{
                cms: {
                    de: 'https://library.ethz.ch/standorte-und-medien/standorte-und-oeffnungszeiten/baubibliothek.html',
                    en: 'https://library.ethz.ch/en/standorte-und-medien/standorte-und-oeffnungszeiten/baubibliothek.html'
                },
                maps: {
                    de: 'https://goo.gl/maps/KG98iEroykNAnnqDA',
                    en: 'https://goo.gl/maps/KG98iEroykNAnnqDA'
                }
            }
        },
        E04:{
            url:{
                cms: {
                    de: 'https://library.ethz.ch/standorte-und-medien/standorte-und-oeffnungszeiten/bibliothek-erdwissenschaften.html',
                    en: 'https://library.ethz.ch/en/standorte-und-medien/standorte-und-oeffnungszeiten/bibliothek-erdwissenschaften.html'
                },
                maps: {
                    de: 'https://goo.gl/maps/qgsJkRK3XL96wRk79',
                    en: 'https://goo.gl/maps/qgsJkRK3XL96wRk79'
                }
            }
        },
        E05:{
            url:{
                cms: {
                    de: 'https://library.ethz.ch/standorte-und-medien/standorte-und-oeffnungszeiten/gruene-bibliothek.html',
                    en: 'https://library.ethz.ch/en/standorte-und-medien/standorte-und-oeffnungszeiten/gruene-bibliothek.html'
                },
                maps: {
                    de: 'https://goo.gl/maps/PDduAysfWEaPWzZQ8',
                    en: 'https://goo.gl/maps/PDduAysfWEaPWzZQ8'
                }
            }
        },
        E06:{
            url:{
                cms: {
                    de: 'https://library.ethz.ch/standorte-und-medien/standorte-und-oeffnungszeiten/gess-bibliothek.html',
                    en: 'https://library.ethz.ch/en/standorte-und-medien/standorte-und-oeffnungszeiten/gess-bibliothek.html'
                },
                maps: {
                    de: 'https://goo.gl/maps/SHMDgEj8fkP6VvgN7',
                    en: 'https://goo.gl/maps/SHMDgEj8fkP6VvgN7'
                }
            }
        },
        E98AZ:{
            url:{
                cms: {
                    de: 'https://afz.ethz.ch/recherche-besuch/recherche/bibliothek.html',
                    en: 'https://afz.ethz.ch/en/search-visit/search/library.html'
                }
            },
            label:{
                libraryName: {
                    de: 'Archiv für Zeitgeschichte',
                    en: 'Archives of Contemporary History'
                }
            }
        },
        E06LI:{
            url:{
                cms: {
                    de: 'https://lit.ethz.ch/die-gruppe/wie-sie-uns-finden.html',
                    en: 'https://lit.ethz.ch/en/the-group/howtofindus.html'
                },
                mail:{
                    de: 'sekretariat@lit.gess.ethz.ch',
                    en: 'sekretariat@lit.gess.ethz.ch'
                }
            },
            label:{
                hint1: {
                    de: 'Die Bibliothek  Professur für Literatur- und Kulturwissenschaft befindet sich',
                    en: 'The library Chair for Literature and Cultural Studies is'
                },
                hint2: {
                    de: 'nicht',
                    en: 'not'
                },
                hint3: {
                    de: 'in der GESS-Bibliothek.',
                    en: 'located at GESS Library.'
                },
                hint4: {
                    de: 'Benutzung nur vor Ort',
                    en: 'On-site use'
                },
                hint5: {
                    de: 'nach bestätigter Voranmeldung bei ',
                    en: 'only after confirmed appointment with '
                },
                maillinktext: {
                    de: 'sekretariat@lit.gess.ethz.ch',
                    en: 'sekretariat@lit.gess.ethz.ch'
                },
                hint6: {
                    de: 'möglich.',
                    en: 'possible.'
                },
                libraryName: {
                    de: 'Professur für Literatur- und Kulturwissenschaft',
                    en: 'Chair for Literature and Cultural Studies'
                }
            }
        },
        E06EQ:{
            url:{
                cms: {
                    de: 'https://ethz.ch/staffnet/de/anstellung-und-arbeit/arbeitsumfeld/diversity.html',
                    en: 'https://ethz.ch/staffnet/en/employment-and-work/working-environment/diversity.html'
                },
                mail:{
                    de: 'diversity@ethz.ch',
                    en: 'diversity@ethz.ch'
                }
            },
            label:{
                hint1: {
                    de: 'Die Bibliothek von ETH Diversity befindet sich',
                    en: 'The library of ETH Diversity is'
                },
                hint2: {
                    de: 'nicht',
                    en: 'not'
                },
                hint3: {
                    de: 'in der GESS-Bibliothek.',
                    en: 'located at GESS Library.'
                },
                hint4: {
                    de: 'Benutzung nur vor Ort',
                    en: 'On-site use'
                },
                hint5: {
                    de: 'nach bestätigter Voranmeldung bei ',
                    en: 'only after confirmed appointment with '
                },
                maillinktext: {
                    de: 'diversity@ethz.ch',
                    en: 'diversity@ethz.ch'
                },
                hint6: {
                    de: 'möglich.',
                    en: 'possible.'
                }
            }
        },
        AETH:{
            url:{
                cms: {
                    de: 'https://library.ethz.ch/archivieren-und-digitalisieren/archivieren/hochschularchiv-der-eth-zuerich.html',
                    en: 'https://library.ethz.ch/en/archiving-and-digitising/archiving/eth-zurich-university-archives.html'
                },
                mail:{
                    de: 'archiv@library.ethz.ch',
                    en: 'archiv@library.ethz.ch'
                }
            },
            label:{
                hint1: {
                    de: 'Benutzung nur vor Ort',
                    en: 'On-site use'
                },
                hint2: {
                    de: ' nach Voranmeldung bei ',
                    en: ' only after appointment with '
                },
                maillinktext: {
                    de: ' archiv@library.ethz.ch',
                    en: ' archiv@library.ethz.ch'
                },
                hint3: {
                    de: '.',
                    en: '.'
                },
                libraryName: {
                    de: 'Hochschularchiv der ETH Zürich',
                    en: 'ETH Zurich University Archives'
                }
            }
        },
        E09:{
            url:{
                cms: {
                    de: 'https://www.phys.ethz.ch/de/dienste-und-berufsbildung/physikbibliothek.html',
                    en: 'https://www.phys.ethz.ch/services/physics-library.html'
                },
                maps: {
                    de: 'https://goo.gl/maps/1ibJR8CqTt4uTjSf8',
                    en: 'https://goo.gl/maps/1ibJR8CqTt4uTjSf8'
                }
            }
        },
        E16:{
            url:{
                cms: {
                    de: 'https://library.ethz.ch/standorte-und-medien/standorte-und-oeffnungszeiten/eth-bibliothek-hdb.html',
                    en: 'https://library.ethz.ch/en/standorte-und-medien/standorte-und-oeffnungszeiten/eth-bibliothek-hdb.html'
                },
                maps: {
                    de: 'https://goo.gl/maps/dr4BzcAXMeDvYAWW7',
                    en: 'https://goo.gl/maps/dr4BzcAXMeDvYAWW7'
                }
            }
        },
        E64:{
            url:{
                cms: {
                    de: 'https://math.ethz.ch/library.html',
                    en: 'https://math.ethz.ch/library.html'
                },
                maps: {
                    de: '',
                    en: ''
                }
            }
        },
        E72:{
            url:{
                cms: {
                    de: 'https://www.library.inf.ethz.ch/',
                    en: 'https://www.library.inf.ethz.ch/en/'
                },
                maps: {
                    de: '',
                    en: ''
                }
            }
        },
        E73BI:{
            url:{
                cms: {
                    de: 'https://library.ethz.ch/standorte-und-medien/standorte-und-oeffnungszeiten/thomas-mann-archiv.html',
                    en: 'https://library.ethz.ch/en/standorte-und-medien/standorte-und-oeffnungszeiten/thomas-mann-archiv.html'
                },
                mail:{
                    de: 'tma@library.ethz.ch',
                    en: 'tma@library.ethz.ch'
                },
                maps: {
                    de: '',
                    en: ''
                }
            },
            label:{
                title:{
                    de: 'ETH Thomas-Mann-Archiv',
                    en: 'ETH Thomas-Mann-Archiv'
                },
                hint: {
                    de: 'Benutzung nur vor Ort. Auskunft erhalten Sie per E-Mail an: ',
                    en: 'Use only on site. Information can be obtained by e-mail to:'
                }
            }
        },
        E73MF:{
            url:{
                cms: {
                    de: 'https://library.ethz.ch/standorte-und-medien/standorte-und-oeffnungszeiten/max-frisch-archiv.html',
                    en: 'https://library.ethz.ch/en/standorte-und-medien/standorte-und-oeffnungszeiten/max-frisch-archiv.html'
                },
                mail:{
                    de: 'mfa@library.ethz.ch',
                    en: 'mfa@library.ethz.ch'
                },
                maps: {
                    de: '',
                    en: ''
                }
            },
            label:{
                title:{
                    de: 'ETH Max Frisch-Archiv',
                    en: 'ETH Max Frisch-Archiv'
                },
                hint: {
                    de: 'Benutzung nur vor Ort. Auskunft erhalten Sie per E-Mail an:',
                    en: 'Use only on site. Information can be obtained by e-mail to:'
                }
            }
        },
        E76:{
            url:{
                cms: {
                    de: 'https://library.ethz.ch/standorte-und-medien/standorte-und-oeffnungszeiten/graphische-sammlung.html',
                    en: 'https://library.ethz.ch/en/standorte-und-medien/standorte-und-oeffnungszeiten/graphische-sammlung.html'
                },
                mail:{
                    de: 'bibliothek@gs.ethz.ch',
                    en: 'bibliothek@gs.ethz.ch'
                },
                maps: {
                    de: 'https://goo.gl/maps/W86E5eU8tMyzh61D6',
                    en: 'https://goo.gl/maps/W86E5eU8tMyzh61D6'
                }
            },
            label:{
                hint: {
                    de: 'Benutzung vor Ort nur nach bestätigter Voranmeldung bei',
                    en: 'On-site use only after confirmed appointment with'
                }
            }
        },
        E79:{
            url:{
                cms: {
                    de: 'https://archiv.gta.arch.ethz.ch/benutzungbibliothek',
                    en: 'https://archiv.gta.arch.ethz.ch/serviceslibrary'
                },
                mail:{
                    de: 'bibliothek@gta.arch.ethz.ch',
                    en: 'bibliothek@gta.arch.ethz.ch'
                }
            },
            label:{
                hint1: {
                    de: 'Benutzung vor Ort',
                    en: 'On-site use'
                },
                hint2: {
                    de: 'nach bestätigter Voranmeldung bei',
                    en: 'only after confirmed appointment with'
                },
                maillinktext:{
                    de: 'bibliothek@gta.arch.ethz.ch',
                    en: 'bibliothek@gta.arch.ethz.ch'
                }
            }
        },
        E33:{
            url:{
                mail: {
                    de: 'infodesk@chem.ethz.ch',
                    en: 'infodesk@chem.ethz.ch'
                },
                cms: {
                    de: 'https://infozentrum.ethz.ch/',
                    en: 'https://infozentrum.ethz.ch/en/'
                }
            },
            label:{
                hint: {
                    de: 'Möchten Sie einen Scan eines Artikels oder Kapitels aus diesem Dokument bestellen?',
                    en: 'Would you like to order a scan of an article or a chapter from this document?'
                },
                maillinktext: {
                    de: 'Schreiben Sie uns!',
                    en: 'Write to us!'
                },
                subject: {
                    de: 'Scan-Anfrage:',
                    en: 'Scan request:'
                },
                body1: {
                    de: 'Bitte geben Sie an, welchen Teil des Titels Sie benötigen (sofern zutreffend):',
                    en: 'Please indicate which part of the title you require (if applicable):'
                },
                body2: {
                    de: 'Autor des Kapitels/Artikels:',
                    en: 'Author of chapter/contribution:'
                },
                body3: {
                    de: 'Titel des Kapitels/Artikels:',
                    en: 'Title of chapter/contribution:'
                },
                body4: {
                    de: 'Seiten (von/bis):',
                    en: 'Pages (from/to):'
                },
                body5: {
                    de: 'Bei Zeitschriften: Jahrgang/Band und Ausgabe:',
                    en: 'For journals: year/volume and issue:'
                },
                body6: {
                    de: 'Es können maximal 50 Seiten gescannt werden.',
                    en: 'A maximum of 50 pages can be scanned.'
                }
            }
        },
        slsp:{
            url:{
                cms: {
                    de: 'https://registration.slsp.ch/libraries?search=',
                    en: 'https://registration.slsp.ch/libraries?lang=en&search='
                }
            }
        },
        common:{
            label:{
                subject: {
                    de: 'Voranmeldung für Benutzung vor Ort von MMS ID',
                    en: 'Appointment for on-site use of MMS ID'
                }
            }
        }
    }
}
