/**
* @ngdoc module
* @name ethVideoLinkModule
*
* @description
*
* - changes link text of video link in service links section, if link leads to startpage of video portal
*
* <b>AngularJS Dependencies</b><br>
* Service /js/services {@link ETH.ethConfigService}<br>
* Service {@link ETH.ethVideoLinkConfig}<br>
*
* <b>CSS/Image Dependencies</b><br>
*
*
*
*/
import {ethConfigService} from '../../../services/eth-config.service';
import {ethVideoLinkConfig} from './eth-video-link.config';
import {ethVideoLinkController} from './eth-video-link.controller';

export const ethVideoLinkModule = angular
    .module('ethVideoLinkModule', [])
        .factory('ethConfigService', ethConfigService)
        .factory('ethVideoLinkConfig', ethVideoLinkConfig)
        .controller('ethVideoLinkController', ethVideoLinkController)
        .component('ethVideoLinkComponent', {
            bindings: {afterCtrl: '<'},
            controller: 'ethVideoLinkController'
        })
