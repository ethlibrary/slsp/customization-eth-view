export class ethAvailabilityHintImprovementController {

    constructor( ethConfigService, ethAvailabilityHintImprovementConfig ) {
        this.ethConfigService = ethConfigService;
        this.config = ethAvailabilityHintImprovementConfig;
    }

    $onInit() {
        try{
            this.parentCtrl = this.afterCtrl.parentCtrl;
            this.availabilityHint = '';
            this.guidCMIStar = '';
            this.changeButton = false;
            this.changeButtonText = '';
            this.source = '';
            if (!this.parentCtrl.result || !this.parentCtrl.result.pnx || !this.parentCtrl.result.delivery) {
                console.error("***ETH*** ethAvailabilityHintImprovementController.$onInit: result or result.pnx or result.delivery not available");
                return;
            }
            if(this.parentCtrl.result.pnx.display.source && this.parentCtrl.result.pnx.display.source.length > 0){
                this.source = this.parentCtrl.result.pnx.display.source[0];
            }

            // E-Lending
            if(this.source === 'ETH_ELending'){
                this.availabilityHint = this.ethConfigService.getLabel(this.config.elending, 'access');
            }


            // HSA: if there is an online link, but not a digitalisation
            if(this.source === 'ETH_Hochschularchiv'){
                if(this.parentCtrl.result.delivery.GetIt1 && this.parentCtrl.result.delivery.GetIt1.length > 0 && this.parentCtrl.result.delivery.GetIt1[0].links && this.parentCtrl.result.delivery.GetIt1[0].links.length > 0){
                    let aLink = this.parentCtrl.result.delivery.GetIt1[0].links.filter( l => {
                        if(l.isLinktoOnline && l.link !== ''){
                            return true;
                        }
                        return false;
                    });
                    // no digitalisation exists, 99117433328705503
                    if ( aLink.length === 0 ) {
                        this.changeButton = true;
                        this.changeButtonText = this.ethConfigService.getLabel(this.config.hsa, 'linktextNoOnline')
                        let availabilityLine = this.parentCtrl.$element[0];
                        if(availabilityLine) {
                            availabilityLine.classList.add('eth-hsa-no-online');
                        }
                    }
                }
            }
            // improve availability text, if availability = "no_inventory"
            let delivery = this.parentCtrl.result.delivery;
            if(delivery.availability[0] === 'no_inventory' && delivery.deliveryCategory[0] === 'Alma-P' && delivery.almaInstitutionsList){
                //print resource : 991019003809705501
                let aPrint = delivery.almaInstitutionsList.filter(e => {
                    return e.availabilityStatus === 'available_in_institution';
                })
                if(aPrint.length > 0){
                    this.changeButton = true;
                    this.changeButtonText = this.ethConfigService.getLabel(this.config.noInventory, 'print1') + "<br>" + this.ethConfigService.getLabel(this.config.noInventory, 'print2');
                    return;
                }
                // online resource: 991170585852805501
                let aOnline = delivery.almaInstitutionsList.filter(e => {
                    return e.availabilityStatus === 'available_online';
                })
                if(aOnline.length > 0){
                    this.changeButton = true;
                    this.changeButtonText = this.ethConfigService.getLabel(this.config.noInventory, 'online1') + "<br>" + this.ethConfigService.getLabel(this.config.noInventory, 'online2');
                    return;
                }
            }
        }
        catch(e){
            console.error("***ETH*** an error occured: ethAvailabilityHintImprovementController $onInit\n\n");
            console.error(e.message);
        }
    }

    $doCheck() {
        try{
            if (this.changeButton){
                let availabilityLine = this.parentCtrl.$element[0];
                let button = availabilityLine.querySelector('.availability-status');
                if(angular.element(button) && angular.element(button).length > 0){
                    angular.element(button).html(this.changeButtonText);
                    this.changeButton = false;
                    this.changeButtonText = '';
                }
            }
        }
        catch(e){
            console.error("***ETH*** an error occured: ethAvailabilityHintImprovementController $doCheck\n\n");
            console.error(e.message);
        }
    }
}

ethAvailabilityHintImprovementController.$inject = ['ethConfigService', 'ethAvailabilityHintImprovementConfig' ];
