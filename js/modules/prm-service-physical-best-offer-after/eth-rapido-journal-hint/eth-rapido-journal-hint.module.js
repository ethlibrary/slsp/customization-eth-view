/**
* @ngdoc module
* @name ethRapidoJournalHintModule
*
* @description
*
*
* If it is a journal and there are ETH holdings:
* -> add a hint to contact service to get the journal to another location
*
* <b>AngularJS Dependencies</b><br>
* Service /js/services {@link ETH.ethConfigService}<br>
* Service {@link ETH.ethRapidoJournalHintConfig}<br>
*
* <b>CSS/Image Dependencies</b><br>
* CSS eth-rapido-journal-hint.css
*
*/

import {ethConfigService} from '../../../services/eth-config.service';
import {ethRapidoJournalHintConfig} from './eth-rapido-journal-hint.config';
import {ethRapidoJournalHintController} from './eth-rapido-journal-hint.controller';

export const ethRapidoJournalHintModule = angular
    .module('ethRapidoJournalHintModule', [])
        .factory('ethConfigService', ethConfigService)
        .factory('ethRapidoJournalHintConfig', ethRapidoJournalHintConfig)
        .controller('ethRapidoJournalHintController', ethRapidoJournalHintController)
        .component('ethRapidoJournalHintComponent', {
            bindings: {afterCtrl: '<'},
            controller: 'ethRapidoJournalHintController'
        })
