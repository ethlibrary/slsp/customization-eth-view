/**
* @ngdoc service
* @name ethPlaceService
*
* @description
*
* getGeoTopicsFromGeoGraph(): geo topics (Marc 651) from graph
* getMapsFromGeoGraph(): e-rara maps and e-maps from graph
* getPoiFromGeoGraph(): pois including dossiers and routes from graph
* getPlaceFromWikidata(): getPlace informations from wikidata
* getPartOfFromWikidata(): get siblings from partOf entity
* getPlaceFromETHorama(): getPlace informations from ETHorama Database
* getPlaceFromGeolinker(): getPlace informations from Metagrid/Histhub
* getQidForGND(): get QID of Place for GND ID 
* getNeartoPois(): get other Pois nearto
*
*
* <b>Used by</b><br>
* Module {@link ETH.ethPlaceModule}<br>
*
*
 */
export const ethPlaceService = ['$http', '$sce', function($http, $sce){

    function getGeoTopicsFromGeoGraph(qid){
        // https://daas.library.ethz.ch/rib/v3/graph/places?q=Q15283
        // https://api.library.ethz.ch/geo/v1/geo-topics/?apikey=Hnwc3kaBnR51pXTenynY7BnG10cgtsDf4YWIA5AbA0Lm9Uq9&q=Q15283
        let baseurl = 'https://api.library.ethz.ch/geo/v1/geo-topics/';
        let url = baseurl + '?edges=true&apikey=Hnwc3kaBnR51pXTenynY7BnG10cgtsDf4YWIA5AbA0Lm9Uq9&q=' + qid;
        $sce.trustAsResourceUrl(url);
        return $http.get(url)
            .then(
                function(response){
                    return response.data;
                },
                function(httpError){
                    if (httpError.status === 404)return null;
                    let error = "***ETH*** an error occured: ethPlaceService.getMapsFromGeoGraph(): " + httpError.status;
                    if (httpError.data && httpError.data.errorMessage) {
                        error += ' - ' + httpError.data.errorMessage;
                    }
                    console.error(error);
                    return null;
                }
            )
    }

    function getMapsFromGeoGraph(lat, lng){
        // https://daas.library.ethz.ch/rib/v3/graph/maps?lat=47.349952&lon=8.490838
        let baseurl = 'https://api.library.ethz.ch/geo/v1/maps';
        let url = baseurl + '?edges=true&apikey=Hnwc3kaBnR51pXTenynY7BnG10cgtsDf4YWIA5AbA0Lm9Uq9';
        url = url + '&lat=' + lat + '&lon=' + lng;
        $sce.trustAsResourceUrl(url);

        return $http.get(url)
            .then(
                function(response){
                    return response.data;
                },
                function(httpError){
                    if (httpError.status === 404)return null;
                    let error = "***ETH*** an error occured: ethPlaceService.getMapsFromGeoGraph(): " + httpError.status;
                    if (httpError.data && httpError.data.errorMessage) {
                        error += ' - ' + httpError.data.errorMessage;
                    }
                    console.error(error);
                    return null;
                }
            )
    }

    function getPoiFromGeoGraph(id){
        let baseurl = 'https://api.library.ethz.ch/geo/v1/pois/';
        let url = baseurl + id;
        url = url + '?apikey=Hnwc3kaBnR51pXTenynY7BnG10cgtsDf4YWIA5AbA0Lm9Uq9&edges=true';

        $sce.trustAsResourceUrl(url);

        return $http.get(url)
            .then(
                function(response){
                    return response.data;
                },
                function(httpError){
                    if (httpError.status === 404)return null;
                    let error = "***ETH*** an error occured: ethPlaceService.getMapsFromGeoGraph(): " + httpError.status;
                    if (httpError.data && httpError.data.errorMessage) {
                        error += ' - ' + httpError.data.errorMessage;
                    }
                    console.error(error);
                    return null;
                }
            )
    }


    function getPlaceFromWikidata(qid, lang){
        let wLang = 'en,de';
        if(lang==='de'){
            wLang = 'de,en';
        }
        let baseurl = 'https://daas.library.ethz.ch/rib/v3/places/';
        let url = baseurl + qid + '?lang=' + wLang;
        $sce.trustAsResourceUrl(url);

        return $http.get(url)
            .then(
                function(response){
                    return response.data;
                },
                function(httpError){
                    if (httpError.status === 404)return null;
                    let error = "***ETH*** an error occured: ethPlaceService.getPlaceFromWikidata(): " + httpError.status;
                    if (httpError.data && httpError.data.errorMessage) {
                        error += ' - ' + httpError.data.errorMessage;
                    }
                    console.error(error);
                    return null;
                }
            );
    }

    function getPartOfFromWikidata(qids, lang){
        let wLang = 'en,de';
        if(lang==='de'){
            wLang = 'de,en';
        }
        let baseurl = 'https://daas.library.ethz.ch/rib/v3/places/partof/';
        let url = baseurl + '?lang=' + wLang + '&qids=' + qids;
        $sce.trustAsResourceUrl(url);
        return $http.get(url)
            .then(
                function(response){
                    return response.data;
                },
                function(httpError){
                    if (httpError.status === 404)return null;
                    let error = "***ETH*** an error occured: ethPlaceService.getPlaceFromWikidata(): " + httpError.status;
                    if (httpError.data && httpError.data.errorMessage) {
                        error += ' - ' + httpError.data.errorMessage;
                    }
                    console.error(error);
                    return null;
                }
            );
    }

    function getPlaceFromETHorama(qid){
        let baseurl = 'https://api.library.ethz.ch/ethorama/v1/pois';
        let url = baseurl + '?apikey=XKosnD8xM5AuyvuovfebqpHUzkrMi0qqlVKcM5gHYDANCyds&details=true&qId=' + qid;
        $sce.trustAsResourceUrl(url);

        return $http.get(url)
            .then(
                function(response){
                    if(response && response.data && response.data.items && response.data.items.length > 0){
                        return response.data.items;
                    }
                    else{
                        return null;
                    }
                },
                function(httpError){
                    if (httpError.status === 404 || httpError.status === 500 || httpError.statusText === 'Bad Request')return null;
                    let error = "***ETH*** an error occured: ethPlaceService.getPlaceFromETHorama():" + httpError.status;
                    if (httpError.data && httpError.data.errorMessage) {
                        error += ' - ' + httpError.data.errorMessage;
                    }
                    console.error(error);
                    return null;
                }
            )
    }

    function getPlaceFromGeolinker(qid){
        let baseurl = 'https://daas.library.ethz.ch/rib/v3/enrichments/geolinker/qid/';
        let url = baseurl + qid;
        $sce.trustAsResourceUrl(url);

        return $http.get(url)
            .then(
                function(response){
                    return response.data;
                },
                function(httpError){
                    if (httpError.status === 404)return null;
                    let error = "***ETH*** an error occured: ethPlaceService.getPlaceFromGeolinker: " + httpError.status;
                    if (httpError.data && httpError.data.errorMessage) {
                        error += ' - ' + httpError.data.errorMessage;
                    }
                    return null;
                }
            );
    }

    function getNameForGND(gnd){
        let baseurl = 'https://daas.library.ethz.ch/rib/v3/places/entityfacts/';
        let url = baseurl + gnd;
        $sce.trustAsResourceUrl(url);
        return $http.get(url)
            .then(
                function(response){
                    return response.data;
                },
                function(httpError){
                    if (httpError.status === 404)return null;
                    let error = "***ETH*** an error occured: ethPlaceService.getQidForGND: " + httpError.status;
                    if (httpError.data && httpError.data.errorMessage) {
                        error += ' - ' + httpError.data.errorMessage;
                    }
                    return null;
                }
            )
    }

    function getQidForGND(gnd){
        let baseurl = 'https://daas.library.ethz.ch/rib/v3/places/gnd-short/';
        let url = baseurl + gnd;
        $sce.trustAsResourceUrl(url);
        return $http.get(url)
            .then(
                function(response){
                    let qid = null;
                    if(response.data.results.bindings && response.data.results.bindings.length > 0){
                        let item = response.data.results.bindings[0].item.value;
                        qid = item.substring(item.lastIndexOf('/')+1);
                    }
                    return qid;
                },
                function(httpError){
                    if (httpError.status === 404)return null;
                    let error = "***ETH*** an error occured: ethPlaceService.getQidForGND: " + httpError.status;
                    if (httpError.data && httpError.data.errorMessage) {
                        error += ' - ' + httpError.data.errorMessage;
                    }
                    return null;
                }
            )
    }

    function getPersonsForPlaceGND(gnd){
        let url = 'https://daas.library.ethz.ch/rib/v3/places/gnd/' + gnd + '/persons/';
        $sce.trustAsResourceUrl(url);
        return $http.get(url)
            .then(
                function(response){
                    return response.data; 
                },
                function(httpError){
                    let error = "***ETH*** an error occured: ethPlaceService.getPersonsForPlaceGND: " + httpError.status;
                    if (httpError.data && httpError.data.errorMessage) {
                        error += ' - ' + httpError.data.errorMessage;
                    }
                    return null;
                }
            )
    }

    function getNeartoPois(lat,lng){
        let baseurl = 'https://daas.library.ethz.ch/rib/v3/geojson';
        let url = baseurl + '?nearto=' + lat + ',' + lng + ',' + '1000';
        $sce.trustAsResourceUrl(url);
        return $http.get(url)
            .then(
                function(response){
                    return response.data;
                },
                function(httpError){
                    if (httpError.status === 404)return null;
                    let error = "***ETH*** an error occured: ethPlaceService.getQidForGND: " + httpError.status;
                    if (httpError.data && httpError.data.errorMessage) {
                        error += ' - ' + httpError.data.errorMessage;
                    }
                    return null;
                }
            )
    }

    return {
        getPlaceFromWikidata: getPlaceFromWikidata,
        getPartOfFromWikidata: getPartOfFromWikidata,
        getPlaceFromETHorama: getPlaceFromETHorama,
        getPlaceFromGeolinker: getPlaceFromGeolinker,
        getMapsFromGeoGraph: getMapsFromGeoGraph,
        getPoiFromGeoGraph: getPoiFromGeoGraph,
        getGeoTopicsFromGeoGraph: getGeoTopicsFromGeoGraph,
        getQidForGND: getQidForGND,
        getNameForGND: getNameForGND,
        getPersonsForPlaceGND: getPersonsForPlaceGND,
        getNeartoPois: getNeartoPois
    }

}]
