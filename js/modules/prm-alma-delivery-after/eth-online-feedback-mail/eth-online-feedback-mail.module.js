/**
* @ngdoc module
* @name ethOnlineFeedbackMailModule
*
* @description
*
* - mail link for feedback, if there are (online resources) access problems
* - NOT if source is Open Access
*
* <b>AngularJS Dependencies</b><br>
* Service {@link ETH.ethSessionService}<br>
* Service /js/services {@link ETH.ethConfigService}<br>
* Service {@link ETH.ethOnlineFeedbackMailConfig}<br>
*
*
* <b>CSS/Image Dependencies</b><br>
* CSS eth-online-feedback-mail.css
*
*/
import {ethConfigService} from '../../../services/eth-config.service';
import {ethSessionService} from '../../../services/eth-session.service';
import {ethOnlineFeedbackMailConfig} from './eth-online-feedback-mail.config';
import {ethOnlineFeedbackMailController} from './eth-online-feedback-mail.controller';
import {ethOnlineFeedbackMailHtml} from './eth-online-feedback-mail.html';

export const ethOnlineFeedbackMailModule = angular
    .module('ethOnlineFeedbackMailModule', [])
        .factory('ethConfigService', ethConfigService)
        .factory('ethSessionService', ethSessionService)
        .factory('ethOnlineFeedbackMailConfig', ethOnlineFeedbackMailConfig)
        .controller('ethOnlineFeedbackMailController', ethOnlineFeedbackMailController)
        .component('ethOnlineFeedbackMailComponent',{
            bindings: {afterCtrl: '<'},
            controller: 'ethOnlineFeedbackMailController',
            template: ethOnlineFeedbackMailHtml
        })
