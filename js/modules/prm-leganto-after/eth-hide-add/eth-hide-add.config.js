export const ethHideAddConfig = function(){
    return {
        label:{
            info: {
                de: 'Mehr Informationen zu Leganto Literaturlisten',
                en: 'More information about Leganto Reading Lists'
            }
        },
        url:{
            leganto: {
                de: 'https://library.ethz.ch/recherchieren-und-nutzen/literatur-und-wissen-verwalten/leganto.html',
                en: 'https://library.ethz.ch/en/searching-and-using/managing-references-and-knowledge/leganto.html'
            }
        }
    }
}
