/**
* @ngdoc module
* @name ethSetIdModule
*
* @description
*
* set attribute id="eth-getit-heading" for getit, viewit, htgi section heading
* priority is getit
*
* <b>AngularJS Dependencies</b><br>
*
* <b>CSS/Image Dependencies</b><br>
*
*/
import {ethSetIdController} from './eth-set-id.controller';

export const ethSetIdModule = angular
    .module('ethSetIdModule', [])
        .controller('ethSetIdController', ethSetIdController)
        .component('ethSetIdComponent', {
            bindings: {afterCtrl: '<'},
            controller: 'ethSetIdController'
        })
