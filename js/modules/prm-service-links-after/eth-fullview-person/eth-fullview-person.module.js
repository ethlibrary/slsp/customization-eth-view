/**
* @ngdoc module
* @name ethFullviewPersonModule
*
* @description
*
* - enhances Fullview
* - checks lmu prometheus, Metagrid, Entityfacts (DNB), Wikidata for person informations (by GND ID)
*
* <b>AngularJS Dependencies</b><br>
* Service /js/services {@link ETH.ethSessionService}<br>
* Service /js/services {@link ETH.ethConfigService}<br>
* Service {@link ETH.ethFullviewPersonService}<br>
* Service js/modules/shared/eth-person-cards {@link ETH.ethPersonCardConfig}<br>
* Service js/modules/shared/eth-person-cards {@link ETH.ethPersonCardService}<br>
*
*
* <b>CSS/Image Dependencies</b><br>
* CSS js/modules/shared/eth-person-cards/eth-person-cards.css
*
*
*/
import {ethConfigService} from '../../../services/eth-config.service';
import {ethSessionService} from '../../../services/eth-session.service';
import {ethPersonCardConfig} from '../../shared/eth-person-card/eth-person-card.config';
import {ethPersonCardService} from '../../shared/eth-person-card/eth-person-card.service';
import {ethPersonCardHtml} from '../../shared/eth-person-card/eth-person-card.html';
import {ethFullviewPersonService} from './eth-fullview-person.service';
import {ethFullviewPersonController} from './eth-fullview-person.controller';


export const ethFullviewPersonModule = angular
    .module('ethFullviewPersonModule', [])
        .factory('ethConfigService', ethConfigService)
        .factory('ethSessionService', ethSessionService)
        .factory('ethPersonCardConfig', ethPersonCardConfig)
        .factory('ethPersonCardService', ethPersonCardService)
        .factory('ethFullviewPersonService', ethFullviewPersonService)
        .controller('ethFullviewPersonController', ethFullviewPersonController)
        .component('ethFullviewPersonComponent',{
            bindings: {afterCtrl: '<'},
            controller: 'ethFullviewPersonController',
            template: ethPersonCardHtml
        })
