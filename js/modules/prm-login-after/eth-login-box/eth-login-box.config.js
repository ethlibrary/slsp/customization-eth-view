export const ethLoginBoxConfig = function(){
    return {
        label: {
            registration: {
                de: 'Noch nicht registriert?',
                en: 'Not yet registered?'
            }
        },
        url:{
            registrationUrl: {
                de: 'https://library.ethz.ch/recherchieren-und-nutzen/ausleihen-und-nutzen/swisscovery-hilfe-auf-einen-blick.html#r',
                en: 'https://library.ethz.ch/en/searching-and-using/borrowing-and-using/swisscovery-hilfe-auf-einen-blick.html#re'
            }
        }
    }
}

