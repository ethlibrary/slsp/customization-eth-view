/**
* @ngdoc service
* @name ethComposeEraraService
*
* @description
* Service to get imported erara (online) record from primo (via RIB)
*
* <b>Used by</b><br>
*
* Module {@link ETH.ethComposeEraraModule}<br>
*
 */
export const ethComposeEraraService = ['$http', '$sce', function($http, $sce){
    // https://daas.library.ethz.ch/rib/v3/search?q=any,contains,MMS_ID_PRINT_990042488650205503
    function getOnlineEraraRecord(mmsid){
        let url = "https://daas.library.ethz.ch/rib/v3/search?q=any,contains,MMS_ID_PRINT_" + mmsid;
        $sce.trustAsResourceUrl(url);

        return $http.get(url)
            .then(
                function(response){
                    return response.data;
                },
                function(httpError){
                    if (httpError.status === 404)return null;
                    let error = "***ETH*** an error occured: ethComposeEraraService.getOnlineEraraRecord: " + httpError.status;
                    if (httpError.data && httpError.data.errorMessage) {
                        error += ' - ' + httpError.data.errorMessage;
                    }
                    console.error(error);
                    return null;
                }
            );
    }

    // https://daas.library.ethz.ch/rib/v3/graph/emaps-erara?emap=99117999050805503
    function getEraraRecordForEMap(mmsid){
        let url = "https://daas.library.ethz.ch/rib/v3/graph/emaps-erara?emap=" + mmsid;
        $sce.trustAsResourceUrl(url);
        return $http.get(url)
            .then(
                function(response){
                    return response.data;
                },
                function(httpError){
                    if (httpError.status === 404)return null;
                    let error = "***ETH*** an error occured: ethComposeEraraService.getEraraRecordForEMap: " + httpError.status;
                    if (httpError.data && httpError.data.errorMessage) {
                        error += ' - ' + httpError.data.errorMessage;
                    }
                    console.error(error);
                    return null;
                }
            );
    }

    // https://daas.library.ethz.ch/rib/v3/graph/emaps-erara?erara=990038990900205503
    function getEMapsRecord(mmsid){
        let url = "https://daas.library.ethz.ch/rib/v3/graph/emaps-erara?erara=" + mmsid;
        $sce.trustAsResourceUrl(url);
        return $http.get(url)
            .then(
                function(response){
                    return response.data;
                },
                function(httpError){
                    if (httpError.status === 404)return null;
                    let error = "***ETH*** an error occured: ethComposeEraraService.getEMapsRecord: " + httpError.status;
                    if (httpError.data && httpError.data.errorMessage) {
                        error += ' - ' + httpError.data.errorMessage;
                    }
                    console.error(error);
                    return null;
                }
            );
    }

    return {
        getOnlineEraraRecord: getOnlineEraraRecord,
        getEMapsRecord: getEMapsRecord,
        getEraraRecordForEMap: getEraraRecordForEMap
    };
}];
