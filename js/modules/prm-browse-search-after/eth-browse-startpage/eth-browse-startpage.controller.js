export class ethBrowseStartpageController {
    constructor( ethConfigService, ethBrowseStartpageConfig ) {
        this.ethConfigService = ethConfigService;
        this.config = ethBrowseStartpageConfig;
    }
    $onInit() {
        try{
            this.parentCtrl = this.afterCtrl.parentCtrl;
            if (!document.querySelector('html') || !document.querySelector('html').classList.add) {
    			console.error("***ETH*** ethBrowseStartpageController.$onInit: classList.add() not available");
    			return;
    		}
            if (!this.parentCtrl.$state || !this.parentCtrl.$state.params) {
    			console.error("***ETH*** ethBrowseStartpageController.$onInit: $state.params.browseQuery not available");
    			return;
    		}
            if(!this.parentCtrl.$state.params.browseQuery){
                document.querySelector('html').classList.add('eth-browse-startpage');
            }
            else{
                document.querySelector('html').classList.remove('eth-browse-startpage');
            }
            this.lang = this.ethConfigService.getLanguage();
            this.vid = window.appConfig.vid.replace(':','-');
        }
        catch(e){
            console.error("***ETH*** an error occured: ethBrowseStartpageController\n\n");
            console.error(e.message);
        }
    }
}
ethBrowseStartpageController.$inject = [ 'ethConfigService', 'ethBrowseStartpageConfig' ];
