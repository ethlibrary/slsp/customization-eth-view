export const ethAlternativeSearchConfig = function(){
    return {
        label: {
            groupName: {
                de: 'Meine Suche schicken an',
                en: 'Send my search to'
            }
        }
    }
}
