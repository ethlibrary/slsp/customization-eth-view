/**
* @ngdoc module
* @name ethPlaceLinkModule
*
* @description
*
* - maps mms id to legacy record id
* - checks if the record is part of an ethorama poi/place
* - checks if there is a wiki link and gets qid and label
* - renders a link to place page
*
* <b>AngularJS Dependencies</b><br>
* Service /js/services {@link ETH.ethConfigService}<br>
* Service {@link ETH.ethPlaceLinkConfig}<br>
* Service {@link ETH.ethPlaceLinkService}<br>
*
*
* <b>CSS/Image Dependencies</b><br>
* CSS eth-place-link.css
*
*/
import {ethConfigService} from '../../../services/eth-config.service';
import {ethPlaceLinkConfig} from './eth-place-link.config';
import {ethPlaceLinkService} from './eth-place-link.service';
import {ethPlaceLinkController} from './eth-place-link.controller';
import {ethPlaceLinkHtml} from './eth-place-link.html';

export const ethPlaceLinkModule = angular
    .module('ethPlaceLinkModule', [])
        .factory('ethConfigService', ethConfigService)
        .factory('ethPlaceLinkConfig', ethPlaceLinkConfig)
        .factory('ethPlaceLinkService', ethPlaceLinkService)
        .controller('ethPlaceLinkController', ethPlaceLinkController)
        .component('ethPlaceLinkComponent',{
            bindings: {afterCtrl: '<'},
            controller: 'ethPlaceLinkController',
            template: ethPlaceLinkHtml
        })
