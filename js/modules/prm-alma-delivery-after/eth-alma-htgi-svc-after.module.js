// How to Get It (not online)
import {ethFullviewArchivesGetitModule} from './eth-fullview-archives-getit/eth-fullview-archives-getit.module';
import {ethOpenurlInterlibraryModule} from './eth-openurl-interlibrary/eth-openurl-interlibrary.module';
import {ethIllLinkModule} from './eth-ill-link/eth-ill-link.module';
import {ethSetIdModule} from './eth-set-id/eth-set-id.module';

export const ethAlmaHtgiSvcAfterModule = angular
    .module('ethAlmaHtgiSvcAfterModule', [])
        .component('almaHtgiSvcAfter',  {
            bindings: {parentCtrl: '<'},
            template: `
            <eth-set-id-component after-ctrl="$ctrl"></eth-set-id-component>
            <eth-openurl-interlibrary-component after-ctrl="$ctrl"></eth-openurl-interlibrary-component>
            <eth-ill-link-component after-ctrl="$ctrl"></eth-ill-link-component>
            <eth-fullview-archives-getit-component after-ctrl="$ctrl"></eth-fullview-archives-getit-component>
            `
        });

ethAlmaHtgiSvcAfterModule.requires.push(ethFullviewArchivesGetitModule.name);
ethAlmaHtgiSvcAfterModule.requires.push(ethOpenurlInterlibraryModule.name);
ethAlmaHtgiSvcAfterModule.requires.push(ethIllLinkModule.name);
ethAlmaHtgiSvcAfterModule.requires.push(ethSetIdModule.name);
