/**
* @ngdoc module
* @name ethElendingLinkModule
*
* @description
*
* Customization for the links section:<br>
* - add link to a CMS page about E-Lending
*
* <b>AngularJS Dependencies</b><br>
* Service /js/services {@link ETH.ethConfigService}<br>
* Service {@link ETH.ethElendingLinkConfig}<br>
*
*
*/
import {ethConfigService} from '../../../services/eth-config.service';
import {ethElendingLinkConfig} from './eth-elending-link.config';
import {ethElendingLinkController} from './eth-elending-link.controller';
import {ethElendingLinkHtml} from './eth-elending-link.html';

export const ethElendingLinkModule = angular
    .module('ethElendingLinkModule', [])
        .factory('ethConfigService', ethConfigService)
        .factory('ethElendingLinkConfig', ethElendingLinkConfig)
        .controller('ethElendingLinkController', ethElendingLinkController)
        .component('ethElendingLinkComponent', {
            bindings: {afterCtrl: '<'},
            controller: 'ethElendingLinkController',
            template: ethElendingLinkHtml
        })
