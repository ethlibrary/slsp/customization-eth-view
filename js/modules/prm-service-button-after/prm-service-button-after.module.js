
import { slspReservationButtonModule } from './slsp-reservation-button/slsp-reservation-button.module';

export const prmServiceButtonAfterModule = angular
    .module('prmServiceButtonAfterModule', [])
    .component('prmServiceButtonAfter', {
        bindings: { parentCtrl: '<' },
        template: `<slsp-reservation-button-component after-ctrl="$ctrl"></slsp-reservation-button-component>
                    `
    });

prmServiceButtonAfterModule.requires.push(slspReservationButtonModule.name);
