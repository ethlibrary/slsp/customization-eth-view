/**
* @ngdoc module
* @name tmiComposeNbModule
*
* @description
*
* Customization for the links section:<br>
* - render links for Nachlassbibliothek online and print resources
*
* <b>AngularJS Dependencies</b><br>
* Service {@link ETH.tmiComposeNbService}<br>
* Service /js/services {@link ETH.ethConfigService}<br>
* Service {@link ETH.tmiComposeNbConfig}<br>
*
*/
import {ethConfigService} from '../../../services/eth-config.service';
import {tmiComposeNbConfig} from './tmi-compose-nb.config';
import {tmiComposeNbController} from './tmi-compose-nb.controller';
import {tmiComposeNbHtml} from './tmi-compose-nb.html';
import {tmiComposeNbService} from './tmi-compose-nb.service';

export const tmiComposeNbModule = angular
    .module('tmiComposeNbModule', [])
        .factory('ethConfigService', ethConfigService)
        .factory('tmiComposeNbConfig', tmiComposeNbConfig)
        .factory('tmiComposeNbService', tmiComposeNbService)
        .controller('tmiComposeNbController', tmiComposeNbController)
        .component('tmiComposeNbComponent', {
            bindings: {afterCtrl: '<'},
            controller: 'tmiComposeNbController',
            template: tmiComposeNbHtml
        })
