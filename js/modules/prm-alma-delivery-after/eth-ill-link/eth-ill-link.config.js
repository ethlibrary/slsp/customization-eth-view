export const ethIllLinkConfig = function(){
    return {
        label: {
            text1:{
                de: 'Artikel/Kapitel ist im swisscovery Network nicht vorhanden oder nicht bestellbar.',
                en: 'Article/chapter is not held in the swisscovery Network or cannot be requested.'
            },
            text2:{
                de: 'Bestellen Sie bitte via',
                en: 'Please request via'
            },
            linktext:{
                de: 'Fernleihe',
                en: 'interlibrary loan'
            },
            text3:{
                de: '.',
                en: '.'
            },
        },
        url:{
            link: {
                de: 'https://library.ethz.ch/recherchieren-und-nutzen/ausleihen-und-nutzen/bestellformulare/fernleihe-kopien-bestellen.html',
                en: 'https://library.ethz.ch/en/searching-and-using/borrowing-and-using/order-forms/interlibrary-loans-ordering-copies.html'
            }
        }
    }
}
