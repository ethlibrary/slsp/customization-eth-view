export class ethTitleAsTextController {

    constructor($scope, $compile, $element) {
        this.$scope = $scope
        this.$compile = $compile;
        this.$element = $element
    }

    $onInit() {
        try{
            this.parentCtrl = this.afterCtrl.parentCtrl;
            this.processDoCheck = false;
            if (this.parentCtrl.loansService.requestParams.type === 'active') {
                this.processDoCheck = true;
            }            
        }
        catch(e) {
            console.error("***ETH*** an error occured: ethTitleAsTextController");
            console.error(e.message);
        }
    }

    $doCheck() {
        try{
            if(!this.processDoCheck){
                return;
            }
            let directive = this.$element[0].parentElement.parentElement;
            let row = directive.querySelector('.title-row');

            if(row){
                let title = this.parentCtrl.item.title;
                let html = `
                <div layout="row" class="title-row layout-row">
                    <h3>${title}</h3>
                </div>
                `;
                angular.element(row).after(this.$compile(html)(this.$scope));
                angular.element(row).remove();
                this.processDoCheck = false;
            }
        }
        catch(e){
            console.error("***ETH*** an error occured: ethTitleAsTextController.$doCheck\n\n");
            console.error(e.message);
            throw(e)
        }
    }
}

ethTitleAsTextController.$inject = ['$scope', '$compile', '$element']

