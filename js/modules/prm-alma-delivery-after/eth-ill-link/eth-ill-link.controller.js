export class ethIllLinkController {

    constructor( ethConfigService, ethIllLinkConfig ) {
        this.ethConfigService = ethConfigService;
        this.config = ethIllLinkConfig;
    }

    $onInit() {
        try{
            // Artikel: cdi_proquest_miscellaneous_2479421945cdi_springer_books_10_1007_978_3_322_86033_0_25
            // Kapitel: cdi_proquest_ebookcentralchapters_7209972_65_134, cdi_walterdegruyter_books_10_1515_9780748627141_063
            this.parentCtrl = this.afterCtrl.parentCtrl;
            this.processDoCheck = false;
            this.isIllLink = false;

            // check docid for 'cdi_'
            let docId = this.parentCtrl.item.pnx.control.recordid[0];
            if(docId.substring(0,4) !== 'cdi_')return;

            // check availability status
            let availability = this.parentCtrl.item.delivery.availability[0];
            if(availability !== 'no_inventory')return;
            this.processDoCheck = true;
        }
        catch(e){
            console.error("***ETH*** an error occured: ethIllLinkController $onInit\n\n");
            console.error(e.message);
        }
    }

    $doCheck() {
        try{
            if(!this.processDoCheck)return;

            // check if rapido is still loading
            if(document.querySelector("[translate='rapido.tiles.placeholder.text']"))return;

            // check for other institutions (GetIt)
            /*if(this.parentCtrl.item.delivery.almaInstitutionsList && this.parentCtrl.item.delivery.almaInstitutionsList.length > 0){
                this.processDoCheck = false;
                return;
            }*/
            // check for no offer
            let digitalCondition = false;
            let physicalCondition = false;
            let noOfferDigital = document.querySelector('[translate="rapido.tiles.no.offer.after.placeholder.digital"]');
            if(noOfferDigital){
                digitalCondition = true;
            }
            let buttonPhysical = document.getElementById('get_it_btn_physical');
            if(!buttonPhysical || buttonPhysical.getAttribute('disabled')){
                physicalCondition = true;
            }
            if(digitalCondition && physicalCondition){
                let aQs = [];
                // magazinearticle, article
                if((this.parentCtrl.item.pnx.display.type[0] === 'magazinearticle' || this.parentCtrl.item.pnx.display.type[0] === 'article') && this.parentCtrl.item.pnx.addata.atitle){
                    if(this.parentCtrl.item.pnx.addata.atitle && this.parentCtrl.item.pnx.addata.atitle[0]){
                        aQs.push(
                            'atitle=' + encodeURIComponent(this.parentCtrl.item.pnx.addata.atitle[0])
                        );
                    }
                    if(this.parentCtrl.item.pnx.addata.jtitle && this.parentCtrl.item.pnx.addata.jtitle[0]){
                        aQs.push(
                            'jtitle=' + encodeURIComponent(this.parentCtrl.item.pnx.addata.jtitle[0])
                        );
                    }
                    if(this.parentCtrl.item.pnx.addata.au && this.parentCtrl.item.pnx.addata.au.length > 0){
                        aQs.push(
                            'au=' + encodeURIComponent(this.parentCtrl.item.pnx.addata.au.join(', '))
                        );
                    }
                    if(this.parentCtrl.item.pnx.addata.volume && this.parentCtrl.item.pnx.addata.volume[0]){
                        aQs.push(
                            'volume=' + encodeURIComponent(this.parentCtrl.item.pnx.addata.volume[0])
                        );
                    }
                    if(this.parentCtrl.item.pnx.addata.pages && this.parentCtrl.item.pnx.addata.pages[0]){
                        aQs.push(
                            'pages=' + encodeURIComponent(this.parentCtrl.item.pnx.addata.pages[0])
                        );
                    }
                    if(this.parentCtrl.item.pnx.addata.issn && this.parentCtrl.item.pnx.addata.issn[0]){
                        aQs.push(
                            'issn=' + encodeURIComponent(this.parentCtrl.item.pnx.addata.issn.join(','))
                        );
                    }
                    if(this.parentCtrl.item.pnx.addata.date && this.parentCtrl.item.pnx.addata.date[0]){
                        aQs.push(
                            'date=' + encodeURIComponent(this.parentCtrl.item.pnx.addata.date[0])
                        );
                    }
                }
                // book chapter
                else if(this.parentCtrl.item.pnx.display.type[0] === 'book_chapter' && this.parentCtrl.item.pnx.addata.atitle){
                    if(this.parentCtrl.item.pnx.addata.atitle && this.parentCtrl.item.pnx.addata.atitle[0]){
                        aQs.push(
                            'atitle=' + encodeURIComponent(this.parentCtrl.item.pnx.addata.atitle[0])
                        );
                    }
                    if(this.parentCtrl.item.pnx.addata.btitle && this.parentCtrl.item.pnx.addata.btitle[0]){
                        aQs.push(
                            'jtitle=' + encodeURIComponent(this.parentCtrl.item.pnx.addata.btitle[0])
                        );
                    }
                    if(this.parentCtrl.item.pnx.addata.au && this.parentCtrl.item.pnx.addata.au.length > 0){
                        aQs.push(
                            'au=' + encodeURIComponent(this.parentCtrl.item.pnx.addata.au.join(', '))
                        );
                    }
                    if(this.parentCtrl.item.pnx.addata.pages && this.parentCtrl.item.pnx.addata.pages[0]){
                        aQs.push(
                            'pages=' + encodeURIComponent(this.parentCtrl.item.pnx.addata.pages[0])
                        );
                    }
                    if(this.parentCtrl.item.pnx.addata.volume && this.parentCtrl.item.pnx.addata.volume[0]){
                        aQs.push(
                            'volume=' + encodeURIComponent(this.parentCtrl.item.pnx.addata.volume[0])
                        );
                    }
                    else {
                        aQs.push(
                            'volume=-'
                        );
                    }
                    if(this.parentCtrl.item.pnx.addata.isbn && this.parentCtrl.item.pnx.addata.isbn[0]){
                        aQs.push(
                            'issn=' + encodeURIComponent(this.parentCtrl.item.pnx.addata.isbn.join(','))
                        );
                    }
                    else if(this.parentCtrl.item.pnx.addata.eisbn && this.parentCtrl.item.pnx.addata.eisbn[0]){
                        aQs.push(
                            'issn=' + encodeURIComponent(this.parentCtrl.item.pnx.addata.eisbn.join(','))
                        );
                    }
                    if(this.parentCtrl.item.pnx.addata.date && this.parentCtrl.item.pnx.addata.date[0]){
                        aQs.push(
                            'date=' + encodeURIComponent(this.parentCtrl.item.pnx.addata.date[0])
                        );
                    }
                }
                else{
                    if(this.parentCtrl.item.pnx.display.title && this.parentCtrl.item.pnx.display.title[0]){
                        aQs.push(
                            'jtitle=' + encodeURIComponent(this.parentCtrl.item.pnx.display.title[0])
                        );
                    }
                    if(this.parentCtrl.item.pnx.display.creator && this.parentCtrl.item.pnx.display.creator[0]){
                        aQs.push(
                            'au=' + encodeURIComponent(this.parentCtrl.item.pnx.display.creator[0])
                        );
                    }
                    if(this.parentCtrl.item.pnx.display.creationdate && this.parentCtrl.item.pnx.display.creationdate[0]){
                        aQs.push(
                            'date=' + encodeURIComponent(this.parentCtrl.item.pnx.display.creationdate[0])
                        );
                    }
                    if(this.parentCtrl.item.pnx.display.publisher && this.parentCtrl.item.pnx.display.publisher[0]){
                        aQs.push(
                            'publisher=' + encodeURIComponent(this.parentCtrl.item.pnx.display.publisher[0])
                        );
                    }
                    if(this.parentCtrl.item.pnx.display.identifier){
                        let aIdent = this.parentCtrl.item.pnx.display.identifier.filter(i => {
                            return i.indexOf('ISSN')>-1 || i.indexOf('ISBN')>-1;
                        })
                        if(aIdent.length > 0){
                            aQs.push(
                                'issn=' + encodeURIComponent(aIdent[0].substring(aIdent[0].indexOf(':')+2))
                            );
                        }
                    }
                }
                this.qs = '?' + aQs.join('&');
                this.isIllLink = true;
            }
            this.processDoCheck = false;
        }
        catch(e){
            console.error("***ETH*** an error occured: ethIllLinkController $doCheck\n\n");
            console.error(e.message);
        }
    }
}

ethIllLinkController.$inject = ['ethConfigService', 'ethIllLinkConfig' ];
