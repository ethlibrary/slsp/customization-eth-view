export const ethNoLocationsConfig = function(){
    return {
        label: {
            hint: {
                de: 'Kein Exemplar an Bibliotheken der ETH Zürich vorhanden.',
                en: 'No items held at libraries of ETH Zurich.'
            }
        }
    }
}
