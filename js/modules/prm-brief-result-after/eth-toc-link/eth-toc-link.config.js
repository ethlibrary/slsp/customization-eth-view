export const ethTocLinkConfig = function(){
    return {
        label: {
            toc: {
                de: 'der DNB zu ISBN ',
                en: 'from the DNB for ISBN '
            }
        }
    }
}
