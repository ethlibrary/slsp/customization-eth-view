export const ethPlacePageLinkConfig = function(){
    return {
        label: {
            placePageLinkText: {
                de: 'Mehr Informationen zu',
                en: 'More informations about'
            }
        }
    }
}
