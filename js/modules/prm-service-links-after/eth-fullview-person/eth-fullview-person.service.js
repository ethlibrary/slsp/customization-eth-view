/**
* @ngdoc service
* @name ethFullviewPersonService
*
* @description
*
* getPersons(): checks lmu prometheus, Metagrid, Entityfacts (DNB), Wikidata for person informations (by GND ID)
*
* <b>Used by</b><br>
*
* Module {@link ETH.ethFullviewPersonModule}<br>
*
*
 */
export const ethFullviewPersonService = ['$http', '$sce', function($http, $sce){

    function getPersons(gndIds, lang){
        let baseurl = 'https://daas.library.ethz.ch/rib/v3/persons';
        let url = baseurl + "/person-gnd?gnd=" + gndIds.join(',') + '&lang=' + lang;

        return $http.get(url)
            .then(
                function(response){
                    return response.data;
                },
                function(httpError){
                    if (httpError.status === 404 || httpError.status === 500)return null;
                    let error = "***ETH*** an error occured: ethFullviewPersonService.getPersons(): " + httpError.status;
                    if (httpError.data && httpError.data.errorMessage) {
                        error += ' - ' + httpError.data.errorMessage;
                    }
                    console.error(error);
                    return null;
                }
            );
    }

    return {
        getPersons: getPersons
    };

}]
