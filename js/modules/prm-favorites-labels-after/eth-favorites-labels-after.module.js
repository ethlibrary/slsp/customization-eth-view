import {ethReferencesHintModule} from './eth-references-hint/eth-references-hint.module';

export const ethFavoritesLabelsAfterModule = angular
    .module('ethFavoritesLabelsAfterModule', [])
        .component('prmFavoritesLabelsAfter',  {
            template: `<eth-references-hint-component></eth-references-hint-component>`
        });

ethFavoritesLabelsAfterModule.requires.push(ethReferencesHintModule.name);
