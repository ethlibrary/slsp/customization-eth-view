/**
* @ngdoc module
* @name ethConnectedPapersModule
*
* @description
*
* Customization for the availability line:<br>
* - get paper informations and link to connected papers
*
*
* <b>AngularJS Dependencies</b><br>
* Service {@link ETH.ethConnectedPapersConfig}<br>
* Service {@link ETH.ethConnectedPapersService}<br>
* Service /js/services {@link ETH.ethConfigService}<br>
* Service /js/services {@link ETH.ethSessionService}<br>
*
*
* <b>CSS/Image Dependencies</b><br>
* CSS eth-connectedpapers.css
*
*
*/
import {ethConfigService} from '../../../services/eth-config.service';
import {ethSessionService} from '../../../services/eth-session.service';
import {ethConnectedPapersService} from './eth-connectedpapers.service';
import {ethConnectedPapersConfig} from './eth-connectedpapers.config';
import {ethConnectedPapersController} from './eth-connectedpapers.controller';
import {ethConnectedPapersHtml} from './eth-connectedpapers.html';

export const ethConnectedPapersModule = angular
    .module('ethConnectedPapersModule', [])
        .factory('ethConfigService', ethConfigService)
        .factory('ethSessionService', ethSessionService)
        .factory('ethConnectedPapersService', ethConnectedPapersService)
        .factory('ethConnectedPapersConfig', ethConnectedPapersConfig)
        .controller('ethConnectedPapersController', ethConnectedPapersController)
        .component('ethConnectedPapersComponent', {
            bindings: {afterCtrl: '<'},
            require: {
                briefResult: '^prmBriefResultContainer'
            },
            controller: 'ethConnectedPapersController',
            template: ethConnectedPapersHtml
        })
