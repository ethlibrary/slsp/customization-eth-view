import {ethChangeAdressModule} from './eth-change-adress/eth-change-adress.module';
import {ethIdpWarningModule} from './eth-idp-warning/eth-idp-warning.module';

export const ethPersonalInfoAfterModule = angular
    .module('ethPersonalInfoAfterModule', [])
        .component('prmPersonalInfoAfter',  {
            bindings: {parentCtrl: '<'},
            template: `<eth-change-adress-component after-ctrl="$ctrl"></eth-change-adress-component>
            <eth-idp-warning-component after-ctrl="$ctrl"></eth-idp-warning-component>`
        });
ethPersonalInfoAfterModule.requires.push(ethChangeAdressModule.name);
ethPersonalInfoAfterModule.requires.push(ethIdpWarningModule.name);
