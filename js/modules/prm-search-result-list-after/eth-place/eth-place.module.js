/**
* @ngdoc module
* @name ethPlaceModule
*
* @description
*
* - constitutes place page (based on result list)
* - offers a result list based on ETHorama dossier
* - renders a map
* - checks Metagrid Geolinker API and Wikidata for place links

*
* <b>AngularJS Dependencies</b><br>
* Service /js/services {@link ETH.ethSessionService}<br>
* Service /js/services {@link ETH.ethConfigService}<br>
* Service {@link ETH.ethPlaceConfig}<br>
* Service {@link ETH.ethPlaceService}<br>
*
* see also modules\prm-service-links-after\eth-place-link
*
* <b>CSS/Image Dependencies</b><br>
* CSS eth-place.css
* CSS js/modules/shared/eth-person-card/splus.css
*
*/
import {ethConfigService} from '../../../services/eth-config.service';
import {ethSessionService} from '../../../services/eth-session.service';
import {ethPlaceConfig} from './eth-place.config';
import {ethPlaceService} from './eth-place.service';
import {ethPlaceController} from './eth-place.controller';
import {ethPlaceHtml} from './eth-place.html';


export const ethPlaceModule = angular
    .module('ethPlaceModule', [])
        .factory('ethConfigService', ethConfigService)
        .factory('ethSessionService', ethSessionService)
        .factory('ethPlaceConfig', ethPlaceConfig)
        .factory('ethPlaceService', ethPlaceService)
        .controller('ethPlaceController', ethPlaceController)
        .component('ethPlaceComponent',{
            //require: {prmFullview: '^^prmFullview' },
            //restrict: 'E',
            bindings: {afterCtrl: '<'},
            controller: 'ethPlaceController',
            template: ethPlaceHtml
        })


        /*
    scrollToElementId(eID) {
        let elm = document.getElementById(eID);
        if (!elm) {
            return
        }
        elm.classList.add("section-focused");

        this.$timeout(() => {
            elm.classList.remove("section-focused");
        }, 500);
        if (this.isOverlayFullView) {
            this.smoothScrollUtil.scrollTo(eID, '', 'sticky-scroll');
        }
        else {
            this.smoothScrollUtil.scrollTo(eID, '', '');
        }
    }
    
    
    import { SmoothScrollUtil } from '../../nddUtils/smoothScrollUtil.service';

    'use strict';
import { Injectable} from 'ng-metadata/core';
import {isBrowserSafari} from '../infra/browserSniffer';

const Observable= require('rxjs/Observable').Observable;
require('rxjs/add/observable/defer');
require('rxjs/add/observable/interval');
require('rxjs/add/operator/map');
require('rxjs/add/operator/takeWhile');

const animationFrame= require('rxjs/scheduler/animationFrame').animationFrame;


interface Node {
    scrollTop: number;
}

interface Window {
    chrome: any;
    StyleMedia: any;
    HtmlElement:any;
}


@Injectable()
export class SmoothScrollUtil {
    private window: Node;
    private existingAnimationSubscription;

    constructor(){
        let scheduler= animationFrame;
        this.msElapsed= Observable.defer(()=>{
            let start = scheduler.now();
            return Observable.interval(0, scheduler).map(()=> scheduler.now() - start);
        });
    }

       public focusOn(elm){
        this.elementFocus(elm);
        return elm;
    }

    public elementFocus(elm){
        if(elm) {
            if(typeof elm === 'string') {
                elm = document.getElementById(elm);
            }
            if(!elm) {
                return;
            }
            let prevTabIndex = elm.getAttribute('tabindex');
            elm.setAttribute('tabindex','-1');
            elm.focus();
            if(prevTabIndex !== null && prevTabIndex !== '') {
                elm.setAttribute('tabindex', prevTabIndex);
            }
            else{
                elm.removeAttribute('tabindex'); 
            }
        }
    }

    public scrollTo(eID, scrollContainerId, scrollContainerTagName, offset: number= 70){
        let scrollContainer= <Node> this.getScrollContainer(scrollContainerId,scrollContainerTagName);
        let elm = eID;
        if (!(elm instanceof HTMLElement)){
            elm = document.getElementById(eID);
        }
        if (!elm){
            return;
        }

        let startY = this.currentYPosition(scrollContainer);

        let stopY;
        if(elm) {
            stopY = this.elmYPosition(elm) - this.elmYPosition(scrollContainer) - offset;
        }
        stopY= stopY<0? 0: stopY;
        let maxY= scrollContainer.scrollHeight - scrollContainer.offsetHeight;
        stopY= Math.min(maxY, stopY);
        let distance = stopY > startY ? stopY - startY : startY - stopY;
        if (distance < 100) {
            scrollContainer.scrollTop= stopY;
            this.focusOn(elm);
            return;
        }
        let unitsPerSecond = Math.round(distance / 100)+30;
        if (unitsPerSecond >= 150) {
            unitsPerSecond = 150;
        }
        if (this.existingAnimationSubscription){
            this.existingAnimationSubscription.unsubscribe(); //stop any existing animations
        }
        this.existingAnimationSubscription= this.msElapsed.map(ms =>
            Math.ceil(unitsPerSecond * ms / 1000)).takeWhile(() => Math.abs(scrollContainer.scrollTop - stopY) > 1).subscribe((distance) => {
                let current= scrollContainer.scrollTop;
                if (stopY > startY) {
                    scrollContainer.scrollTop = Math.min(stopY, current + distance);
                }
                if (stopY < startY){
                    scrollContainer.scrollTop = Math.max(stopY, current - distance);
                }
        }, ()=>{
            console.log("something went wrong during smooth scroll");
            this.focusOn(elm);
        }, ()=>{
            this.focusOn(elm);
        });

    }
    
        private currentYPosition(scrollContainer) {
            return scrollContainer.scrollTop;
        }
    
        private elmYPosition(elm) {
            let y = elm.offsetTop;
            let node = elm;
            while (node.offsetParent && node.offsetParent != document.body) {
                node = node.offsetParent;
                y += node.offsetTop;
            } return y;
        }
    
        private getScrollContainer(scrollContainerId,scrollContainerTagName){
            let scrollContainer;
            if(scrollContainerId){
                return document.getElementById(scrollContainerId);
            }
            if(scrollContainerTagName){
                return document.querySelector(scrollContainerTagName);
            }
            let isIE= false || !!document.documentMode;
            //if chrome or safari or edge get document body
            let w: Window = <Window> window;
            if(isBrowserSafari()){
                return document.documentElement;
            }
            if ((Object.prototype.toString.call(w.HTMLElement).indexOf('Constructor') > 0) ||
                (!isIE && !!w.StyleMedia)){
                return document.body;
            }
            return document.body.parentNode;
        }
    
    
    
}



        
        */