export const ethProvenienzEraraLinkConfig = function(){
    return {
        label: {
            erara:{
                de: 'Quelle in e-rara',
                en: 'Source in e-rara'
            },
            swisscovery: {
                de: 'Details Provenienzvermerk',
                en: 'Details provenance mark'
            }
        }
    }
}
