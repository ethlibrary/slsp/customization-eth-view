/**
* @ngdoc module
* @name ethVideoModule
*
* @description
*
* Changes link text of video link in view online section, if link leads to startpage of video portal
*
* <b>AngularJS Dependencies</b><br>
* Service /js/services {@link ETH.ethConfigService}<br>
* Service {@link ETH.ethVideoConfig}<br>
*
* <b>CSS/Image Dependencies</b><br>
*
*/
import {ethConfigService} from '../../../services/eth-config.service';
import {ethVideoConfig} from './eth-video.config';
import {ethVideoController} from './eth-video.controller';

export const ethVideoModule = angular
    .module('ethVideoModule', [])
        .factory('ethConfigService', ethConfigService)
        .factory('ethVideoConfig', ethVideoConfig)
        .controller('ethVideoController', ethVideoController)
        .component('ethVideoComponent', {
            bindings: {afterCtrl: '<'},
            controller: 'ethVideoController'
        })
