export class ethPlaceLinkController {

    constructor( ethConfigService, ethPlaceLinkConfig, ethPlaceLinkService ) {
        this.ethConfigService = ethConfigService;
        this.config = ethPlaceLinkConfig;
        this.ethPlaceLinkService = ethPlaceLinkService;
    }

    $onInit() {
        try{
            this.parentCtrl = this.afterCtrl.parentCtrl;
            this.aPlacePageLinksETHorama = [];
            this.aPlacePageLinksTopics = [];
            this.lang = this.ethConfigService.getLanguage();
            this.getETHoramaPlaces(this.parentCtrl.item.pnx.control.sourcerecordid[0]);
            this.getTopicPlaces();
        }
        catch(e){
            console.error("***ETH*** an error occured: ethPlaceLinkController.onInit()\n\n");
            console.error(e.message);
        }
    }

    getETHoramaPlaces(docId){
        let vid = window.appConfig.vid;
        let tab = window.appConfig['primo-view']['available-tabs'][0];
        let baseurl = `/discovery/search?tab=${tab}&vid=${vid}&lang=${this.lang}&query=any,contains,[wd/place]`;
        this.ethPlaceLinkService.getPlaceFromETHorama(docId)
            .then(data => {
                if(!data || !data.items || data.items.length === 0)return null;
                let pois = [];
                if (data.items.length > 0) {
                    data.items.forEach(e => {
                        pois.push(e);
                    })
                }
                return this.ethPlaceLinkService.enrichPOIsWithQID(pois);
            })
            .then(data => {
                if(!data || data.length === 0)return null;
                let filteredData = data.filter( e => {
                    return e.qid && e.qid != '';
                })
                filteredData.forEach(e => {
                    let aFilteredPlacePageLinks = this.aPlacePageLinksETHorama.filter( x => {
                        return e.qid === x.qid;
                    })
                    if(aFilteredPlacePageLinks.length === 0){
                        this.aPlacePageLinksETHorama.push({'qid': e.qid, 'label': e.wikiTitle.replace(/_/g,' '), 'url': baseurl + e.qid});
                    }
                })
                this.aPlacePageLinksETHorama.sort(function(a, b){
                    let labelA = a.label.toLowerCase(), labelB = b.label.toLowerCase();
                    if (labelA < labelB){
                        return -1;
                    }
                    else if (labelA > labelB){
                        return 1;
                    }
                    else{
                        return 0;
                    }
                })
            })
            .catch( (e) => {
                console.error("***ETH*** an error occured: ethPlaceLinkController.getETHoramaPlaces()");
                console.error(e.message);
                throw(e)
            })
    }

    getTopicPlaces(){
        // GND ID
        if(this.parentCtrl.item.pnx.display.lds03 && this.parentCtrl.item.pnx.display.lds03.length > 0){
            let lds03 = this.parentCtrl.item.pnx.display.lds03;
            let aGndIds = [];
            for(let i = 0; i < lds03.length; i++){
                // ALMA Ressources: link in value
                if(lds03[i].indexOf('/gnd/') > -1){
                    let part = lds03[i].substring(lds03[i].indexOf('/gnd/') + 5);
                    part = part.substring(0, part.indexOf('">'));
                    if(part.indexOf('(DE-588)')>-1){
                        part = part.replace('(DE-588)','')
                    }
                    if(aGndIds.indexOf(part)===-1)aGndIds.push(part);
                }
                // External data: text
                else if (lds03[i].indexOf(': ') > -1) {
                    let part = lds03[i].substring(lds03[i].lastIndexOf(': ') + 2);
                    if(part.indexOf('(DE-588)')>-1){
                        part = part.replace('(DE-588)','')
                    }
                    if(aGndIds.indexOf(part)===-1)aGndIds.push(part);
                }
            }
            this.ethPlaceLinkService.getTopicPlaces(aGndIds)
                .then((data) => {
                    try{
                        if(!data || !data.results || data.results.length === 0)return;
                        let vid = window.appConfig.vid;
                        let tab = window.appConfig['primo-view']['available-tabs'][0];
                        data.results.forEach(p => {
                            if(p.qid){
                                let query = '[wd/place]' + p.qid;
                                p.url = `/discovery/search?tab=${tab}&query=any,contains,${query}&vid=${vid}`;
                                this.aPlacePageLinksTopics.push(p);
                            }
                        })
                    }
                    catch(e){
                        console.error("***ETH*** an error occured: ethPlaceLinkController:ethPlaceLinkService.getTopicPlaces(): \n\n");
                        console.error(e.message);
                    }
                });
        }
    }

}

ethPlaceLinkController.$inject = ['ethConfigService', 'ethPlaceLinkConfig', 'ethPlaceLinkService'];
