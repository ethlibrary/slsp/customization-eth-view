/**
* @ngdoc module
* @name ethRapidoHintModule
*
* @description
*
* Customization for the Top Bar:<br>
* - Rapido hint at the top of the page
*
*
* <b>AngularJS Dependencies</b><br>
* Service /js/services {@link ETH.ethConfigService}<br>
* Service {@link ETH.ethRapidoHintConfig}<br>
* Service /js/services {@link ETH.ethSessionService}<br>
*
*
* <b>CSS/Image Dependencies</b><br>
* CSS eth-rapido-hint.css
*
*/
import {ethConfigService} from '../../../services/eth-config.service';
import {ethSessionService} from '../../../services/eth-session.service';
import {ethRapidoHintConfig} from './eth-rapido-hint.config';
import {ethRapidoHintController} from './eth-rapido-hint.controller';
import {ethRapidoHintHtml} from './eth-rapido-hint.html';

export const ethRapidoHintModule = angular
    .module('ethRapidoHintModule', [])
        .factory('ethConfigService', ethConfigService)
        .factory('ethSessionService', ethSessionService)
        .factory('ethRapidoHintConfig', ethRapidoHintConfig)
        .controller('ethRapidoHintController', ethRapidoHintController)
        .component('ethRapidoHintComponent',{
            controller: 'ethRapidoHintController',
            template: ethRapidoHintHtml
        })
