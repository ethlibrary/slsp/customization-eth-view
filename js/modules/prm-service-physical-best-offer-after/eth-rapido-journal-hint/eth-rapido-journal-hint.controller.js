export class ethRapidoJournalHintController {

    constructor( $scope, $compile, ethSessionService, ethConfigService, ethRapidoJournalHintConfig, $timeout, $anchorScroll) {
        this.$scope = $scope;
        this.$compile = $compile;
        this.ethSessionService = ethSessionService;
        this.ethConfigService = ethConfigService;
        this.config = ethRapidoJournalHintConfig;
        this.$timeout = $timeout;
        this.$anchorScroll = $anchorScroll;
    }

    $onInit() {
        // 990000341770205503 no rapido service
        // 990000164420205503 digi service
        try{
            this.parentCtrl = this.afterCtrl.parentCtrl;
            this.doIt = false;
            // only if journal
            if(this.parentCtrl.item.pnx.display.type.length === 0 || this.parentCtrl.item.pnx.display.type[0] !== 'journal'){
                return;
            }
            // only if ETH holdings
            if(!this.parentCtrl.item.delivery || !this.parentCtrl.item.delivery.holding || this.parentCtrl.item.delivery.holding.length === 0){
                return;
            }
            this.doIt = true;
        }
        catch(e){
            console.error("***ETH*** an error occured: ethRapidoJournalHintController.onInit \n\n");
            console.error(e.message);
        }
    }

    $doCheck() {
        try{
            if(!this.doIt)return;
            // wait until stuff is loaded ...
            if(this.parentCtrl.tilesplaceholderactive)return;
            // metadata for AEM form
            let recordid = this.parentCtrl.item.pnx.control.recordid[0];
            let mmsid = recordid.substring(4);

            let title = '';
            if(this.parentCtrl.item.pnx.display.title && this.parentCtrl.item.pnx.display.title.length > 0){
                title = this.parentCtrl.item.pnx.display.title[0];
            }
            else if(this.parentCtrl.item.pnx.display.series && this.parentCtrl.item.pnx.display.series.length > 0){
                title = this.parentCtrl.item.pnx.display.series[0];
                if(title.indexOf('$$Q') > -1){
                    title = title.substring(0, title.indexOf('$$Q'))
                }
            }
            this.link = this.ethConfigService.getUrl(this.config, 'cms') + '?jtitle=' + encodeURIComponent(title) + '&mmsid=' + encodeURIComponent(mmsid);
            this.$timeout(() => {
                // do it only once
                let hintContainer = document.querySelector('.eth-rapido-journal-hint');
                if(hintContainer){
                    angular.element(hintContainer).remove();
                };
                let html = `
                <div class='eth-rapido-journal-hint'>
                    <p>
                    {{::$ctrl.ethConfigService.getLabel($ctrl.config, \'hint1\')}}
                    <md-button class="md-primary eth-rapido-journal-hint__button" ng-click="$ctrl.scrollToGetit()">
                        ${this.ethConfigService.getLabel(this.config, 'linktext')}
                    </md-button>{{::$ctrl.ethConfigService.getLabel($ctrl.config, \'hint2\')}}
                    </p>
                </div>
                `;
                let target = document.querySelector('#rapidoOffer div.full-view-section-content prm-full-view-service-container prm-service-physical-best-offer .offer_details-wrapper');
                angular.element(target).prepend(this.$compile(html)(this.$scope)).addClass('eth-rapido-journal-hint-container');
                // hide OTB text
                /*let otbText = document.querySelector('#rapidoOffer [translate="rapido.tiles.no.offer.after.placeholder.digital"]');
                if(otbText){
                    angular.element(otbText.parentElement).addClass('eth-rapido-journal-hint-disable-otb');
                }*/
            }, 150);
            this.doIt = false;
        }
        catch(e){
            console.error("***ETH*** ethRapidoJournalHintController.$doCheck:");
            console.error(e.message);
        }
    }
    scrollToGetit(){
        this.$anchorScroll('eth-getit-heading')
        return true;
    }
}

ethRapidoJournalHintController.$inject = ['$scope', '$compile', 'ethSessionService', 'ethConfigService', 'ethRapidoJournalHintConfig', '$timeout', '$anchorScroll'];
