export const ethBibNewsConfig = function(){
    return {
        label: {
            heading: {
                de: 'News',
                en: 'News'
            },
            more:{
                de: 'Zur News',
                en: 'News'
            },
            all:{
                de: 'Alle News',
                en: 'All news'
            }
        },
        url:{
            all: {
                de: 'https://library.ethz.ch/news-und-kurse/news-swisscovery.html',
                en: 'https://library.ethz.ch/en/news-and-courses/news-swisscovery.html'
            }
        }
    }
}
