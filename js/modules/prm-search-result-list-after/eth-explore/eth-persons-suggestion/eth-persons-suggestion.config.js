export const ethPersonsSuggestionConfig = function(){
    return {
        label: {
            heading: {
                de: 'Personen',
                en: 'Persons'
            },
            headingIntro1: {
                de: 'Die angezeigten Personen basieren auf einer',
                en: 'The persons shown are based on a '
            },
            headingIntro2: {
                de: 'lexikalischen Suche',
                en: 'lexical search'
            },
            headingIntro3: {
                de: 'in Wikidata.',
                en: 'in Wikidata.'
            },
            sortorder: {
                de: 'Sortierung nach Personen mit Bild und Relevanz (gemäss Wikidata).',
                en: 'Sorting by persons with image and by relevance in Wikidata.'
            },
            searchThis: {
                de: 'Diese Person suchen',
                en: 'Search for this person'
            },
            birthday: {
                de: 'Geburtstag',
                en: 'Birthday'
            }
        },
        url:{
            info: {
                de: 'https://library.ethz.ch',
                en: 'https://library.ethz.ch'
            }
        }
    }
}
