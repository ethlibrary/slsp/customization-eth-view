/**
* @ngdoc module
* @name ethBibNewsModule
*
* @description
*
* - News from ETH Library CMS (AEM Website)
*
*
* <b>AngularJS Dependencies</b><br>
* Service /js/services {@link ETH.ethConfigService}<br>
* Service {@link ETH.ethBibNewsConfig}<br>
* Service {@link ETH.ethBibNewsService}<br>
*
*
* <b>CSS/Image Dependencies</b><br>
* CSS eth-bib-news.css
*
*/
import {ethConfigService} from '../../../services/eth-config.service';
import {ethBibNewsConfig} from './eth-bib-news.config';
import {ethBibNewsController} from './eth-bib-news.controller';
import {ethBibNewsHtml} from './eth-bib-news.html';
import {ethBibNewsService} from './eth-bib-news.service';

export const ethBibNewsModule = angular
    .module('ethBibNewsModule', [])
        .factory('ethConfigService', ethConfigService)
        .factory('ethBibNewsConfig', ethBibNewsConfig)
        .factory('ethBibNewsService', ethBibNewsService)
        .controller('ethBibNewsController', ethBibNewsController)
        .component('ethBibNewsComponent',{
            bindings: {afterCtrl: '<'},
            controller: 'ethBibNewsController',
            template: ethBibNewsHtml
        })
