import {ethRapidoOfferModule} from './eth-rapido-offer/eth-rapido-offer.module';
import {slspIconLabelRapidoModule} from './slsp-icon-label-rapido/slsp-icon-label-rapido.module';
import {ethDisableRequestButtonModule} from './eth-disable-request-button/eth-disable-request-button.module';
import {ethRapidoEthmemberHintModule} from './eth-rapido-ethmember-hint/eth-rapido-ethmember-hint.module';
import {ethRapidoJournalHintModule} from './eth-rapido-journal-hint/eth-rapido-journal-hint.module';

export const prmServicePhysicalBestOfferAfterModule = angular
    .module('prmServicePhysicalBestOfferAfterModule', [])
        .component('prmServicePhysicalBestOfferAfter',  {
            bindings: {parentCtrl: '<'},
            template: `<eth-rapido-offer-component after-ctrl="$ctrl"></eth-rapido-offer-component>
            <slsp-icon-label-rapido-component after-ctrl="$ctrl"></slsp-icon-label-rapido-component>
            <eth-disable-request-button-component after-ctrl="$ctrl"></eth-disable-request-button-component>
            <eth-rapido-journal-hint-component after-ctrl="$ctrl"></eth-rapido-journal-hint-component>
            <eth-rapido-ethmember-hint-component after-ctrl="$ctrl"></eth-rapido-ethmember-hint-component>
            `
        });


prmServicePhysicalBestOfferAfterModule.requires.push(ethRapidoOfferModule.name);
prmServicePhysicalBestOfferAfterModule.requires.push(slspIconLabelRapidoModule.name);
prmServicePhysicalBestOfferAfterModule.requires.push(ethDisableRequestButtonModule.name);
prmServicePhysicalBestOfferAfterModule.requires.push(ethRapidoEthmemberHintModule.name);
prmServicePhysicalBestOfferAfterModule.requires.push(ethRapidoJournalHintModule.name);
