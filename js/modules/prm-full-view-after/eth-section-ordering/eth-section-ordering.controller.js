export class ethSectionOrderingController {

    constructor($scope) {
        this.$scope = $scope;
    }

    $onInit() {
        try {
            this.parentCtrl = this.afterCtrl.parentCtrl;
            this.$scope.$watch('this.$ctrl.parentCtrl.services', (newValue, oldValue, scope) => {
                if(!!newValue){
                    scope.$ctrl.orderSections(scope.$ctrl.parentCtrl.services);
                }
            }, true);
        } catch (e) {
            console.error("***ETH*** ethSectionOrderingController $onInit");
            console.error(e.message);
        }
    };

    orderSections(sections) {
        try {
            // remove virtual browse
            let filterResultVirtualBrowse = sections.filter((s, i) => {return s.serviceName === 'virtualBrowse';} );
            if(filterResultVirtualBrowse.length > 0){
                sections.splice(sections.indexOf(filterResultVirtualBrowse[0]), 1);
            }
        } catch (e) {
            console.error("***ETH*** ethSectionOrderingController orderSections");
            console.error(e.message);
        }
    }
}
ethSectionOrderingController.$inject = ['$scope'];
