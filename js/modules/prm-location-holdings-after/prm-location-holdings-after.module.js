import { slspOnSiteModule } from './slsp-on-site/slsp-on-site.module';

export const prmLocationHoldingsAfterModule = angular
    .module('prmLocationHoldingsAfterModule', [])
    .component('prmLocationHoldingsAfter', {
        bindings: { parentCtrl: '<' },
        template: `
        <slsp-on-site-component after-ctrl="$ctrl"></slsp-on-site-component>
		`
    });

prmLocationHoldingsAfterModule.requires.push(slspOnSiteModule.name);
