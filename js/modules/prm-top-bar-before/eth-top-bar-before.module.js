
import {ethGitHintModule} from './eth-git-hint/eth-git-hint.module';

export const ethTopBarBeforeModule = angular
    .module('ethTopBarBeforeModule', [])
        .component('prmTopBarBefore',  {
            template: `<eth-survey-link-component after-ctrl="$ctrl"></eth-survey-link-component>
            <eth-git-hint-component after-ctrl="$ctrl"></eth-git-hint-component>`
        });

ethTopBarBeforeModule.requires.push(ethGitHintModule.name);
