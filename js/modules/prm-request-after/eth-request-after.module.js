
import {ethRequestHintsModule} from './eth-request-hints/eth-request-hints.module';

export const ethRequestAfterModule = angular
    .module('ethRequestAfterModule', [])
        .component('prmRequestAfter',  {
            bindings: {parentCtrl: '<'},
            template: `<eth-request-hints-component after-ctrl="$ctrl"></eth-request-hints-component>`
        });

ethRequestAfterModule.requires.push(ethRequestHintsModule.name);
