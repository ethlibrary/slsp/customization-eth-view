export class ethChangeTabsController {
    $onInit() {
        setTimeout(function(){
            function triggerSearch(){
                setTimeout(function(){
                    document.querySelectorAll(".search-actions .button-confirm")[0].click();
                }, 100)
            }
            let tabs = document.querySelectorAll('md-select-menu [id^="select_option_"]');
            tabs.forEach(t => {
                if(!!t) {
                    t.onclick = function(){
                        triggerSearch();
                    }
                }
            })
        }, 200)
    }
}
